<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promotions extends CI_Controller {
        private $pagesize = 20;
	function __construct()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model('admin/Promotions_manage_data');
        $this->load->library('My_PHPMailer');
		$this->load->helper('emailsmstemplate');
	}
	public function create_promotions()
	{
		if($this->session->userdata("logged_in")){
			$data['promotion_type']=$this->Promotions_manage_data->get_all_promotion_type();
			$data["menu_flag"]="";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/promotions/create_promotions',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_all_promotion_quote(){
		if($this->session->userdata("logged_in")){
			$deal_value=$this->input->post('deal_val');
			$all_promotion_quote=$this->Promotions_manage_data->get_all_promotion_quote($deal_value);
			echo json_encode($all_promotion_quote);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function get_all_promotion_quote_variables(){
		if($this->session->userdata("logged_in")){
			$quote_selector_val=$this->input->post('quote_selector_val');
			$all_promotion_quote_addons=$this->Promotions_manage_data->get_all_promotion_quote_addons($quote_selector_val);
			$all_promotion_type_variables=$this->Promotions_manage_data->get_all_promotion_type_variables($quote_selector_val);
			echo json_encode(array($all_promotion_quote_addons,$all_promotion_type_variables));
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function save_promotions(){
		if($this->session->userdata("logged_in")){
			$buy_deal_value="";
			$buy_deal_type="";
			$get_deal_type="";
			$final_offer_value="";
            $promotion_at_check_out="";
			//print_r($this->input->post());exit;
			extract($this->input->post());
				//generate a promo_id
				$date = new DateTime();
				$mt=microtime();
				$salt="promo_id";
				$rand=mt_rand(1, 1000000);
				$ts=$date->getTimestamp();
				$str=$salt.$rand.$ts.$mt;
				$promo_uid=md5($str);
               if(is_array($buy_deal_value)){
                   //array_pop($buy_deal_value);
                   $buy_deal_value=implode(",",$buy_deal_value);
               }
               
               if(is_array($get_deal_value)){
                   //array_pop($get_deal_value);
                   $get_deal_value=implode(",",$get_deal_value);
               }

               //print_r($buy_deal_type);

               $qty_based_tiered=0;
               if(is_array($buy_deal_type)){
                    //$quote_selector=='7' is for tiered qty based new promo
                    if(in_array('Qty',$buy_deal_type) && $quote_selector=='7'){
                        $qty_based_tiered=1;
                   }else{
                        $qty_based_tiered= 0;
                   }
                   //array_pop($buy_deal_type);
                   $buy_deal_type=implode(",",$buy_deal_type);
               }

               //print_r($get_deal_type);
               //echo "qty_based_tiered=".$qty_based_tiered;
               //exit;

               if(is_array($get_deal_type)){
                   //array_pop($get_deal_type);
                   $get_deal_type=implode(",",$get_deal_type);
                   if($qty_based_tiered == 0){
                        $promotion_at_check_out=1;
                   }
               } 

                /////////////////////
			    if(!isset($buy_deal_value)){
					$buy_deal_value="";
			   }
			   if(!isset($get_deal_value)){
					$get_deal_value="";
			   }
			   if(!isset($buy_deal_type)){
					$buy_deal_type="";
			   }
			   if(!isset($get_deal_type)){
					$get_deal_type="";
			   }
				
			   ////////////////
			   
                $final_offer_value=htmlspecialchars($final_offer_value);
				
				$added_promo=$this->Promotions_manage_data->save_promotions($promo_uid,$deal_type,$quote_selector,$deal_name,$buy_deal_value,$get_deal_value,$buy_deal_type,$get_deal_type,$final_offer_value,$start_date,$end_date,$promotion_at_check_out,$promotion_dependency);
				$changes='<span class="change_to_log"><span class="change_head">Promotion created</span></span>';
              	$name=$this->session->userdata("email");
				$parts = explode("@", $name);
				$username = $parts[0];

				if($added_promo){
					$deal_type=$this->get_promo_type_name($deal_type);
					$final_offer_value='Deal Type: '.$deal_type.'<br>'.'Deal Name: '.$deal_name.'<br>'.
					'Deal Quote: '.$final_offer_value.'<br>'.'<span class="log_dates">Start Date: '.date("D,j M Y H:i", strtotime($start_date)).'<br>'.
					'End Date: '.date("D,j M Y H:i", strtotime($end_date)).'</span>';
					
					$promo_status='<span class="change_to_log"><b>Draft created</b></span>';
								
					
					$update_log=$this->Promotions_manage_data->update_promotion_log($promo_uid,$changes,$final_offer_value,$username,$promo_status);
					if($update_log){
						redirect(base_url().'admin/Promotions/manage_promo_items/'.$promo_uid);
					}
					else{
						redirect(base_url().'admin/Promotions/manage_promo_items/'.$promo_uid);
					}
				}
				else{
						redirect(base_url().'admin/Promotions/create_promotions');
					}
				
				
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function promotion_not_found(){
		if($this->session->userdata("logged_in")){
			echo 'Promotion Not Found <a href="'.base_url().'admin/Promotions/create_promotions">Create New Promotions</a>';
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
    
    public function get_quote_selector_value($quote_selector){
        return $this->Promotions_manage_data->get_quote_selector_value($quote_selector);
    }
	
	public function manage_promo_items($promo_uid)
	{
		if($this->session->userdata("logged_in")){
			$data['promo_data']=$this->Promotions_manage_data->get_promo_master_data($promo_uid);
			$data['controller']=$this;
			$data['promoitems_on_purchase']=$this->Promotions_manage_data->promoitems_on_purchase($promo_uid);
            $data['bundle_promoitems_on_purchase']=$this->Promotions_manage_data->bundle_promoitems_on_purchase($promo_uid);
			$data['promoitems_on_purchase_free']=$this->Promotions_manage_data->promoitems_on_purchase_free($promo_uid);
			$data['parent_category']=$this->Promotions_manage_data->get_all_parent_category();
            $data['su_admin_email']=$this->Promotions_manage_data->get_su_admin_email();
			$data['controller']=$this;
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/promotions/add_promo_items');
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
    public function get_parent_Category_name($pcat_id,$promo_uid,$id){
        return '<li> All Items Under parent Category '.$this->Promotions_manage_data->get_parent_Category_name($pcat_id).' <button class="material-button btn-xsm btn-danger" bundle_id="'.$id.'" onclick="delete_BundleItem(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></li>';
    }
    public function get_Category_name($cat_id,$promo_uid,$id){
        return '<li> All Items Under Category '.$this->Promotions_manage_data->get_Category_name($cat_id).' <button class="material-button btn-xsm btn-danger" bundle_id="'.$id.'" onclick="delete_BundleItem(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></li>';
    }
    public function get_Sub_Category_name($sub_cat_id,$promo_uid,$id){
        return '<li> All Items Under Sub Category '.$this->Promotions_manage_data->get_Sub_Category_name($sub_cat_id).' <button class="material-button btn-xsm btn-danger" bundle_id="'.$id.'" onclick="delete_BundleItem(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></li>';
    }
    public function get_Brand_name($brand_id,$promo_uid,$id){
        return '<li> All Items Under Brand '.$this->Promotions_manage_data->get_Brand_name($brand_id).'<button class="material-button btn-xsm btn-danger" bundle_id="'.$id.'" onclick="delete_BundleItem(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></li>';
    }
    
    public function get_Product_name($product_id,$promo_uid,$id){
         return '<li> All Items Under Product '.$this->Promotions_manage_data->get_Product_name($product_id).' <button class="material-button btn-xsm btn-danger" bundle_id="'.$id.'" onclick="delete_BundleItem(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></li>';
    }

    public function delete_bundle_item(){
        if($this->session->userdata("logged_in")){
            $bundle_id=$this->input->post('bundle_id');
            $promo_id=$this->input->post('promo_id');
            $deactivate_flag=$this->input->post('deactivate_flag');
            
            if($this->Promotions_manage_data->delete_bundle_item($bundle_id)){
                echo "1";
            }
            if($deactivate_flag==1){
                $changes_done='<span class="change_to_log">Promotion Deactivated.<span class="change_head">There are no items in this promotion.</span></span>';
                $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);  
            }
            $approval_flag=$this->input->post('approval_flag');
            if($approval_flag==1){
                $this->Promotions_manage_data->cancel_promotion_approval_request($promo_id);
                $changes_done='<span class="change_to_log">Request Canceled.<span class="change_head">There are no items in this promotion.</span></span>';
                $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);
            }
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
	
	public function getEndDateDepend($id){
		$data=$this->Promotions_manage_data->getEndDateDepend($id);
		if($data=="date"){
			return "readonly='readonly'";
		}
		else{
			return " ";
		}
	}
	
	public function get_promo_type_name($id){
		return $this->Promotions_manage_data->get_promo_type_name($id);
	}
	
	public function get_promoitems_on_purchase_list_free($product_id,$inv_id,$promo_uid,$quantity_def){
		$is_promo_editable=$this->Promotions_manage_data->get_is_promo_editable_in_promo_master_data($promo_uid);
		$get_product_name=$this->Promotions_manage_data->get_product_name($product_id);
		$data=$this->Promotions_manage_data->get_inv_attribute($inv_id,$promo_uid);
		$str='';
		$strb='';
		$strp="";
		if($quantity_def>1){
			$unit="units";
		}
		if($quantity_def<=1){
			$unit="unit";
		}

		foreach($data as $d){
					$input='<span id="'.$inv_id.'_free_edit_input_'.$promo_uid.'" style="display:none"><input type="number" value="" placeholder="enter new quantity" id="'.$inv_id.'_free_'.$promo_uid.'">&nbsp; <button class="material-button btn-xs btn-success" inv_id="'.$inv_id.'" promo_uid="'.$promo_uid.'" onclick="update_free_units_number(this)" data-tooltip="Update"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>&nbsp; <button class="material-button btn-xs btn-danger" inv_id="'.$d['id'].'" promo_id="'.$promo_uid.'" onclick="hide_quantity_span_edit(this)"><i class="fa fa-times" aria-hidden="true"></i></button><br><span class="help-block" id="'.$inv_id.'_free_error_'.$promo_uid.'"></span></span>';
			$str.=''.$d['sku_id'];
			$strp.=stripslashes($get_product_name);
			$strp.='&nbsp; ('.$d['attribute_1'].'=';
			$d['attribute_1_value']= substr($d['attribute_1_value'], 0, strpos($d['attribute_1_value'], ":"));
			
			$strp.=$d['attribute_1_value'].', ';
			$strp.=$d['attribute_2'].'=';
			$strp.=$d['attribute_2_value'].' )';
			
			$strb.='<td style="text-align: right;">'.$input.'<button class="material-button btn-warning"  id="'.$inv_id.'_free_edit_input_button_'.$promo_uid.'" inv_id="'.$d['id'].'" promo_id="'.$promo_uid.'"  onclick="show_quantity_span_edit(this)" data-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td><td style="text-align: center;"><button class="btn btn-xs btn-success free_button" id="'.$inv_id.'_free_disp_button_'.$promo_uid.'" quantity_def_free="'.$quantity_def.'">'.$quantity_def.' '.$unit.'</button></td><td> ';
			if($is_promo_editable=="no"){
				$strb.='<button class="material-button btn-danger" inv_id="'.$d['id'].'" promo_id="'.$promo_uid.'" onclick="delete_this_selected_item_free(this)" data-tooltip="Delete" disabled><i class="fa fa-trash" aria-hidden="true"></i></button>';
			}
			else{
				$strb.='<button class="material-button btn-danger" inv_id="'.$d['id'].'" promo_id="'.$promo_uid.'" onclick="delete_this_selected_item_free(this)" data-tooltip="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>';
			}
			
			$strb.='</td>';
			
			
		}


		return '<tr id="'.$inv_id.'_tr_line_'.$promo_uid.'"><td>'.$str." ".$strp.'</td>'.$strb.'</tr>';
	}

	public function update_promo_free_units_number(){
		if($this->session->userdata("logged_in")){
			$promo_id=$this->input->post('promo_id');
			$inv_id=$this->input->post('inv_id');
			$quant_val=$this->input->post('quant_val');
			$deactivate_flag=$this->input->post('deactivate_flag');
			$success=$this->Promotions_manage_data->update_promo_free_units_number($promo_id,$inv_id,$quant_val);
			if($success){
				echo "1";
			}
			
			if($deactivate_flag==1){
				$changes_done='<span class="change_to_log">Promotion Deactivated.<span class="change_head">Giveaway items is empty.</span></span>';
				$this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);	
				
			}
            $approval_flag=$this->input->post('approval_flag');
            if($approval_flag==1){
                $this->Promotions_manage_data->cancel_promotion_approval_request($promo_id);
                $changes_done='<span class="change_to_log">Request Canceled.<span class="change_head">Giveaway items is empty.</span></span>';
                $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);
            }
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_promoitems_on_purchase_list($product_id,$promo_uid){
		$get_product_name=$this->Promotions_manage_data->get_product_name($product_id);
		$get_product_list_in_product=$this->Promotions_manage_data->get_product_list_in_product_promo($product_id,$promo_uid);
		$count_total_promotion_for_buying=$this->Promotions_manage_data->count_total_promotion_for_buying($promo_uid);
		if(!empty($get_product_list_in_product)){
			$table='<table class="table table-hover"';
		
		$str="data-content='".$table;
		$str_ids='sku_id="';
		$i=0;
		foreach($get_product_list_in_product as $list_in_product){
			$arr=$this->get_inv_attribute($list_in_product['inv_id'],$promo_uid,$product_id);
			$str.=$arr[0];
			$str_ids.=$arr[1];
			$i++;
		}
		$str.="</table>'";
		$str_ids.='"';
		//echo  $str; exit;
		if($i>1){
			$i=$i." SKUs";
		}
		else{
			$i=$i." SKU";
		}
		$final_str=$get_product_name." ".'<button class="btn btn-success btn-xs productToPromotion" count_items_for_purchasing="'.$count_total_promotion_for_buying.'"  data-popover="true" data-html="true" '.$str.' > '.$i.'</button>';
		return $final_str;
		
		}
else{
	return $final_str="No Items Added";
}
		

		
	}
	
	public function get_inv_attribute($list_in_product,$promo_uid,$product_id){
		$is_promo_editable=$this->Promotions_manage_data->get_is_promo_editable_in_promo_master_data($promo_uid);
		$data=$this->Promotions_manage_data->get_inv_attribute($list_in_product);
		$str="";
		$str_id="";
		foreach($data as $d){
			$str.='<tr id="'.$list_in_product.'_tr_of_data_content_'.$promo_uid.'"><td>'.$d['sku_id'];
			$str.='('.$d['attribute_1'].'=';
			$str.=$d['attribute_1_value'].', ';
			$str.=$d['attribute_2'].'=';
			$str.=$d['attribute_2_value'].' )</td><td>'; 
			if($is_promo_editable=="no"){
				$str.='<button class="material-button btn-danger btn_for_del_tag_buying" inv_id="'.$d['id'].'" promo_id="'.$promo_uid.'"  product_id="'.$product_id.'" onclick="delete_this_selected_item(this)" data-tooltip="Delete" disabled><i class="fa fa-trash" aria-hidden="true"></i></button>';
			}
			else{
				$str.='<button class="material-button btn-danger btn_for_del_tag_buying" inv_id="'.$d['id'].'" promo_id="'.$promo_uid.'"  product_id="'.$product_id.'" onclick="delete_this_selected_item(this)" data-tooltip="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>';
			}
			
			$str.='</td></tr>';
			$str_id.=$d['id'].',';
		}
		
		return array($str,$str_id);
	}
	
	public function delete_this_promo_item_purchasing(){
		if($this->session->userdata("logged_in")){
			$promo_id=$this->input->post('promo_id');
			$inv_id=$this->input->post('inv_id');
			$product_id=$this->input->post('product_id');
			$deactivate_flag=$this->input->post('deactivate_flag');
            $approval_flag=$this->input->post('approval_flag');

			$success=$this->Promotions_manage_data->delete_this_promo_item_purchasing($promo_id,$inv_id);
			$count_total_promotion_for_buying=$this->Promotions_manage_data->count_total_promotion_for_buying($promo_id);
			$resultsarr=array();
			
			if($success){
				array_push($resultsarr,'1');
				$retData=$this->get_promoitems_on_purchase_list($product_id,$promo_id);
				if(empty($retData)){
					$retData="";
				}
				array_push($resultsarr,$retData,$count_total_promotion_for_buying);
				echo json_encode($resultsarr);
			}
			if($deactivate_flag==1){
				$changes_done='<span class="change_to_log">Promotion Deactivated.<span class="change_head">There are no items in this promotion.</span></span>';
				$this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);	
			}
			
            if($approval_flag==1){
                $this->Promotions_manage_data->cancel_promotion_approval_request($promo_id);
                $changes_done='<span class="change_to_log">Request Canceled.<span class="change_head">There are no items in this promotion.</span></span>';
                $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);
            }
		
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function delete_this_promo_item_purchasing_free(){
		if($this->session->userdata("logged_in")){
			$promo_id=$this->input->post('promo_id');
			$inv_id=$this->input->post('inv_id');
			$product_id=$this->input->post('product_id');
            $deactivate_flag=$this->input->post('deactivate_flag');
            $approval_flag=$this->input->post('approval_flag');
            if($deactivate_flag==1){
                $changes_done='<span class="change_to_log">Promotion Deactivated.<span class="change_head">Giveaway items is empty.</span></span>';
                $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);  
            }
                         
            if($approval_flag==1){
                $this->Promotions_manage_data->cancel_promotion_approval_request($promo_id);
                $changes_done='<span class="change_to_log">Request Canceled.<span class="change_head">Giveaway items is empty.</span></span>';
                $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);
            } 
			$success=$this->Promotions_manage_data->delete_this_promo_item_purchasing_free($promo_id,$inv_id);
			if($success){
				echo "1";
			}
             
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function add_items_tagged_for_purchasing()
		{
			if($this->session->userdata("logged_in")){
			    $products_tagged_for_purchasing="";
                $parent_category="";
                $category="";
                $sub_category="";
                $attributes="";
                $attributes_selector="";
                //print_r($this->input->post());exit;
				extract($this->input->post());
				$added=$this->Promotions_manage_data->add_items_tagged_for_purchasing($promo_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
				if($added){
					$this->session->set_flashdata('notification','Proceed To Step 2');
					redirect(base_url().'admin/Promotions/manage_promo_items/'.$promo_uid);
				}
				
			}
			else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
			}
		}
        
    public function getAllStoreRelatedProductDiscount(){
        if($this->session->userdata("logged_in")){
            $all_promotions_str="";
            $promo_uid=$this->input->post('promo_uid');
            $all_promotions=$this->Promotions_manage_data->getAllStoreRelatedProductDiscount($promo_uid);

            $level=1;$moq_valid=0;
            $inv_obj=$this->Promotions_manage_data->get_moq_of_invs($level,$promo_uid);
            
            //print_r($inv_obj);

            
            if(!empty($inv_obj)){
                $all_promotions_str.='<h4><b>Sorry! Cannot Proceed!</b><br></h4>';
                foreach($inv_obj as $item){
                    $moq_valid++;   
                    $all_promotions_str.=' Your promo qty is less than the stated (MOQ='.$item['moq'].') for this ';
                    $all_promotions_str.= 'SKU ID  : '.$item['sku_id'].'<br>';
                }
                $all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }else{
                //$all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }
           

            if(!empty($all_promotions)){


                $all_promotions_str.='<h4 class="modal-title" id="modal_title">Existing Promotions Running On Selected Products</h4>';

                $all_promotions_str.="<ul>";
                foreach($all_promotions as $data){
                    if($data['activated_once']==0){
                        
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                    }
                    if($data['activated_once']==1){
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                        if($data['promo_active']==1 && $data['promo_approval']==1 && $data['approval_sent']==1){
                        $promotion_status = "Active";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1 && $data['activated_once']==1)   {
                        $promotion_status = "Deactivated";
                        }
                    }
                    
                    
                   $all_promotions_str.='<li> <a target="_blank" href="'.base_url().'admin/Promotions/manage_promo_items/'.$data['promo_uid'].'">'. $data['promo_quote'].'</a> Status: '.$promotion_status.'<br>';
                   $all_promotions_str.=' <i><small>Promo Start Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_start_date'])) ;
                   $all_promotions_str.='</span> Promo End Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_end_date'])).'</span> </small></i></li>';
                }
                $all_promotions_str.="</ul>";
            }

            echo $all_promotions_str;
        }
        else
        {
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function getAllparent_categoryRelatedProductDiscount(){
        if($this->session->userdata("logged_in")){
            $all_promotions_str="";
            $promo_uid=$this->input->post('promo_uid');
            $parent_category=$this->input->post('parent_category');

            $level=2;$moq_valid=0;
            $inv_obj=$this->Promotions_manage_data->get_moq_of_invs($level,$promo_uid, $parent_category);
            
            //print_r($inv_obj);

            
            if(!empty($inv_obj)){
                $all_promotions_str.='<h4><b>Sorry! Cannot Proceed!</b><br></h4>';
                foreach($inv_obj as $item){
                    $moq_valid++;   
                    $all_promotions_str.=' Your promo qty is less than the stated (MOQ='.$item['moq'].') for this ';
                    $all_promotions_str.= 'SKU ID  : '.$item['sku_id'].'<br>';
                }
                $all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }else{
                //$all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }
           
            
            $all_promotions=$this->Promotions_manage_data->getAllparent_categoryRelatedProductDiscount($promo_uid,$parent_category);
            if(!empty($all_promotions)){
                $all_promotions_str.="<ul>";
                foreach($all_promotions as $data){
                    if($data['activated_once']==0){
                        
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                    }
                    if($data['activated_once']==1){
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                        if($data['promo_active']==1 && $data['promo_approval']==1 && $data['approval_sent']==1){
                        $promotion_status = "Active";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1 && $data['activated_once']==1)   {
                        $promotion_status = "Deactivated";
                        }
                    }
                   $all_promotions_str.='<li> <a target="_blank" href="'.base_url().'admin/Promotions/manage_promo_items/'.$data['promo_uid'].'">'. $data['promo_quote'].'</a> Status: '.$promotion_status.'<br>';
                   $all_promotions_str.=' <i><small>Promo Start Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_start_date'])) ;
                   $all_promotions_str.='</span> Promo End Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_end_date'])).'</span> </small></i></li>';
                }
                $all_promotions_str.="</ul>";
            }
            echo $all_promotions_str;
        }
        else
        {
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function getAllcategoryRelatedProductDiscount(){
        if($this->session->userdata("logged_in")){
            $all_promotions_str="";
            $promo_uid=$this->input->post('promo_uid');
            $parent_category=$this->input->post('parent_category');
            $category=$this->input->post('category');

            $level=3;$moq_valid=0;
            $inv_obj=$this->Promotions_manage_data->get_moq_of_invs($level,$promo_uid, $parent_category,$category);
            
            //print_r($inv_obj);

            
            if(!empty($inv_obj)){
                $all_promotions_str.='<h4><b>Sorry! Cannot Proceed!</b><br></h4>';
                foreach($inv_obj as $item){
                    $moq_valid++;   
                    $all_promotions_str.=' Your promo qty is less than the stated (MOQ='.$item['moq'].') for this ';
                    $all_promotions_str.= 'SKU ID  : '.$item['sku_id'].'<br>';
                }
                $all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }else{
                //$all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }


            $all_promotions=$this->Promotions_manage_data->getAllcategoryRelatedProductDiscount($promo_uid,$parent_category,$category);
            if(!empty($all_promotions)){
                $all_promotions_str.="<ul>";
                foreach($all_promotions as $data){
                    
                     if($data['activated_once']==0){
                        
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                    }
                    if($data['activated_once']==1){
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                        if($data['promo_active']==1 && $data['promo_approval']==1 && $data['approval_sent']==1){
                        $promotion_status = "Active";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1 && $data['activated_once']==1)   {
                        $promotion_status = "Deactivated";
                        }
                    }
                    
                   $all_promotions_str.='<li> <a target="_blank" href="'.base_url().'admin/Promotions/manage_promo_items/'.$data['promo_uid'].'">'. $data['promo_quote'].'</a> Status: '.$promotion_status.'<br>';
                   $all_promotions_str.=' <i><small>Promo Start Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_start_date'])) ;
                   $all_promotions_str.='</span> Promo End Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_end_date'])).'</span> </small></i></li>';
                }
                $all_promotions_str.="</ul>";
            }
            echo $all_promotions_str;
        }
        else
        {
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }  
    public function getAllsub_categoryRelatedProductDiscount(){
        if($this->session->userdata("logged_in")){
            $all_promotions_str="";
            $promo_uid=$this->input->post('promo_uid');
            $parent_category=$this->input->post('parent_category');
            $category=$this->input->post('category');
            $sub_category=$this->input->post('sub_category');

            $level=4;$moq_valid=0;
            $inv_obj=$this->Promotions_manage_data->get_moq_of_invs($level,$promo_uid, $parent_category,$category,$sub_category);
            
            //print_r($inv_obj);

            
            if(!empty($inv_obj)){
                $all_promotions_str.='<h4><b>Sorry! Cannot Proceed!</b><br></h4>';
                foreach($inv_obj as $item){
                    $moq_valid++;   
                    $all_promotions_str.=' Your promo qty is less than the stated (MOQ='.$item['moq'].') for this ';
                    $all_promotions_str.= 'SKU ID  : '.$item['sku_id'].'<br>';
                }
                $all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }else{
                //$all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }

            $all_promotions=$this->Promotions_manage_data->getAllsub_categoryRelatedProductDiscount($promo_uid,$parent_category,$category,$sub_category);
            if(!empty($all_promotions)){
                $all_promotions_str.="<ul>";
                foreach($all_promotions as $data){
                    
                     if($data['activated_once']==0){
                        
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                    }
                    if($data['activated_once']==1){
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                        if($data['promo_active']==1 && $data['promo_approval']==1 && $data['approval_sent']==1){
                        $promotion_status = "Active";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1 && $data['activated_once']==1)   {
                        $promotion_status = "Deactivated";
                        }
                    }
                    
                   $all_promotions_str.='<li> <a target="_blank" href="'.base_url().'admin/Promotions/manage_promo_items/'.$data['promo_uid'].'">'. $data['promo_quote'].'</a> Status: '.$promotion_status.'<br>';
                   $all_promotions_str.=' <i><small>Promo Start Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_start_date'])) ;
                   $all_promotions_str.='</span> Promo End Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_end_date'])).'</span> </small></i></li>';
                }
                $all_promotions_str.="</ul>";
            }
            echo $all_promotions_str;
        }
        else
        {
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    } 
    public function getAllattributesRelatedProductDiscount(){
        if($this->session->userdata("logged_in")){
            $all_promotions_str="";
            $promo_uid=$this->input->post('promo_uid');
            $parent_category=$this->input->post('parent_category');
            $category=$this->input->post('category');
            $sub_category=$this->input->post('sub_category');
            $attributes=$this->input->post('attributes');

            
            $level=5;$moq_valid=0;
            $inv_obj=$this->Promotions_manage_data->get_moq_of_invs($level,$promo_uid, $parent_category,$category,$sub_category,$attributes);
            
            //print_r($inv_obj);

            
            if(!empty($inv_obj)){
                $all_promotions_str.='<h4><b>Sorry! Cannot Proceed!</b><br></h4>';
                foreach($inv_obj as $item){
                    $moq_valid++;   
                    $all_promotions_str.=' Your promo qty is less than the stated (MOQ='.$item['moq'].') for this ';
                    $all_promotions_str.= 'SKU ID  : '.$item['sku_id'].'<br>';
                }
                $all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }else{
                //$all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }

            $all_promotions=$this->Promotions_manage_data->getAllattributesRelatedProductDiscount($promo_uid,$parent_category,$category,$sub_category,$attributes);
            if(!empty($all_promotions)){
                $all_promotions_str.="<ul>";
                foreach($all_promotions as $data){
                    if($data['activated_once']==0){
                        
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                    }
                    if($data['activated_once']==1){
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                        if($data['promo_active']==1 && $data['promo_approval']==1 && $data['approval_sent']==1){
                        $promotion_status = "Active";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1 && $data['activated_once']==1)   {
                        $promotion_status = "Deactivated";
                        }
                    }
                    
                    
                    
                   $all_promotions_str.='<li> <a target="_blank" href="'.base_url().'admin/Promotions/manage_promo_items/'.$data['promo_uid'].'">'. $data['promo_quote'].'</a> Status: '.$promotion_status.'<br>';
                   $all_promotions_str.=' <i><small>Promo Start Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_start_date'])) ;
                   $all_promotions_str.='</span> Promo End Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_end_date'])).'</span> </small></i></li>';
                }
                $all_promotions_str.="</ul>";
            }
            echo $all_promotions_str;
        }
        else
        {
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    } 
    public function getAllattributes_selectorRelatedProductDiscount(){
        if($this->session->userdata("logged_in")){
            $all_promotions_str="";
            $promo_uid=$this->input->post('promo_uid');
            $parent_category=$this->input->post('parent_category');
            $category=$this->input->post('category');
            $sub_category=$this->input->post('sub_category');
            $attributes=$this->input->post('attributes');
            $attributes_selector=$this->input->post('attributes_selector');

            $level=6;$moq_valid=0;
            $inv_obj=$this->Promotions_manage_data->get_moq_of_invs($level,$promo_uid, $parent_category,$category,$sub_category,$attributes,$attributes_selector);
            
            //print_r($inv_obj);

            
            if(!empty($inv_obj)){
                $all_promotions_str.='<h4><b>Sorry! Cannot Proceed!</b><br></h4>';
                foreach($inv_obj as $item){
                    $moq_valid++;   
                    $all_promotions_str.=' Your promo qty is less than the stated (MOQ='.$item['moq'].') for this ';
                    $all_promotions_str.= 'SKU ID  : '.$item['sku_id'].'<br>';
                }
                $all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }else{
                //$all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }

            $all_promotions=$this->Promotions_manage_data->getAllattributes_selectorRelatedProductDiscount($promo_uid,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            if(!empty($all_promotions)){
                $all_promotions_str.="<ul>";
                foreach($all_promotions as $data){
                    
                    if($data['activated_once']==0){
                        
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                    }
                    if($data['activated_once']==1){
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                        if($data['promo_active']==1 && $data['promo_approval']==1 && $data['approval_sent']==1){
                        $promotion_status = "Active";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1 && $data['activated_once']==1)   {
                        $promotion_status = "Deactivated";
                        }
                    }
                    
                    
                   $all_promotions_str.='<li> <a target="_blank" href="'.base_url().'admin/Promotions/manage_promo_items/'.$data['promo_uid'].'">'. $data['promo_quote'].'</a> Status: '.$promotion_status.'<br>';
                   $all_promotions_str.=' <i><small>Promo Start Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_start_date'])) ;
                   $all_promotions_str.='</span> Promo End Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_end_date'])).'</span> </small></i></li>';
                }
                $all_promotions_str.="</ul>";
            }
            echo $all_promotions_str;
        }
        else
        {
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    } 
    public function getAllproducts_tagged_for_purchasingRelatedProductDiscount(){
        if($this->session->userdata("logged_in")){
            $all_promotions_str="";
            $promo_uid=$this->input->post('promo_uid');
            $parent_category=$this->input->post('parent_category');
            $category=$this->input->post('category');
            $sub_category=$this->input->post('sub_category');
            $attributes=$this->input->post('attributes');
            $attributes_selector=$this->input->post('attributes_selector');
            $products_tagged_for_purchasing=$this->input->post('products_tagged_for_purchasing');

            $level=7;$moq_valid=0;
            $inv_obj=$this->Promotions_manage_data->get_moq_of_invs($level,$promo_uid,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$products_tagged_for_purchasing);
            
            //print_r($inv_obj);

            
            if(!empty($inv_obj)){
                $all_promotions_str.='<h4><b>Sorry! Cannot Proceed!</b><br></h4>';
                foreach($inv_obj as $item){
                    $moq_valid++;   
                    $all_promotions_str.=' Your promo qty is less than the stated (MOQ='.$item['moq'].') for this ';
                    $all_promotions_str.= 'SKU ID  : '.$item['sku_id'].'<br>';
                }
                $all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }else{
                //$all_promotions_str.= '<input type="hidden" id="moq_valid" value="'.$moq_valid.'">';
            
            }
           

            $all_promotions=$this->Promotions_manage_data->getAllproducts_tagged_for_purchasingRelatedProductDiscount($promo_uid,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$products_tagged_for_purchasing);
            if(!empty($all_promotions)){
                $all_promotions_str="<ul type='none' style='padding-left: 0px;'>";
                foreach($all_promotions as $inventory_id => $data_arr){
                    $all_promotions_str.='<li><ul style="list-style-type:decimal;">';
                    $all_promotions_str.="<u>For Inventory -> <b>".$this->get_inv_attribute_data($inventory_id)."</b></u>";
                    foreach($data_arr as $data){
                        
                     if($data['activated_once']==0){
                        
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                    }
                    if($data['activated_once']==1){
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1){
                            $promotion_status = "Approved";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==1){
                            $promotion_status = "Approval Sent";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==0 && $data['approval_sent']==0){
                            $promotion_status = "Draft";
                        }
                        if($data['promo_active']==1 && $data['promo_approval']==1 && $data['approval_sent']==1){
                        $promotion_status = "Active";
                        }
                        if($data['promo_active']==0 && $data['promo_approval']==1 && $data['approval_sent']==1 && $data['activated_once']==1)   {
                        $promotion_status = "Deactivated";
                        }
                    }
                        
                        
                   $all_promotions_str.='<li> <a target="_blank" href="'.base_url().'admin/Promotions/manage_promo_items/'.$data['promo_uid'].'">'. $data['promo_quote'].'</a> Status: '.$promotion_status.'<br>';
                   $all_promotions_str.=' <i><small>Promo Start Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_start_date'])) ;
                   $all_promotions_str.='</span> Promo End Date: <span style="color:red">'.date("D,j M Y H:i", strtotime($data['promo_end_date'])).'</span> </small></i></li>';
                   }
                    $all_promotions_str.='</ul></li>';
                }
                 $all_promotions_str.="</ul>";
            }
            echo $all_promotions_str;
        }
        else
        {
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }

    public function get_inv_attribute_data($inventory_id){
        $invdata=$this->Promotions_manage_data->get_inv_attribute($inventory_id);
        $invs="";
        if(!empty($invdata)){
            foreach($invdata as $data){
                $invs.=$data['sku_id'].'(';
                if($data['attribute_1']!=""){
                    $invs.=$data['attribute_1'].' : ';
                    $invs.=$data['attribute_1_value'];
                }
                if($data['attribute_2']!=""){
                    $invs.=" | ".$data['attribute_2'].' : ';
                    $invs.=$data['attribute_2_value'];
                }
                if($data['attribute_3']!=""){
                    $invs.=" | ".$data['attribute_3'].' : ';
                    $invs.=$data['attribute_3_value'];
                }
                if($data['attribute_4']!=""){
                    $invs.=" | ".$data['attribute_4'].' : ';
                    $invs.=$data['attribute_4_value'];
                }
                $invs.=')';
            }
        }
        return $invs;
    }     
		
	public function add_items_tagged_for_purchasing_free(){
		if($this->session->userdata("logged_in")){
			extract($this->input->post());
				$added=$this->Promotions_manage_data->add_items_tagged_for_purchasing_free($promo_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);;
				if($added){
					$this->session->set_flashdata('notification','Update Giveaway Units');
					redirect(base_url().'admin/Promotions/manage_promo_items/'.$promo_uid);
				}
			}
			else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
			}
	}

    public function get_category(){
        if($this->session->userdata("logged_in")){
            $pcat_id=$this->input->post('pcat_id');
            $all_sub_category=$this->Promotions_manage_data->get_all_category($pcat_id);
            echo json_encode($all_sub_category);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
	public function get_sub_catageory(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post('cat_id');
			$all_sub_category=$this->Promotions_manage_data->get_all_sub_category($cat_id);
			echo json_encode($all_sub_category);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function get_all_brand(){
		if($this->session->userdata("logged_in")){
			$sub_cat_val=$this->input->post('sub_cat_val');
			$all_brand=$this->Promotions_manage_data->get_all_brand($sub_cat_val);
			echo json_encode($all_brand);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_all_attributes(){
		if($this->session->userdata("logged_in")){
			$brands_val=$this->input->post('brands_val');
			$all_attributes=$this->Promotions_manage_data->get_all_attributes($brands_val);
			echo json_encode($all_attributes);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_all_items_in_type(){
		if($this->session->userdata("logged_in")){
			$product_val=$this->input->post('product_val');
			$all_items_in_type=$this->Promotions_manage_data->get_all_items_in_type($product_val);
			echo json_encode($all_items_in_type);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_all_items_in_type_filter(){
		if($this->session->userdata("logged_in")){
			$product_val=$this->input->post('product_val');
			$type_filter=$this->Promotions_manage_data->get_all_items_in_type_filter($product_val);
			echo json_encode($type_filter);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_all_items_after_filter(){
		if($this->session->userdata("logged_in")){
			$color_val=$this->input->post('color_val');
			$size_val=$this->input->post('size_val');
			$product_val=$this->input->post('product_val');
			$type_filter=$this->Promotions_manage_data->get_all_items_after_filter($color_val,$size_val,$product_val);
			echo json_encode($type_filter);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function update_promotion_deal_name(){
		$deal_name=$this->input->post('new_Deal_Name');
		$promo_id=$this->input->post('promo_id');
		$deactivate_flag=$this->input->post('deactivate_flag');
		$success=$this->Promotions_manage_data->update_promotion_deal_name($promo_id,$deal_name);
		if($deactivate_flag==1){
			$changes_done='<span class="change_to_log">Deactivated.<span class="change_head">Deal name changed</span></span>';
			$this->save_as_draft_promotion_ajax($promo_id,$changes_done);	
		}
        $approval_flag=$this->input->post('approval_flag');
            if($approval_flag==1){
                $this->Promotions_manage_data->cancel_promotion_approval_request($promo_id);
                $changes_done='<span class="change_to_log">Request Canceled.<span class="change_head">Deal name changed</span></span>';
                $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);
            }
		$getpromodata=$this->Promotions_manage_data->getcurrentpromodata($promo_id);
		
		$name=$this->session->userdata("email");
		$parts = explode("@", $name);
		$username = $parts[0];
		$changes='<span class="change_to_log"><span class="change_head">Deal Name</span> Edited to <span class="change_data">&#145;'.$deal_name.'&#146;</span></span>';
		if(!empty($getpromodata)){
			foreach ($getpromodata as $data){


				if($data['promo_start_date']!=""){
					$start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
				}
				else{
					$start_date="";
				}
				if($data['promo_end_date']!=""){
					$end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
				}
				else{
					$end_date="";
				}


				
				$deal_type=$this->get_promo_type_name($data['promo_type']);
				$final_offer_value='Deal Type: '.$deal_type.'<br>'.'Deal Name: '.$data['promo_name'].'<br>'.
				'Deal Quote: '.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
				'End Date: '.$end_date.'</span>';
				$promo_status='<span class="change_to_log"><b>Saved As Draft</b></span>';
			}
		}
		
		
					
		$update_log=$this->Promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
		

		
		if($success){
			echo "1";
		}
		
	}
	public function update_promotion_deal_quote(){
		$buy_deal_value="";
		$get_deal_value="";
		$get_deal_type="";
		$buy_deal_type="";
        $promotion_at_check_out="";
		
		$new_deal_quote=$this->input->post('new_deal_quote');
		$buy_deal_value=$this->input->post('buy_deal_value');
		$get_deal_value=$this->input->post('get_deal_value');
        $promotion_at_check_out=$this->input->post('promotion_at_check_out');

        $new_deal_quote=htmlspecialchars($new_deal_quote);

		$start_date=$this->input->post('start_date');
		$endDate=$this->input->post('endDate');
		$promo_id=$this->input->post('promo_id');
		

		
		if($get_deal_type==curr_code){
			$this->Promotions_manage_data->clear_promotion_items_taged_for_purchasing_free($promo_id);
		}
		
		if($buy_deal_value==""||$buy_deal_value==0){
			$this->Promotions_manage_data->clear_promotion_items_taged_for_purchasing($promo_id);
		}
		
		if($get_deal_value==""||$get_deal_value==0){
			$this->Promotions_manage_data->clear_promotion_items_taged_for_purchasing_free($promo_id);
		}
        if($promotion_at_check_out==1){
            $this->Promotions_manage_data->clear_promotion_items_taged_for_purchasing($promo_id);
            $this->Promotions_manage_data->clear_promotion_items_taged_for_purchasing_free($promo_id);
        }
		
		$deactivate_flag=$this->input->post('deactivate_flag');

		$buy_deal_type=$this->input->post('buy_deal_type');
		$get_deal_type=$this->input->post('get_deal_type');
        
        if($deactivate_flag==1){
            $changes_done='<span class="change_to_log">Deactivated. <span class="change_head">Deal quote changed</span></span>';
            $this->save_as_draft_promotion_ajax($promo_id,$changes_done);   
        }
        $approval_flag=$this->input->post('approval_flag');
            if($approval_flag==1){
                $this->Promotions_manage_data->cancel_promotion_approval_request($promo_id);
                $changes_done='<span class="change_to_log">Request Canceled.<span class="change_head">Deal quote changed</span></span>';
                $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);
            }

		$success=$this->Promotions_manage_data->update_promotion_deal_quote($promo_id,$new_deal_quote,$buy_deal_value,$get_deal_value,$buy_deal_type,$get_deal_type,$endDate,$start_date,$promotion_at_check_out);
		
		$getpromodata=$this->Promotions_manage_data->getcurrentpromodata($promo_id);
		
		$name=$this->session->userdata("email");
		$parts = explode("@", $name);
		$username = $parts[0];
        $changes='<span class="change_to_log"><span class="change_head">Deal Quote</span> Edited to <span class="change_data">&#145;'.$new_deal_quote.'&#146;</span></span>';
		if(!empty($getpromodata)){
			foreach ($getpromodata as $data){
				$status=$data['promo_active'];
				if($status==1){
					$status="Active";
				}
				else{
					$status="Deactive";
				}
				if($data['promo_start_date']!=""){
					$start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
				}
				else{
					$start_date="";
				}
				if($data['promo_end_date']!=""){
					$end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
				}
				else{
					$end_date="";
				}
				$promo_active_status="";
				$promo_approval_status="";
				$promo_approval_sent="";
				if($data['promo_active']==1){
					$promo_active_status.='<b>Active</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				else{
					$promo_active_status.='<b>Deactive</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				
				if($data['promo_approval']==1){
					$promo_approval_status.='<b>Approved</b>  '.$data['approved_by'].' since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				else{
					$promo_approval_status.='<b>Unapproved</b>  since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				
				if($data['approval_sent']==0){
					$promo_approval_sent.='<b>Not Sent</b>'; 
				}
				else{
					$promo_approval_sent.='<b>Sent</b> By '.$username.' on '.date("D,j M Y H:i", strtotime($data['time_approval_sent'])); 
				}
				
				$deal_type=$this->get_promo_type_name($data['promo_type']);
				$final_offer_value='Deal Type: '.$deal_type.'<br>'.'<b>Deal Name:</b> '.$data['promo_name'].'<br>'.
				'<b>Deal Quote:</b> '.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
				'End Date: '.$end_date.'</span>';
                if($approval_flag==1){
                     $promo_status='<span class="change_to_log"><b>Edited in Active Mode. Changed to Draft</b></span>';
                }
                if($deactivate_flag==1){
                     $promo_status='<span class="change_to_log"><b>Edited in Approval Mode. Changed to Draft</b></span>';
                }
                if($deactivate_flag==0 && $approval_flag==0){
                    $promo_status='<span class="change_to_log"><b>Draft modified</b></span>';
                }
				
			}
		}
		
		
					
		$update_log=$this->Promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
		
		
		
		if($success){
			echo "1";
		}
	}
	public function update_promotion_end_date_date(){
		$end_date=$this->input->post('end_date');
		$promo_id=$this->input->post('promo_id');
		$deactivate_flag=$this->input->post('deactivate_flag');
        
        if($deactivate_flag==1){
            $changes_done='<span class="change_to_log">Deactivated. <span class="change_head">End date modified</span></span>';
            $this->save_as_draft_promotion_ajax($promo_id,$changes_done);   
        }
        $approval_flag=$this->input->post('approval_flag');
            if($approval_flag==1){
                $this->Promotions_manage_data->cancel_promotion_approval_request($promo_id);
                $changes_done='<span class="change_to_log">Request Canceled.<span class="change_head">End date modified</span></span>';
                $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);
         }
        
		$success=$this->Promotions_manage_data->update_promotion_end_date_date($promo_id,$end_date);
		$getpromodata=$this->Promotions_manage_data->getcurrentpromodata($promo_id);
		
		$name=$this->session->userdata("email");
		$parts = explode("@", $name);
		$username = $parts[0];
		$changes='<span class="change_to_log"><span class="change_head">End date</span> edited to<span class="change_data">&#145;'.$end_date.'&#146;</span></span>';
		if(!empty($getpromodata)){
			foreach ($getpromodata as $data){
				$status=$data['promo_active'];
				if($status==1){
					$status="Active";
				}
				else{
					$status="Deactive";
				}
				if($data['promo_start_date']!=""){
					$start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
				}
				else{
					$start_date="";
				}
				if($data['promo_end_date']!=""){
					$end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
				}
				else{
					$end_date="";
				}
				$promo_active_status="";
				$promo_approval_status="";
				$promo_approval_sent="";
				if($data['promo_active']==1){
					$promo_active_status.='<b>Active</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				else{
					$promo_active_status.='<b>Deactive</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				
				if($data['promo_approval']==1){
					$promo_approval_status.='<b>Approved</b>  '.$data['approved_by'].' since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				else{
					$promo_approval_status.='<b>Unapproved</b>  since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				
				if($data['approval_sent']==0){
					$promo_approval_sent.='<b>Not Sent</b>'; 
				}
				else{
					$promo_approval_sent.='<b>Sent</b> By '.$username.' on '.date("D,j M Y H:i", strtotime($data['time_approval_sent'])); 
				}
				
				$deal_type=$this->get_promo_type_name($data['promo_type']);
				$final_offer_value='Deal Type: '.$deal_type.'<br>'.'Deal Name: '.$data['promo_name'].'<br>'.
				'Deal Quote: '.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
				'End Date: '.$end_date.'</span>';
				if($approval_flag==1){
                     $promo_status='<span class="change_to_log"><b>Edited in Active Mode. Changed to Draft</b></span>';
                }
                if($deactivate_flag==1){
                     $promo_status='<span class="change_to_log"><b>Edited in Approval Mode. Changed to Draft</b></span>';
                }
                if($deactivate_flag==0 && $approval_flag==0){
                    $promo_status='<span class="change_to_log"><b>Draft modified</b></span>';
                }
			}
		}
		
		
					
		$update_log=$this->Promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
		
		if($success){
			echo "1";
		}
	}
	public function update_promotion_start_date(){
		$start_date=$start_date=$this->input->post('start_date');
		$promo_id=$this->input->post('promo_id');
		$end_date=$this->input->post('end_date');
        $deactivate_flag=$this->input->post('deactivate_flag');
        if($deactivate_flag==1){
            $changes_done='<span class="change_to_log">Deactivated.<span class="change_head">Start date modified</span></span>';
            $this->save_as_draft_promotion_ajax($promo_id,$changes_done);   
        }
        $approval_flag=$this->input->post('approval_flag');
        if($approval_flag==1){
            $this->Promotions_manage_data->cancel_promotion_approval_request($promo_id);
            $changes_done='<span class="change_to_log">Request Canceled.<span class="change_head">Start date modified</span></span>';
            $this->save_as_draft_promotion_ajax_temp($promo_id,$changes_done);
        }
		$success=$this->Promotions_manage_data->update_promotion_start_date($promo_id,$start_date,$end_date);

		$getpromodata=$this->Promotions_manage_data->getcurrentpromodata($promo_id);
		
		$name=$this->session->userdata("email");
		$parts = explode("@", $name);
		$username = $parts[0];
		$changes='<span class="change_to_log"><span class="change_head">Start Date</span> Edited to<span class="change_data">&#145;'.$start_date.'&#146;</span></span>';
		if(!empty($getpromodata)){
			foreach ($getpromodata as $data){
				$status=$data['promo_active'];
				if($status==1){
					$status="Active";
				}
				else{
					$status="Deactive";
				}
				if($data['promo_start_date']!=""){
					$start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
				}
				else{
					$start_date="";
				}
				if($data['promo_end_date']!=""){
					$end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
				}
				else{
					$end_date="";
				}
				$promo_active_status="";
				$promo_approval_status="";
				$promo_approval_sent="";
				if($data['promo_active']==1){
					$promo_active_status.='<b>Active</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				else{
					$promo_active_status.='<b>Deactive</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				
				if($data['promo_approval']==1){
					$promo_approval_status.='<b>Approved</b>  '.$data['approved_by'].' since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				else{
					$promo_approval_status.='<b>Unapproved</b>  since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				
				if($data['approval_sent']==0){
					$promo_approval_sent.='<b>Not Sent</b>'; 
				}
				else{
					$promo_approval_sent.='<b>Sent</b> By '.$username.' on '.date("D,j M Y H:i", strtotime($data['time_approval_sent'])); 
				}
				
				$deal_type=$this->get_promo_type_name($data['promo_type']);
				$final_offer_value='Deal Type: '.$deal_type.'<br>'.'Deal Name: '.$data['promo_name'].'<br>'.
				'Deal Quote: '.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
				'End Date: '.$end_date.'</span>';
				if($approval_flag==1){
                     $promo_status='<span class="change_to_log"><b>Edited in Active Mode. Changed to Draft</b></span>';
                }
                if($deactivate_flag==1){
                     $promo_status='<span class="change_to_log"><b>Edited in Approval Mode. Changed to Draft</b></span>';
                }
                if($deactivate_flag==0 && $approval_flag==0){
                    $promo_status='<span class="change_to_log"><b>Draft modified</b></span>';
                }
			}
		}
		
		
					
		$update_log=$this->Promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
		
		if($success){
			echo "1";
		}
	}

	public function send_approval_for_promotion(){
	    
        $promo_id=$promo_id=$this->input->post('promo_id');
        $su_email=$this->input->post('su_email');
        
        $date = new DateTime();
        $mt=microtime();
        $salt="promo_id";
        $rand=mt_rand(1, 1000000);
        $ts=$date->getTimestamp();
        $str=$salt.$rand.$ts.$mt;
        $temp_email_num=md5($str);
        
        
        
        $default_su_email=$this->Promotions_manage_data->get_su_admin_email();
        
        
        $success=$this->Promotions_manage_data->send_approval_for_promotion($promo_id,$temp_email_num,$su_email);
        
        $getpromodata=$this->Promotions_manage_data->getcurrentpromodata($promo_id);
        
        $name=$this->session->userdata("email");
        $parts = explode("@", $name);
        $username = $parts[0];
        if($default_su_email==$su_email){
            $changes='<span class="change_to_log">Sent for <span class="change_head">approval</span> by  <span class="change_data">&#145;'.$username.'&#146;</span></span>';
        }
       if($default_su_email!=$su_email){
            $changes='<span class="change_to_log">Sent for <span class="change_head">approval</span> by  <span class="change_data">&#145;'.$username.'&#146;</span> to '.$su_email.'</span>';
        }
        

		if(!empty($getpromodata)){
			foreach ($getpromodata as $data){
                $promo_name=$data['promo_name'];
                $promo_statement=$data['promo_quote'];
				if($data['promo_start_date']!=""){
					$start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
				}
				else{
					$start_date="";
				}
				if($data['promo_end_date']!=""){
					$end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
				}
				else{
					$end_date="";
				}
				$promo_status='<span class="change_to_log"><b>sent for Approval</b></span>'; 

				$deal_type=$this->get_promo_type_name($data['promo_type']);
				$final_offer_value='Deal Type: '.$deal_type.'<br>'.'<b> Deal Name: </b>'.$data['promo_name'].'<br>'.
				'<b>Deal Quote: </b>'.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
				'End Date: '.$end_date.'</span>';
			}
		}
		
		
					
		$update_log=$this->Promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
		
        $email_stuff_arr=email_stuff();
        $mail_body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="format-detection" content="telephone=no" />
        <title>'.$email_stuff_arr["name_emailtemplate"].'</title>
        <style type="text/css">
            body {
                font-family: Helvetica, Arial, sans-serif;
                color: rgb(95, 95, 95);
                font-size: 15px;
            }
        </style>
    </head>
    <body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
	
        <center style="background-color: #e1e1e1;">
		<!-- Start of preheader -->

<!-- End of preheader --> 
            <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer">
                <tr>
                    <table style="max-width: 800px; border-left: solid 1px #e6e6e6; border-right: solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" width="500" style="padding: 0px;">
                                                    <table border="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                          
						 
                           
                            <tr>
                                <td>
                                   
<table width="100%"  bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
   <tbody>
    <tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi '.$su_email.', </p><br><p style="padding:0;margin:0;color:grey;line-height:22px;font-size: 16px;"> The below offer needs your approvals to proceed. Kindly provide your response by clicking the appropriate button</p>
                           
                         </td>
						 <td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;background-color:#f9f9f9;padding:20px;" align="center" bgcolor="#F9F9F9" valign="top"> <h4 style="padding:0;margin:0;font-size:20px;font-weight:bold;"> Offer Deal </h4>
						
                           
                         </td></tr>
      <tr>
         <td>
            <table width="800" align="center"   cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
				   
                     <td width="100%">
						<hr width="70%" align="center" color="#000">
                        <table   width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              
                              <!-- row data row -->
                              <tr>
                                 <td>
                                    <!-- row data -->
                                    <table  width="350" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="175" height="43" align="center" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;">
                                                <div class="imgpop" >
                                                  Deal Type:
                                                </div>
                                             </td>

                                             <td width="175" height="43" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;" >
                                                <div class="imgpop">
                                                    '.$deal_type.'
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of row data -->
                                 </td>
                              </tr>
                              <!-- end of row data row -->
                              <!-- row data row -->
                              <tr>
                                 <td>
                                    <!-- row data -->
                                    <table  width="350" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="175" height="43" align="center" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;">
                                                <div class="imgpop" >
                                                  Promo Name
                                                </div>
                                             </td>

                                             <td width="175" height="43" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;">
                                                <div class="imgpop" >
                                                    '.$promo_name.'
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of row data -->
                                 </td>
                              </tr>
                              <!-- end of row data row -->
                                                            <!-- row data row -->
                              <tr>
                                 <td>
                                    <!-- row data -->
                                    <table  width="350" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="175" height="43" align="center" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;">
                                                <div class="imgpop" >
                                                  Promo Statement
                                                </div>
                                             </td>

                                             <td width="175" height="43" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;">
                                                <div class="imgpop" >
                                                    '.htmlspecialchars_decode($promo_statement).'
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of row data -->
                                 </td>
                              </tr>
                              <!-- end of row data row -->
                               <!-- row data row -->
                              <tr>
                                 <td>
                                    <!-- row data -->
                                    <table  width="350" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="175" height="43" align="center" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;">
                                                <div class="imgpop">
                                                  Start Date
                                                </div>
                                             </td>

                                             <td width="175" height="43" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;">
                                                <div class="imgpop">
                                                    '.$start_date.'
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of row data -->
                                 </td>
                              </tr>
                              <!-- end of row data row -->
                              <!-- row data row -->
                              <tr>
                                 <td>
                                    <!-- row data -->
                                    <table  width="350" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="175" height="43" align="center" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;">
                                                <div class="imgpop" >
                                                  End Date
                                                </div>
                                             </td>

                                             <td width="175" height="43" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:left; line-height: 24px;">
                                                <div class="imgpop">
                                                    '.$end_date.'
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of row data -->
                                 </td>
                              </tr>
                              <!-- end of row data row -->
                              <tr>
                                 <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
					 
                  </tr>
				  
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- Start of seperator -->  
<table align="center" width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="footer">
   <tbody>
      <tr>
	   
 

         <td>
            <table width="800"  cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table  width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                             <tr>
                                 <!-- Social icons -->
                                    <table  width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="43" height="43" align="center">
                                                <div class="imgpop">
                                                  <a href="'.base_url().'suadmin/Su_admin/email_promotion_update/'.$temp_email_num.'/'.$promo_id.'/accept"><button style="background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;">Approve</button></a>
                                                </div>
                                             </td>
                                             <td align="left" width="20" style="font-size:1px; line-height:1px;"><span style="display:inline-block;width:20px"></span></td>
                                             <td width="43" height="43" align="center">
                                                <div class="imgpop">
                                                    <a href="'.base_url().'suadmin/Su_admin/email_promotion_update/'.$temp_email_num.'/'.$promo_id.'/hold"><button style="background-color: #e6b800; /* Yellow */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;">Hold</button></a>
                                                </div>
                                             </td>
                                            <td align="left" width="20" style="font-size:1px; line-height:1px;"><span style="display:inline-block;width:20px"></span></td>
                                             <td width="43" height="43" align="center">
                                                <div class="imgpop">
                                                   <a href="'.base_url().'suadmin/Su_admin/email_promotion_update/'.$temp_email_num.'/'.$promo_id.'/reject"><button style="background-color: #e60000; /* Red */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;">Reject</button></a>
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of Social icons -->
                              </tr>
                          
                              <tr>
                                 <td height="30" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                            
                              <tr>
                                <td> <hr width="70%" align="center" color="#000"></td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
				
               </tbody>
            </table>
         </td>
		 
      </tr>
	  <tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;font-size: 15px;"><br/>Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p>
                           
                         </td>
						</tr>

   </tbody>
</table>
<!-- Start of seperator -->  

                                </td>
                            </tr>
							
							
							
							
							
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer" style="background-color: #2e2e2e;">
                                        <tr>
                                            <td align="center" valign="top" width="500" class="flexibleContainerCell" style="padding: 0px;">
                                                <table border="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" bgcolor="#2e2e2e">
                                                            <div style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #fff; text-align: center; line-height: 140%;">
                                                                <p>
                                                                    <span style="color: #fff;">'.$email_stuff_arr["footercopyrights_emailtemplate"].'</span><br />
                                                                    <span style="color: orange;">via Flamingo E-commerce from Axonlabs </span>
                                                                </p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </tr>
            </table>
			
        </center>
    </body>
</html>
';

   $mail = new PHPMailer;
        $mail = new PHPMailer;
        /*$mail->isSMTP();
        $mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
        $mail->Port = '587';//  live - comment it 
        $mail->Auth = true;//  live - comment it 
        $mail->SMTPAuth = true;
        $mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
        $mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
        $mail->SMTPSecure = 'tls';*/
        $mail->From = $email_stuff_arr["fromemail_emailtemplate"];
        $mail->FromName = $email_stuff_arr["name_emailtemplate"];

        $mail->addAddress($su_email, 'Rajit Kumar');

        $mail->isHTML(true);
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        /*$filepath="assets/pictures/invoices/Invoice_".$order_id.$order_item_id.".pdf";*/
    
        //$mail->Subject = $email_stuff_arr["fromemail_emailtemplate"].' is seeking your approval for promotions/offer!';
        $mail->Subject = SITE_NAME.' is seeking your approval for promotions/offer!';
        $mail->Body    = $mail_body;
        /*$mail->AddAttachment($filepath);*/
        if($name != '')
        {       
            if(!$mail->send()){ echo "Not send ".$mail->ErrorInfo." ".$name;}
        }
		
		if($success){
			echo "1";
		}
	}
	
	public function activate_promotion(){
		$promo_id=$promo_id=$this->input->post('promo_id');
		$success=$this->Promotions_manage_data->activate_promotion($promo_id);
		
		$getpromodata=$this->Promotions_manage_data->getcurrentpromodata($promo_id);
		
		$name=$this->session->userdata("email");
		$parts = explode("@", $name);
		$username = $parts[0];
		$changes='<span class="change_to_log">Promotion activated<span class="change_head">by '.$username.'</span></span>';
		if(!empty($getpromodata)){
			foreach ($getpromodata as $data){
				$status=$data['promo_active'];
				if($status==1){
					$status="Active";
				}
				else{
					$status="Deactive";
				}
				if($data['promo_start_date']!=""){
					$start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
				}
				else{
					$start_date="";
				}
				if($data['promo_end_date']!=""){
					$end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
				}
				else{
					$end_date="";
				}
				$promo_active_status="";
				$promo_approval_status="";
				$promo_approval_sent="";
				if($data['promo_active']==1){
					$promo_active_status.='<b>Active</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				else{
					$promo_active_status.='<b>Deactive</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				
				if($data['promo_approval']==1){
					$promo_approval_status.='<b>Approved</b>  '.$data['approved_by'].' since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				else{
					$promo_approval_status.='<b>Unapproved</b>  since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				
				if($data['approval_sent']==0){
					$promo_approval_sent.='<b>Not Sent</b>'; 
				}
				else{
					$promo_approval_sent.='<b>Sent</b> By '.$username.' on '.date("D,j M Y H:i", strtotime($data['time_approval_sent'])); 
				}
				
				$deal_type=$this->get_promo_type_name($data['promo_type']);
				$final_offer_value='Deal Type: '.$deal_type.'<br>'.'Deal Name: '.$data['promo_name'].'<br>'.
				'Deal Quote: '.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
				'End Date: '.$end_date.'</span>';
				$promo_status='<span class="change_to_log"><b>Activated</b></span>';
			}
		}
		
		
					
		$update_log=$this->Promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
		
		
		if($success){
			echo "1";
		}
	}

	public function save_as_draft_promotion_ajax($promo_id="",$changes_done=""){
		if($this->input->post('promo_id')){
			$promo_id=$promo_id=$this->input->post('promo_id');
		}

		$success=$this->Promotions_manage_data->save_as_draft_promotion($promo_id);
		
		$getpromodata=$this->Promotions_manage_data->getcurrentpromodata($promo_id);
		
		if($changes_done ==""){
			$changes_done='<span class="change_to_log"><span class="change_head">Saved as draft</span></span>';
		}
		$name=$this->session->userdata("email");
		$parts = explode("@", $name);
		$username = $parts[0];
		$changes=$changes_done;
		if(!empty($getpromodata)){
			foreach ($getpromodata as $data){
				$status=$data['promo_active'];
				if($status==1){
					$status="Active";
				}
				else{
					$status="Deactive";
				}
				if($data['promo_start_date']!=""){
					$start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
				}
				else{
					$start_date="";
				}
				if($data['promo_end_date']!=""){
					$end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
				}
				else{
					$end_date="";
				}
				$promo_active_status="";
				$promo_approval_status="";
				$promo_approval_sent="";
				if($data['promo_active']==1){
					$promo_active_status.='<b>Active</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				else{
					$promo_active_status.='<b>Deactive</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				
				if($data['promo_approval']==1){
					$promo_approval_status.='<b>Approved</b>  '.$data['approved_by'].' since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				else{
					$promo_approval_status.='<b>Unapproved</b>  since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				
				if($data['approval_sent']==0){
					$promo_approval_sent.='<b>Not Sent</b>'; 
				}
				else{
					$promo_approval_sent.='<b>Sent</b> By '.$username.' on '.date("D,j M Y H:i", strtotime($data['time_approval_sent'])); 
				}
				
				$deal_type=$this->get_promo_type_name($data['promo_type']);
				$final_offer_value='Deal Type: '.$deal_type.'<br>'.'Deal Name: '.$data['promo_name'].'<br>'.
				'Deal Quote: '.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
				'End Date: '.$end_date.'</span>';
				$promo_status='<span class="change_to_log"><b>Changed To Un Approved And Deactivated</b></span>';
			}
		}
		
		
					
		$update_log=$this->Promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
		
	}
	
	public function save_as_draft_promotion_ajax_temp($promo_id="",$changes_done=""){
		if($this->input->post('promo_id')){
			$promo_id=$promo_id=$this->input->post('promo_id');
		}

		$success=$this->Promotions_manage_data->save_as_draft_promotion_temp($promo_id);
		
		$getpromodata=$this->Promotions_manage_data->getcurrentpromodata($promo_id);
		
		if($changes_done ==""){
			$changes_done='<span class="change_to_log"><span class="change_head">Saved as draft</span></span>';
		}

		$name=$this->session->userdata("email");
		$parts = explode("@", $name);
		$username = $parts[0];
		$changes=$changes_done;
		if(!empty($getpromodata)){
			foreach ($getpromodata as $data){
				$status=$data['promo_active'];
				if($status==1){
					$status="Active";
				}
				else{
					$status="Deactive";
				}
				if($data['promo_start_date']!=""){
					$start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
				}
				else{
					$start_date="";
				}
				if($data['promo_end_date']!=""){
					$end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
				}
				else{
					$end_date="";
				}
				$promo_active_status="";
				$promo_approval_status="";
				$promo_approval_sent="";
				if($data['promo_active']==1){
					$promo_active_status.='<b>Active</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				else{
					$promo_active_status.='<b>Deactive</b> since '.date("D,j M Y H:i", strtotime($data['time_active'])); 
				}
				
				if($data['promo_approval']==1){
					$promo_approval_status.='<b>Approved</b>  '.$data['approved_by'].' since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				else{
					$promo_approval_status.='<b>Unapproved</b>  since '.date("D,j M Y H:i", strtotime($data['time_approval'])); 
				}
				
				if($data['approval_sent']==0){
					$promo_approval_sent.='<b>Not Sent</b>'; 
				}
				else{
					$promo_approval_sent.='<b>Sent</b> By '.$username.' on '.date("D,j M Y H:i", strtotime($data['time_approval_sent'])); 
				}
				
				$deal_type=$this->get_promo_type_name($data['promo_type']);
				$final_offer_value='Deal Type: '.$deal_type.'<br>'.'Deal Name: '.$data['promo_name'].'<br>'.
				'Deal Quote: '.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
				'End Date: '.$end_date.'</span>';
				$promo_status='<span class="change_to_log"><b>Changed To Deactivated</b></span>';
			}
		}
		
		
					
		$update_log=$this->Promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
		
	}


	public function save_as_draft_promotion(){

		$promo_id=$promo_id=$this->input->post('promo_id');
        $getpromodata=$this->Promotions_manage_data->getcurrentpromodata($promo_id);

        $name=$this->session->userdata("email");
        $parts = explode("@", $name);
        $username = $parts[0];
        $changes='<span class="change_to_log"><span class="change_head">Saved as draft</span> By <span class="change_data">&#145;' .$username.'</span></span>';

        if(!empty($getpromodata)){
            foreach ($getpromodata as $data){

                if($data['promo_start_date']!=""){
                    $start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
                }
                else{
                    $start_date="";
                }
                if($data['promo_end_date']!=""){
                    $end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
                }
                else{
                    $end_date="";
                }

                
                $deal_type=$this->get_promo_type_name($data['promo_type']);
                $final_offer_value='Deal Type: '.$deal_type.'<br>'.'Deal Name: '.$data['promo_name'].'<br>'.
                'Deal Quote: '.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
                'End Date: '.$end_date.'</span>';
                if($data['promo_active']==1){
                    $promo_status='<span class="change_to_log"><b>Deactivated</b></span>';
                }
                else{
                    $promo_status='<span class="change_to_log"><b>Save As Draft</b></span>';
                }
                
            }
        }
        
        
                    
        $update_log=$this->Promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
		$success=$this->Promotions_manage_data->save_as_draft_promotion_temp($promo_id);
		
		if($success){
			echo "1";
		}
	}

	public function deactive_promotions()
	{
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/promotions/deactive_promotions');
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function approved_promotions()
    {
        if($this->session->userdata("logged_in")){
			$data["menu_flag"]="";
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/promotions/approved_promotions');
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function approved_promotions_processing(){
        if($this->session->userdata("logged_in")){
            
            
            $params = $columns = $totalRecords = $data = array();

    $params = $this->input->post();
    
    $totalRecords=$this->Promotions_manage_data->approved_promotions_processing($params,"get_total_num_recs");
    
    $queryRecordsResult=$this->Promotions_manage_data->approved_promotions_processing($params,"get_recs");
    
    $i=count($queryRecordsResult);
    $i=0;
    foreach($queryRecordsResult as $queryRecordsObj){

        if($queryRecordsObj->promo_approval==0){
            $promo_complete="Pending Approval";
        }
        if($queryRecordsObj->promo_approval==1){
            $promo_complete="Approved";
        }
        
        if($queryRecordsObj->promo_start_date!=""){
            $start_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_start_date));
        }
        else{
            $start_date="";
        }
        
        if($queryRecordsObj->promo_end_date!=""){
            $end_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_end_date));
        }
        else{
            $end_date="";
        }
        
        $row=array();
        $row[]=++$i;
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->id.'</a>';
        $row[]=$promo_complete;
        $row[]='<button class="btn btn-info btn-xs" promo_id='.$queryRecordsObj->promo_uid.' onclick="getAllLogOfThisPromotion(this)"> View Log</button>';
        $row[]=$this->get_promo_type_name($queryRecordsObj->promo_type);
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_name.'</a>';
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_quote.'</a>';
        $row[]=$end_date;
        $row[]=$start_date;
        
        $data[]=$row;
        $row=array();
    }
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
            );
    echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }

	public function active_promotions()
	{
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/promotions/active_promotions');
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
		
	public function promotion_pending_approval()
	{
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/promotions/pending_promotions');
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
    public function draft_promotions()
    {
        if($this->session->userdata("logged_in")){
			$data["menu_flag"]="";
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/promotions/draft_promotions');
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function rejected_promotions()
    {
        if($this->session->userdata("logged_in")){
			$data["menu_flag"]="";
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/promotions/rejected_promotions.php');
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function rejected_promotions_processing(){
    if($this->session->userdata("logged_in")){
            
            
    $params = $columns = $totalRecords = $data = array();

    $params = $this->input->post();
    
    $totalRecords=$this->Promotions_manage_data->rejected_promotions_processing($params,"get_total_num_recs");
    
    $queryRecordsResult=$this->Promotions_manage_data->rejected_promotions_processing($params,"get_recs");
    
    $i=count($queryRecordsResult);
    $i=0;
    foreach($queryRecordsResult as $queryRecordsObj){


         $promo_complete="Rejected";
 
        
        if($queryRecordsObj->promo_start_date!=""){
            $start_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_start_date));
        }
        else{
            $start_date="";
        }
        
        if($queryRecordsObj->promo_end_date!=""){
            $end_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_end_date));
        }
        else{
            $end_date="";
        }
        
        $row=array();
        $row[]=++$i;
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->id.'</a>';
        $row[]=$promo_complete;
        $row[]='<button class="btn btn-info btn-xs" promo_id='.$queryRecordsObj->promo_uid.' onclick="getAllLogOfThisPromotion(this)"> View Log</button>';
        $row[]=$this->get_promo_type_name($queryRecordsObj->promo_type);
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_name.'</a>';
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_quote.'</a>';
        $row[]=$end_date;
        $row[]=$start_date;
        
        $data[]=$row;
        $row=array();
    }
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
            );
    echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
	public function expired_promotions()
	{
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/promotions/expired_promotions');
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
    public function revertedDraft_promotions(){
        if($this->session->userdata("logged_in")){
			$data["menu_flag"]="";
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/promotions/revertedDraft_promotions');
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
    public function revertedDraft_promotions_processing(){
    if($this->session->userdata("logged_in")){
            
            
    $params = $columns = $totalRecords = $data = array();

    $params = $this->input->post();
    
    $totalRecords=$this->Promotions_manage_data->revertedDraft_promotions_processing($params,"get_total_num_recs");
    
    $queryRecordsResult=$this->Promotions_manage_data->revertedDraft_promotions_processing($params,"get_recs");
    
    $i=count($queryRecordsResult);
    $i=0;
    foreach($queryRecordsResult as $queryRecordsObj){

        if($queryRecordsObj->approval_sent==0){
            $promo_complete="Draft";
        }
        if($queryRecordsObj->approval_sent==1){
            $promo_complete="Complete";
        }
        
        if($queryRecordsObj->promo_start_date!=""){
            $start_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_start_date));
        }
        else{
            $start_date="";
        }
        
        if($queryRecordsObj->promo_end_date!=""){
            $end_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_end_date));
        }
        else{
            $end_date="";
        }
        
        $row=array();
        $row[]=++$i;
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->id.'</a>';
        $row[]=$promo_complete;
        $row[]='<button class="btn btn-info btn-xs" promo_id='.$queryRecordsObj->promo_uid.' onclick="getAllLogOfThisPromotion(this)"> View Log</button>';
        $row[]=$this->get_promo_type_name($queryRecordsObj->promo_type);
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_name.'</a>';
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_quote.'</a>';
        $row[]=$end_date;
        $row[]=$start_date;
        
        $data[]=$row;
        $row=array();
    }
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
            );
    echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }

    
    public function draft_promotions_processing(){
    if($this->session->userdata("logged_in")){
            
            
    $params = $columns = $totalRecords = $data = array();

    $params = $this->input->post();
    
    $totalRecords=$this->Promotions_manage_data->draft_promotions_processing($params,"get_total_num_recs");
    
    $queryRecordsResult=$this->Promotions_manage_data->draft_promotions_processing($params,"get_recs");
    
    $i=count($queryRecordsResult);
    $i=0;
    foreach($queryRecordsResult as $queryRecordsObj){
        /*if($queryRecordsObj->promo_active==0){
            $promo_active="Stop";
        }
        if($queryRecordsObj->promo_active==1){
            $promo_active="Running";
        }
        if($queryRecordsObj->promo_approval==0){
            $promo_approval="Pending Approval";
        }
        if($queryRecordsObj->promo_approval==1){
            $promo_approval="Approved";
        }*/
        if($queryRecordsObj->approval_sent==0){
            $promo_complete="Draft";
        }
        if($queryRecordsObj->approval_sent==1){
            $promo_complete="Complete";
        }
        
        if($queryRecordsObj->promo_start_date!=""){
            $start_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_start_date));
        }
        else{
            $start_date="";
        }
        
        if($queryRecordsObj->promo_end_date!=""){
            $end_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_end_date));
        }
        else{
            $end_date="";
        }
        
        $row=array();
        $row[]=++$i;
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->id.'</a>';
        $row[]=$promo_complete;
        $row[]='<button class="btn btn-info btn-xs" promo_id='.$queryRecordsObj->promo_uid.' onclick="getAllLogOfThisPromotion(this)"> View Log</button>';
        $row[]=$this->get_promo_type_name($queryRecordsObj->promo_type);
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_name.'</a>';
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_quote.'</a>';
        $row[]=$end_date;
        $row[]=$start_date;
        
        $data[]=$row;
        $row=array();
    }
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
            );
    echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
	public function pending_promotions_processing(){
		if($this->session->userdata("logged_in")){
			
			
			$params = $columns = $totalRecords = $data = array();

	$params = $this->input->post();
	
	$totalRecords=$this->Promotions_manage_data->pending_promotions_processing($params,"get_total_num_recs");
	
	$queryRecordsResult=$this->Promotions_manage_data->pending_promotions_processing($params,"get_recs");
	
	$i=count($queryRecordsResult);
	$i=0;
	foreach($queryRecordsResult as $queryRecordsObj){

        if($queryRecordsObj->promo_approval==0 && $queryRecordsObj->hold_flag==0){
            $promo_approval="Pending Approval";
        }
        if($queryRecordsObj->promo_approval==1){
            $promo_approval="Approved";
        }
        if($queryRecordsObj->promo_approval==0 && $queryRecordsObj->hold_flag==1){
            $promo_approval="Put On Hold";
        }

        
        if($queryRecordsObj->promo_start_date!=""){
            $start_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_start_date));
        }
        else{
            $start_date="";
        }
        
        if($queryRecordsObj->promo_end_date!=""){
            $end_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_end_date));
        }
        else{
            $end_date="";
        }
        
        $row=array();
        $row[]=++$i;
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->id.'</a>';
        $row[]=$promo_approval;
		$row[]='<button class="btn btn-info btn-xs" promo_id='.$queryRecordsObj->promo_uid.' onclick="getAllLogOfThisPromotion(this)"> View Log</button>';
		$row[]=$this->get_promo_type_name($queryRecordsObj->promo_type);
		$row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_name.'</a>';
		$row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_quote.'</a>';
        $row[]=$end_date;
		$row[]=$start_date;
		
		$data[]=$row;
		$row=array();
	}
	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);
	echo json_encode($json_data);  // send data as json format
	
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
    public function active_promotions_processing(){
		if($this->session->userdata("logged_in")){
			
			
			$params = $columns = $totalRecords = $data = array();

	$params = $this->input->post();
	
	$totalRecords=$this->Promotions_manage_data->active_promotions_processing($params,"get_total_num_recs");
	
	$queryRecordsResult=$this->Promotions_manage_data->active_promotions_processing($params,"get_recs");
	
	$i=count($queryRecordsResult);
	$i=0;
	foreach($queryRecordsResult as $queryRecordsObj){
		if($queryRecordsObj->promo_active==0){
            $promo_active="Stop";
        }
        if($queryRecordsObj->promo_active==1){
            $promo_active="Running";
        }
        /*if($queryRecordsObj->promo_approval==0){
            $promo_approval="Pending Approval";
        }
        if($queryRecordsObj->promo_approval==1){
            $promo_approval="Approved";
        }
        if($queryRecordsObj->approval_sent==0){
            $promo_complete="Draft";
        }
        if($queryRecordsObj->approval_sent==1){
            $promo_complete="Complete";
        }*/
        
        if($queryRecordsObj->promo_start_date!=""){
            $start_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_start_date));
        }
        else{
            $start_date="";
        }
        
        if($queryRecordsObj->promo_end_date!=""){
            $end_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_end_date));
        }
        else{
            $end_date="";
        }
        
        $row=array();
        $row[]=++$i;
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->id.'</a>';
        $row[]=$promo_active;
		$row[]='<button class="btn btn-info btn-xs" promo_id='.$queryRecordsObj->promo_uid.' onclick="getAllLogOfThisPromotion(this)"> View Log</button>';
		$row[]=$this->get_promo_type_name($queryRecordsObj->promo_type);
		$row[]=$queryRecordsObj->promo_name;
		$row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_quote.'</a>';
		$row[]=$start_date;
		$row[]=$end_date;
		$data[]=$row;
		$row=array();
	}
	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);
	echo json_encode($json_data);  // send data as json format
	
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

    public function deactive_promotions_processing(){
	if($this->session->userdata("logged_in")){
			
			
	$params = $columns = $totalRecords = $data = array();

	$params = $this->input->post();
	
	$totalRecords=$this->Promotions_manage_data->deactive_promotions_processing($params,"get_total_num_recs");
	
	$queryRecordsResult=$this->Promotions_manage_data->deactive_promotions_processing($params,"get_recs");
	
	$i=count($queryRecordsResult);
	$i=0;
	foreach($queryRecordsResult as $queryRecordsObj){
		if($queryRecordsObj->promo_active==0){
            $promo_active="Stop";
        }
        if($queryRecordsObj->promo_active==1){
            $promo_active="Running";
        }
        /*if($queryRecordsObj->promo_approval==0){
            $promo_approval="Pending Approval";
        }
        if($queryRecordsObj->promo_approval==1){
            $promo_approval="Approved";
        }
        if($queryRecordsObj->approval_sent==0){
            $promo_complete="Draft";
        }
        if($queryRecordsObj->approval_sent==1){
            $promo_complete="Complete";
        }
        */
        if($queryRecordsObj->promo_start_date!=""){
            $start_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_start_date));
        }
        else{
            $start_date="";
        }
        
        if($queryRecordsObj->promo_end_date!=""){
            $end_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_end_date));
        }
        else{
            $end_date="";
        }
        
        $row=array();
        $row[]=++$i;
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->id.'</a>';
        $row[]=$promo_active;
		$row[]='<button class="btn btn-info btn-xs" promo_id='.$queryRecordsObj->promo_uid.' onclick="getAllLogOfThisPromotion(this)"> View Log</button>';
		$row[]=$this->get_promo_type_name($queryRecordsObj->promo_type);
		$row[]=$queryRecordsObj->promo_name;
		$row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_quote.'</a>';
		$row[]=$start_date;
		$row[]=$end_date;
		$data[]=$row;
		$row=array();
	}
	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);
	echo json_encode($json_data);  // send data as json format
	
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

    public function expired_promotions_processing(){
        if($this->session->userdata("logged_in")){
            
            
            $params = $columns = $totalRecords = $data = array();

    $params = $this->input->post();
    
    $totalRecords=$this->Promotions_manage_data->expired_promotions_processing($params,"get_total_num_recs");
    
    $queryRecordsResult=$this->Promotions_manage_data->expired_promotions_processing($params,"get_recs");
    
    $i=count($queryRecordsResult);
    $i=0;
    foreach($queryRecordsResult as $queryRecordsObj){
        /*if($queryRecordsObj->promo_active==0){
            $promo_active="Stop";
        }
        if($queryRecordsObj->promo_active==1){
            $promo_active="Running";
        }
        if($queryRecordsObj->promo_approval==0){
            $promo_approval="Pending Approval";
        }
        if($queryRecordsObj->promo_approval==1){
            $promo_approval="Approved";
        }
        if($queryRecordsObj->approval_sent==0){
            $promo_complete="Draft";
        }
        if($queryRecordsObj->approval_sent==1){
            $promo_complete="Complete";
        }
        */
        if($queryRecordsObj->promo_start_date!=""){
            $start_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_start_date));
        }
        else{
            $start_date="";
        }
        
        if($queryRecordsObj->promo_end_date!=""){
            $end_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_end_date));
        }
        else{
            $end_date="";
        }
        
        
        $today = new DateTime('');
        $expireDate = new DateTime( $end_date);
        
        if($expireDate < $today){
            $validity="Expired";
        }
        else{
            $validity="Persist";
        }
        
        $row=array();
        $row[]=++$i;
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->id.'</a>';
        $row[]=$validity;
        $row[]='<button class="btn btn-info btn-xs" promo_id='.$queryRecordsObj->promo_uid.' onclick="getAllLogOfThisPromotion(this)"> View Log</button>';
        $row[]=$this->get_promo_type_name($queryRecordsObj->promo_type);
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_name.'</a>';
        $row[]='<a href="'.base_url().'admin/Promotions/manage_promo_items/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_quote.'</a>';
        $row[]=$end_date;
        $row[]=$start_date;
        
        $data[]=$row;
        $row=array();
    }
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
            );
    echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
	public function get_all_log_data_of_promotion(){
		if($this->session->userdata("logged_in")){
			$promo_id=$this->input->post('promo_id');
			$getAllData=$this->Promotions_manage_data->get_all_log_data_of_promotion($promo_id);
			$str='<div class="table-responsive"><table class="table table-hover table-condensed"><thead>
			<tr>
				<th width="5%">S No.</th>
		        <th width="15%">Log</th>
		       <th width="10%">Date Time</th>
                <th width="10%">By</th>
		       
		        <th width="30%">Promotion Summary</th>
		        <th width="30%">Promotion Status</th>
		      </tr>
		    </thead>
			<tbody>';
			if(!empty($getAllData)){
				$i=1;
				foreach($getAllData as $data){
					$str.='<tr><td>'.$i.'</td><td>'.$data['changes'].'</td><td>'.date("D,j M Y H:i", strtotime($data['timestamp'])).'</td><td>'.$data['user_id'].'</td><td><button class="accordion"></button><div class="panel">'.$data['commit'].'</div></td><td>'.$data['promo_status'].'</td></tr>';
					$i++;
				}
			}
			else{
				$str.='<tr><td colspan="6">No Data to Show</td></tr>';
			}
			$str.='</tbody></table></div>';
			echo $str;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
		
	public function get_all_num_data_of_all_promotion(){
	   {
        if($this->session->userdata("logged_in")){
            $all_num_promo=array();
            $revertdraft_promotion_num=$this->Promotions_manage_data->revertdraft_promotion_num();
            $all_num_promo['revertdraft_promotion_num']=$revertdraft_promotion_num;
            $expired_promotion_num=$this->Promotions_manage_data->expired_promotion_num();
            $all_num_promo['expired_promotion_num']=$expired_promotion_num;
            $active_promotion_num=$this->Promotions_manage_data->active_promotion_num();
            $all_num_promo['active_promotion_num']=$active_promotion_num;
            $draft_promotion_num=$this->Promotions_manage_data->draft_promotion_num();
            $all_num_promo['draft_promotion_num']=$draft_promotion_num;
            $deactivated_promotion_num=$this->Promotions_manage_data->deactivated_promotion_num();
            $all_num_promo['deactivated_promotion_num']=$deactivated_promotion_num;
            $pending_promotion_num=$this->Promotions_manage_data->pending_promotion_num();
            $all_num_promo['pending_promotion_num']=$pending_promotion_num;
            $approved_promotion_num=$this->Promotions_manage_data->approved_promotion_num();
            $all_num_promo['approved_promotion_num']=$approved_promotion_num;
            $rejetced_promotions_num=$this->Promotions_manage_data->rejected_promotions_num();
            $all_num_promo['rejected_promotions_num']=$rejetced_promotions_num;
            echo json_encode($all_num_promo);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    } 
	}
}
	 
	
