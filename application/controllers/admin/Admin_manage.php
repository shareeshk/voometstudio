<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin_manage extends CI_Controller {
	function __construct(){
		parent::__construct();
       
		$data["controller"]=$this;
		$this->load->model('admin/Admin_manage_model');
		$this->load->helper('emailsmstemplate');
		$this->load->helper('general');
		
	}
	public function index(){
		if($this->session->userdata("user_type")=="Master Country"){
		    if($this->session->userdata('user_type')=="Master Country"){					   
                        redirect(site_url("admin/Orders/onhold_orders"));	
                    }else{
                        $this->load->view('admin/header');
                        $this->load->view('admin/login');
                    }
                }else if($this->session->userdata("user_type")=="Su Master Country"){
                    redirect(site_url("admin/Catalogue/inventory_filter"));
                }else if ($this->session->userdata("user_type") == "vendor") {
					redirect(site_url("admin/Catalogue/vendor_inventory"));
				}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	 public function add_subadmins(){
		if($this->session->userdata("user_type")=="Master Country"){
			$data["controller"]=$this;
			$this->load->view('admin/vmanage');
			$this -> load -> view('admin/add_subadmins');
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function add_subadmins_action(){
		if($this->session->userdata("user_type")=="Master Country"){
			///////////////////////////////
			$config['upload_path'] = 'assets/pictures/images/profile_pics/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '2048';
			$config['max_width'] = '5024';
			$config['max_height'] = '5800';
			$this -> load -> library('upload', $config);
			if (!$this -> upload -> do_upload('profile_pic')) {
				echo "something went wrong, display errors" . $this -> upload -> display_errors();
			} else {
				$upload_data = $this -> upload -> data();
				//$file_name_arr=explode(".",$upload_data['file_name']);
				//$file_name=$this->sample_code_for_images().".".$file_name_arr[count(//$file_name_arr)-1];
				//$filenamepath = "assets/pictures/images/profile_pics/" . $file_name;
				$filenamepath = "assets/pictures/images/profile_pics/" . $upload_data['file_name'];
				
		}
			//////////////////////////////
			$name=$this->input->post("name");
			$email=$this->input->post("email");
			$password=md5($this->input->post("password"));
			$mobile=$this->input->post("mobile");
			$landline=$this->input->post("landline");
			$gender=$this->input->post("gender");
			$dob=$this->input->post("dob");
			$user_type=$this->input->post("user_type");
			$panel_access=$this->input->post("panel_access");
			$panel_access=implode(",",$panel_access);
			$flag=$this->Admin_manage_model->add_subadmins_action($name,$email,$password,$mobile,$landline,$gender,$dob,$user_type,$panel_access,$filenamepath);
			echo $flag;
		}else{
			redirect(base_url());
		}
	}
	public function check_email_subadmins(){
		$email=$this->input->post("email");
		$data=$this->Admin_manage_model->check_email_subadmins($email);
	}
	public function check_mobile_subadmins(){
		$mobile=$this->input->post("mobile");
		$data=$this->Admin_manage_model->check_mobile_subadmins($mobile);
	}
	
	public function view_subadmins(){
		if($this->session->userdata("user_type")=="Master Country"){
			$data["controller"]=$this;
			$data["view_delivery_staff"]=$this->Admin_manage_model->view_subadmins();
			$this->load->view('admin/vmanage');
			$this -> load -> view('admin/view_subadmins',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function update_subadmins($subadmin_user_id){
		if($this->session->userdata("user_type")=="Master Country"){
			$data["controller"]=$this;
			$data["edit_subadmin_user_id"]=$subadmin_user_id;
			$this->session->set_userdata("edit_subadmin_user_id",$subadmin_user_id);
			$data["delivery_staff"]=$this->Admin_manage_model->update_subadmins($subadmin_user_id);
			$this->load->view('admin/vmanage');
			$this -> load -> view('admin/update_subadmins',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function check_email_subadmins_edit(){
		$email=$this->input->post("email");
		$edit_subadmin_user_id=$this->session->userdata("edit_subadmin_user_id");
		$data=$this->Admin_manage_model->check_email_subadmins_edit($email,$edit_subadmin_user_id);
	}
	public function check_mobile_subadmins_edit(){
		$mobile=$this->input->post("mobile");
		$edit_subadmin_user_id=$this->session->userdata("edit_subadmin_user_id");
		$data=$this->Admin_manage_model->check_mobile_subadmins_edit($mobile,$edit_subadmin_user_id);
	}
	public function sample_code_for_images(){
				$seed = str_split('abcdefghijklmnopqrstuvwxyz'
		                     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
						shuffle($seed); // probably optional since array_is randomized; this may be redundant
						$rand = '';
						foreach (array_rand($seed, 8) as $k) $rand .= $seed[$k];	
						return $rand;
			}
	public function edit_subadmins_action(){
		if($this->session->userdata("user_type")=="Master Country"){
			///////////////////////////////
			if($_FILES["profile_pic"]["size"]!='0'){
				$config['upload_path'] = 'assets/pictures/images/profile_pics/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = '2048';
				$config['max_width'] = '5024';
				$config['max_height'] = '5800';
				$this -> load -> library('upload', $config);
				if (!$this -> upload -> do_upload('profile_pic')) {
					echo "something went wrong, display errors" . $this -> upload -> display_errors();
				} else {
					$upload_data = $this -> upload -> data();
					//$file_name_arr=explode(".",$upload_data['file_name']);
					//$file_name=$this->sample_code_for_images().".".$file_name_arr[count($file_name_arr)-1];
					//$filenamepath = "assets/pictures/images/profile_pics/" . $file_name;
					$filenamepath = "assets/pictures/images/profile_pics/" . $upload_data['file_name'];
				}
			}
			else{
				$filenamepath="";
			}
			//////////////////////////////
			
			$edit_subadmin_user_id=$this->input->post("edit_subadmin_user_id");
			$name=$this->input->post("name");
			$email=$this->input->post("email");
			$password=md5($this->input->post("password"));
			$mobile=$this->input->post("mobile");
			$landline=$this->input->post("landline");
			$gender=$this->input->post("gender");
			$dob=$this->input->post("dob");
			$user_type=$this->input->post("user_type");
			$panel_access=$this->input->post("panel_access");
			$panel_access=implode(",",$panel_access);
			$flag=$this->Admin_manage_model->edit_subadmins_action($edit_subadmin_user_id,$name,$email,$password,$mobile,$landline,$gender,$dob,$user_type,$panel_access,$filenamepath);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function delete_subadmins($subadmin_user_id){
		if($this->session->userdata("user_type")=="Master Country"){
			$flag=$this->Admin_manage_model->delete_subadmins($subadmin_user_id);
			if($flag){
			
				$this->session->set_flashdata('notification',"Deleted successfully");
				redirect(base_url().('admin/Admin_manage/view_subadmins')); 
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_total_unread_return_msg(){
		if($this->session->userdata("user_type")=="Master Country"){
			$data["controller"]=$this;
			$total_unread_msg=$this->Admin_manage_model->get_total_unread_return_msg();
			echo $total_unread_msg;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_total_unread_return_msg_awaiting(){
		if($this->session->userdata("user_type")=="Master Country"){
			$data["controller"]=$this;
			$total_unread_msg=$this->Admin_manage_model->get_total_unread_return_msg_awaiting();
			echo $total_unread_msg;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_total_unread_return_msg_customer_rejected(){
		if($this->session->userdata("user_type")=="Master Country"){
			$data["controller"]=$this;
			$total_unread_msg=$this->Admin_manage_model->get_total_unread_return_msg_customer_rejected();
			echo $total_unread_msg;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_total_unread_return_msg_customer_accepted(){
		if($this->session->userdata("user_type")=="Master Country"){
			$data["controller"]=$this;
			$total_unread_msg=$this->Admin_manage_model->get_total_unread_return_msg_customer_accepted();
			echo $total_unread_msg;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_total_unread_return_msg_admin_rejected(){
		if($this->session->userdata("user_type")=="Master Country"){
			$data["controller"]=$this;
			$total_unread_msg=$this->Admin_manage_model->get_total_unread_return_msg_admin_rejected();
			echo $total_unread_msg;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_total_unread_return_msg_refund_failure(){
		if($this->session->userdata("user_type")=="Master Country"){
			$data["controller"]=$this;
			$total_unread_msg=$this->Admin_manage_model->get_total_unread_return_msg_refund_failure();
			echo $total_unread_msg;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_total_count_of_replacement_and_return(){
		
		$temp=array();
		$temp['return_msg']=($this->Admin_manage_model->get_total_unread_return_msg());
		$temp['msg_awaiting']=$this->Admin_manage_model->get_total_unread_return_msg_awaiting();
		$temp['msg_customer_rejected']=$this->Admin_manage_model->get_total_unread_return_msg_customer_rejected();
		$temp['msg_customer_accepted']=$this->Admin_manage_model->get_total_unread_return_msg_customer_accepted();
		$temp['msg_admin_rejected']=$this->Admin_manage_model->get_total_unread_return_msg_admin_rejected();
		$temp['msg_refund_failure']=$this->Admin_manage_model->get_total_unread_return_msg_refund_failure();
		
		
		$temp['need_to_be_refund']=$this->Admin_manage_model->get_total_need_to_be_refund();
		$temp['refund_in_process']=$this->Admin_manage_model->get_total_refund_in_process();	
		
		$temp['success_partial_refund']=$this->Admin_manage_model->get_total_unread_return_msg_refund_success_partial_refund();
		$temp['success_full_refund']=$this->Admin_manage_model->get_total_refund_success_full_refund();
		
		////////////////////////////////////////
		$partial_refund=0;
		$full_refund=0;
		
		$queryRecordsResult=$this->Admin_manage_model->returns_refund_request_orders_refund_success();
		
		foreach($queryRecordsResult as $queryRecordsObj){
				
				$grandtotal_of_return_refund_success=$this->Admin_manage_model->get_grandtotal_of_return_refund_success($queryRecordsObj->order_item_id);
				$order_return_decision_data_obj=$this->Admin_manage_model->get_order_return_decision_data($queryRecordsObj->order_item_id);
				//////////////////////////////////////////////////////////////////////
	
				$return_refund_request_again_details_arr=$this->Admin_manage_model->return_refund_request_again_need_to_refund_details($queryRecordsObj->order_item_id);
				
				$sub_return_refund_table_data_refunded_arr=$this->Admin_manage_model->sub_return_refund_table_data_refunded_by_order_item_id($queryRecordsObj->order_item_id);
				
				if($queryRecordsObj->purchased_cash_back_value>0){
					$total_grand_purchased=($queryRecordsObj->grandtotal_of_quantity_purchased-$queryRecordsObj->purchased_cash_back_value);
	
				}else{
					
					$total_grand_purchased=$queryRecordsObj->grandtotal_of_quantity_purchased;
					
					if($order_return_decision_data_obj->promotion_surprise_gift_type==curr_code){

						$quantity_filled=$order_return_decision_data_obj->quantity;
						$purchased_qty=$queryRecordsObj->quantity;

						if(!empty($return_refund_request_again_details_arr)){
							
							if(($return_refund_request_again_details_arr["admin_action"]=="accepted") || ($return_refund_request_again_details_arr["admin_action"]=="refund") || ($return_refund_request_again_details_arr["admin_action"]=="refunded")){
								$quantity_filled+=$return_refund_request_again_details_arr["quantity_requested_admin"];	
							}
						}
						if($quantity_filled==$purchased_qty){
							$total_grand_purchased=($queryRecordsObj->grandtotal_of_quantity_purchased-$order_return_decision_data_obj->promotion_surprise_gift);
						}
					}
					if($queryRecordsObj->promotion_invoice_discount>0){
						$order_item_invoice_discount_value=round($queryRecordsObj->promotion_invoice_discount/$queryRecordsObj->cart_quantity,2);
						$total_grand_purchased-=floatval($order_item_invoice_discount_value);
					}
					
				}
			
				
				if((round($grandtotal_of_return_refund_success)!=round($total_grand_purchased)) && count($sub_return_refund_table_data_refunded_arr)==0){
					//partial
					$partial_refund++;
				}
				if((round($grandtotal_of_return_refund_success)==round($total_grand_purchased)) || count($sub_return_refund_table_data_refunded_arr)>0){
					//full
					$full_refund++;
				}
		}
		
		if($full_refund!=0 && $full_refund>0){
			$temp['success_full_refund']="yes";
		}else{
			$temp['success_full_refund']="";
		}
		if($partial_refund!=0 && $partial_refund>0){
			$temp['success_partial_refund']="yes";
		}else{
			$temp['success_partial_refund']="";
		}
		
		////////////////////////////////////////
		
		
		
		//$temp['refund_failure']=$this->Admin_manage_model->get_total_refund_failure();
		
		
		$temp['return_msg_replacement']=$this->Admin_manage_model->get_total_unread_return_msg_replacement();
		$temp['msg_awaiting_replacement']=$this->Admin_manage_model->get_total_unread_return_msg_awaiting_replacement();
		$temp['msg_customer_accepted_replacement']=$this->Admin_manage_model->get_total_unread_return_msg_customer_accepted_replacement();
		$temp['msg_balance_clear_replacement']=$this->Admin_manage_model->get_total_unread_return_msg_balance_clear_replacement();
		
		
		$temp['msg_customer_rejected_replacement']=$this->Admin_manage_model->get_total_unread_return_msg_customer_rejected_replacement();
		$temp['msg_admin_rejected_replacement']=$this->Admin_manage_model->get_total_unread_return_msg_admin_rejected_replacement();
		$temp['ready_to_replace']=$this->Admin_manage_model->get_total_return_stock_ready_to_replace();
		$temp['return_stock_requests_replacement']=$this->Admin_manage_model->get_total_return_stock_requests_replacement();
		$temp['return_stock_notification']=$this->Admin_manage_model->get_total_return_stock_notification();
		$temp['replacement_return_cancel']=$this->Admin_manage_model->get_total_replacement_return_cancel();
		
			$par=0;
			$full=0;
			$queryRecordsResult=$this->Admin_manage_model->returns_replacement_request_orders_refund();

			foreach($queryRecordsResult as $queryRecordsObj){
				$history_orders_obj=$this->Admin_manage_model->get_data_from_history_orders($queryRecordsObj->prev_order_item_id);
				$purchased_quantity=$history_orders_obj->quantity;
				$replaced_quantity=$queryRecordsObj->quantity;
				//////////////////////////////////////////////////////////////////////
				if($purchased_quantity==$replaced_quantity){
					$full++;
				}
				if($purchased_quantity>$replaced_quantity){
					$par++;
				}
			}
		if($full!=0 && $full>0){
			$temp['success_refund_replacement']="yes";
		}else{
			$temp['success_refund_replacement']="";
		}
		if($par!=0 && $par>0){
			$temp['success_partial_refund_replacement']="yes";
		}else{
			$temp['success_partial_refund_replacement']="";
		}

		echo json_encode($temp);
	}
	
	public function get_total_count_of_order_management(){
		
		$temp=array();
		$temp['onhold_orders']=($this->Admin_manage_model->get_total_count_of_onhold_orders());
		$temp['order_approved']=$this->Admin_manage_model->get_total_count_of_order_approved();
		$temp['order_confirmed']=$this->Admin_manage_model->get_total_count_of_order_confirmed();
		$temp['order_packed']=$this->Admin_manage_model->get_total_count_of_order_packed();
		$temp['order_shipped']=$this->Admin_manage_model->get_total_count_of_order_shipped();
		$temp['order_delivered']=$this->Admin_manage_model->get_total_count_of_order_delivered();
		$temp['order_cancelled']=$this->Admin_manage_model->get_total_count_of_order_cancelled();
		
		echo json_encode($temp);
	}
	public function get_total_count_of_cancels_refund(){
		
		$temp=array();
		$temp['refund_in_process']=$this->Admin_manage_model->get_total_count_cancel_refund_in_process();
		$temp['refund_success']=$this->Admin_manage_model->get_total_count_cancel_refund_success();
		$temp['refund_failure']=$this->Admin_manage_model->get_total_count_cancel_refund_failure();
		
		echo json_encode($temp);
	}
	public function get_total_count_of_notification(){
		$temp=array();
		$temp['low_stock_notification']=$this->Admin_manage_model->get_total_count_of_notification();
		$temp['request_out_of_stock']=$this->Admin_manage_model->get_total_count_of_request_of_out_of_stock();
		$temp['new_vendor_sku_notification']=$this->Admin_manage_model->get_total_of_unread_data_of_vendor_sku();
		echo json_encode($temp);
	}
	public function get_notify_active(){
		$flag=$this->Admin_manage_model->get_notify_active();
		echo $flag;
	}
	public function get_notify_invoice(){
		$flag=$this->Admin_manage_model->get_notify_invoice();
		echo $flag;
	}
	public function get_notify_cancel_order(){
		$flag=$this->Admin_manage_model->get_notify_cancel_order();
		echo $flag;
	}
	public function get_notify_return_replacement(){
		$flag=$this->Admin_manage_model->get_notify_return_replacement();
		echo $flag;
	}
	public function get_notify_return_refund(){
		$flag=$this->Admin_manage_model->get_notify_return_refund();
		echo $flag;
	}
	public function get_notify_replacement_reject(){
		$flag=$this->Admin_manage_model->get_notify_replacement_reject();
		echo $flag;
	}
	public function get_notify_replacement_accept(){
		$flag=$this->Admin_manage_model->get_notify_replacement_accept();
		echo $flag;
	}
	public function get_notify_replacement_ready_to_replace(){
		$flag=$this->Admin_manage_model->get_notify_replacement_ready_to_replace();
		echo $flag;
	}
	public function get_notify_replacement_cancel(){
		$flag=$this->Admin_manage_model->get_notify_replacement_cancel();
		echo $flag;
	}
	public function get_notify_replacement_stock(){
		$flag=$this->Admin_manage_model->get_notify_replacement_stock();
		echo $flag;
	}
	public function get_notify_replacement_stock_yes_no_decision(){
		$flag=$this->Admin_manage_model->get_notify_replacement_stock_yes_no_decision();
		echo $flag;
	}
	public function get_notify_return_reject(){
		$flag=$this->Admin_manage_model->get_notify_return_reject();
		echo $flag;
	}
	public function get_notify_refund_accept(){
		$flag=$this->Admin_manage_model->get_notify_refund_accept();
		echo $flag;
	}
	public function get_notify_refund_again(){
		$flag=$this->Admin_manage_model->get_notify_refund_again();
		echo $flag;
	}
	public function get_notify_refund_account(){
		$flag=$this->Admin_manage_model->get_notify_refund_account();
		echo $flag;
	}
	public function get_notify_cancel_account(){
		$flag=$this->Admin_manage_model->get_notify_cancel_account();
		echo $flag;
	}
	public function get_notify_replacement_account(){
		$flag=$this->Admin_manage_model->get_notify_replacement_account();
		echo $flag;
	}
	public function get_notify_bank_transfer_account(){
		$flag=$this->Admin_manage_model->get_notify_bank_transfer_account();
		echo $flag;
	}
	
	public function get_notify_return_cancel_for_repl_and_refund_stock(){
		$flag1=$this->Admin_manage_model->get_notify_return_cancel();
		$flag2=$this->Admin_manage_model->get_notify_return_cancel_rep();
		$flag3=$this->Admin_manage_model->get_notify_return_cancel_stock_notification();
		
		if($flag1=="yes" || $flag2=="yes" || $flag3=="yes"){
			echo "yes";
		}else{
			echo "no";
		}
		
	}
	public function get_notify_promotion_account(){
		$flag=$this->Admin_manage_model->get_notify_promotion_account();
		echo $flag;
	}
	public function get_notify_promotion_account_invoice(){
		$flag=$this->Admin_manage_model->get_notify_promotion_account_invoice();
		echo $flag;
	}
	public function get_notify_refund_failure(){
		$flag=$this->Admin_manage_model->get_notify_refund_failure();
		echo $flag;
	}
	public function get_notify_refund_account_incoming_again(){
		$flag=$this->Admin_manage_model->get_notify_refund_account_incoming_again();
		echo $flag;
	}
	public function request_quote($id=''){
            
		if($this->session->userdata("logged_in")){
			$vendor_id='';
			$user_type=$this->session->userdata("user_type");
			$data["menu_flag"]="request_quote";
			if($id!='' && $user_type!='vendor' ){
				$vendor_id=$id;
				$data["menu_flag"]="request_quote";
			}
			if($user_type=='vendor'){
				$vendor_id=$this->session->userdata("user_id");
				$data["menu_flag"]="request_quote";
			}

			$data["vendor_id"]=$vendor_id;
			
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/request_quote',$data);
		}else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
		}
	}
	public function request_quote_processing(){
	
		if($this->session->userdata("logged_in")){
			$user_type=$this->session->userdata("user_type");
			if($user_type!='vendor' ){
				
			}
			if($user_type=='vendor'){
				$vendor_id=$this->session->userdata("user_id");
				
			}
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Admin_manage_model->request_quote_processing($params,"get_total_num_recs");

			$queryRecordsResult=$this->Admin_manage_model->request_quote_processing($params,"get_recs");
			$s_no=0;
			foreach($queryRecordsResult as $queryRecordsObj){
					$row=array();
					$row[]=++$s_no;
					
					$vtxt='';
					
					$vtxt.= '<a target="_blank" href="'.base_url().'admin/Manage_cust/view/'.$queryRecordsObj->rq_customer_id.'">'.ucfirst($queryRecordsObj->rq_name).'</a>';
					$vtxt.= (strtolower($queryRecordsObj->rq_customer_id)=="guest") ? '<small> (Guest)</small>' : '';
					$vtxt.= '<br>'.$queryRecordsObj->rq_email;
					$vtxt.= '<br>'.$queryRecordsObj->rq_mobile;
					$vtxt.= '<br><b>Request ID</b>';
					$vtxt.= '<br>'.$queryRecordsObj->rq_unique_id;

					$row[]=$vtxt;
					
					$t1='';
					$t1.= '<table class="table table-bordered">';
					
					//$t1.="<li>"."<small><b>Qty : </b></small>".$queryRecordsObj->rq_qty."</li>";
					//$t1.="<li>"."<small><b>Units : </b></small>".$queryRecordsObj->rq_units."</li>";
					$t1.="<thead>"."<th><small>SKU</small></th><th><small>Qty</small></th><th><small>Product Vendor</small></th></thead>";
					
					$rq_sku_ids=$queryRecordsObj->rq_sku_ids;
					$rq_inv_ids=$queryRecordsObj->rq_inv_ids;
					$rq_inv_qtys=$queryRecordsObj->rq_inv_qtys;
					$ids=explode(',',$rq_sku_ids);
					$rq_inv_ids_arr=explode(',',$rq_inv_ids);
					$rq_inv_qtys_arr=explode(',',$rq_inv_qtys);
					if(!empty($ids) && $rq_sku_ids!=''){
							$rand_1=sample_code();

							if($user_type!='vendor' ){
								
								$rq_v_ids=$queryRecordsObj->vendor_id_list;
								$v_ids_arr=explode(',',$rq_v_ids);
							}

							if(!empty($rq_inv_ids_arr)){
								foreach($rq_inv_ids_arr as $key=> $inv_id){

									if(isset($ids[$key]) && isset($rq_inv_qtys_arr[$key])){
										$t1.="<tr>".'<td><a href="'.base_url().'detail/'."{$rand_1}{$inv_id}".'">'.$ids[$key].' </a></td><td>'.$rq_inv_qtys_arr[$key].'</td>';

										if(!empty($ids) && !empty($rq_v_ids)){
											$ve=get_product_vendor_details($v_ids_arr[$key]);
											if(!empty($ve)){
												$t1.="<td><small>".''.$ve->name.'</small></td>';
											}
										}
										$t1.='</tr>';
									}
								}
							}
					}else{
						$t1.="<tr><td colspan='3'>Skus not Selected</td></tr>";
					}
					
					$t1.="</table>";
					
					$row[]= $t1;
					
					$ref_details1='';

					$ref_details1.='<ul class="list-unstyled">';
					$ref_details1.="<li>"."<small><b>Pincode : </b></small>".$queryRecordsObj->rq_pincode."</li>";
					$ref_details1.= '<li><b><small>Location : </small></b>'.$queryRecordsObj->rq_location."</li>";
					$ref_details1.="</ul>";

					$row[]= $ref_details1;

					$row[]=date('d/m/Y H:i:s',strtotime($queryRecordsObj->rq_timestamp));
					
					$data[]=$row;
					$row=array();

			}
			$json_data = array(
							"draw"            => intval( $params['draw'] ),   
							"recordsTotal"    => intval( $totalRecords ),  
							"recordsFiltered" => intval($totalRecords),
							"data"            => $data   // total data array
							);
			echo json_encode($json_data);  // send data as json format
		}else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
		}
	}
	public function become_a_reseller($id=''){
            
		if($this->session->userdata("logged_in")){
			$vendor_id='';
			$user_type=$this->session->userdata("user_type");
			$data["menu_flag"]="become_a_reseller";
			if($id!='' && $user_type!='vendor' ){
				$vendor_id=$id;
				$data["menu_flag"]="become_a_reseller";
			}
			if($user_type=='vendor'){
				$vendor_id=$this->session->userdata("user_id");
				$data["menu_flag"]="become_a_reseller";
			}

			$data["vendor_id"]=$vendor_id;
			
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/become_a_reseller',$data);
		}else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
		}
	}
	public function become_a_reseller_processing(){
	
		if($this->session->userdata("logged_in")){
			$user_type=$this->session->userdata("user_type");
			
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Admin_manage_model->become_a_reseller_processing($params,"get_total_num_recs");

			$queryRecordsResult=$this->Admin_manage_model->become_a_reseller_processing($params,"get_recs");
			$s_no=0;
			foreach($queryRecordsResult as $queryRecordsObj){
					$row=array();
					$row[]=++$s_no;
					$vtxt='';
					$row[]=$queryRecordsObj->becomeareseller_secure_id;
					$vtxt.= '<small><b>Company Name :</b></small> '.ucfirst($queryRecordsObj->becomeareseller_companyname);
					
					$vtxt.= '<br><small><b>Name : </b></small>'.$queryRecordsObj->becomeareseller_name;
					$vtxt.= '<br><small><b>Email : </b></small>'.$queryRecordsObj->becomeareseller_email;
					$vtxt.= '<br><small><b>Type : </b></small>'.$queryRecordsObj->becomeareseller_type;
					
					$row[]=$vtxt;
									
					$ref_details1='';

					$ref_details1.='<ul class="list-unstyled">';
					$ref_details1.="<li>"."<small><b></b></small>".$queryRecordsObj->becomeareseller_city."</li>";
					$ref_details1.="</ul>";

					$row[]= $ref_details1;

					$row[]=date('d/m/Y H:i:s',strtotime($queryRecordsObj->becomeareseller_timestamp));
					
					$data[]=$row;
					$row=array();

			}
			$json_data = array(
							"draw"            => intval( $params['draw'] ),   
							"recordsTotal"    => intval( $totalRecords ),  
							"recordsFiltered" => intval($totalRecords),
							"data"            => $data   // total data array
							);
			echo json_encode($json_data);  // send data as json format
		}else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
		}
	}
}


