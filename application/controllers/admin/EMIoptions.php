<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class EMIoptions extends CI_Controller {
        private $pagesize = 20;
	function __construct(){
		parent::__construct();
		$this->load->database();
        $data["controller"]=$this;
		$this->load->model('admin/Model_EMIoptions');
        $this->load->helper(array('url','form'));
		$this->load->helper('emailsmstemplate');
	}
	/* emioptions starts */
	public function emioptions_list(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
                        $data["menu_flag"]="emioptions_list";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/emioptions/emioptions_list',$data);
		}else{
			redirect(base_url());
		}
	}
	public function add_EMIoptions(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$name_emioptions=addslashes($this->input->post("name_emioptions"));
			$planname_emioptions=addslashes($this->input->post("planname_emioptions"));
			$emiplan_emioptions_details_json=addslashes($this->input->post("emiplan_emioptions_details_json"));
			$emistartvalue_emioptions=addslashes($this->input->post("emistartvalue_emioptions"));
			
			
				$this->load->library('upload');
			$files = $_FILES;
			
			
			//generate a $thread_id
			$date = new DateTime();
			$mt=microtime();
			$salt="thread_id";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$c_filen=md5($str);
			if(!is_dir('assets/data/emioptions_images')){
				mkdir('assets/data/emioptions_images');
			}
			
			
			
			$image_emioptions='';
			$common_image_name=$files['image_emioptions']['name'];
			if($common_image_name!=""){
				$c_ext = pathinfo($common_image_name, PATHINFO_EXTENSION);
				
				
				$_FILES['image_emioptions']['name']=$c_filen.".".$c_ext;
				$_FILES['image_emioptions']['type']= $files['image_emioptions']['type'];
				$_FILES['image_emioptions']['tmp_name']= $files['image_emioptions']['tmp_name'];
				$_FILES['image_emioptions']['error']= $files['image_emioptions']['error'];
				$_FILES['image_emioptions']['size']= $files['image_emioptions']['size'];  
				$this->upload->initialize($this->set_upload_options_config(200,200,"emioptions"));
				
				if (!$this -> upload -> do_upload("image_emioptions")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					//print_r($upload_data);
					$image_emioptions = "assets/data/emioptions_images/". $upload_data['file_name'];
				}
			}
			
			
			
			$flag=$this->Model_EMIoptions->add_EMIoptions($name_emioptions,$planname_emioptions,$emiplan_emioptions_details_json,$emistartvalue_emioptions,$image_emioptions);
			echo $flag;
		}else{
			redirect(base_url());
		}
	}
	
	private function set_upload_options_config($width,$height,$path=''){
		$config = array();
		if($path=="emioptions"){
			$config['upload_path'] = './assets/data/emioptions_images';
		}
		
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '1048576';
		$config['max_width'] = $width;
		$config['max_height'] = $height;
		$config['overwrite']     = FALSE;

		return $config;
	}
	
	public function EMIoptions_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_EMIoptions->EMIoptions_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_EMIoptions->EMIoptions_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->emioptions_id.'" />';
				
				
					$ht='<img src="'.base_url().$queryRecordsObj->image_emioptions_path.'" class="img-responsive" width="100" height="122"><br>';
					$row[]=$ht;
				
				
				
				$row[]=$queryRecordsObj ->name;
				
				$row[]=$queryRecordsObj ->planname;
				$emiplan_details_str="";
				if($queryRecordsObj ->emiplan_details_json!=""){
				$emiplan_details_json_arr=json_decode($queryRecordsObj ->emiplan_details_json);
					if(!empty($emiplan_details_json_arr)){
						foreach($emiplan_details_json_arr as $v){
							$v_arr=explode("|||||",$v);
							$emiplan_details_str.="EMI Plan : ".$v_arr[0]."<br>";
							$emiplan_details_str.="Interest : ".$v_arr[1]."<br>";
							$emiplan_details_str.="Total Cost : ".curr_sym.$v_arr[2]."<br><br>";
						}
					}
				}
				
				$row[]=$emiplan_details_str;

				$row[]=$queryRecordsObj ->emistartvalue;
				
				$row[]=$queryRecordsObj -> timestamp;	
			
				$str='<button id="multiple_delete_button3" class="btn btn-danger btn-xs btn-block" onclick=delete_assigned_inv("'.$queryRecordsObj ->emioptions_id.'")>delete</button>';
				
				//$row[]=$str;
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
		}else{
			$this->logged_out();
		}
	}
	
	public function delete_emioptions(){
		if($this->session->userdata("logged_in")){	
			$selected_list=$this->input->post("selected_list");
			
			
			$flag=$this -> Model_EMIoptions->delete_emioptions($selected_list);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	/* emioptions ends */
}
