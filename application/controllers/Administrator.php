<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Administrator extends CI_Controller {
	function __construct(){
		parent::__construct();
       
		$data["controller"]=$this;
		$this->load->model('admin/Admin_manage_model');
		$this->load->helper('emailsmstemplate');
		
	}
	public function index(){
		if($this->session->userdata("user_type")=="Master Country"){
		    if($this->session->userdata('user_type')=="Master Country"){					   
				redirect(site_url("admin/Orders/onhold_orders"));	
            }else{
                $this->load->view('admin/header');
                $this->load->view('admin/login');
            }
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function admin_vendor(){
		$data["controller"]=$this;
		$data["admin_type"]='vendor';
		if($this->session->userdata("user_type")=="vendor"){
			redirect(site_url("admin/Catalogue/vendor_inventory"));
		}else{
				$this->load->view('admin/header',$data);
				$this->load->view('admin/login');
		}
	}
}


