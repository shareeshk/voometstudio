<?php
	class Account extends CI_controller{
		function __construct(){
			parent::__construct();	
			$this->load->model('Customer_frontend');
			$this->load->model('Franchise_frontend');

			$this->load->model('Customer_account');
			$this->load->model('Front_promotions');			
			$this->load->library('My_PHPMailer');
			$this->load->helper('notifications');
			$this->load->model('Customer_blogs');
			$this->load->model('Model_search');
			$this->load->helper('emailsmstemplate');
			$this->load->helper('general');
			$this->load->helper('s3_fileupload');
		}
	
		public function index(){
			if($this->session->userdata("logged_in")){
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_Spage"]="cur_Spage";
				$data["cur_page"]="p_info";
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
				$this->load->view('header',$data);
				$this->load->view('customer/home');
				$this->load->view('footer');
			}
			else{
				redirect(base_url());
			}
			
		}
		public function wishlist(){
			if($this->session->userdata("logged_in")){
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="mywishlist";
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
				$this->load->view('header',$data);
				$this->load->view('customer/wishlist');
				$this->load->view('footer');
			}
			else{
				redirect(base_url());
			}
			
		}
/*functions for all order page starts*/		
		public function my_order($search_status='',$search_order_id=''){

			if($this->session->userdata("logged_in")){
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="my_order";
				$customer_id=$this->session->userdata("customer_id");
				$order_item_id_arr=array();
				$all_orders_gp=array();
				$all_orders_collection=array();
				$all_orders_collection=$this->Customer_account->get_all_orders($customer_id);
				if(!empty($all_orders_collection)){
					foreach($all_orders_collection as $order_statusarr){
						$order_item_id_arr[]=$order_statusarr["order_item_id"];
					}
				}
				
				$all_distinct_orders=$this->Customer_account->get_all_distinct_orders_id($customer_id);
				$all_distinct_orders=array_unique($all_distinct_orders);

				if($search_order_id!=''){
					$all_distinct_orders=array_filter($all_distinct_orders, function($v) use ($search_order_id) {
						return  $v == $search_order_id;
					});
				}


				
				foreach($all_distinct_orders as $distinct_ordersObj){
					$res_data=$this->Customer_account->all_orders($distinct_ordersObj,$search_status);
					if(!empty($res_data)){
						$all_orders_gp[$distinct_ordersObj][]=$res_data;
					}	
				}		
				
				$data["all_orders"]=$all_orders_gp;
				$data["search_order_id"]=$search_order_id;
				$data["search_status"]=$search_status;
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
				$this->load->view('header',$data);
				$this->load->view('customer/order');
				$this->load->view('footer');
			}
			else{
				redirect(base_url());
			}
		}
		
		public function get_all_order_items($id,$order_id){
			return $this->Customer_account->get_all_order_items($id,$order_id);
		}
		
		public function get_all_order_items_second_cycle($id,$order_id){
			return $this->Customer_account->get_all_order_items_second_cycle($id,$order_id);
		}
		
		
		public function get_order_placed_date($id){
			return $this->Customer_account->get_order_placed_date($id);
		}
		public function get_order_grand_total($id){
			$grandtotal=$this->Customer_account->get_order_grand_total($id);
			$total_order_val=0;
			
			foreach($grandtotal as $total){
				$total_order_val+=$total['grandtotal'];
			}
			return $total_order_val;
		}
		public function get_discount_price($order_id){
			$total_discount_price_arr=$this->Customer_account->get_discount_price($order_id);
			$total_discount_price=0;
			foreach($total_discount_price_arr as $discount_price_arr){
				$total_discount_price+=$discount_price_arr['discount_price'];
			}
			return $total_discount_price;
		}
		
		public function get_product_name($product_id){
			$product_name=$this->Customer_account->get_product_name($product_id);
			return $product_name;
		}
		
		public function get_order_item_status_summary($id,$order_id){
			$order_item_status=$this->Customer_account->get_order_item_status_summary($id);
			$rep_data=$this->get_order_details_admin_acceptance_refund_data_for_replacement($id);
			$ref_data=$this->get_order_details_admin_acceptance_refund_data($id);
			
			/*echo '<pre>';
			//print_r($rep_data);
			print_r($ref_data);
			echo '</pre>';
			*/
                        
			if(!empty($order_item_status)){
                            $order_item_status_arr=array_reverse($order_item_status[0]);
                        }else{
                            $order_item_status_arr=array();
                        }
			$state_as="";
			$flag=0;
			$btn="";
			$order_replacement="0";
			$msg_rep="";
			$order_refund="0";
			$msg_ref="";
			$order_cancel="0";//added
			$dilivered_time_arr=$this->get_delivered_time_of_order_item($order_id,$id);
			if(!empty($dilivered_time_arr)){
				foreach($dilivered_time_arr as $time){
				
					$hour=date("H:i:s", strtotime($time["order_delivered_time"]));
					$date=date("Y:m:d", strtotime($time["order_delivered_date"]));
					$dilivered_time=$date." ".$hour;
				}
				
				$refund_policy=$this->Customer_account->get_refund_policy_order($id,$order_id);
           //print_r($refund_policy);exit;
           
            if(!empty($refund_policy)){
               
                
                foreach($refund_policy as $data){
                    if($data['new_days_for_refund']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_refund'].' day', strtotime($dilivered_time) );
                        if(time() > $dateTimeValid){
                            $order_refund=1;
                        }
                        $msg_ref='<span  data-toggle="popover" data-trigger="hover" data-content="This product has revised Refund period of '.$data["new_days_for_refund"].' day(s).And refunds are done through '.$data["refund_method"].' only"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';

                    }
                    if($data['refund_restrict']=="remove_refund"){
                        $msg_ref='<span  data-toggle="popover" data-trigger="hover" data-content="'.$data["message"].'"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                        $order_refund=1;
                        
                    }
                    
                }
                
            }
            
            $replacement_policy=$this->Customer_account->get_replacement_policy_order($id,$order_id);
           //print_r($replacement_policy);exit;
           
            if(!empty($replacement_policy)){
               
                foreach($replacement_policy as $data){
                    if($data['new_days_for_replacement']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_replacement'].' day', strtotime($dilivered_time) );
                        if(time() > $dateTimeValid){
                            $order_replacement=1;
                        }
                        $msg_rep="<small  style='color:red'>This product has revised Replacement period of ".$data['new_days_for_replacement']." day(s).</small>";

                    }
                    if($data['replacement_restrict']=="remove_replacement"){
                        $msg_rep="<small  style='color:red'>".$data['message']."</small>";
                        $order_replacement=1;
                    }
                    
                }
                
            }   
				
			}
			
			
            
            $cancel_policy=$this->Customer_account->get_cancellation_policy_order($id,$order_id);
			$order_cancel_by_order_status="0";
            if(!empty($cancel_policy)){
                $order_cancel="0";
				$order_cancel_by_order_status="0";
				$order_status_type_cancel="";
                $msg="";
                foreach($cancel_policy as $data){
                    if($data['new_days_for_cancellation']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_cancellation'].' day', strtotime($data['timestamp']) );
                        if(time() > $dateTimeValid){
                            $order_cancel=1;
                        }
                        //<i class="fa fa-question-circle" aria-hidden="true"></i>
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="This product has revised Cancellation period of '.$data["new_days_for_cancellation"].'day(s)"><i class="fa fa-question-circle" aria-hidden="true"></i></span>'; 

                    }
                    if($data['new_hour_limit']!=""){
                        $dateTimeValid = strtotime("+".$data['new_hour_limit']." hours", strtotime($data['timestamp']));
            
                        if(time() > $dateTimeValid){
                            $order_cancel=1;
                        }
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="This product has revised Cancellation period of '.$data["new_hour_limit"].'hour(s)"><i class="fa fa-question-circle" aria-hidden="true"></i></span>'; 
                        
                    }
                    if($data['cancellation_restrict']=="remove_cancellation"){
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="'.$data["message"].'"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                        $order_cancel=1;
                    }
					if($data['order_status_type']!=""){
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="This SKU Cannot be cancelled after order is '.$data['order_status_type'].'"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                        $order_cancel_by_order_status=1;
						$order_status_type_cancel=$data['order_status_type'];
                    }
                    
               }
            } 
			$str='';
			
                foreach($order_item_status_arr as $k => $v){
						
                        if($v=='1'){
							
                            $flag=1;
                            $statuss=explode('_',$k);
                            foreach($statuss as $state){
                                $state_as.=' '.ucfirst($state);
                            }
                            if($k=="order_placed"||$k=="order_confirmed"||$k=="order_packed"){
								$status_arr=explode( '_', $k );
								
                                $str='<div class="f-row capitalize"><strong>'.$status_arr[1].'</strong></div><div class="f-row small-text margin-top">Your item was '.$status_arr[1].' on '.date_format(date_create($order_item_status_arr['timestamp']), "D, jS M'y").'</div>';
								
                                if($order_cancel==1){
                                    $btn=$msg;
                                }
								if($order_cancel_by_order_status==1){
									//echo $order_status_type_cancel."|".$status_arr[1];
									if(strtolower($order_status_type_cancel)=="placed"){
										if(strtolower($status_arr[1])=="placed" || strtolower($status_arr[1])=="confirmed" || strtolower($status_arr[1])=="packed"){
											$order_cancel=1;
											$btn='<a href="#" class="disabled"><button style="cursor:not-allowed"  class="btn btn-default btn-sm f-row margin-top"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button></a>';
										}
									}
									if(strtolower($order_status_type_cancel)=="confirmed"){
										if(strtolower($status_arr[1])=="confirmed" || strtolower($status_arr[1])=="packed"){
											$order_cancel=1;
											$btn='<a href="#" class="disabled"><button style="cursor:not-allowed"  class="btn btn-default btn-sm f-row margin-top"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button></a>';
										}
									}
									if(strtolower($order_status_type_cancel)=="packed"){
										if(strtolower($status_arr[1])=="packed"){
											$order_cancel=1;
											$btn='<a href="#" class="disabled"><button style="cursor:not-allowed"  class="btn btn-default btn-sm f-row margin-top"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button></a>';
										}
									}
									
								}
                                if($order_cancel==0){
									$btn='<a href="'.base_url().'Account/cancel_order/'.$id.'"><button class="btn btn-default btn-sm f-row margin-top"><i class="fa fa-times" aria-hidden="true"></i> Cancel </button></a>';
                                }
         
                            }
                            if($k=="order_shipped"){
                                $str='<div class="f-row capitalize"><strong>Shipped</strong></div><div class="f-row small-text margin-top">Your item was Shipped on '.date_format(date_create($order_item_status_arr['timestamp']), "D, jS M'y").'</div>';
								
								$btn='<a href="#" class="disabled"><button style="cursor:not-allowed"  class="btn btn-default btn-sm f-row margin-top"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button></a>';
                            }
	
							if($k=="order_replacement_pickup" || $k=="order_return_pickup" || $k=="return_order_id"){
								
								$str='<div class="f-row capitalize"><strong>Delivered</strong></div><div class="f-row small-text margin-top">Your item was delivered on '.date_format(date_create($order_item_status_arr['timestamp']), "D, jS M'y").'</div>';
								
								$btn='<a href="#" class="disabled"><button style="cursor:not-allowed"  class="btn btn-default btn-sm f-row margin-top"><i class="fa fa-undo" aria-hidden="true"></i> Return</button></a>';
								
								if(!empty($rep_data) && !empty($ref_data)){
									
									$repl_status=$rep_data['status'];
									$ref_status=$ref_data['status'];
									if($repl_status=="replaced" || $repl_status=="refund" || $repl_status=="refunded" || $ref_status="refund" || $ref_status="refunded"){
										
										$str='<div class="f-row capitalize"><strong>returned</strong></div><div class="f-row small-text margin-top">Your item was Returned </div>';
										
										$btn='<a href="'.base_url().'Account/order_details/'.$order_id.'"><button class="btn btn-default btn-sm f-row margin-top">View Return/Refund status</button></a>';
									}
								}elseif(!empty($rep_data) && empty($ref_data)){
									
									$repl_status=$rep_data['status'];
									
									if($repl_status=="replaced" || $repl_status=="refund" || $repl_status=="refunded"){
										
										$str='<div class="f-row capitalize"><strong>Returned</strong></div><div class="f-row small-text margin-top">Your item was returned </div>';
										
										$btn='<a href="'.base_url().'Account/order_details/'.$order_id.'"><button class="btn btn-default btn-sm f-row margin-top">View Return/Refund status</button></a>';
									}
									
								}elseif(empty($rep_data) && !empty($ref_data)){
									
									$ref_status=$ref_data['status'];
									
									if($ref_status="refund" || $ref_status="refunded"){
										
										$str='<div class="f-row capitalize"><strong>Returned</strong></div><div class="f-row small-text margin-top">Your item was returned </div>';
										
										$btn='<a href="'.base_url().'Account/order_details/'.$order_id.'"><button class="btn btn-default btn-sm f-row margin-top">View Return/Refund status</button></a>';
									}
									
								}else{
									//delivered status // common// defined on top
								}
								 
							}
                            if($k=="order_delivered" || $k=="order_delayed"){
								
								 $str='<div class="f-row capitalize"><strong>Delivered</strong></div><div class="f-row small-text margin-top">Your item was delivered on '.date_format(date_create($order_item_status_arr['timestamp']), "D, jS M'y").'</div>';
								 
								if($order_replacement==1 && $order_refund==1){
									$btn='<div class="f-row"><strong> Return Not Allowed </strong></div>';
								}else{
									
									$req_count_arr=$this->get_request_details_of_order_item($id);
									$replace_req_count=$req_count_arr["replace_req_count"];
									$refund_req_count=$req_count_arr["refund_req_count"];
									
									if($replace_req_count>0 || $refund_req_count>0){
										$btn='<a href="#" class="disabled"><button style="cursor:not-allowed"  class="btn btn-default btn-sm f-row margin-top"><i class="fa fa-undo" aria-hidden="true"></i> Return</button></a>';
									}else{
										
										$btn.='<a href="'.base_url().'Account/return_order/'.$id.'"><button class="btn btn-default btn-sm f-row margin-top"><i class="fa fa-undo" aria-hidden="true"></i> Return</button></a>';
									}

								}
                            }

                            if($k=="order_cancelled"){
                                $str='<div class="f-row capitalize"><strong>Cancelled</strong></div><div class="f-row small-text margin-top">Your item was Cancelled on '.date_format(date_create($order_item_status_arr['timestamp']), "D, jS M'y").'</div>';
                            }
                           // return $state_as." @ ".date_format(date_create($order_item_status_arr['timestamp']), "D, jS M'y").'<br><br>'.$btn;
                           
							return '<div class="col-sm-3 products-block">'.$str.'</div><div class="col-sm-2 products-block">'.$btn.'</div>';
                            break;
                        }
                    }
                    if($flag=='0'){
								
                                if($order_cancel==1){
                                    $btn=$msg;
                                }
                                if($order_cancel==0){
									$btn='<a href="'.base_url().'Account/cancel_order/'.$id.'"><button class="btn btn-default btn-sm f-row margin-top-bottom-mobile"><i class="fa fa-times" aria-hidden="true"></i> Cancel </button></a>';
                                } 
                            
                        $state_as='<div class="f-row"><strong>Order yet to be processed</strong></div>';
                        //return $state_as." @ ".date_format(date_create($order_item_status_arr['timestamp']), "D, jS M'y").'<br><br>'.$btn;
						
						 return '<div class="col-sm-3 products-block">'.$state_as.'</div><div class="col-sm-2 products-block">'.$btn.'</div>';
                    } 
			
		}
/*functions for all order page ends*/	

/*functions for order details page start*/
	
		public function order_details($id){
			if($this->session->userdata("logged_in")){
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="my_order_details";				
				$data["order_id"]=$id;			
				
				//echo $id;

				$data['all_order_items']=$this->Customer_account->get_all_order_items_in_order_id($id);
				
				//print_r($data['all_order_items']);

				$order_summary_mail_data_arr=$this->Customer_account->get_order_summary_mail_data($id);
				$data["order_summary_mail_data_arr"]=$order_summary_mail_data_arr;

				if(empty($data['all_order_items'])){
					//order_id not exists
					redirect(base_url().'Account/my_order');		
				}else{
				
					$data['invoice_offers']=$this->Customer_account->get_all_order_invoice_offers($id);
					$customer_id=$this->session->userdata("customer_id");
					
					$bank_account_number=$this->Customer_account->get_customer_all_bank($customer_id);
					$bank_detail=array();
					if(!empty($bank_account_number)){
						foreach($bank_account_number as $account_number){
							$bank_detail[]=$this->get_bank_detail($account_number['account_number']);
						}
					}
					$data['bank_detail']=$bank_detail;
	
					$ft_img=$this->Customer_frontend->foot_image_index();
					$data['ft_img']='';
					if(!empty($ft_img)){
						if(isset($ft_img->background_image)){
							$data['ft_img']=$ft_img->background_image;
						}
					}
					$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
					$this->load->view('header',$data);
					$this->load->view('customer/order_details');
					$this->load->view('footer');	
				}
			
			}else{
				redirect(base_url());
			}
		}
		public function order_details_for_mail($id){
			if($this->session->userdata("logged_in")){
				redirect(base_url()."Account/order_details/".$id);
			
			}else{
				redirect(base_url()."login/".$id);
				exit;
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="my_order_details";				
				$data["order_id"]=$id;				
				$data['all_order_items']=$this->Customer_account->get_all_order_items_in_order_id($id);
				
				if(empty($data['all_order_items'])){
					//order_id not exists
					redirect(base_url().'Account/my_order');		
				}else{
				
					$data['invoice_offers']=$this->Customer_account->get_all_order_invoice_offers($id);
					$customer_id=$this->session->userdata("customer_id");
					
					$bank_account_number=$this->Customer_account->get_customer_all_bank($customer_id);
					$bank_detail=array();
					if(!empty($bank_account_number)){
						foreach($bank_account_number as $account_number){
							$bank_detail[]=$this->get_bank_detail($account_number['account_number']);
						}
					}
					$data['bank_detail']=$bank_detail;
	
					$ft_img=$this->Customer_frontend->foot_image_index();
					$data['ft_img']='';
					if(!empty($ft_img)){
						if(isset($ft_img->background_image)){
							$data['ft_img']=$ft_img->background_image;
						}
					}
					$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
					$this->load->view('header',$data);
					$this->load->view('customer/order_details_for_mail');
					$this->load->view('footer');	
				}
			}
		}
		public function order_details_old($id){
			if($this->session->userdata("logged_in")){
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="my_order_details";				
				$data["order_id"]=$id;				
				$data['all_order_items']=$this->Customer_account->get_all_order_items_in_order_id($id);
				
				if(empty($data['all_order_items'])){
					//order_id not exists
					redirect(base_url().'Account/my_order');		
				}else{
				
					$data['invoice_offers']=$this->Customer_account->get_all_order_invoice_offers($id);
					$customer_id=$this->session->userdata("customer_id");
					
					$bank_account_number=$this->Customer_account->get_customer_all_bank($customer_id);
					$bank_detail=array();
					if(!empty($bank_account_number)){
						foreach($bank_account_number as $account_number){
							$bank_detail[]=$this->get_bank_detail($account_number['account_number']);
						}
					}
					$data['bank_detail']=$bank_detail;

					$data["wallet_exists"]=$this->Customer_account->get_wallet_exists_for_customer($customer_id);
					$ft_img=$this->Customer_frontend->foot_image_index();
					$data['ft_img']='';
					if(!empty($ft_img)){
						if(isset($ft_img->background_image)){
							$data['ft_img']=$ft_img->background_image;
						}
					}
					$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
					$this->load->view('header',$data);
					$this->load->view('customer/order_details_backup');
					$this->load->view('footer');	
				}
			
			}else{
				redirect(base_url());
			}
		}
		public function get_shipping_address_order_item_id($id){
			$shipping_address_id=$this->Customer_account->get_shipping_address_order_item_id($id);
			return $this->Customer_account->get_shipping_address($shipping_address_id);
		}
		public function get_total_num_of_order_items_in_order_id($id){
			$count=$this->Customer_account->get_total_num_of_order_items_in_order_id($id);
			if($count > 1){
				return $count.' Products';
			}
			else{
				return $count.' Product';
			}
		}
		public function get_order_payment_method($id){
			$payment_method=$this->Customer_account->get_order_payment_method($id);
			if($payment_method=="cod"){
				return "(to be paid through ".$payment_method." )";
			}
			if($payment_method!="cod"){
				return "(paid through ".$payment_method." )";
			}
		}
		public function get_order_details_order_status($id,$order_id){
			// $id == order item id
		   
		    $order_item_status=$this->Customer_account->get_order_item_status_summary($id);
                    if(!empty($order_item_status)){
                        $order_item_status_arr=array_reverse($order_item_status[0]);
                    }else{
                        $order_item_status_arr=array();
                    }
            $dilivered_time_arr=$this->get_delivered_time_of_order_item($order_id,$id);
			
			$order_replacement=0;
		    $msg_rep="";
			$order_refund=0;
			$msg_ref="";
			$order_cancel=0;
			$msg="";
			$order_cancel_by_order_status=0;
			
			if(!empty($dilivered_time_arr)){
			foreach($dilivered_time_arr as $time){
				$hour=date("H:i:s", strtotime($time["order_delivered_time"]));
				$date=date("Y:m:d", strtotime($time["order_delivered_date"]));
				$dilivered_time=$date." ".$hour;
			}
			$refund_policy=$this->Customer_account->get_refund_policy_order($id,$order_id);
            if(!empty($refund_policy)){
                foreach($refund_policy as $data){
                    if($data['new_days_for_refund']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_refund'].' day', strtotime($dilivered_time) );
                        if(time() > $dateTimeValid){
                            $order_refund=1;
                        }
                        $msg_ref='<span  data-toggle="popover" data-trigger="hover" data-content="This product has revised Refund period of '.$data["new_days_for_refund"].' day(s).And refunds are done through '.$data["refund_method"].' only"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                    }
                    if($data['refund_restrict']=="remove_refund"){
                        $msg_ref='<span  data-toggle="popover" data-trigger="hover" data-content="'.$data["message"].'"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                        $order_refund=1;
                    }
                }
            }
            
            $replacement_policy=$this->Customer_account->get_replacement_policy_order($id,$order_id);
           //print_r($replacement_policy);exit;
           
            if(!empty($replacement_policy)){
                
                foreach($replacement_policy as $data){
                    if($data['new_days_for_replacement']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_replacement'].' day', strtotime($dilivered_time) );
                        if(time() > $dateTimeValid){
                            $order_replacement=1;
                        }
                        $msg_rep="<small  style='color:red'>This product has revised Replacement period of ".$data['new_days_for_replacement']." day(s).</small>";

                    }
                    if($data['replacement_restrict']=="remove_replacement"){
                        $msg_rep="<small  style='color:red'>".$data['message']."</small>";
                        $order_replacement=1;
                    }
                    
                }
                
            }   
			
			}
            $state_as="";
            $flag=0;
            $btn="";
		    $cancel_policy=$this->Customer_account->get_cancellation_policy_order($id,$order_id);
            if(!empty($cancel_policy)){
                $order_cancel="0";
                $msg="";
                foreach($cancel_policy as $data){
                    if($data['new_days_for_cancellation']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_cancellation'].' day', strtotime($data['timestamp']) );
                        if(time() > $dateTimeValid){
                            $order_cancel=1;
                        }
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="This product has revised Cancellation period of '.$data["new_days_for_cancellation"].'day(s)"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';

                    }
                    if($data['new_hour_limit']!=""){
                        $dateTimeValid = strtotime("+".$data['new_hour_limit']." hours", strtotime($data['timestamp']));
						   
						   //echo  time()." Now <br>"  ;                   
						   //echo $dateTimeValid;exit;
						   
                        if(time() > $dateTimeValid){
                            $order_cancel=1;
                        }
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="This product has revised Cancellation period of '.$data["new_hour_limit"].'hours(s)"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                    }
                    if($data['cancellation_restrict']=="remove_cancellation"){
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="'.$data["message"].'"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                        $order_cancel=1;
                    }
					
					if($data['order_status_type']!=""){
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="This SKU Cannot be cancelled after order is '.$data['order_status_type'].'"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                        $order_cancel_by_order_status=1;
						$order_status_type_cancel=$data['order_status_type'];
                    }
                    
                }
                
            }

			foreach($order_item_status_arr as $k => $v){
				$addbtn='';
				if($v=='1'){
					$btn='<div class="btn-group btn-group-sm small" role="group" aria-label="">';
					$flag=1;
					$status_arr=explode( '_', $k );
					if($k=="order_placed"||$k=="order_confirmed"||$k=="order_packed"){
						if($order_cancel==1){
							$addbtn=$msg;
						}
						

						if($order_cancel_by_order_status==1){
							//echo $order_status_type_cancel."|".$status_arr[1];
							if(strtolower($order_status_type_cancel)=="placed"){
								if(strtolower($status_arr[1])=="placed" || strtolower($status_arr[1])=="confirmed" || strtolower($status_arr[1])=="packed"){
									$order_cancel=1;
									$btn='<a href="#" class="disabled"><button style="cursor:not-allowed"  class="btn btn-default text-uppercase"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button></a>';
								}
							}
							if(strtolower($order_status_type_cancel)=="confirmed"){
								if(strtolower($status_arr[1])=="confirmed" || strtolower($status_arr[1])=="packed"){
									$order_cancel=1;
									$btn='<a href="#" class="disabled"><button style="cursor:not-allowed"  class="btn btn-default text-uppercase"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button></a>';
								}
							}
							if(strtolower($order_status_type_cancel)=="packed"){
								if(strtolower($status_arr[1])=="packed"){
									$order_cancel=1;
									$btn='<a href="#" class="disabled"><button style="cursor:not-allowed"  class="btn btn-default text-uppercase"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button></a>';
								}
							}
						}
						if($order_cancel==0){
						$addbtn='<button class="btn btn-default text-uppercase" onclick="location.href=\''.base_url().'Account/cancel_order/'.$id.'\'">Cancel</button>';
						}   
					}
					if($k=="order_shipped"){
						$addbtn='<button class="btn btn-default text-uppercase" disabled>Cancel</button>';
					}
					if($k=="order_delivered"){
						
						if($order_replacement==1 && $order_refund==1){
							$addbtn="<button class='btn btn-default text-uppercase' disabled>Return</button><button class='btn btn-default text-uppercase' onclick='review_service(".$id.")'>Review Product</button>";;
						}else{
							
							$req_count_arr=$this->get_request_details_of_order_item($id);
							$replace_req_count=$req_count_arr["replace_req_count"];
							$refund_req_count=$req_count_arr["refund_req_count"];
							
							if($replace_req_count>0 || $refund_req_count>0){
								$addbtn='<button class="btn btn-default text-uppercase" disabled>Return</button>';
							}else{
								$addbtn='<button class="btn btn-default text-uppercase" onclick="location.href=\''.base_url().'Account/return_order/'.$id.'\'">Return</button>';
							}
							$addbtn.='<button class="btn btn-default text-uppercase" onclick="review_service('.$id.')">Review Product</button>';  
							
						}
					}
					
					
					if($k=="order_cancelled"){
						$addbtn="";
					}
					
					$orders_status_data=$this->get_orders_status_data($id);
					if($orders_status_data["type_of_order"]=="replaced"){
						$addbtn="";
					}
					$closebtn='</div>';
					
					$finalbtn=$btn.$addbtn.$closebtn;
					
					return $finalbtn;
					break;
					
				}
						
			}
			
			if($flag==0){
				if($order_cancel==1){
					 $btn=$msg;
				}
				if($order_cancel==0){
					$btn='<a href="'.base_url().'Account/cancel_order/'.$id.'"><button class="btn btn-default btn-sm margin-top"><i class="fa fa-times" aria-hidden="true"></i> Cancel </button></a>';
				} 
				return $btn;
			}   
		}
		public function get_order_details_order_status_for_mail($id,$order_id){
			// $id == order item id
		   
		    $order_item_status=$this->Customer_account->get_order_item_status_summary($id);
            $order_item_status_arr=array_reverse($order_item_status[0]);
            $dilivered_time_arr=$this->get_delivered_time_of_order_item($order_id,$id);
			
			$order_replacement=0;
		    $msg_rep="";
			$order_refund=0;
			$msg_ref="";
			$order_cancel=0;
			$msg="";
			
			if(!empty($dilivered_time_arr)){
			foreach($dilivered_time_arr as $time){
				$hour=date("H:i:s", strtotime($time["order_delivered_time"]));
				$date=date("Y:m:d", strtotime($time["order_delivered_date"]));
				$dilivered_time=$date." ".$hour;
			}
			$refund_policy=$this->Customer_account->get_refund_policy_order($id,$order_id);
            if(!empty($refund_policy)){
                foreach($refund_policy as $data){
                    if($data['new_days_for_refund']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_refund'].' day', strtotime($dilivered_time) );
                        if(time() > $dateTimeValid){
                            $order_refund=1;
                        }
                        $msg_ref='<span  data-toggle="popover" data-trigger="hover" data-content="This product has revised Refund period of '.$data["new_days_for_refund"].' day(s).And refunds are done through '.$data["refund_method"].' only"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                    }
                    if($data['refund_restrict']=="remove_refund"){
                        $msg_ref='<span  data-toggle="popover" data-trigger="hover" data-content="'.$data["message"].'"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                        $order_refund=1;
                    }
                }
            }
            
            $replacement_policy=$this->Customer_account->get_replacement_policy_order($id,$order_id);
           //print_r($replacement_policy);exit;
           
            if(!empty($replacement_policy)){
                
                foreach($replacement_policy as $data){
                    if($data['new_days_for_replacement']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_replacement'].' day', strtotime($dilivered_time) );
                        if(time() > $dateTimeValid){
                            $order_replacement=1;
                        }
                        $msg_rep="<small  style='color:red'>This product has revised Replacement period of ".$data['new_days_for_replacement']." day(s).</small>";

                    }
                    if($data['replacement_restrict']=="remove_replacement"){
                        $msg_rep="<small  style='color:red'>".$data['message']."</small>";
                        $order_replacement=1;
                    }
                    
                }
                
            }   
			
			}
            $state_as="";
            $flag=0;
            $btn="";
		    $cancel_policy=$this->Customer_account->get_cancellation_policy_order($id,$order_id);
            if(!empty($cancel_policy)){
                $order_cancel="0";
                $msg="";
                foreach($cancel_policy as $data){
                    if($data['new_days_for_cancellation']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_cancellation'].' day', strtotime($data['timestamp']) );
                        if(time() > $dateTimeValid){
                            $order_cancel=1;
                        }
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="This product has revised Cancellation period of '.$data["new_days_for_cancellation"].'day(s)"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';

                    }
                    if($data['new_hour_limit']!=""){
                        $dateTimeValid = strtotime("+".$data['new_hour_limit']." hours", strtotime($data['timestamp']));
						   
						   //echo  time()." Now <br>"  ;                   
						   //echo $dateTimeValid;exit;
						   
                        if(time() > $dateTimeValid){
                            $order_cancel=1;
                        }
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="This product has revised Cancellation period of '.$data["new_hour_limit"].'hours(s)"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                    }
                    if($data['cancellation_restrict']=="remove_cancellation"){
                        $msg='<del> Cancellation </del><span  data-toggle="popover" data-trigger="hover" data-content="'.$data["message"].'"><i class="fa fa-question-circle" aria-hidden="true"></i></span>';
                        $order_cancel=1;
                    }
                    
                }
                
            }

			foreach($order_item_status_arr as $k => $v){
				$addbtn='';
				if($v=='1'){
					$btn='<div class="btn-group btn-group-sm small" role="group" aria-label="">';
					$flag=1;
					if($k=="order_placed"||$k=="order_confirmed"||$k=="order_packed"){
						if($order_cancel==1){
							$addbtn=$msg;
						}
					
						if($order_cancel==0){
						$addbtn='<button class="btn btn-default text-uppercase" disabled onclick="location.href=\''.base_url().'Account/cancel_order/'.$id.'\'">Cancel</button>';
						}   
					}
					if($k=="order_shipped"){
						$addbtn='<button class="btn btn-default text-uppercase" disabled>Cancel</button>';
					}
					if($k=="order_delivered"){
						
						if($order_replacement==1 && $order_refund==1){
							$addbtn="<button class='btn btn-default text-uppercase' disabled>Return</button><button class='btn btn-default text-uppercase' onclick='review_service(".$id.")'>Review Product</button>";;
						}else{
							
							$req_count_arr=$this->get_request_details_of_order_item($id);
							$replace_req_count=$req_count_arr["replace_req_count"];
							$refund_req_count=$req_count_arr["refund_req_count"];
							
							if($replace_req_count>0 || $refund_req_count>0){
								$addbtn='<button class="btn btn-default text-uppercase" disabled>Return</button>';
							}else{
								$addbtn='<button class="btn btn-default text-uppercase" disabled onclick="location.href=\''.base_url().'Account/return_order/'.$id.'\'">Return</button>';
							}
							$addbtn.='<button class="btn btn-default text-uppercase" disabled onclick="review_service('.$id.')">Review Product</button>';  
							
						}
					}
					
					
					if($k=="order_cancelled"){
						$addbtn="";
					}
					
					$orders_status_data=$this->get_orders_status_data($id);
					if($orders_status_data["type_of_order"]=="replaced"){
						$addbtn="";
					}
					$closebtn='</div>';
					
					$finalbtn=$btn.$addbtn.$closebtn;
					
					return $finalbtn;
					break;
					
				}
						
			}
			
			if($flag==0){
				if($order_cancel==1){
					 $btn=$msg;
				}
				if($order_cancel==0){
					$btn='<a href="'.base_url().'Account/cancel_order/'.$id.'"><button class="btn btn-default btn-sm margin-top" disabled><i class="fa fa-times" aria-hidden="true"></i> Cancel </button></a>';
				} 
				return $btn;
			}   
		}
		
		public function get_request_details_of_order_item($id){
			//get_request_details_of_order_item both replacement and refund
			$req_count_arr=$this->Customer_account->get_request_details_of_order_item($id);
			return $req_count_arr;
			
		}
		public function get_order_details_order_status_achieved($id){
			$order_item_status=$this->Customer_account->get_order_item_status_summary($id);
			
                        if(!empty($order_item_status)){
                            $order_item_status_arr=array_reverse($order_item_status[0]);
                        }else{
                            $order_item_status_arr=array();
                        }
			
			$state_as="";
			$flag=0;
			$table="";
			
			foreach($order_item_status_arr as $k => $v){
				$date=$order_item_status_arr["order_delivered_date"];
				$time=$order_item_status_arr["order_delivered_time"];
				if($v=='1'){
					
					$flag=1;
					$statuss=explode('_',$k);
					foreach($statuss as $state){
						$state_as.=' '.ucfirst($state);
					}
					
					if($k=="order_placed"||$k=="order_confirmed"||$k=="order_packed"){
						$table='<ul class="list-unstyled">
									<li>Delivery expected by <br>'.date_format(date_create($this->get_delivery_date($id)), "D, jS F y").'</li>
								</ul>';
					}
					if($k=="order_delivered"){
						$table='<ul class="list-unstyled">
									<li>Delivered on  <br>'.date_format(date_create($date), "D, jS M y").' '.$time.'</li>
								</ul>';
					}
					if($k=="order_cancelled"){
						$table='<ul class="list-unstyled">
						
									<li><del>Delivery expected by  <br>'.date_format(date_create($this->get_delivery_date($id)), "D, jS F y").'</del></li>
								</ul>';
					}
				if($k=="order_shipped"){
						$table='<ul class="list-unstyled">
									<li><del>Delivery expected by  <br>'.date_format(date_create($this->get_delivery_date($id)), "D, jS F y").'</del></li>
								</ul>';
					}
				if($k=="order_replacement_pickup"){
					$table='<ul class="list-unstyled">
									<li><del>Delivery expected by  <br>'.date_format(date_create($this->get_delivery_date($id)), "D, jS F y").'</del></li>
								</ul>';
				}
				if($k=="order_return_pickup"){
					$table='<ul class="list-unstyled">
									<li><del>Delivery expected by  <br>'.date_format(date_create($this->get_delivery_date($id)), "D, jS F y").'</del></li>
								</ul>';
				}

					return $table;
					break;
				}

			}

if($flag==0){
	$state_as="yet to be placed";
	$table='<ul class="list-unstyled">
									<li>Delivery expected by <br>'.date_format(date_create($this->get_delivery_date($id)), "D, jS F y").'</li>
								</ul>';
	
								return $table;
}
		}
		public function get_expected_order_item_expected_delivery($id){
			return $this->Customer_account->get_expected_order_item_expected_delivery($id);
			
		}
		public function get_delivery_date($id){
			$obj=$this->Customer_account->get_delivery_date($id);
			$date='';
			if(!empty($obj)){
				if($obj->exact_delivery_date!=''){
					$date=$obj->exact_delivery_date;
					$date = str_replace('/', '-', $date);
					$date=date('Y-m-d', strtotime($date));
				}else{
					$date=$obj->expected_delivery_date;
				}	
			}
			return $date;
		}
		
		public function get_order_details_in_steps_status($id){
			$order_item_status=$this->Customer_account->get_order_item_status_summary($id);
			$search='';
			
			foreach($order_item_status as $status){
				
				$stages_rev_arr=array("order_delivered","order_shipped","order_packed","order_confirmed","order_placed");
				$flag_order_cancelled="no";
				foreach($stages_rev_arr as $stagest){
					if($status[$stagest]==1 && $status["order_cancelled"]==1){
						$order_cancelled_at=$stagest;
						$flag_order_cancelled="yes";
						break;
					}
				}
				$stages_arr=array("order_placed"=>"Your Order has been Placed","order_confirmed"=>"Your Order has been Confirmed","order_packed"=>"Your item has been Packed",
				"order_shipped"=>"Your item has been Shipped","order_delivered"=>"Your item has been Delivered");
				if($flag_order_cancelled=="no"){
						foreach($stages_arr as $stages_db_key => $stages_db_val){
							if( $stages_db_key=="order_placed"){
								//$search.='<span class="col-md-1 order_status_line1"></span>';
							}
							$status_time=date_format(date_create($status[$stages_db_key."_timestamp"]),"D, jS F y h:i a");
							if($status[$stages_db_key]==1){
	
								$search.='<div class="stepwizard-step">
									<button class="btn btn-success btn-circle" data-trigger="hover click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" id="'.$stages_db_key."_".$id.'" content_id="#content_'.$stages_db_key."_".$id.'" onclick="toggle_content_order_status(this,'.$id.')">
							
							<i class="fa fa-check" aria-hidden="true"></i> </button></div>
							<div style="display:none;" id="content_'.$stages_db_key."_".$id.'">
							<ul class="list-unstyled">
							<li class="text-success big">'.$stages_db_val.'</li>
							<li class="text-success small">'.$status_time.'</li>
							</ul>
							</div>';
								
							}
							if($status[$stages_db_key]==0){
								
								$search.='
									<div class="stepwizard-step"><button class="btn btn-default btn-circle" data-trigger="hover click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" id="'.$stages_db_key."_".$id.'" disabled="disabled">
							
							<i class="fa fa-pause" aria-hidden="true"></i> </button></div>
							<div style="display:none;" id="content_'.$stages_db_key."_".$id.'">
							<ul class="list-unstyled">
							<li class="text-muted big">'.$stages_db_val.'</li>
							<li class="text-muted small">Status awaiting for an update</li>
							</ul>
							</div>';
							}
						}
					
				}
///////////////////////////////////////////////////////////////////////////////////////////
				if($flag_order_cancelled=="yes"){
					foreach($stages_arr as $stages_db_key => $stages_db_val){
							if($stages_db_key=="order_placed" || $stages_db_key=="order_shipped"){
								//$search.='<span class="col-md-1 order_status_line1"></span>';
							}
							
					
					$status_time=date_format(date_create($status[$stages_db_key."_timestamp"]),"D, jS M'y");
					if($status[$stages_db_key]==1 && $order_cancelled_at==$stages_db_key){
						$status_time_cancelled=date_format(date_create($status["order_cancelled_timestamp"]),"D, jS F y h:i a");
							
								
							$search.='
									<div class="stepwizard-step"><button class="btn btn-danger btn-circle" href="#" data-trigger="hover click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" id="'.$stages_db_key."_".$id.'" content_id="#content_'.$stages_db_key."_".$id.'" onclick="toggle_content_order_status(this,'.$id.')"> 
							
							<i class="fa fa-times" aria-hidden="true"></i> </button></div>
							<div style="display:none;" id="content_'.$stages_db_key."_".$id.'">
							<ul class="list-unstyled">
							<li class="text-danger big">Your item has been cancelled</li>
							<li class="text-danger small">'.$status_time_cancelled.'</li>
							</ul>
							</div>';
						}
						if($status[$stages_db_key]==1 && $order_cancelled_at!=$stages_db_key){
							
									
							$search.='
									<div class="stepwizard-step"><button class="btn btn-success btn-circle" data-trigger="hover click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="'.$stages_db_key."_".$id.'" content_id="#content_'.$stages_db_key."_".$id.'" onclick="toggle_content_order_status(this,'.$id.')">
							
							<i class="fa fa-check" aria-hidden="true"></i> </button></div>
							<div style="display:none;" id="content_'.$stages_db_key."_".$id.'">
							<ul class="list-unstyled">
							<li class="text-success big">'.$stages_db_val.'</li>
							<li class="text-success small">'.$status_time.'</li>
							</ul>
							</div>';
						}
						if($status[$stages_db_key]==0){
							
								
							$search.='
									<div class="stepwizard-step"><button class="btn btn-default btn-circle" data-trigger="hover click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="'.$stages_db_key."_".$id.'" disabled="disabled">
							
							<i class="fa fa-pause" aria-hidden="true"></i> </button></div>
							<div style="display:none;" id="content_'.$stages_db_key."_".$id.'">
							<ul class="list-unstyled">
							<li class="text-muted big">'.$stages_db_val.'</li>
							<li class="text-muted small">Status awaiting for an update</li>
							</ul>
							</div>';
						}

/////////////////////////
						
				} 
							}
							}
			
				$search.='';
			return $search;
		}
	public function get_order_details_return_data($id){
		if($this->session->userdata("logged_in")){
			$order_item_id=$id;
			$return_data=$this->Customer_account->get_order_details_return_data($order_item_id);
			return $return_data;
		}
		else{
			redirect(base_url());
		}
	}
	public function get_order_details_return_data_for_mail($id){
			$order_item_id=$id;
			$return_data=$this->Customer_account->get_order_details_return_data($order_item_id);
			return $return_data;
	}
	
	public function get_data_from_replacement_desired_return_order_id($id){
		if($this->session->userdata("logged_in")){
			$return_order_id=$id;
			$get_data_from_replacement_desired_return_order_id=$this->Customer_account->get_data_from_replacement_desired_return_order_id($return_order_id);
			return $get_data_from_replacement_desired_return_order_id;
		}
		else{
			redirect(base_url());
		}
	}
	public function get_data_from_replacement_desired_return_order_id_for_mail($id){
			$return_order_id=$id;
			$get_data_from_replacement_desired_return_order_id=$this->Customer_account->get_data_from_replacement_desired_return_order_id($return_order_id);
			return $get_data_from_replacement_desired_return_order_id;

	}
public function get_data_from_returns_desired_return_order_id($id){
		if($this->session->userdata("logged_in")){
			$return_order_id=$id;
			$get_data_from_returns_desired_return_order_id=$this->Customer_account->get_data_from_returns_desired_return_order_id($return_order_id);
			return $get_data_from_returns_desired_return_order_id;
		}
		else{
			redirect(base_url());
		}
	}
	public function get_data_from_returns_desired_return_order_id_for_mail($id){
			$return_order_id=$id;
			$get_data_from_returns_desired_return_order_id=$this->Customer_account->get_data_from_returns_desired_return_order_id($return_order_id);
			return $get_data_from_returns_desired_return_order_id;

	}
	
	public function get_data_from_return_stock_notification_return_order_id($id){
		if($this->session->userdata("logged_in")){
			$return_order_id=$id;
			$get_data_from_return_stock_notification_return_order_id=$this->Customer_account->get_data_from_return_stock_notification_return_order_id($return_order_id);
			return $get_data_from_return_stock_notification_return_order_id;
		}
		else{
			redirect(base_url());
		}
	}
	public function get_data_from_return_stock_notification_return_order_id_for_mail($id){
			$return_order_id=$id;
			$get_data_from_return_stock_notification_return_order_id=$this->Customer_account->get_data_from_return_stock_notification_return_order_id($return_order_id);
			return $get_data_from_return_stock_notification_return_order_id;
	}
public function get_replacement_item_data($replacement_with_inventory_id){
	return $this->Customer_account->get_replacement_item_data($replacement_with_inventory_id);
}

public function get_reason_for_return_order_id($return_order_id){
	return $this->Customer_account->get_reason_for_return_order_id($return_order_id);
}

public function get_primary_return_reason($reason_return_id){
	return $this->Customer_account->get_primary_return_reason($reason_return_id);
}

public function get_primary_return_reason_admin($reason_return_id){
	return $this->Customer_account->get_primary_return_reason_admin($reason_return_id);
}

public function get_sub_return_reason($sub_issue_id){
	return $this->Customer_account->get_sub_return_reason($sub_issue_id);
}

public function get_final_amount_of_returns_transaction($return_order_id){
	$desired_quntity_for_replacement=null;
	$price_of_desired_item=null;
	$original_item_cost=null;
	$desired_quntity_for_refund=null;
	$final_amount_after_ret_ref=null;
	$final_amount_after_rep=null;
	$final_amount=null;
	
	$money_from_replacement= $this->Customer_account->get_data_from_replacement_desired_return_order_id($return_order_id);
		if($money_from_replacement){
			foreach($money_from_replacement as $money){
				$desired_quntity_for_replacement=$money['quantity_replacement'];
				$price_of_desired_item=$money['replacement_value_each_inventory_id'];
				$original_item_cost=$this->Customer_account->get_indivisual_price_from_orders($money['order_item_id']);
			}
			$final_amount_after_rep=($desired_quntity_for_replacement*$original_item_cost)-($desired_quntity_for_replacement*$price_of_desired_item);
		}
		
		
	$money_from_return_refund= $this->Customer_account->get_data_from_returns_desired_return_order_id($return_order_id);
	if($money_from_return_refund){
			foreach($money_from_return_refund as $money){
				
				$desired_quntity_for_refund=$money['quantity_refund'];
				if($original_item_cost==null){
					$original_item_cost=$this->Customer_account->get_indivisual_price_from_orders($money['order_item_id']);
				}
			}
			$final_amount_after_ret_ref=$desired_quntity_for_refund*$original_item_cost;
		}
	
	if($final_amount_after_rep!=null){
		$final_amount=$final_amount_after_rep+$final_amount_after_ret_ref;
	}
	else{
		$final_amount=$final_amount_after_ret_ref;
	}
	
	if($final_amount>0){
		$get_refund_medium=$this->Customer_account->get_refund_medium_from_return_desired_or_rep_desired($return_order_id);
		$refund_medium="";
		if(!empty($get_refund_medium)){
			foreach($get_refund_medium as $medium){
				$refund_medium.=' Refund will be initiated through <b>'.$medium['refund_method'].' </b>';
				if(!empty($medium['refund_bank_id'])){
					$refund_medium.='to your selected bank '.$this->get_bank_details($medium['refund_bank_id']);
				}
			}
		}
		//return '<div class="col-sm-6">(You Get)</div><div class="col-sm-6"> Rs. '.abs($final_amount).' </div>'.$refund_medium;
		//return $refund_medium;
		return "";
	}
	if($final_amount<0){
		
		$get_payment_method=$this->Customer_account->get_payment_method_from_return_rep_desired($return_order_id);
		$payment_method="";
		if(!empty($get_payment_method)){
		
				$payment_method.=' Balance amount to be paid by you through <b>'.$get_payment_method.' </b> as selected by you';


		}
		
		//return '<div class="col-sm-6">(You Pay)</div><div class="col-sm-6"> Rs. '.abs($final_amount).'</div>'.$payment_method;
		return "";
	}
	if($final_amount==0){
		//return '<div class="col-sm-6">(Amount Squared)</div><div class="col-sm-6"> Rs. '.abs($final_amount).'</div>';
		return "";
	}
	
}


public function get_bank_details($id){
	$bank_details=$this->Customer_account->get_bank_details($id);
	$ret_bank_details="";
	if(!empty($bank_details)){
		foreach($bank_details as $details){
			$ret_bank_details.='<ul class="list-unstyled"><li style="padding-top:2px;padding-bottom:2px;">Bank Name : '.$details['bank_name'].'</li><li style="padding-top:2px;padding-bottom:2px;">Branch Name : '.$details['branch_name'].'</li><li style="padding-top:2px;padding-bottom:2px;">IFSC Code : '.$details['ifsc_code'].'</li><li style="padding-top:2px;padding-bottom:2px;">Account Number : '.$details['account_number'].'</li><li style="padding-top:2px;padding-bottom:2px;">City : '.$details['city'].'</li><li style="padding-top:2px;padding-bottom:2px;">Account Holder Name : '.$details['account_holder_name'].'</li><li style="padding-top:2px;padding-bottom:2px;">Mobile Number : '.$details['mobile_number'].'</li></ul>';
		}
	}
	return $ret_bank_details;
}

/*functions for order details page end*/

/*functions for cancel order page start*/
		public function cancel_order($id){
			if($this->session->userdata("logged_in")){
				$customer_id=$this->session->userdata("customer_id");
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="cancel_order_item";
				$order_id=$this->get_order_id_from_item_order_id($id);
				$data["order_id"]=$order_id;

				$data["order_payment_type"]=$this->Customer_account->get_order_payment_method($order_id);
				$data["order_item_id"]=$id;
				$data["cancel_reason"]=$this->Customer_account->get_order_cancel_reasons();
				$data["customer_id"]=$this->session->userdata("customer_id");
					$bank_account_number=$this->Customer_account->get_customer_all_bank($data["customer_id"]);
							$bank_detail=array();
							if(!empty($bank_account_number)){
								foreach($bank_account_number as $account_number){
									$bank_detail[]=$this->get_bank_detail($account_number['account_number']);
								}
							}
				$data['bank_detail']=$bank_detail;
				
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();

				$order_item_data_all=get_orderitem_details($id);
				$data["order_item_data_all"]=$order_item_data_all;

				$this->load->view('header',$data);
				$this->load->view('customer/cancel_order');
				$this->load->view('footer');
			}
			else{
				redirect(base_url());
			}
		}
		
		public function get_order_item_discount_price($id){
			return $this->Customer_account->get_order_item_discount_price($id);
		}
		public function get_order_id_from_item_order_id($id){
			return $this->Customer_account->get_order_id_from_item_order_id($id);
		}
		public function get_order_item_grand_total($id){
			return $this->Customer_account->get_order_item_grand_total($id);
		}
		public function get_order_item_data($id){
			return $this->Customer_account->get_order_item_data($id);
		}
		
		
		
		public function send_email_in_order_process($cancel_reason,$cancel_reason_comment,$order_summary_mail_data,$email,$current_flag){
			$email_stuff_arr=email_stuff();
			$order_id=$order_summary_mail_data["order_id"];
			$shipping_address_id=$this->Customer_account->get_shipping_address_id_after_cancellation($order_summary_mail_data["order_item_id"]);
			$shipping_address_table_data_arr=$this->Customer_account->get_shipping_address($shipping_address_id);
			$orders_status_data=$this->Customer_account->get_orders_status_data_after_cancellation($order_summary_mail_data["order_item_id"]);
			foreach($shipping_address_table_data_arr as $shipping_address_table_data){
				$shipping_address='<table style="color:#565656;line-height:15px;font-size:15px;background-color:#fff;"><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;padding-bottom:15px"><b>Items will be shipped to:</b><br></td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;"><b>'.$shipping_address_table_data['customer_name'].'</b></td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;"> <span>Mob : </span>'.$shipping_address_table_data['mobile'].'</td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;">'.$shipping_address_table_data['address1'].',</td></tr><tr><td colspan="2" style="color: #565656;line-height: 22px;font-size: 15px;">'.$shipping_address_table_data['address2'].',</td></tr><tr><td colspan="2" style="color: #565656;line-height:15px;font-size: 15px;">'.$shipping_address_table_data['city'].', '.$shipping_address_table_data['pincode'].'</td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;">'.$shipping_address_table_data['state'].', '.$shipping_address_table_data['country'].'</td></tr></table><p style="padding:0;margin:0;color:#565656;font-size: 15px;"><br/>Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br>';
			}
				
			$order_details='';
			$order_details.='<table>';
			$order_details.='<tr>';
			$order_details.='<th width="20%">Image</th>';
			$order_details.='<th width="40%">Product</th>';
			$order_details.='<th width="10%">Item Price</th>';
			$order_details.='<th width="10%">Qty</th>';
			$order_details.='<th width="20%">Subtotal</th>';
			$order_details.='</tr>';
				
			$order_details.='<tr>';
			$order_details.='<td align="center">';
			$order_details.="<img src='".base_url().$order_summary_mail_data['thumbnail']."'>";
			$order_details.='</td>';
			$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
			
			if($order_summary_mail_data['ord_sku_name']!=''){
				$order_details.=$order_summary_mail_data['ord_sku_name']."<br>";
			}else{
				$order_details.=$order_summary_mail_data['product_name']."<br>";
			}
			$order_details.=$order_summary_mail_data['product_description']."<br>";
			
			
			if(!empty($order_summary_mail_data['attribute_1'])){
				$order_details.=$order_summary_mail_data['attribute_1']." : ".$order_summary_mail_data['attribute_1_value'].'<br>';
			}
			if(!empty($order_summary_mail_data['attribute_2'])){
				$order_details.=$order_summary_mail_data['attribute_2']." : ".$order_summary_mail_data['attribute_2_value'].'<br>';
			}
			if(!empty($order_summary_mail_data['attribute_3'])){
				$order_details.=$order_summary_mail_data['attribute_3']." : ".$order_summary_mail_data['attribute_3_value'].'<br>';
			}
			if(!empty($order_summary_mail_data['attribute_4'])){
				$order_details.=$order_summary_mail_data['attribute_4']." : ".$order_summary_mail_data['attribute_4_value'];
			}
			if(!empty($order_summary_mail_data['sku_id'])){
				$order_details.="SKU : ".$order_summary_mail_data['sku_id'];
			}
			
			
			
			$order_details.='</p></td>';
			$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
			$order_details.=$order_summary_mail_data['product_price'];
			$order_details.='</td>';
			$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
			$order_details.=$order_summary_mail_data['quantity'];
			$order_details.='</p></td>';
			$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
			$order_details.=$order_summary_mail_data['subtotal'];
			$order_details.='</td>';
			$order_details.='</tr>';
			
			
			
			//////////////////////////////////////////////////
			
			//// stright n% discount and another promo radio both is there then starts 
				if($order_summary_mail_data['promotion_default_discount_promo']!="" && $order_summary_mail_data['promotion_quote']!="" && $order_summary_mail_data['default_discount']>0 && $order_summary_mail_data['promotion_quote']!=$order_summary_mail_data['promotion_default_discount_promo']){
					$order_details.='<tr>';
					$order_details.='<td align="center"></td>';
					$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
					
					$order_details.="Base Discount : <br>".trim($order_summary_mail_data['promotion_default_discount_promo']);
					
					$order_details.='</td>';
					$order_details.='<td valign="top" style="padding:12px 10px 0 10px;margin:0;text-align:center;"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
					$product_price_after_default_discount=round($order_summary_mail_data['product_price']-(($order_summary_mail_data['product_price']*$order_summary_mail_data['default_discount'])/100));
					
					$order_details.=curr_sym." ".$product_price_after_default_discount." <del>".curr_sym." ".$order_summary_mail_data['product_price']."</del>";
					$order_details.='</td>';
					$order_details.='<td colspan="2"></td>';
					$order_details.='</tr>';
				}
				//// stright n% discount and another promo radio both is there then starts 
				
				
				
				// Straight 5% discount on SKU’s starts 
				if(intval($order_summary_mail_data['quantity_with_promotion'])>0){
					if(intval($order_summary_mail_data['promotion_discount'])>0 && (intval($order_summary_mail_data['promotion_discount'])==intval($order_summary_mail_data['default_discount']))){
						
						$promotion_straight_discount_str='Discount @ ('.$order_summary_mail_data['promotion_discount'].'%)';
						$eligible_straight_quantity_str='Eligible Quantity : '.$order_summary_mail_data['quantity_with_promotion'];
						$applied_straight_offer_str='Applied Offer : '.$order_summary_mail_data['promotion_quote'];
						$promo_default_straight_discount_price=floatval($order_summary_mail_data['ord_max_selling_price']-$order_summary_mail_data['individual_price_of_product_with_promotion']);
						$promo_straight_discount_offer_calculation_display=$order_summary_mail_data['quantity_with_promotion'].' *  '.curr_sym." ".$promo_default_straight_discount_price;
						$promo_straight_discount_offer_calculation=$order_summary_mail_data['quantity_with_promotion']*$promo_default_straight_discount_price;
						$order_details.='<tr>';
						$order_details.='<td>';
						$order_details.='</td>';
						$order_details.='<td colspan="2" style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$promotion_straight_discount_str;
							$order_details.=" ".$eligible_straight_quantity_str."<br>";
							$order_details.=$applied_straight_offer_str;
						$order_details.='</td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$promo_straight_discount_offer_calculation_display;
						$order_details.='</td>';
						$order_details.='<td  valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=' '.curr_sym.' '.$promo_straight_discount_offer_calculation;
						$order_details.='</td>';
						$order_details.='</tr>';
					
					}
				}
				// Straight 5% discount on SKU’s ends
				/// buy 5 sku and 6% discount table starts 
				if(intval($order_summary_mail_data['quantity_with_promotion'])>0){
					if(intval($order_summary_mail_data['promotion_discount'])>0 && (intval($order_summary_mail_data['promotion_discount'])!=intval($order_summary_mail_data['default_discount']))){
						$promotion_discount_str='Promotion Discount @ ('.$order_summary_mail_data['promotion_discount'].'%)';
						$eligible_quantity_str='Eligible Quantity : '.$order_summary_mail_data['quantity_with_promotion'];
						$applied_offer_str='Applied Offer : '.$order_summary_mail_data['promotion_quote'];
						$promo_discount_price=floatval($order_summary_mail_data['ord_max_selling_price']-$order_summary_mail_data['individual_price_of_product_with_promotion']);
						$promo_discount_offer_calculation_display=$order_summary_mail_data['quantity_with_promotion'].' * '.curr_sym." ".$promo_discount_price;
						$promo_discount_offer_calculation=$order_summary_mail_data['quantity_with_promotion']*$promo_discount_price;
						$order_details.='<tr>';
						$order_details.='<td>';
						$order_details.='</td>';
						$order_details.='<td colspan="2" style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$promotion_discount_str;
							$order_details.=" ".$eligible_quantity_str."<br>";
							$order_details.=$applied_offer_str;
						$order_details.='</td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$promo_discount_offer_calculation_display;
						$order_details.='</td>';
						$order_details.='<td  valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=' '.curr_sym.' '.$promo_discount_offer_calculation;
						$order_details.='</td>';
						$order_details.='</tr>';
					}
				}
				/// buy 5 sku and 6% discount table ends
				/// Free item starts 
				if($order_summary_mail_data['promotion_item']!=""){
					$free_items_arr=$this->get_free_skus_from_inventory_with_details($order_summary_mail_data['promotion_item'],$order_summary_mail_data['promotion_item_num']);
					foreach($free_items_arr as $free_itemsObj){
						$order_details.='<tr>';
						
						$order_details.='<td align="center">';
						$order_details.="<img src='".base_url().$free_itemsObj->image."'  style='width:85px;'>";
						$order_details.='</td>';
						
						$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
						if($free_itemsObj->sku_name!=''){
							$order_details.=$free_itemsObj->sku_name."<br>";
						}else{
							$order_details.=$free_itemsObj->product_name."<br>";
						}
						
						if(!empty($free_itemsObj->attribute_1)){
							$order_details.=$free_itemsObj->attribute_1." : ".$free_itemsObj->attribute_1_value.'<br>';
						}
						if(!empty($free_itemsObj->attribute_2)){
							$order_details.=$free_itemsObj->attribute_2." : ".$free_itemsObj->attribute_2_value.'<br>';
						}
						if(!empty($free_itemsObj->attribute_3)){
							$order_details.=$free_itemsObj->attribute_3." : ".$free_itemsObj->attribute_3_value.'<br>';
						}
						if(!empty($free_itemsObj->attribute_4)){
							$order_details.=$free_itemsObj->attribute_4." : ".$free_itemsObj->attribute_4_value.'<br>';
						}
						if(!empty($free_itemsObj->sku_id)){
							$order_details.="SKU : ".$free_itemsObj->sku_id;
						}
						$order_details.='</p>';
						$order_details.='</td>';
						$order_details.='<td colspan="3" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.='Free - You saved <del>'.curr_sym." ".$order_summary_mail_data['promotion_item_num']*$order_summary_mail_data['promotion_item_multiplier']*$free_itemsObj->selling_price.'</del> ('.$order_summary_mail_data['promotion_item_num']*$order_summary_mail_data['promotion_item_multiplier'].' * '.curr_sym.' '.$free_itemsObj->selling_price.')';
							$order_details.='<br>';
							$order_details.='(Offer Applied : '.$order_summary_mail_data['promotion_quote'].')';
						$order_details.='</td>';
						$order_details.='</tr>';
					}
				}
				/// Free item ends 
				$invoice_offers=$this->Customer_account->get_all_order_invoice_offers($order_id);
				$shipping_charge_waived="no";
				if(!empty($invoice_offers)){
					foreach($invoice_offers as $data){
						$promotion_invoice_free_shipping=$data["promotion_invoice_free_shipping"];
						$invoice_offer_achieved=$data["invoice_offer_achieved"];
						if($promotion_invoice_free_shipping>0 && $invoice_offer_achieved==1){
							$shipping_charge_waived="yes";
						}else{
							$shipping_charge_waived="no";
						}
					}
				}
				if($shipping_charge_waived=="yes"){
					$shipping_charge_modified=0;
					$subtotal_modified=$order_summary_mail_data['subtotal'];
				}
				else{
					$shipping_charge_modified=$order_summary_mail_data['shipping_charge'];
					$subtotal_modified=$order_summary_mail_data['grandtotal'];
				}
			
			/////////////////////////////////////////////////////////////////
			
							/* addon section */
							//if($order_summary_mail_data['ord_addon_products_status']=='1'){
							if(0){
								$order_details.='<tr>';
								$order_details.='<td>';
									$arr=json_decode($order_summary_mail_data['ord_addon_products']);
									$count=count($arr);
									if($order_summary_mail_data['ord_addon_inventories']!=''){
										$order_details.='<tr><th align="center" colspan="5" style="text-align: center;text-decoration: underline;">
										 + Additional Products - Included</th></tr>';
										$i=1;
										foreach($arr as $val){
											
											$name=$val->inv_name;
											$image=$val->inv_image;
											$sku_id=$val->inv_sku_id;
											$price=$val->inv_price;
											$order_details.='<tr>
											<td align="center"><img src="'.base_url().$image.'"  style="width:85px;"></td>
											<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.$name.'<br>SKU id : '.$sku_id.'</p></td>
											<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.curr_sym.$price.'</td>
											<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">1</td>
											<td  width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.curr_sym.$price.'
											</td></tr>';
										}
										
								}
								$order_details.='</td>';
								$order_details.='</tr>';
						
							}
							/* addon section */
			
			
		$order_details.='<tr>';
		$order_details.='<td colspan=5 align="center">';
		$order_details.='<p style="margin:10px 0 0 0;padding:5px;background-color:#f9f9f9;border:1px solid #eee;color:#666;font-size:13px;text-align:center">(+) Delivery: '.curr_sym.' '.($shipping_charge_modified);
		$order_details.='</p></td>';
		$order_details.='</tr>';
			
			
			
			if($current_flag!="shipped" && ($current_flag!="cancelled")){
			$order_details.='<tr>';
			$order_details.='<td colspan=5 align="center" style="padding:0 0 17px 0">';
			$order_details.='<p style="padding:4px 5px;background-color:#fffed5;border:1px solid #f9e2b2;color:#565656;margin:10px 0 0 0;text-align:center;font-size:12px">Delivery scheduled by '.date("D j M, Y",strtotime($order_summary_mail_data['expected_delivery_date']));
			$order_details.='</p></td>';
			$order_details.='</tr>';
			}
			
			$order_details.='<tr>';
				$order_details.='<td colspan=5 style="border-top: 2px solid #565656;border-bottom: 1px solid #e6e6e6; padding:10px 20px 10px 20px;;margin: 0;background-color: #f9f9f9;">';
				$order_details.='<p style="padding:0;margin:0;text-align:right;color:#004b9b;line-height:22px;white-space:nowrap;font-size:18px">Sub Total <span style="font-size:18;color:#004b9b;">'.curr_sym.' '.$subtotal_modified;
				$order_details.='</span></p></td>';
				$order_details.='</tr>';
			
			
			$order_details.='</table>';
			
			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:10px"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi '.$shipping_address_table_data['customer_name'].', </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">';
			
			
			if($current_flag=="cancelled"){
				$usermessage.='We confirm that your order had been cancelled<br>';
				$cancel_reason_text=$this->Customer_account->get_cancel_reason_to_check($cancel_reason);
				$usermessage.="<b>Reason Noted For Cancellation</b> : ".$cancel_reason_text." (".$cancel_reason_comment.")";
			}
			
			$usermessage.='</p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">';
			
			
			if($current_flag=="cancelled"){
				//$usermessage.='Thank you for shopping!';
			}
			
			if($current_flag!="shipped"){
				$rating_mail="";
			}
			
			$get_order_placed_date_customer_id_arr=$this->Customer_account->get_order_placed_date_customer_id($order_summary_mail_data['order_id']);
			$order_placed_date=$get_order_placed_date_customer_id_arr["order_placed_date"];
			$order_placed_date_format=date("jS F, Y \a\\t H:i",strtotime($order_placed_date))." Hrs";
			if($current_flag!="shipped"){
				//$usermessage.=' Meanwhile, you can check the status of your order on '.$email_stuff_arr["name_emailtemplate"].'</p><br><p style="text-align:center;padding:0;margin:0" align="center"> <a style="width:200px;margin:0px auto;background-color:#ff8c00;text-align:center;border:#ff8c00 solid 1px;padding:8px 0;text-decoration:none;border-radius:2px;display:block;color:#fff;font-size:13px" align="center" href="'.base_url().'Account/order_details_for_mail/'.$order_summary_mail_data['order_id'].'" target="_blank"> <span style="color:#ffffff;font-size:13px;background-color:#ff8c00">TRACK ORDER</span> </a> </p>';
				
				$usermessage.=' </td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">	<table border="0" width="100%"><tbody><tr><td colspan="2" style="padding:10px;background-color:#004b9b"><span style="color:#fff;"><b> Below is the summary of your order ID '.$order_summary_mail_data['order_id'].' dated '.$order_placed_date_format.'</b></span></td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">'.$order_details.'</td></tr></tbody></table></td></tr><tr><td style="background-color:#fff;><table width="100%"><tbody><tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:20px 20px 0 20px" bgcolor="#ffffff"></td></tr></tbody></table></td></tr><tr><td valign="top" align="left" style="margin: 0 auto;clear:both;padding: 20px 20px 0 20px;background-color:#fff;">'.$shipping_address.'</td></tr><tr><td><table width="100%" cellspacing="0" cellpadding="0" style="max-width:802px;"> <tbody><tr>'.$rating_mail.'<td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br> <table width="100%" cellspacing="0" cellpadding="0"> <tbody><tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Get in touch with our customer care team at +91 80505 04950</p> </td> </tr> </tbody></table> </td> </tr> </tbody></table></td></tr>';
				
				$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';
				
				$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

				$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			}
			
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($email);
			//if($current_flag=="approved"){
				$current_base_url=base_url();
				if((preg_match("/voometstudio.in/",$current_base_url))){
					$mail->AddBCC("rajitkumar1974@gmail.com", "Rajit Kumar");
					$mail->AddBCC("suchetarao18@gmail.com", "Sucheta Rao");
				}
			//}
			//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
			//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			
			if($current_flag=="cancelled"){
				$sub="Order Cancelled";
			}
			$order_id=$order_summary_mail_data['order_id'];
			$mail->Subject = $sub. ' - Order ID : '.$order_id;
			$mail->Body    = $usermessage;
			//if(0){
			if(!$mail->send()){
				echo 'mail is not sent';
			}
			////////////////// admin mail starts /////
			
			
			$usermessage_admin='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:10px"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi Admin, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">';
			
			
			if($current_flag=="cancelled"){
				$usermessage_admin.=$shipping_address_table_data['customer_name'].' has cancelled the below order.<br>';
				$cancel_reason_text=$this->Customer_account->get_cancel_reason_to_check($cancel_reason);
				$usermessage_admin.="<b>Reason For Cancellation</b> : ".$cancel_reason_text." (".$cancel_reason_comment.")<br>";
				$usermessage_admin.="If the payment was made online then you must initiate a refund immediately.";
			}
			
			$usermessage_admin.='</p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">';
			
			
			if($current_flag=="cancelled"){
				//$usermessage_admin.='Thank you for shopping!';
			}
			
			if($current_flag!="shipped"){
				$rating_mail="";
			}
			
			$get_order_placed_date_customer_id_arr=$this->Customer_account->get_order_placed_date_customer_id($order_summary_mail_data['order_id']);
			$order_placed_date=$get_order_placed_date_customer_id_arr["order_placed_date"];
			$order_placed_date_format=date("jS F, Y \a\\t H:i",strtotime($order_placed_date))." Hrs";
			if($current_flag!="shipped"){
				//$usermessage_admin.='You can check the status of your order on '.$email_stuff_arr["name_emailtemplate"].'</p><br><p style="text-align:center;padding:0;margin:0" align="center"> <a style="width:200px;margin:0px auto;background-color:#ff8c00;text-align:center;border:#ff8c00 solid 1px;padding:8px 0;text-decoration:none;border-radius:2px;display:block;color:#fff;font-size:13px" align="center" href="'.base_url().'Account/order_details_for_mail/'.$order_summary_mail_data['order_id'].'" target="_blank"> <span style="color:#ffffff;font-size:13px;background-color:#ff8c00">TRACK ORDER</span> </a> </p>';
				
				$usermessage_admin.='</td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">	<table border="0" width="100%"><tbody><tr><td colspan="2" style="padding:10px;background-color:#004b9b"><span style="color:#fff;"><b> Below is the summary of the order ID '.$order_summary_mail_data['order_id'].' dated '.$order_placed_date_format.'</b></span></td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">'.$order_details.'</td></tr></tbody></table></td></tr><tr><td style="background-color:#fff;><table width="100%"><tbody><tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:20px 20px 0 20px" bgcolor="#ffffff"></td></tr></tbody></table></td></tr>';

				if($current_flag!="cancelled"){
					$usermessage_admin.='<tr><td valign="top" align="left" style="margin: 0 auto;clear:both;padding: 20px 20px 0 20px;background-color:#fff;">'.$shipping_address.'</td></tr>';
				}
				
				$usermessage_admin.='<tr><td><table width="100%" cellspacing="0" cellpadding="0" style="max-width:802px;"> <tbody><tr>'.$rating_mail.'<td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br> <table width="100%" cellspacing="0" cellpadding="0"> <tbody><tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Get in touch with our customer care team at +91 80505 04950</p> </td> </tr> </tbody></table> </td> </tr> </tbody></table></td></tr>';
				
				$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';
				
				$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

				$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			}
			
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress("rajitkumar1974@gmail.com");
			
			//if($current_flag=="approved"){
				$current_base_url=base_url();
				if((preg_match("/voometstudio.in/",$current_base_url))){
					$mail->AddBCC("rajitkumar1974@gmail.com", "Rajit Kumar");
					$mail->AddBCC("suchetarao18@gmail.com", "Sucheta Rao");
					
				}
			//}
			//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
			//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			
			if($current_flag=="cancelled"){
				$sub="Order Cancelled";
			}
			
			$mail->Subject = $sub;
			$mail->Body    = $usermessage_admin;
			//if(0){
			if(!$mail->send()){
				echo 'mail is not sent';
			}
			///////////////// admin mail ends ///////
			//echo true;
	}
	
	
	
		public function submit_cancel_order_item($id){
			
			$order_item_id=$id;
			$customer_id=$this->session->userdata("customer_id");
			extract($_POST);
			//print_r($_POST);
			//exit;
			
			if(!isset($refund_method)){
				$refund_method="";
			}
			/*first add order item to cancels*/
			$user_type=$this->session->userdata("user_type");
			$add_cancel_order=$this->Customer_account->add_cancel_orders_to_cancels_table($order_item_id,$cancel_reason,$cancel_reason_comment,$user_type);
			
			if($invoice_cash_back_reject==1 || $invoice_surprise_gift_reject==1){
				$status="reject";
				if($invoice_cash_back_reject==1){
					$flag_cashback=$this->Customer_account->update_customer_response_for_invoice_cashback($order_id,$status,$customer_id,$total_amount_cash_back);	
				}
				if($invoice_surprise_gift_reject==1){
					$flag_sg=$this->Customer_account->update_customer_status_surprise_gifts_on_invoice($order_id,$status,$customer_id);
				}
			}
			
		//////////////////////////////////	
			
			if($add_cancel_order){ /*if cancels table is added*/
						$order_active_data=$this->Customer_account->get_all_order_data_from_order_item($order_item_id);/*fetch data from active order table based on order_item_id*/
					
					foreach($order_active_data as $data){
						$purchased_price=$data['purchased_price'];
						$order_item_id=$data['order_item_id'];
						$prev_order_item_id=$data['prev_order_item_id'];
						$customer_id=$data['customer_id'];
						$order_id=$data['order_id'];
						$shipping_address_id=$data['shipping_address_id'];
						$inventory_id=$data['inventory_id'];
						$product_id=$data['product_id'];
						$sku_id=$data['sku_id'];
						$tax_percent=$data['tax_percent'];
						$product_price=$data['product_price'];
						$shipping_charge=$data['shipping_charge'];
						$subtotal=$data['subtotal'];
						$wallet_status=$data['wallet_status'];
						$wallet_amount=$data['wallet_amount'];
						$grandtotal=$data['grandtotal'];
						$quantity=$data['quantity'];
						$timestamp=$data['timestamp'];
						$image=$data['image'];
						$random_number=$data['random_number'];
						$invoice_email=$data['invoice_email'];
						$invoice_number=$data['invoice_number'];
						$logistics_name=$data['logistics_name'];
						$logistics_weblink=$data['logistics_weblink'];
						$tracking_number=$data['tracking_number'];
						$expected_delivery_date=$data['expected_delivery_date'];
						$vendor_id=$data['vendor_id'];
						$payment_type=$data['payment_type'];
						$payment_status=$data['payment_status'];
						$return_period=$data['return_period'];
						$delivery_mode=$data['delivery_mode'];
						$logistics_id=$data['logistics_id'];
						$logistics_delivery_mode_id=$data['logistics_delivery_mode_id'];
						$logistics_territory_id=$data['logistics_territory_id'];
						$logistics_parcel_category_id=$data['logistics_parcel_category_id'];

						$promotion_available=$data['promotion_available'];
						$promotion_residual=$data['promotion_residual'];
						$promotion_discount=$data['promotion_discount'];
						$promotion_cashback=$data['promotion_cashback'];
						$promotion_surprise_gift=$data['promotion_surprise_gift'];
						$promotion_surprise_gift_type=$data['promotion_surprise_gift_type'];
						$promotion_surprise_gift_skus=$data['promotion_surprise_gift_skus'];
						$promotion_surprise_gift_skus_nums=$data['promotion_surprise_gift_skus_nums'];
						$default_discount=$data['default_discount'];
						$promotion_item_multiplier=$data['promotion_item_multiplier'];
						$promotion_item=$data['promotion_item'];
						$promotion_item_num=$data['promotion_item_num'];
						$promotion_clubed_with_default_discount=$data['promotion_clubed_with_default_discount'];
						$promotion_id_selected=$data['promotion_id_selected'];
						$promotion_type_selected=$data['promotion_type_selected'];
						$promotion_minimum_quantity=$data['promotion_minimum_quantity'];
						$promotion_quote=$data['promotion_quote'];
						$promotion_default_discount_promo=$data['promotion_default_discount_promo'];
						$individual_price_of_product_with_promotion=$data['individual_price_of_product_with_promotion'];
						$individual_price_of_product_without_promotion=$data['individual_price_of_product_without_promotion'];
						$total_price_of_product_without_promotion=$data['total_price_of_product_without_promotion'];
						$total_price_of_product_with_promotion=$data['total_price_of_product_with_promotion'];
						$quantity_without_promotion=$data['quantity_without_promotion'];
						$quantity_with_promotion=$data['quantity_with_promotion'];
						$cash_back_value=$data['cash_back_value'];
                                                $razorpayOrderId=$data['razorpayOrderId'];
                                                $razorpayPaymentId=$data['razorpayPaymentId'];

						
						$ord_max_selling_price=$data['ord_max_selling_price'];
						$ord_selling_discount=$data['ord_selling_discount'];
						$ord_tax_percent_price=$data['ord_tax_percent_price'];
						$ord_taxable_price=$data['ord_taxable_price'];
						$ord_sku_name=$data['ord_sku_name'];
                                                
                                                /* coupon */

                                                $ord_coupon_code=$data['ord_coupon_code'];
						$ord_coupon_name=$data['ord_coupon_name'];
						$ord_coupon_type=$data['ord_coupon_type'];
						$ord_coupon_value=$data['ord_coupon_value'];
						$ord_coupon_reduce_price=$data['ord_coupon_reduce_price'];
						$ord_coupon_status=$data['ord_coupon_status'];
                                                
                                                /* coupon */

						/* addon products details */
						$ord_addon_products_status=$data['ord_addon_products_status'];
						$ord_addon_products=$data['ord_addon_products'];
						$ord_addon_inventories=$data['ord_addon_inventories'];
						$ord_addon_total_price=$data['ord_addon_total_price'];
						
						
						/* addon products details */
						//,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price
						
						

                        $total_price_without_shipping_price=$data['total_price_without_shipping_price'];
                        $promotion_invoice_discount=$data['promotion_invoice_discount'];

                    }

                if($promotion_invoice_discount>0){

                    $invoice_discount=(($subtotal/$total_price_without_shipping_price)*100);
                    $each_invoice_amount=($promotion_invoice_discount*$invoice_discount/100);
                    //$invoice_amount=round($promotion_invoice_discount-$each_invoice_amount,2);
                    $order_item_invoice_discount_value=round($each_invoice_amount,2);
                    $order_item_invoice_discount_value_each=round($order_item_invoice_discount_value/$quantity,2);
                }else{
                    $order_item_invoice_discount_value=0;
                    $order_item_invoice_discount_value_each=0;
                }


                $stock_updated=$this->Customer_account->add_inventory_stock_when_customer_cancels_order_item($inventory_id,$quantity,$promotion_available,$promotion_minimum_quantity,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$ord_addon_products_status,$ord_addon_inventories);
					$notify_cancel=1;
					/* move above data to cancelled_orders table*/
					$add_to_cancel_orders=$this->Customer_account->active_order_add_to_cancel_orders($purchased_price,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$vendor_id,$return_period,$delivery_mode,$payment_type,$payment_status,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$order_item_invoice_discount_value,$prev_order_item_id,$notify_cancel,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price);
					
					if($add_to_cancel_orders){
						
						////////////////////// send customer email starts ////
						$order_summary_mail_data_arr=$this->Customer_account->get_order_summary_mail_data_after_cancellation($order_item_id);
						
						
							foreach($order_summary_mail_data_arr as $order_summary_mail_data){
							
								$email=$this->Customer_account->get_email_db($order_summary_mail_data['customer_id']);
								if($email!=''){
									
									$this->send_email_in_order_process($cancel_reason,$cancel_reason_comment,$order_summary_mail_data,$email,"cancelled");
								}//mail	
				
								}
								
								
						////////////////////  send customer emaile ends ////
						
						
						$cancel_id=$this->Customer_account->get_cancel_id_of_this_transaction($order_item_id);/*get cancel_id from cancels table*/

						if($refund_method=="Wallet" || $refund_method=="Razorpay"){
							/// Wallet
							
							//////////////////////////////////
							$refund_bank_id="";
						
							$this->Customer_account->update_cancels_table($order_item_id,$refund_method,$refund_bank_id);
							
						}
						if($refund_method=="Bank Transfer"){
						
							if(isset($bank_account_number)){
								$get_bank_account_id=$this->Customer_account->get_bank_account_details($bank_account_number,$customer_id);
								if(!$get_bank_account_id){
									$add_bank_details=$this->Customer_account->add_bank_details($bank_name,$bank_branch_name,$bank_city,$bank_ifsc_code,$bank_account_number,$bank_account_confirm_number,$account_holder_name,$bank_return_refund_phone_number,$customer_id);
									if($add_bank_details){
										$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number,$customer_id);
									}
								}
								else{
									$refund_bank_account_details_arr=$this->Customer_account->get_refund_bank_account_details($get_bank_account_id);
									
									$add_bank_details=$this->Customer_account->add_bank_details_delivered($refund_bank_account_details_arr["bank_name"],$refund_bank_account_details_arr["branch_name"],$refund_bank_account_details_arr["city"],$refund_bank_account_details_arr["ifsc_code"],$refund_bank_account_details_arr["account_number"],$refund_bank_account_details_arr["confirm_account_number"],$refund_bank_account_details_arr["account_holder_name"],$refund_bank_account_details_arr["mobile_number"],$refund_bank_account_details_arr["customer_id"]);
									if($add_bank_details){
										$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number,$customer_id);
									}
								}
							}								
							$refund_bank_id=$get_bank_account_id;
							$this->Customer_account->update_cancels_table($order_item_id,$refund_method,$refund_bank_id);
							
						}	
						//common for refund_method wallet,bank transfer and empty (cod)

						$update_order_item_id_to_cancel=$this->Customer_account->update_order_item_id_to_cancel($order_item_id);/*changing order_item to cancelled*/
						
						
						if($update_order_item_id_to_cancel){
							$this->Customer_account->remove_data_from_active_order_table($order_item_id);
							redirect(base_url()."Account/my_order");
						}	 
					}
					else{
						
					}
				
			}
	/*if not added to cancels table*/
			else{
				
			}
			
			
			
		}


		public function get_order_payment_done($id){
			$payment_method=$this->Customer_account->get_order_payment_method($id);
			if(strtolower($payment_method)!="cod"){
				return true;
			}
		}

/*functions for cancel order page ends*/


/*functions for return order page start*/
		public function return_order($id){
			
			if($this->session->userdata("logged_in")){
				
				$order_id=$this->get_order_id_from_item_order_id($id);
				if($order_id==''){
					redirect(base_url().'Account/my_order');
				}
				$req_count_arr=$this->get_request_details_of_order_item($id);
				if($req_count_arr)
				
				$replace_req_count=$req_count_arr["replace_req_count"];
				$refund_req_count=$req_count_arr["refund_req_count"];
				
				if($replace_req_count>0 || $refund_req_count>0){
					//return request is not allowed
					redirect(base_url().'Account/my_order');
				}else{
					$method=current_url();
					$this->session->set_userdata("cur_page",$method);
					$data['page_name']='';
					$data['page_type']='';
					$data['page_id']='';
					$data['products_obj']='';
					$data['p_category']=$this->provide_first_level_menu();
					$data["controller"]=$this;
					$data["current_controller"]="account";
					$data["cur_page"]="return_order_item";
					
					$data["order_id"]=$order_id;
                    $data["order_payment_type"]=$this->Customer_account->get_order_payment_method($order_id);
					$data["order_item_id"]=$id;
					$product_id=$this->Customer_account->get_product_id_by_order_item_id_and_order_id($id,$order_id);
					
					$his_obj=get_orderitem_details_history($id);

					$ord_addon_products_status=0;
					if(!empty($his_obj)){
						$ord_addon_products_status=$his_obj->ord_addon_products_status;
					}

					//echo $product_id;exit;

					$product_info_arr=array();

					if($ord_addon_products_status=='1'){

						$primary_reasons_pcat_id="";
						$primary_reasons_cat_id="";
						$primary_reasons_subcat_id="";
					}else{
						if($product_id!=""){
							$product_info_arr=$this->Customer_account->get_product_info_by_product_id($product_id);
							if(!empty($product_info_arr)){
								$primary_reasons_pcat_id=$product_info_arr["pcat_id"];
								$primary_reasons_cat_id=$product_info_arr["cat_id"];
								$primary_reasons_subcat_id=$product_info_arr["subcat_id"];
							}else{
								$primary_reasons_pcat_id="";
								$primary_reasons_cat_id="";
								$primary_reasons_subcat_id="";
							}
						}
						else{
							$primary_reasons_pcat_id="";
							$primary_reasons_cat_id="";
							$primary_reasons_subcat_id="";
						}
					}
					$data["refund_policy"]=$this->Customer_account->get_refund_policy_order($id,$order_id);
					$data["replacement_policy"]=$this->Customer_account->get_replacement_policy_order($id,$order_id);
					$data["primary_reason_return"]=$this->Customer_account->get_order_return_primary_reasons($primary_reasons_pcat_id,$primary_reasons_cat_id,$primary_reasons_subcat_id);
					$data["issue_reason_return"]=$this->Customer_account->get_order_return_issue_reason();
					$customer_id=$this->session->userdata("customer_id");
					
					$bank_account_number=$this->Customer_account->get_customer_all_bank($customer_id);
					$bank_detail=array();
					if(!empty($bank_account_number)){
						foreach($bank_account_number as $account_number){
							$bank_detail[]=$this->get_bank_detail($account_number['account_number']);
						}
					}
					$data['bank_detail']=$bank_detail;
					
					$ft_img=$this->Customer_frontend->foot_image_index();
					$data['ft_img']='';
					if(!empty($ft_img)){
						if(isset($ft_img->background_image)){
							$data['ft_img']=$ft_img->background_image;
						}
					}
					$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();

					$order_item_data_all=get_orderitem_details_completed($id);
					$data["order_item_data_all"]=$order_item_data_all;

					$this->load->view('header',$data);
					$this->load->view('customer/return_order');
					$this->load->view('footer');
					
				}

			}
			else{
				redirect(base_url());
			}
		}

		public function get_delivered_time_of_order_item($order_id,$order_item_id){
			return $this->Customer_account->get_delivered_time_of_order_item($order_id,$order_item_id);
		}
		public function submit_return_order_item($id){
			
			$order_item_id=$id;
			//print_r($this->input->post());
			//exit;
			//generate a return_order_id
			$date = new DateTime();
			$mt=microtime();
			$salt="return_order_id";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$return_order_id=md5($str);
			
			$customer_id=$this->session->userdata('customer_id');
			print_r($this->input->post());
			
			//exit;
			
			extract($this->input->post());
            $refund_bank_id="";
            if($return_type=="Refund"){
				if($refund_method=="Bank Transfer"){
	
					if(isset($bank_account_number)){
								$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number,$customer_id);
								if(!$get_bank_account_id){
									$add_bank_details=$this->Customer_account->add_bank_details($bank_name,$bank_branch_name,$bank_city,$bank_ifsc_code,$bank_account_number,$bank_account_confirm_number,$account_holder_name,$bank_return_refund_phone_number,$customer_id);
									if($add_bank_details){
										$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number,$customer_id);
									}
								}else{
									$refund_bank_account_details_arr=$this->Customer_account->get_refund_bank_account_details($get_bank_account_id);
									
									$add_bank_details=$this->Customer_account->add_bank_details_delivered($refund_bank_account_details_arr["bank_name"],$refund_bank_account_details_arr["branch_name"],$refund_bank_account_details_arr["city"],$refund_bank_account_details_arr["ifsc_code"],$refund_bank_account_details_arr["account_number"],$refund_bank_account_details_arr["confirm_account_number"],$refund_bank_account_details_arr["account_holder_name"],$refund_bank_account_details_arr["mobile_number"],$refund_bank_account_details_arr["customer_id"]);
									if($add_bank_details){
										$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number,$customer_id);
									}
								}
							}

					$refund_bank_id=$get_bank_account_id;

				}
				if($refund_method=="Wallet"){
					$refund_bank_id="";		
				}
				
				//////////////////added//////////////////////////
				
				if(isset($req_promotion_available)){
					
					$promotion_available=$req_promotion_available;
					$promotion_minimum_quantity=$req_promotion_minimum_quantity;
					$promotion_quote=$req_promotion_quote;
					$promotion_default_discount_promo=$req_promotion_default_discount_promo;
					
					$promotion_surprise_gift=$req_promotion_surprise_gift;
					$promotion_surprise_gift_type=$req_promotion_surprise_gift_type;
					$promotion_surprise_gift_skus=$req_promotion_surprise_gift_skus;
					$promotion_surprise_gift_skus_nums=$req_promotion_surprise_gift_skus_nums;
					
					$promotion_item=$req_promotion_item;
					$promotion_item_num=$req_promotion_item_num;
					$promotion_cashback=$req_promotion_cashback;
					$promotion_item_multiplier=$req_promotion_item_multiplier;
					$promotion_discount=$req_promotion_discount;
					$default_discount=$req_default_discount;
					$quantity_without_promotion=$req_quantity_without_promotion;
					$quantity_with_promotion=$req_quantity_with_promotion;
					$cash_back_value=$req_cash_back_value;
					$individual_price_of_product_with_promotion=$req_individual_price_of_product_with_promotion;
					$individual_price_of_product_without_promotion=$req_individual_price_of_product_without_promotion; 
					$total_price_of_product_with_promotion=$req_total_price_of_product_with_promotion;
					$total_price_of_product_without_promotion=$req_total_price_of_product_without_promotion;
				

					$refund_req=$this->Customer_account->insert_into_returns_refund_desired_table_with_promo($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_refund,$refund_method,$refund_bank_id,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion,$original_inventory_id,$order_item_invoice_discount_value_each);
				
				}else{

					if($ord_addon_products_status=='1'){
					//	$desired_quantity_refund=$ord_addon_products_count;
					}
					
					$refund_req=$this->Customer_account->insert_into_returns_refund_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_refund,$refund_method,$refund_bank_id,$original_inventory_id,$order_item_invoice_discount_value_each);	
				}
				//////////////////added//////////////////////////
	
			}
			
			if($return_type=='Replacement'){
				
				$reduce_stock=1;
				if(isset($replacement_limited_method)){
					
					if($replacement_limited_method=="procced_with_replace_all_when_stock_become_available"){
						$reduce_stock=0;
					}
				}
				if($reduce_stock==1){
					$stock_reduced=$this->Customer_account->reduce_stocks_of_inventory_when_replacement_request($selected_inventory_id_for_rep,$desired_quantity_replacement);
				}
				
				if(isset($replacement_limited_method)){
					
					if($replacement_limited_method=="procced_with_replace_all_when_stock_become_available"){

			
					$shipping_detail_arr=$this->get_corresponding_shipping_charge_customer_has_to_pay($order_item_id,$desired_quantity_replacement,$selected_inventory_id_for_rep);

					$shipping_charge=$shipping_detail_arr["shipping_charge"];
					$shipping_charge_percent_paid_by_customer=$shipping_detail_arr["customer_has_to_pay"];

					$shipping_charge_requested=(($shipping_charge_percent_paid_by_customer)/100)*$shipping_charge;
					
						//$refund_method="";
						$refund_bank_id="";
						$balance_amount_paid_by="";
						if(!empty($bank_account_number_rep)){
							$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number_rep,$customer_id);
							if(!$get_bank_account_id){
								$add_bank_details=$this->Customer_account->add_bank_details($bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep,$customer_id);
								if($add_bank_details){
									$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number_rep,$customer_id);
									
								}
							}
							$refund_bank_id=$get_bank_account_id;
							$refund_method_rep="Bank Transfer";
						}
						/////////////////////////////////////////////
						
						if(isset($req_promotion_available)){

							$promotion_available=$req_promotion_available;
							$promotion_minimum_quantity=$req_promotion_minimum_quantity;
							$promotion_quote=$req_promotion_quote;
							$promotion_default_discount_promo=$req_promotion_default_discount_promo;
							$promotion_surprise_gift=$req_promotion_surprise_gift;
							$promotion_surprise_gift_type=$req_promotion_surprise_gift_type;
							$promotion_surprise_gift_skus=$req_promotion_surprise_gift_skus;
							$promotion_surprise_gift_skus_nums=$req_promotion_surprise_gift_skus_nums;
							$promotion_item=$req_promotion_item;
							$promotion_item_num=$req_promotion_item_num;
							$promotion_cashback=$req_promotion_cashback;
							$promotion_item_multiplier=$req_promotion_item_multiplier;
							$promotion_discount=$req_promotion_discount;
							$default_discount=$req_default_discount;
							$quantity_without_promotion=$req_quantity_without_promotion;
							$quantity_with_promotion=$req_quantity_with_promotion;
							$cash_back_value=$req_cash_back_value;
							$individual_price_of_product_with_promotion=$req_individual_price_of_product_with_promotion;
							$individual_price_of_product_without_promotion=$req_individual_price_of_product_without_promotion; 
							$total_price_of_product_with_promotion=$req_total_price_of_product_with_promotion;
							$total_price_of_product_without_promotion=$req_total_price_of_product_without_promotion;
				
							$refund_req=$this->Customer_account->save_data_to_return_stock_notification_with_promo($order_item_id,$return_order_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$timelimitdefined,$refund_method_rep,$refund_bank_id,$return_value_each,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion,$shipping_charge_purchased,$shipping_charge_requested,$shipping_charge_percent_paid_by_customer,$original_inventory_id,$order_item_invoice_discount_value_each);
						
						}else{
							
							$flag=$this->Customer_account->save_data_to_return_stock_notification($order_item_id,$return_order_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$timelimitdefined,$refund_method_rep,$refund_bank_id,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$shipping_charge_percent_paid_by_customer,$original_inventory_id,$order_item_invoice_discount_value_each);
						}
						
						/////////////////////////////////////////////
					
						//$flag=$this->Customer_account->save_data_to_return_stock_notification($order_item_id,$return_order_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$timelimitdefined,$refund_method_rep,$refund_bank_id,$return_value_each);
						/*if($flag){
							redirect(base_url()."Account/my_order");
						}
						else{
							echo "error";
						}*/
						exit;
					}
					
					if($replacement_limited_method=="procced_with_replace_and_refund"){
							
						$refund_bank_id="";	
						//$refund_method="";
						if(!empty($bank_account_number_rep)){
							
							$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number_rep,$customer_id);
							if(!$get_bank_account_id){
								$add_bank_details=$this->Customer_account->add_bank_details($bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep,$customer_id);
								if($add_bank_details){
									$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number_rep,$customer_id);
								}
							}
							$refund_bank_id=$get_bank_account_id;
							$refund_method_rep="Bank Transfer";
						}
						
						/*dump for refund table*/
						
						$desired_quantity_refund=$desired_quantity_replacement-$stock_quantity_available_at_replacement;
						
						
						if(isset($req_promotion_available)){

							$promotion_available=$req_promotion_available;
							$promotion_minimum_quantity=$req_promotion_minimum_quantity;
							$promotion_quote=$req_promotion_quote;
							$promotion_default_discount_promo=$req_promotion_default_discount_promo;
							$promotion_surprise_gift=$req_promotion_surprise_gift;
							$promotion_surprise_gift_type=$req_promotion_surprise_gift_type;
							$promotion_surprise_gift_skus=$req_promotion_surprise_gift_skus;
							$promotion_surprise_gift_skus_nums=$req_promotion_surprise_gift_skus_nums;
							$promotion_item=$req_promotion_item;
							$promotion_item_num=$req_promotion_item_num;
							$promotion_cashback=$req_promotion_cashback;
							$promotion_item_multiplier=$req_promotion_item_multiplier;
							$promotion_discount=$req_promotion_discount;
							$default_discount=$req_default_discount;
							$quantity_without_promotion=$req_quantity_without_promotion;
							$quantity_with_promotion=$req_quantity_with_promotion;
							$cash_back_value=$req_cash_back_value;
							$individual_price_of_product_with_promotion=$req_individual_price_of_product_with_promotion;
							$individual_price_of_product_without_promotion=$req_individual_price_of_product_without_promotion; 
							$total_price_of_product_with_promotion=$req_total_price_of_product_with_promotion;
							$total_price_of_product_without_promotion=$req_total_price_of_product_without_promotion;
				
							$refund_req=$this->Customer_account->insert_into_returns_refund_desired_table_with_promo($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_refund,$refund_method_rep,$refund_bank_id,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion,$original_inventory_id,$order_item_invoice_discount_value_each);
						
						}else{
							
							$refund_req=$this->Customer_account->insert_into_returns_refund_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_refund,$refund_method_rep,$refund_bank_id,$original_inventory_id,$order_item_invoice_discount_value_each);
						}
		
						/*dump for replacement desired table*/	
						
						$desired_quantity_replacement=$stock_quantity_available_at_replacement;
						
						if(!isset($pay_balance_method)){
							$balance_amount_paid_by="";
						}
						if(isset($pay_balance_method)){
							$balance_amount_paid_by=$pay_balance_method;
							$refund_method_rep="";
							$refund_bank_id="";
						}
						
						if($stock_quantity_available_at_replacement!=0){
							///////////////
							$paid_by="";
							if($return_value_each>$replacement_value_each){
								$paid_by="admin";
							}
							if($return_value_each<$replacement_value_each){
								$paid_by="customer";
							}
							
							$amount_paid=$desired_quantity_replacement*abs($return_value_each-$replacement_value_each);
							
							$payment_status="";
							//if($balance_amount_paid_by=="cod" || $balance_amount_paid_by=="check_dd"){
								$payment_status="pending";
							//}
							//if($balance_amount_paid_by=="wallet" || $balance_amount_paid_by=="online_payment"){
								//$payment_status="paid";
							//}
					
							//////////////////
							
							if(!isset($pay_balance_method)){
								$balance_amount_paid_by="";
							}
							if(isset($pay_balance_method)){
								$balance_amount_paid_by=$pay_balance_method;
							}
						
							$return_order_id_replaced=$return_order_id."1";
							
							//$shipping_charge_requested=$this->get_corresponding_shipping_charge_for_replacement_request($order_item_id,$desired_quantity_replacement);
							
							if($return_value_each==$replacement_value_each && $shipping_charge_requested!=0){
								$paid_by="customer";
							}
							if($balance_amount_paid_by != ''){
								$payment_status="pending";
							}
							
							$replacement_with_limited_stock_available=$this->Customer_account->insert_into_returns_replacement_desired_table($return_order_id_replaced,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method_rep,$refund_bank_id,$paid_by,$amount_paid,$payment_status,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$original_inventory_id,$shipping_charge_percent_paid_by_customer,$order_item_invoice_discount_value_each);	
						}	
							
					}else{
					
						if(!isset($pay_balance_method)){
							$balance_amount_paid_by="";
						}
						if(isset($pay_balance_method)){
							$balance_amount_paid_by=$pay_balance_method;
							$refund_method_rep="";
							$refund_bank_id="";
						}
						$paid_by="";
						
						if($return_value_each>$replacement_value_each){
							$paid_by="admin";
						}
						if($return_value_each<$replacement_value_each){
							$paid_by="customer";
						}
						$amount_paid=$desired_quantity_replacement*abs($return_value_each-$replacement_value_each);
						
						$payment_status="";
						//if($balance_amount_paid_by=="cod" || $balance_amount_paid_by=="check_dd"){
							$payment_status="pending";
						//}
						//if($balance_amount_paid_by=="wallet" || $balance_amount_paid_by=="online_payment"){
							//$payment_status="paid";
						//}
						//$shipping_charge_requested=$this->get_corresponding_shipping_charge_for_replacement_request($order_item_id,$desired_quantity_replacement);
						
						if($return_value_each==$replacement_value_each && $shipping_charge_requested!=0){
							$paid_by="customer";
						}
						if($balance_amount_paid_by != ''){
							$payment_status="pending";
						}
						
						
						$replacement_with_stock_available=$this->Customer_account->insert_into_returns_replacement_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method_rep,$refund_bank_id,$paid_by,$amount_paid,$payment_status,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$original_inventory_id,$shipping_charge_percent_paid_by_customer,$order_item_invoice_discount_value_each);
					}
					
					exit;	
				}

				if(($return_value_each>$replacement_value_each)){
					if(!isset($refund_method_rep)){
						
						$refund_method="";
						$refund_bank_id="";	
						
						/*dump for replacement desired table*/	
						//$desired_quantity_replacement=$stock_quantity_available_at_replacement;
						
						if(!isset($pay_balance_method)){
							$balance_amount_paid_by="";
						}
						if(isset($pay_balance_method)){
								$balance_amount_paid_by=$pay_balance_method;
						}
						$paid_by="";
						
						$amount_paid=0;
						
						$payment_status="";
							
						$amount_paid=$desired_quantity_replacement*abs($return_value_each-$replacement_value_each);
						
						//$shipping_charge_requested=$this->get_corresponding_shipping_charge_for_replacement_request($order_item_id,$desired_quantity_replacement);

						$replacement_with_limited_stock_available=$this->Customer_account->insert_into_returns_replacement_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method,$refund_bank_id,$paid_by,$amount_paid,$payment_status,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$original_inventory_id,$shipping_charge_percent_paid_by_customer,$order_item_invoice_discount_value_each);
					}
					if(isset($refund_method_rep)){
						if($refund_method_rep=="Bank Transfer"){
							
							if(!empty($bank_account_number_rep)){
								$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number_rep,$customer_id);
								if(!$get_bank_account_id){
									$add_bank_details=$this->Customer_account->add_bank_details($bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep,$customer_id);
									if($add_bank_details){
										$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number_rep,$customer_id);	
									}
								}else{
										$refund_bank_account_details_arr=$this->Customer_account->get_refund_bank_account_details($get_bank_account_id);
										
										$add_bank_details=$this->Customer_account->add_bank_details_delivered($refund_bank_account_details_arr["bank_name"],$refund_bank_account_details_arr["branch_name"],$refund_bank_account_details_arr["city"],$refund_bank_account_details_arr["ifsc_code"],$refund_bank_account_details_arr["account_number"],$refund_bank_account_details_arr["confirm_account_number"],$refund_bank_account_details_arr["account_holder_name"],$refund_bank_account_details_arr["mobile_number"],$refund_bank_account_details_arr["customer_id"]);
										if($add_bank_details){
											$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number,$customer_id);
										}
								}
							}
							
							$refund_method="Bank Transfer";
							$refund_bank_id=$get_bank_account_id;	
								/*dump for replacement desired table*/	
							//$desired_quantity_replacement=$stock_quantity_available_at_replacement;
							if(!isset($pay_balance_method)){
								$balance_amount_paid_by="";
							}
							if(isset($pay_balance_method)){
									$balance_amount_paid_by=$pay_balance_method;
							}
							$paid_by="";
							if($return_value_each>$replacement_value_each){
									$paid_by="admin";
							}
							if($return_value_each<$replacement_value_each){
								$paid_by="customer";
							}
							$amount_paid=$desired_quantity_replacement*abs($return_value_each-$replacement_value_each);
							
							$payment_status="";
							//if($balance_amount_paid_by=="cod" || $balance_amount_paid_by=="check_dd"){
								$payment_status="pending";
							//}
							//if($balance_amount_paid_by=="wallet" || $balance_amount_paid_by=="online_payment"){
								//$payment_status="paid";
							//}
							//$shipping_charge_requested=$this->get_corresponding_shipping_charge_for_replacement_request($order_item_id,$desired_quantity_replacement,$selected_inventory_id_for_rep);

							if($return_value_each==$replacement_value_each && $shipping_charge_requested!=0){
								$paid_by="customer";
							}
							if($balance_amount_paid_by != ''){
								$payment_status="pending";
							}

							$replacement_with_limited_stock_available=$this->Customer_account->insert_into_returns_replacement_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method,$refund_bank_id,$paid_by,$amount_paid,$payment_status,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$original_inventory_id,$shipping_charge_percent_paid_by_customer,$order_item_invoice_discount_value_each);
						}												
						if($refund_method_rep=="Wallet"){
							$refund_method="Wallet";
							$refund_bank_id="";
							if(!isset($pay_balance_method)){
								$balance_amount_paid_by="";
							}
							if(isset($pay_balance_method)){
									$balance_amount_paid_by=$pay_balance_method;
								}
							$paid_by="";
							if($return_value_each>$replacement_value_each){	
								$paid_by="admin";
							}
							if($return_value_each<$replacement_value_each){	
								$paid_by="customer";
							}
							$amount_paid=$desired_quantity_replacement*abs($return_value_each-$replacement_value_each);
							
							$payment_status="";
							//if($balance_amount_paid_by=="cod" || $balance_amount_paid_by=="check_dd"){
								$payment_status="pending";
							//}
							//if($balance_amount_paid_by=="wallet" || $balance_amount_paid_by=="online_payment"){
								//$payment_status="paid";
							//}
							//
							//$shipping_charge_requested=$this->get_corresponding_shipping_charge_for_replacement_request($order_item_id,$desired_quantity_replacement);
						
							if($return_value_each==$replacement_value_each && $shipping_charge_requested!=0){
										$paid_by="customer";
							}
							if($balance_amount_paid_by != ''){
									$payment_status="pending";
							}	

							$replacement_with_limited_stock_available=$this->Customer_account->insert_into_returns_replacement_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method,$refund_bank_id,$paid_by,$amount_paid,$payment_status,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$original_inventory_id,$shipping_charge_percent_paid_by_customer,$order_item_invoice_discount_value_each);
						}
					}
						
				}

				if($return_value_each<$replacement_value_each){
					
					if(!empty($bank_account_number_rep)){
						$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number_rep,$customer_id);
						if(!$get_bank_account_id){
							$add_bank_details=$this->Customer_account->add_bank_details($bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep,$customer_id);
							if($add_bank_details){
								$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number_rep,$customer_id);
								
							}
						}
						$refund_method_rep="Bank Transfer";
						$refund_bank_id=$get_bank_account_id;
					}
					
					/*dump for replacement desired table*/	
					//$desired_quantity_replacement=$stock_quantity_available_at_replacement;
					if(!isset($pay_balance_method)){
						$balance_amount_paid_by="";
					}
					if(isset($pay_balance_method)){
						$balance_amount_paid_by=$pay_balance_method;
						$refund_method_rep="";
						$refund_bank_id="";
					}
					$paid_by="customer";
					$amount_paid=$desired_quantity_replacement*abs($return_value_each-$replacement_value_each);
					$payment_status="";
					//if($balance_amount_paid_by=="cod" || $balance_amount_paid_by=="check_dd"){
						$payment_status="pending";
					//}
					//if($balance_amount_paid_by=="wallet" || $balance_amount_paid_by=="online_payment"){
						//$payment_status="paid";
					//}
					//$shipping_charge_requested=$this->get_corresponding_shipping_charge_for_replacement_request($order_item_id,$desired_quantity_replacement);
					
					if($return_value_each==$replacement_value_each && $shipping_charge_requested!=0){
						$paid_by="customer";
					}
					if($balance_amount_paid_by != ''){
						$payment_status="pending";
					}		
								
					$replacement_with_limited_stock_available=$this->Customer_account->insert_into_returns_replacement_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method_rep,$refund_bank_id,$paid_by,$amount_paid,$payment_status,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$original_inventory_id,$shipping_charge_percent_paid_by_customer,$order_item_invoice_discount_value_each);

				}	
					
				if($return_value_each==$replacement_value_each){
					
					
					$refund_method_rep="";
					$refund_bank_id="";
					if(!isset($pay_balance_method)){
						$balance_amount_paid_by="";
					}
					if(isset($pay_balance_method)){
						$balance_amount_paid_by=$pay_balance_method;
					}
					
					$paid_by="";
					$amount_paid=0;
					$payment_status="";
						
					/*dump for replacement desired table*/	
					//$shipping_charge_requested=$this->get_corresponding_shipping_charge_for_replacement_request($order_item_id,$desired_quantity_replacement);
					
					if($return_value_each==$replacement_value_each && $shipping_charge_requested!=0){
						$paid_by="customer";
					}
							
					if($balance_amount_paid_by != ''){
						$payment_status="pending";
					}		
					
					$replacement_with_limited_stock_available=$this->Customer_account->insert_into_returns_replacement_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method_rep,$refund_bank_id,$paid_by,$amount_paid,$payment_status,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$original_inventory_id,$shipping_charge_percent_paid_by_customer,$order_item_invoice_discount_value_each);

				}
					
			}//if condition finished //replacement
		}
		
		
		public function show_available_replacement_options(){
			if($this->session->userdata("logged_in")){
				$product_id=$this->input->post("product_id");
				$desired_quantity_replacement=$this->input->post("desired_quantity_replacement");
				$inventory_id=$this->input->post("inventory_id");
				$free_inventory_available=$this->input->post("free_inventory_available");
				$no_refund=$this->input->post("no_refund");

				//$inventory_obj=$this->get_inventory_info_by_inventory_id($inventory_id);
				//$moq=$inventory_obj->moq;
				
				$result_available_replacements=$this->Customer_account->show_available_replacement_options($product_id,$desired_quantity_replacement,$inventory_id,$free_inventory_available,$no_refund);
				//print_r($result_available_replacements);
				$modified_result_available_replacements=array();

				foreach($result_available_replacements as $data){
					$arrars=[];
					$arrars['stock']=$data['stock'];
					$arrars['status_quantity']=$data['status_quantity'];
					$arrars['product_id']=$data['product_id'];
					$arrars['id']=$data['id'];
					$arrars['selling_price']=$data['selling_price'];
					$product_id=$this->Front_promotions->get_product_id($data['id']);
					
					if(!empty($product_id)){
						$a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
					}
					//print_r($a_TO_z_ids);
					
					if(!empty($a_TO_z_ids)){
						foreach($a_TO_z_ids as $a_TO_z_id){
							$brand_id=$a_TO_z_id['brand_id'];
							$subcat_id=$a_TO_z_id['subcat_id'];
							$cat_id=$a_TO_z_id['cat_id'];
							$pcat_id=$a_TO_z_id['pcat_id'];
						}
						$all_straight_discounts=$this->Front_promotions->get_all_straight_discount();

						if(!empty($all_straight_discounts)){
							
						foreach($all_straight_discounts as $discount){
							$storeLevelStraightDiscount=$this->Front_promotions->getStoreLevelStraightDiscount($discount['promo_uid']);
							if($storeLevelStraightDiscount){
								$discount_per=$discount['get_type'];
								if(strpos($discount_per, "%") !== false){
									$discount_per=str_replace("%","",$discount_per);
								}
								$selling_price=$data['max_selling_price'];
								$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
								$arrars['selling_price']=$price_after_discount;
							}
							$parentCategoryLevelStraightDiscount=$this->Front_promotions->getParentCategoryLevelStraightDiscount($discount['promo_uid'],$pcat_id);
							if($parentCategoryLevelStraightDiscount){
								$discount_per=$discount['get_type'];
								if(strpos($discount_per, "%") !== false){
									$discount_per=str_replace("%","",$discount_per);
								}
								$selling_price=$data['max_selling_price'];
								$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
								$arrars['selling_price']=$price_after_discount;
							}
							$categoryLevelStraightDiscount=$this->Front_promotions->getCategoryLevelStraightDiscount($discount['promo_uid'],$cat_id);
							if($categoryLevelStraightDiscount){
								$discount_per=$discount['get_type'];
								if(strpos($discount_per, "%") !== false){
									$discount_per=str_replace("%","",$discount_per);
								}
								$selling_price=$data['max_selling_price'];
								$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
								$arrars['selling_price']=$price_after_discount;
							}
							$subCategoryLevelStraightDiscount=$this->Front_promotions->getSubCategoryLevelStraightDiscount($discount['promo_uid'],$subcat_id);
							if($subCategoryLevelStraightDiscount){
								$discount_per=$discount['get_type'];
								if(strpos($discount_per, "%") !== false){
									$discount_per=str_replace("%","",$discount_per);
								}
								$selling_price=$data['max_selling_price'];
								$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
								$arrars['selling_price']=$price_after_discount;
							}
							
							$brandLevelStraightDiscount=$this->Front_promotions->getBrandLevelStraightDiscount($discount['promo_uid'],$brand_id);
							if($brandLevelStraightDiscount){
								$discount_per=$discount['get_type'];
								if(strpos($discount_per, "%") !== false){
									$discount_per=str_replace("%","",$discount_per);
								}
								$selling_price=$data['max_selling_price'];
								$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
								$arrars['selling_price']=$price_after_discount;
							}
							$productLevelStraightDiscount=$this->Front_promotions->getProdctLevelStraightDiscount($discount['promo_uid'],$product_id);
							if($productLevelStraightDiscount){
								$discount_per=$discount['get_type'];
								if(strpos($discount_per, "%") !== false){
									$discount_per=str_replace("%","",$discount_per);
								}
								$selling_price=$data['max_selling_price'];
								$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
								$arrars['selling_price']=$price_after_discount;
							}
							$inventoryLevelStraightDiscount=$this->Front_promotions->getInventoryLevelStraightDiscount($discount['promo_uid'],$data['id']);
							if($inventoryLevelStraightDiscount){
								$discount_per=$discount['get_type'];
								if(strpos($discount_per, "%") !== false){
									$discount_per=str_replace("%","",$discount_per);
								}
								$selling_price=$data['max_selling_price'];
								$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
								$arrars['selling_price']=$price_after_discount;
							}
						}
						}
					}
					$arrars['image']=$data['image'];
					$arrars['thumbnail']=$data['thumbnail'];
					
					$arrars['attribute_1']=$data['attribute_1'];
					$arrars['attribute_1_value']=$data['attribute_1_value'];
					$arrars['attribute_2']=$data['attribute_2'];
					$arrars['attribute_2_value']=$data['attribute_2_value'];
					array_push($modified_result_available_replacements,$arrars);
				}
				//print_r($modified_result_available_replacements);
				echo json_encode($modified_result_available_replacements);
			}
			else{
				redirect(base_url());
			}
		}
		public function get_per_unit_price_of_order_item($order_item_id){
			$data_arr=$this->Customer_account->get_per_unit_price_of_order_item($order_item_id);

			foreach($data_arr as $data){
				
				$subtotal=$data["subtotal"];
				$quantity=$data["quantity"];
			}
			$price_per_unit=round($subtotal/$quantity,2);
			return $price_per_unit;
		}
		public function get_per_unit_price_of_order_item_from_active_orders($order_item_id){
			$data_arr=$this->Customer_account->get_per_unit_price_of_order_item_from_active_orders($order_item_id);

			foreach($data_arr as $data){
				
				$subtotal=$data["subtotal"];
				$quantity=$data["quantity"];
			}
			$price_per_unit=round($subtotal/$quantity,2);
			return $price_per_unit;
		}

		/*functions for return order page ends*/


		/*function for update basic info*/
		public function update_basic_info(){
			$customer_id=$this->session->userdata("customer_id");
			
			extract($this->input->post());
			

			$updated=$this->Customer_account->update_basic_info($name,$gender,$address1,$address2,$city,$state,$pin,$country,$customer_id);

			if($updated){
					$this->session->set_flashdata('notification','Personal information is updated successfully');
					$this->session->set_userdata(array('customer_name'=>$name));
					$this->session->set_userdata(array('customer_gender'=>$gender));

					$user_type=$this->session->userdata('user_type');	
					if($user_type=='franchise_customer'){
						redirect(base_url()."Account/franchise_account");
					}else{
						redirect(base_url()."Account/");
					}

					
			}
			else{
				redirect(base_url()."Account/");
			}
			
		}
		/*function for update basic info ends*/
		/*function for change_password */
		public function change_password(){
					if($this->session->userdata("logged_in")){
						$method=current_url();
						$this->session->set_userdata("cur_page",$method);
						$data['page_name']='';
						$data['page_type']='';
						$data['page_id']='';
						$data['products_obj']='';
						$data['p_category']=$this->provide_first_level_menu();
						$data["controller"]=$this;
						$data["current_controller"]="account";
						$data["cur_Spage"]="cur_Spage";
						$data["cur_page"]="change_password";
						$ft_img=$this->Customer_frontend->foot_image_index();
						$data['ft_img']='';
						if(!empty($ft_img)){
							if(isset($ft_img->background_image)){
								$data['ft_img']=$ft_img->background_image;
							}
						}
						$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
						$this->load->view('header',$data);
						$this->load->view('customer/change_password');
						$this->load->view('footer');
					}
					else{
						redirect(base_url());
					}
				}
		public function check_current_password(){
			$current_password=md5($this->input->post('current_password'));
			$customer_id=$this->session->userdata("customer_id");
			$verify=$this->Customer_account->check_current_password($current_password,$customer_id);
			if($verify){
				echo "1";
			}
			else{
				echo "0";
			}
		}

		public function update_password(){
			extract($this->input->post());
			$password=md5($new_password);
			$customer_id=$this->session->userdata("customer_id");
			$updated=$this->Customer_account->update_password($password,$customer_id,$new_password);
			if($updated){
				$this->session->set_flashdata('notification','Password is updated successfully');
				redirect(base_url()."Account/change_password/");
			}
			else{
				redirect(base_url()."Account/change_password/");
			}
			
		}
		/*function for change_password ends*/
		/*function for addresses */
		public function addresses(){
			if($this->session->userdata("logged_in")){
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="addresses";
				$data["cur_Spage"]="cur_Spage";
				$customer_id=$this->session->userdata("customer_id");
				$data["get_all_states_arr"]=$this->Customer_account->get_all_states();
				$data["all_addresses"]=$this->Customer_account->get_all_customer_addresses($customer_id);
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
				$this->load->view('header',$data);
				$this->load->view('customer/addresses');
				$this->load->view('footer');
			}
			else{
				redirect(base_url());
			}	
			
		}
		public function add_new_address(){
			extract($this->input->post());
			$customer_id=$this->session->userdata("customer_id");
			$address_added=$this->Customer_account->add_new_address($customer_id,$name,$mobile,$door_address,$street_address,$city,$state,$pin,$country,$make_default);
			if($address_added){
				$this->session->set_flashdata('notification','Address is added successfully');
				redirect(base_url()."Account/addresses");
			}
			else{
				redirect(base_url()."Account/addresses");
			}
		}

		public function set_this_address_default($id){
			$customer_address_id=$id;
			$customer_id=$this->session->userdata("customer_id");
			$updated=$this->Customer_account->set_this_address_default($customer_address_id,$customer_id);
			if($updated){
				$this->session->set_flashdata('notification','Selected address is set as default address successfully');
				redirect(base_url()."Account/addresses/");
			}
			else{
				redirect(base_url()."Account/addresses/");
			}
		}
		public function delete_this_address($id){
			$customer_address_id=$id;
			$customer_id=$this->session->userdata("customer_id");
			$updated=$this->Customer_account->delete_this_address($customer_address_id,$customer_id);
			if($updated){
				$this->session->set_flashdata('notification','Selected address is deleted successfully');
				redirect(base_url()."Account/addresses/");
			}
			else{
				redirect(base_url()."Account/addresses/");
			}
		}

		public function edit_this_address($id){
			if($this->session->userdata("logged_in")){
				$customer_address_id=$id;
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="addresses";
				$customer_id=$this->session->userdata("customer_id");
				$data["this_address"]=$this->Customer_account->get_this_customer_address($customer_id,$customer_address_id);
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
				$data["get_all_states_arr"]=$this->Customer_account->get_all_states();
				$this->load->view('header',$data);
				$this->load->view('customer/edit_addresses');
				$this->load->view('footer');
			}
			else{
				redirect(base_url());
			}
		}

		public function save_edit_address($id){
			$customer_address_id=$id;
			extract($this->input->post());
			$customer_id=$this->session->userdata("customer_id");
			$address_added=$this->Customer_account->save_edit_address($customer_id,$customer_address_id,$name,$mobile,$door_address,$street_address,$city,$state,$pin,$country,$make_default);
			if($address_added){
				$this->session->set_flashdata('notification','Address has been Updated successfully');
				redirect(base_url()."Account/addresses");
			}
			else{
				redirect(base_url()."Account/addresses");
			}
		}
		/*function for addresses end*/
		/*function for update login credentials */
		public function login_credentials(){
			if($this->session->userdata("logged_in")){
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="login_credentials";
				$data["cur_Spage"]="cur_Spage";
				$customer_id=$this->session->userdata("customer_id");

				$data["login_credentials"]=$this->Customer_account->get_customer_login_credentials($customer_id);
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
				$this->load->view('header',$data);
				$this->load->view('customer/login_credentials');
				$this->load->view('footer');
			}
			else{
				redirect(base_url());
			}	
			
		}
		public function check_change_email(){
			$email_change=$this->input->post('change_email');
			$customer_id=$this->session->userdata("customer_id");
			$verify=$this->Customer_account->check_email_change($email_change,$customer_id);
			if($verify){
				echo "1";
			}
			else{
				echo "0";
			}
		}
		public function change_email_customer(){
			$customer_id=$this->session->userdata("customer_id");
			$email=$this->input->post('email');
			$updated=$this->Customer_account->update_email($email,$customer_id);
			if($updated){
				$this->session->set_flashdata('notification','Email is updated successfully');
				$this->session->set_userdata(array('customer_email'=>$email));
				redirect(base_url()."Account/login_credentials/");
			}
			else{
				redirect(base_url()."Account/login_credentials/");
			}
		}

		public function check_change_mobile(){
			$mobile_change=$this->input->post('mobile_change');
			$customer_id=$this->session->userdata("customer_id");
			$verify=$this->Customer_account->check_mobile_change($mobile_change,$customer_id);
			if($verify){
				echo "1";
			}
			else{
				echo "0";
			}
		}
		public function change_mobile_customer(){
			$customer_id=$this->session->userdata("customer_id");
			$mobile=$this->input->post('mobile');
			if($mobile!=$this->session->userdata(customer_mobile)){
				$updated=$this->Customer_account->update_mobile($mobile,$customer_id);
				if($updated){
					$this->session->set_flashdata('notification','Mobile is updated successfully');
					$this->session->set_userdata(array('customer_mobile'=>$mobile));
					redirect(base_url()."Account/login_credentials/");
				}
				else{
					redirect(base_url()."Account/login_credentials/");
				}
			}
			else{
					redirect(base_url()."Account/login_credentials/");
				}
			
		}
/*function for update login credentials ends*/
/*function for update profile*/
public function profile(){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="profile";
		$data["cur_Spage"]="cur_Spage";
		$customer_id=$this->session->userdata("customer_id");
		$data["profile"]=$this->Customer_account->get_customer_profile($customer_id);
		$data["default_customer_skills"]=$this->Customer_account->get_default_customer_skills();
		$data["default_customer_preferred_material"]=$this->Customer_account->get_default_customer_preferred_material();
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/profile');
		$this->load->view('footer');
	}
	else{
		redirect(base_url());
	}
}

public function update_additional_profile(){
	extract($this->input->post());
	$customer_id=$this->session->userdata("customer_id");
	$skills=$this->input->post('skills');
	$skill_material=$this->input->post('prefer_material');
	$updated=$this->Customer_account->update_additional_customer_info($customer_id,$skill_material);
	if($updated){
		$this->session->set_flashdata('notification','Additional profile data is updated successfully');
		redirect(base_url()."Account/profile/");
	}
	else{
		redirect(base_url()."Account/profile/");
	}

}
/*function for update profile end*/

/*function for deactivate_account */
public function deactivate_account(){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="deactivate_account";
		$data["cur_Spage"]="cur_Spage";
		$customer_id=$this->session->userdata("customer_id");
		
		$data["wallet_amount"]=$this->Customer_account->get_customer_wallet_amount($customer_id);
		$data["products_to_be_delivered"]=$this->Customer_account->get_customer_products_to_be_delivered($customer_id);
		$data["myprofile"]=$this->Customer_account->get_customer_profile_info($customer_id);
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/deactivate_account');
		$this->load->view('footer');

	}
	else{
		redirect(base_url());
	}
}
/*function for deactivate_account end*/



/*function for wallet */
public function wallet(){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="wallet";
		$data["customer_id"]=$this->session->userdata("customer_id");
		$bank_account_number=$this->Customer_account->get_customer_all_bank($data["customer_id"]);
				$bank_detail=array();
				if(!empty($bank_account_number)){
					foreach($bank_account_number as $account_number){
						$bank_detail[]=$this->get_bank_detail($account_number['account_number']);
					}
				}
				$data['bank_detail']=$bank_detail;
		
		$data["wallet_amount"]=$this->Customer_account->get_customer_wallet_amount($data["customer_id"]);
		$data["wallet_summary"]=$this->Customer_account->get_customer_wallet_details($data["customer_id"]);
		
		$data["customer_wallet_transaction_bank_details"]=$this->Customer_account->get_customer_wallet_transaction_bank_details($data["customer_id"]);
		
		$data["wallet_transaction_bank_table"]=$this->Customer_account->get_wallet_transaction_bank_table($data["customer_id"]);
		
		$data["wallet_transaction"]=$this->Customer_account->get_wallet_transaction($data["customer_id"]);
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/wallet');
		$this->load->view('footer');
	}
	else{
		redirect(base_url());
	}	
		
}
	
	public function get_customer_wallet_id($customer_id){
		return $this->Customer_account->get_customer_wallet_id($customer_id);
	}

	public function wallet_transaction_processing(){
		
	
		$params = $columns = $totalRecords = $data = array();

	$params = $this->input->post();
	
	$totalRecords=$this->Customer_account->wallet_transaction_processing($params,"get_total_num_recs");
	
	$queryRecordsResult=$this->Customer_account->wallet_transaction_processing($params,"get_recs");
	
	$i=count($queryRecordsResult);
	$s_no=0;
	foreach($queryRecordsResult as $queryRecordsObj){
		$s_no++;
		$row=array();
		//$row[]=$queryRecordsObj->id;
		$row[]=$s_no;
		$row[]=$queryRecordsObj->transaction_details;
		$row[]=$queryRecordsObj->credit;
		$row[]=$queryRecordsObj->wallet_type;
		$row[]=$queryRecordsObj->debit;
		
		$row[]=$queryRecordsObj->amount;
		$row[]=$queryRecordsObj->timestamp;
		$data[]=$row;
		$row=array();
	}
	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);
	echo json_encode($json_data);  // send data as json format

		
	}


/*function for wallet end*/

/*function for card */
public function cards(){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="cards";
		$customer_id=$this->session->userdata("customer_id");
		$data["card"]=$this->Customer_account->get_customer_all_cards($customer_id);
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/card');
		$this->load->view('footer');
	}
	else{
		redirect(base_url());
	}	
		
}


public function add_customer_new_card(){
	extract($this->input->post());
	$customer_id=$this->session->userdata("customer_id");
	$address_added=$this->Customer_account->add_customer_new_card($customer_id,$card_number,$name_on_card,$card_type,$month,$year,$card_label);
	if($address_added){
		$this->session->set_flashdata('notification','Card is added successfully');
		redirect(base_url()."Account/cards");
	}
	else{
		redirect(base_url()."Account/cards");
	}
}

public function make_this_card_default($card_id){
	$customer_id=$this->session->userdata("customer_id");
	$card_updated=$this->Customer_account->make_this_card_default($customer_id,$card_id);
	if($card_updated){
		$this->session->set_flashdata('notification','This Card is updated as default successfully');
		redirect(base_url()."Account/cards");
	}
	else{
		redirect(base_url()."Account/cards");
	}
}

public function delete_this_card($card_id){
	$customer_id=$this->session->userdata("customer_id");
	$card_deleted=$this->Customer_account->delete_this_card($customer_id,$card_id);
	if($card_deleted){
		$this->session->set_flashdata('notification','This Card is deleted successfully');
		redirect(base_url()."Account/cards");
	}
	else{
		redirect(base_url()."Account/cards");
	}
}
/*function for card end*/


/*function for bank details*/
public function bank(){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="bank";
		$customer_id=$this->session->userdata("customer_id");
		$bank_account_number=$this->Customer_account->get_customer_all_bank($customer_id);

		//print_r($bank_account_number);
		
		$bank_detail=array();
		if(!empty($bank_account_number)){
			foreach($bank_account_number as $account_number){
				$bank_detail[]=$this->get_bank_detail($account_number['account_number']);
			}
		}

		$data['bank_detail']=$bank_detail;
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/bank');
		$this->load->view('footer');
	}
	else{
		redirect(base_url());
	}	
}

public function update_bank_details($bank_id,$order_item_id=0){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="update_bank_details";
		if($order_item_id!=0){
			$bank_account_number=$this->Customer_account->get_bank_account_number_bank_id($bank_id,'delivered');
		}
		else{
			$bank_account_number=$this->Customer_account->get_bank_account_number_bank_id($bank_id,'refund');
		}
		$bank_detail=array();
		if(!empty($bank_account_number)){
			if($order_item_id!=0){
				$bank_detail=$this->get_bank_detail_delivered($bank_account_number);
			}
			else{
				$bank_detail=$this->get_bank_detail($bank_account_number);
			}
		}
		$data['bank_detail']=$bank_detail;
		$data['order_item_id']=$order_item_id;
		$data['wallet_bank_transfer']="no";
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/update_bank_details');
		$this->load->view('footer');
	}
	else{
		redirect(base_url());
	}	
}


public function get_bank_detail($account_number){
	$customer_id=$this->session->userdata("customer_id");
	return $this->Customer_account->get_bank_detail($account_number,$customer_id);
}
public function get_bank_detail_delivered($account_number){
	$customer_id=$this->session->userdata("customer_id");
	return $this->Customer_account->get_bank_detail_delivered($account_number,$customer_id);
}
public function update_bank_details_action(){
	if($this->session->userdata("logged_in")){
		
		$wallet_bank_transfer=$this->input->post("wallet_bank_transfer");
		$bank_id=$this->input->post("bank_id");
		$bank_name_rep=$this->input->post("bank_name_rep");
		$bank_branch_name_rep=$this->input->post("bank_branch_name_rep");
		$bank_city_rep=$this->input->post("bank_city_rep");
		$bank_ifsc_code_rep=$this->input->post("bank_ifsc_code_rep");
		$bank_account_number_rep=$this->input->post("bank_account_number_rep");
		$bank_account_confirm_number_rep=$this->input->post("bank_account_confirm_number_rep");
		$account_holder_name_rep=$this->input->post("account_holder_name_rep");
		$bank_return_refund_phone_number_rep=$this->input->post("bank_return_refund_phone_number_rep");
		$order_item_id=$this->input->post("order_item_id");
		
		$customer_id=$this->session->userdata("customer_id");
		if($wallet_bank_transfer=="no"){
			if($order_item_id!=0){
				$this->Customer_account->update_returns_desired_with_bank_details_update($order_item_id);
				$flag=$this->Customer_account->update_delivered_bank_details_action($bank_id,$bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep);
				echo $flag;
			}
			else{
				$flag=$this->Customer_account->update_bank_details_action($bank_id,$bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep);
				echo $flag;
			}
		}
		if($wallet_bank_transfer=="yes"){
			
				$flag=$this->Customer_account->update_wallet_bank_transfer_details_action($bank_id,$bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep);
				echo $flag;
		}
	}
	else{
		redirect(base_url());
	}	
}
/*function for bank details ends*/


/*function for complaint_history start*/

public function complaint_history(){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="comp_cas_hist";
		$customer_id=$this->session->userdata("customer_id");
		$data["all_master_complaint_data"]=$this->Customer_account->get_all_master_data_of_complaints($customer_id);
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/complaint_history');
		$this->load->view('footer');

	}
	else{
		redirect(base_url());
	}	
		
}

public function all_complaint_processing(){
	$params = $columns = $totalRecords = $data = array();

	$params = $this->input->post();
	
	$totalRecords=$this->Customer_account->all_complaint_processing($params,"get_total_num_recs");
	
	$queryRecordsResult=$this->Customer_account->all_complaint_processing($params,"get_recs");
	
	$i=count($queryRecordsResult);
	foreach($queryRecordsResult as $queryRecordsObj){
		$row=array();
		
		$row[]=date("D, jS M'y",strtotime($queryRecordsObj->timestamp));
		$row[]='<a href="'.base_url().'Account/complaint_thread/'.$queryRecordsObj->complaint_thread_id.'">'.$queryRecordsObj->subjects.'</a>';
		$row[]='<a href="'.base_url().'Account/complaint_thread/'.$queryRecordsObj->complaint_thread_id.'">'.$queryRecordsObj->id.'</a>';
		$row[]=ucfirst($queryRecordsObj->regarding);
		$row[]=ucfirst($queryRecordsObj->severity);
		$row[]=$queryRecordsObj->close_case_text;
		$data[]=$row;
		$row=array();
	}
	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);
	echo json_encode($json_data);  // send data as json format
	
}

/*function for complaint_history ends*/

/*function for complaint_create start*/

public function complaint_create(){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="comp_cret_cas";
		$customer_id=$this->session->userdata("customer_id");
		$data['regarding']=$this->Customer_account->get_all_complaint_regarding();
		//$data['service']=$this->Customer_account->get_all_service_regarding();
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/complaint_create');
		$this->load->view('footer');

	}
	else{
		redirect(base_url());
	}	
		
}

public function get_all_complaint_regarding(){
	echo $this->Customer_account->get_all_complaint_regarding();
}
public function get_all_service_regarding(){
	echo json_encode($this->Customer_account->get_all_service_regarding());
}
public function get_all_category_service($id){
	echo json_encode($this->Customer_account->get_all_category_service($id));
}
public function get_all_country_code(){
	echo json_encode($this->Customer_account->get_all_country_code());
}

public function create_complaint(){
	if($this->session->userdata("logged_in")){
		
		print_r($this->input->post());
		extract($_POST);
		//generate a $complaint_thread_id
		$date = new DateTime();
		$mt=microtime();
		$salt="create_complaint_id";
		$rand=mt_rand(1, 1000000);
		$ts=$date->getTimestamp();
		$str=$salt.$rand.$ts.$mt;
		$complaint_thread_id=md5($str);
		
		//generate a $thread_id
		$date = new DateTime();
		$mt=microtime();
		$salt="thread_id";
		$rand=mt_rand(1, 1000000);
		$ts=$date->getTimestamp();
		$str=$salt.$rand.$ts.$mt;
		$thread_id=md5($str);
		
		
		$customer_id=$this->session->userdata('customer_id');
		$user_type=$this->session->userdata('user_type');			

		if(!empty($_FILES["filesToUpload"]["name"][0])){
			if (!file_exists($folder="assets/complaints_attachment")) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
			
					if (!file_exists($folder=$folder.'/'.$complaint_thread_id)) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
					if (!file_exists($folder=$folder.'/'.$thread_id)) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
					
					for($i=0;$i<count($_FILES["filesToUpload"]["name"]);$i++){
						$mask=umask(0);
						mkdir($folder.'/'.$i,0777);
						umask($mask);
						$sourcePath=$_FILES["filesToUpload"]["tmp_name"][$i];
						$targetPath=$folder.'/'.$i."/".$_FILES["filesToUpload"]["name"][$i];
						$file_path=$targetPath;
						if(move_uploaded_file($sourcePath,$targetPath)){						
							$this->Customer_account->save_complaint_attachment($file_path,$thread_id,$complaint_thread_id,$customer_id);
						}
					}
				}
			$regarding=$this->Customer_account->get_regarding_name($regarding);
			$service=$this->Customer_account->get_service_name($service);
			$category=$this->Customer_account->get_category_name($category);
			
			$this->Customer_account->create_new_complaints($complaint_thread_id,$user_type,$acc_id,$severity,$regarding,$service,$category,$subject);
			$this->Customer_account->add_thread_to_complaints($complaint_thread_id,$thread_id,$user_type,$customer_id,$discription,$contact_method,$country_code,$phone_number,$ext_number);
			
			redirect(base_url()."Account/complaint_history");		
					
	}
	else{
		redirect(base_url());
	}
}

public function get_regarding_name($regarding){
	return $this->Customer_account->get_regarding_name($regarding);
}
public function get_service_name($service){
	return $this->Customer_account->get_service_name($service);
}
public function get_category_name($category){
	return $this->Customer_account->get_category_name($category);
}

/*function for complaint_create ends*/

/*function for complaint_thread start*/

public function complaint_thread($id){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="comp_det_cas";
		$data["case_id"]=$id;
		$customer_id=$this->session->userdata("customer_id");
		$complaint_thread_id=$id;
		$data["master_complaint_data"]=$this->Customer_account->get_master_data_of_complaints($complaint_thread_id);
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/complaint_thread');
		$this->load->view('footer');
	}
	else{
		redirect(base_url());
	}
}

public function get_complaint_case_present_status($id){
	if($id==0){
		return "Pending Admin Action";
	}
	if($id==1){
		return "Pending Customer Action";
	}
	if($id==2){
		return "Resolved By Customer";
	}
	if($id==3){
		return "Resolved By Admin";
	}
	if($id==4){
		return "Disputed";
	}
}

public function get_complaint_case_user_fullname($id){
	return $this->Customer_account->get_complaint_case_user_fullname($id);
}

public function change_complaint_case_status(){
if($this->session->userdata("logged_in")){
	$case_id=$this->input->post('case_id');
	$action=$this->input->post('action');
	$success=$this->Customer_account->change_complaint_case_status($case_id,$action);
	if($success){
		echo "1";
	}
	else{
		echo "0";
	}
	}
	else{
		redirect(base_url());
	}	
}

public function add_conversation_to_complaint(){
	extract($_POST);

	//generate a $thread_id
	$date = new DateTime();
	$mt=microtime();
	$salt="thread_id";
	$rand=mt_rand(1, 1000000);
	$ts=$date->getTimestamp();
	$str=$salt.$rand.$ts.$mt;
	$thread_id=md5($str);
	$complaint_thread_id=$case_id;
	$user_type=$this->session->userdata('user_type');
	$customer_id=$this->session->userdata("customer_id");
	if(!empty($_FILES["filesToUpload"]["name"][0])){
			if (!file_exists($folder="assets/complaints_attachment")) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
			
					if (!file_exists($folder=$folder.'/'.$complaint_thread_id)) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
					if (!file_exists($folder=$folder.'/'.$thread_id)) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
					
					for($i=0;$i<count($_FILES["filesToUpload"]["name"]);$i++){
						$mask=umask(0);
						mkdir($folder.'/'.$i,0777);
						umask($mask);
						$sourcePath=$_FILES["filesToUpload"]["tmp_name"][$i];
						$targetPath=$folder.'/'.$i."/".$_FILES["filesToUpload"]["name"][$i];
						$file_path=$targetPath;
						if(move_uploaded_file($sourcePath,$targetPath)){						
							$this->Customer_account->save_complaint_attachment($file_path,$thread_id,$complaint_thread_id,$customer_id);
						}
					}
				}
	$action=0;
		$this->Customer_account->change_complaint_case_status($complaint_thread_id,$action);
		$this->Customer_account->add_thread_to_complaints($complaint_thread_id,$thread_id,$user_type,$customer_id,$discription,$contact_method,$country_code,$phone_number,$ext_number);
}

public function get_all_thread_complaint(){
	$complaint_id=$this->input->post('complaint_id');
	$all_complaint_data=$this->Customer_account->get_all_thread_complaint($complaint_id);
	if(!empty($all_complaint_data)){
		$threadscontainer="";
		$email_stuff_arr=email_stuff();
		foreach($all_complaint_data as $complaint_data){
			
			$timestamp = $complaint_data['timestamp'];
			$splitTimeStamp = explode(" ",$timestamp);
			$date = $splitTimeStamp[0];
			$time = $splitTimeStamp[1];
			$date = date('M d, Y',strtotime($timestamp));
			$time = date('H:i A',strtotime($timestamp));
			
			$threadscontainer.='<div class="row">';
			if($complaint_data['created_by']!="customer"){
				$threadscontainer.='<div class="col-sm-4 text-right">
		            				<p><b>'.ucfirst($complaint_data['created_by']).'</b></p>
		            				<p>'.$date.'</p><p>'.$time.'</p>';
					if($complaint_data['contact_method']=="web"){
						 $threadscontainer.='<p><i class="fa fa-desktop fa-2x" aria-hidden="true"></i></p>';
					}
					else{
						$threadscontainer.='<p><i class="fa fa-phone fa-2x" aria-hidden="true"></i></p>
											<p>Phone No. :+'.$complaint_data['phone_country'].' '.$complaint_data['phone_number'].'</p>';
							if(!empty($complaint_data['phone_extension'])){
								$threadscontainer.='<p>'.$complaint_data['phone_extension'].'</p>';
							}
											
						
						
					}
		          
		            $threadscontainer.='</div>';
				$checked1="";$checked2="";$checked3="";$checked4="";$checked5="";
				$choiceVal="";
				if($complaint_data['feed_back_rating']==1){
					$checked1="checked";
					$choiceVal="Poor";
				}
				if($complaint_data['feed_back_rating']==2){
					$checked2="checked";
					$choiceVal="Below Average";
				}
				if($complaint_data['feed_back_rating']==3){
					$checked3="checked";
					$choiceVal="Average";
				}
				if($complaint_data['feed_back_rating']==4){
					$checked4="checked";
					$choiceVal="Very Good";
				}
				if($complaint_data['feed_back_rating']==5){
					$checked5="checked";
					$choiceVal="Excellent";
				}
				
				$threadscontainer.='<div class="col-sm-8 well" style="background-color: #eee;">
								'.$complaint_data['descriptions'].'<div class="col-sm-12">'.$this->get_all_attachment_of_thread_id($complaint_data['thread_id']).'</div>
	            			<div class="row">
	            			<div class="col-sm-12">
	            			<br>	
	            			<p>Thank you for using Business Division</p>
	            			<br>
	            			<p>Best regards,</p>
	            			<br>
	            			<p>Admin.</p>
	            			<p>'.$email_stuff_arr["regardsteam_emailtemplate"].'</p>
	            			<p>We value your feedback. Please rate my response using the link below.</p>
	            			<br>
	            			<div class="elem"></div>
	            			</div>	
	            			</div>
	            			<div class="row">
	            				<br><br>
	            				<div class="col-sm-12 well">
	            					<p>Was this response helpful? Click here to rate:</p>
	            					<div class="col-sm-6 star-rating">
									  <input type="radio" '.$checked1.'  onmouseover="starValue(this)" onmouseout="starFinalValue(this)" onclick="save_star_val(this)" thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Poor" value="1"><i></i>
									  <input type="radio" '.$checked2.' onmouseover="starValue(this)" onmouseout="starFinalValue(this)" onclick="save_star_val(this)" thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Below Average" value="2"><i></i>
									  <input type="radio" '.$checked3.' onmouseover="starValue(this)" onmouseout="starFinalValue(this)" onclick="save_star_val(this)" thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Average" value="3"><i></i>
									  <input type="radio" '.$checked4.' onmouseover="starValue(this)" onmouseout="starFinalValue(this)" onclick="save_star_val(this)" thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Very Good " value="4"><i></i>
									  <input type="radio" '.$checked5.' onmouseover="starValue(this)" onmouseout="starFinalValue(this)" onclick="save_star_val(this)" thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Excellent" value="5"><i></i>
									</div>
									<div class="col-sm-6 choice" id="selectedStarVal_'.$complaint_data['thread_id'].'">'.$choiceVal.'</div>
	            				</div>	
	            			</div>	
            			</div>';
				
			}
			else{
				$threadscontainer.='<div class="col-sm-4 text-right">
		            				<p><b>'.$this->get_complaint_case_user_fullname($complaint_data['user_id']).'</b></p>
		            				<p>'.$date.'</p><p>'.$time.' +0530</p>';
					if($complaint_data['contact_method']=="web"){
						 $threadscontainer.='<p><i class="fa fa-desktop fa-2x" aria-hidden="true"></i></p>';
					}
					else{
						$threadscontainer.='<p><i class="fa fa-phone fa-2x" aria-hidden="true"></i></p>
											<p>Phone No. :+'.$complaint_data['phone_country'].'-'.$complaint_data['phone_number'];
							if(!empty($complaint_data['phone_extension'])){
								$threadscontainer.='-'.$complaint_data['phone_extension'];
							}
											
					$threadscontainer.='</p>';
						
					}
		          
		            $threadscontainer.='</div>';
				$threadscontainer.='<div class="col-sm-8 well" style="background-color: #eee;">'.stripslashes($complaint_data['descriptions']).'<div class="col-sm-12">'.$this->get_all_attachment_of_thread_id($complaint_data['thread_id']).'</div></div>';
			}					
							
			
			$threadscontainer.='</div><hr>';
		}	
		
		echo $threadscontainer;	
	}
	else{
		echo "No Data To Show";
	}
}

public function get_all_attachment_of_thread_id($id){
	$all_attachments=$this->Customer_account->get_all_attachment_of_thread_id($id);
	$str="";
	if(!empty($all_attachments)){
		$str.='<hr>';
		$str.='<p>Attachments</p>';
		$str.='<p><br></p>';
		foreach($all_attachments as $data){
			$str.='<p><a href="'.base_url().$data['file_path'].'" download>'.basename($data['file_path']).'</a></p>';
		}
	}
	return $str;
}

public function save_star_ratings_of_thread(){
	if($this->session->userdata("logged_in")){
	$thread_id=$this->input->post('thread_id');
	$star_val=$this->input->post('star_val');
	$success=$this->Customer_account->save_star_ratings_of_thread($thread_id,$star_val);
	if($success){
		echo "1";
	}
	else{
		echo "0";
	}

	}
	else{
		redirect(base_url());
	}
}

/*function for complaint_thread ends*/


public function test(){
	if($this->session->userdata("logged_in")){


	}
	else{
		redirect(base_url());
	}	
		
}





////////////////////////////////////////////////////////////////				
		/* clone from template controller starts*/
		public function provide_parent_category()
			{
				$method=current_url();
				$this->session->set_userdata("cur_page",$method);
				$data['page_name']='';
				$data['page_type']='';
				$data['page_id']='';
				$data['products_obj']='';
				$data['p_category'] = $this->Customer_frontend->provide_parent_category();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$this->load->view('header',$data);
			}
			public function provide_category($pcat_id)
			{
				return $this->Customer_frontend->provide_category($pcat_id);
				
			}
			public function provide_sub_category($cat_id)
			{
				$sub_cat=$this->Customer_frontend->provide_sub_category($cat_id);
				//$sub_cat=$this->Customer_frontend->provide_sub_category($cat_value->cat_id);
						
				$menu3=array();
				foreach ($sub_cat as $subcat_value)
				{
					$menu3[]=array('subcat_id'=>$subcat_value->subcat_id,'subcat_name'=>$subcat_value->subcat_name,'order'=>$subcat_value->menu_sort_order);			
				}
					@$this->sortBy('order',$menu3);
				return $menu3;
		
			}
			public function provide_brands($subcat_id)
			{
				return $this->Customer_frontend->provide_brands($subcat_id);
				
			}
			public function provide_first_level_menu()
			{
				$p_menu=$this->Customer_frontend->provide_parent_category_level();
		
				$menu1=array();
				
				foreach($p_menu as $val)
				{
					if($val->menu_level==1)
					{
						$menu1[]=array('parent_menu'=>$val->pcat_name,'type'=>'parent_cat','id'=>$val->pcat_id,'order'=>$val->menu_sort_order,'show_sub_menu'=>$val->show_sub_menu);
					}
				}
				
				$s_menu=$this->Customer_frontend->provide_category_level();
				
				foreach($s_menu as $value)
				{
					if($value->menu_level==1)
					{
						$menu1[]=array('parent_menu'=>$value->cat_name,'type'=>'cat','id'=>$value->cat_id,'order'=>$value->menu_sort_order);
					}
				}
				@$this->sortBy('order',$menu1);
				//echo '<pre>';
				//print_r($menu1);
				//echo '</pre>';
				
				return $menu1;
			}
			public function get_all_tree_of_products($p_id)
			{
				$data=$this->Customer_frontend->get_all_tree_of_products($p_id);
				
				return $data;
			}
			public function sortBy($field,&$array,$direction='asc')
			{
				/*usort($array, create_function('$a, $b', '
					$a = $a["' . $field . '"];
					$b = $b["' . $field . '"];
		
					if ($a == $b)
					{
						return 0;
					}
		
					return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
				'));*/
		
				return true;
			}
			
			public function to_return_value($p1,$p2,$p3)
			{
				$arr=array();
				$pcat_id=substr($p1, 9);$cat_id=substr($p2, 9);$subcat_id=substr($p3, 9);
				
				if(!empty($pcat_id)){
					if(!ctype_digit($pcat_id))
					{
						redirect(base_url()."error_to_view");
					}
				}
				if(!empty($cat_id)){
					if(!ctype_digit($cat_id))
					{
						redirect(base_url()."error_to_view");
						
					}
				}
				if(!empty($subcat_id)){
					if(!ctype_digit($subcat_id))
					{
						redirect(base_url()."error_to_view");
					}
				}
				if(!empty($pcat_id) && is_numeric($pcat_id))
				{
		
					$p_name=$this->Customer_frontend->get_name_of_parent($pcat_id);
					if(isset($p_name[0]->pcat_name))
					{
						$arr['pcat_name']=$p_name[0]->pcat_name;
					}
					else
					{
						redirect(base_url()."error_to_view");
					}
					
				}
				
				if(!empty($cat_id) && is_numeric($cat_id))
				{
					$cat_name=$this->Customer_frontend->get_name_of_category($pcat_id,$cat_id);
					
					if(isset($cat_name[0]->cat_name))
					{
						$arr['cat_name']=$cat_name[0]->cat_name;
						
					}
					else
					{
						redirect(base_url()."error_to_view");
					}
					
				}
				
				if(!empty($subcat_id) && is_numeric($subcat_id))
				{
					$subcat_name=$this->Customer_frontend->get_name_of_subcategory($pcat_id,$cat_id,$subcat_id);
					
					if(isset($subcat_name[0]->subcat_name))
					{
						$arr['subcat_name']=$subcat_name[0]->subcat_name;
					}
					else
					{
						redirect(base_url()."error_to_view");
					}
				}
				return $arr;
		
			}
			public function get_list_of_filters($p1,$p2,$p3)
			{
		
				$arr=array();
				$pcat_id=substr($p1, 9);$cat_id=substr($p2, 9);$subcat_id=substr($p3, 9);
				
				if(!empty($pcat_id)){
					if(!ctype_digit($pcat_id))
					{
						redirect(base_url()."error_to_view");
					}
				}
				if(!empty($cat_id)){
					if(!ctype_digit($cat_id))
					{
						redirect(base_url()."error_to_view");
						
					}
				}
				if(!empty($subcat_id)){
					if(!ctype_digit($subcat_id))
					{
						redirect(base_url()."error_to_view");
					}
				}
				if(!empty($pcat_id) && is_numeric($pcat_id))
				{
						$arr['pcat_id']=$pcat_id;
				}
				
				if(!empty($cat_id) && is_numeric($cat_id))
				{
						$arr['cat_id']=$cat_id;
				}
				
				if(!empty($subcat_id) && is_numeric($subcat_id))
				{	
						$arr['subcat_id']=$subcat_id;
				}
				
				$data=$this->Customer_frontend->get_list_of_filters($arr);
		
				usort($data, array($this, "cmp"));
		
				//echo '<pre>';
				//print_r($data);
				//echo '</pre>';
				
				//exit;
				return $data;
			}
			public function get_count_of_products($f_id)
			{
				$data=$this->Customer_frontend->get_count_of_products($f_id);
				return $data;
			}
			public function get_list_of_filter_options($filter_box_id)
			{
				$data=$this->Customer_frontend->get_list_of_filter_options($filter_box_id);
				//echo '<pre>';
				//print_r($data);
				//echo '</pre>';
				
				//exit;
				return $data;
			}
			
			public function to_return_ids($p1,$p2,$p3)
			{
				$arr=array();
				$pcat_id=substr($p1, 9);$cat_id=substr($p2, 9);$subcat_id=substr($p3, 9);
				
				if(!empty($pcat_id)){
					if(!ctype_digit($pcat_id))
					{
						redirect(base_url()."error_to_view");
					}
				}
				if(!empty($cat_id)){
					if(!ctype_digit($cat_id))
					{
						redirect(base_url()."error_to_view");
						
					}
				}
				if(!empty($subcat_id)){
					if(!ctype_digit($subcat_id))
					{
						redirect(base_url()."error_to_view");
					}
				}
				if(!empty($pcat_id) && is_numeric($pcat_id))
				{
						$arr['pcat_id']=$pcat_id;
				}
				
				if(!empty($cat_id) && is_numeric($cat_id))
				{
						$arr['cat_id']=$cat_id;
				}
				
				if(!empty($subcat_id) && is_numeric($subcat_id))
				{	
						$arr['subcat_id']=$subcat_id;	
				}
				$data=$this->to_return_products($arr);
				
				return $data;
			}
			
			public function to_return_products($arr)
			{
				$ids = implode(', ',$arr);
		
				$data=$this->Customer_frontend->to_return_products($arr);
				
				return $data;
				/*echo '<pre>';
				print_r($data);
				echo '</pre>';
				exit;*/
			}
			
			public function sample_code()
			{
				$seed = str_split('abcdefghijklmnopqrstuvwxyz'
		                     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		                     .'0123456789'); // and any other characters
						shuffle($seed); // probably optional since array_is randomized; this may be redundant
						$rand = '';
						foreach (array_rand($seed, 9) as $k) $rand .= $seed[$k];	
						return $rand;
			}
			
			public function incrementalHash($len = 5){//NOT USED
			  $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
			  $base = strlen($charset);
			  $result = '';
		
			  $now = explode(' ', microtime())[1];
			  while ($now >= $base){
				$i = $now % $base;
				$result = $charset[$i] . $result;
				$now /= $base;
			  }
			  echo substr($result, -5);
			}
			public function cmp($a, $b)
			{
				return strcmp($a->sort_order, $b->sort_order);
			}
		
		/* clone from template controller ends*/
		
		
	public function carttable_dump(){
		$localstorage_data = json_decode(file_get_contents("php://input"));
		$flag=$this->Customer_account->carttable_dump($localstorage_data);
		echo $flag;
	}
	public function carttable_update_deviations(){
		$this->Customer_account->carttable_update_deviations();
	}
	public function carttable_dump_combo(){
		$localstorage_data = json_decode(file_get_contents("php://input"));
		$flag=$this->Customer_account->carttable_dump_combo($localstorage_data);
		echo $flag;
	}	
	public function carttable_dump_combo_search(){
		$localstorage_data = json_decode($this->input->post('combo_products'));
		print_r(json_decode($localstorage_data));
		$localstorage_data=(json_decode($localstorage_data));

		$flag=$this->Customer_account->carttable_dump_combo($localstorage_data);
		echo $flag;
	}	
	
	public function carttable_dump_combo_search_delete(){
		
		$flag=$this->Customer_account->carttable_dump_combo_search_delete();
		echo $flag;
	}	
	public function get_combo_cart(){
		$res=$this->Customer_account->get_combo_cart();
		echo json_encode($res);
	}
	public function get_cart_count(){
		$cart_count=$this->Customer_account->get_cart_count();
		echo $cart_count;
	}
	public function carttable_exists_check(){
		$status_carttable_updation=$this->Customer_account->carttable_exists_check($this->input->post());
		echo $status_carttable_updation;
	}
	public function carttable_dump_for_session_customers(){
		$status_carttable_updation=$this->Customer_account->carttable_dump_for_session_customers($this->input->post());
		echo $status_carttable_updation;
	}
	public function update_cart_changed_moq(){
		$localstorage_data = json_decode(file_get_contents("php://input"));
		$status_carttable_updation=$this->Customer_account->update_cart_changed_moq($localstorage_data);
		echo $status_carttable_updation;
	}
	public function delete_carttable_for_customer(){
		$status_carttable_updation=$this->Customer_account->delete_carttable_for_customer();
	}
	public function get_cartinfo_in_json_format(){
		$res=$this->Customer_account->get_cartinfo_in_json_format();
		$change_in_state_fin=0;

		foreach($res as $resObj){
			
			$cart_id=$resObj["cart_id"];
			/* check addons are active  */

			$addon_products='';
			$addon_products_status=0;
			$addon_inventories='';
			$addon_total_price=0;
			$addon_total_shipping_price=0;
			$addon_products_fin='';
			
			$change_in_state=0;$temp=[];$inv_arr=[];

			if($resObj["addon_products_status"]=='1'){
				
				$addon_products=json_decode($resObj["addon_products"]);

				//print_r($addon_products);
				$total_inv_price=0;
				if(!empty($addon_products)){
				
					foreach($addon_products as $key2=> $addon){
						//print_r($addon);
							$inv_id=$addon->inv_id;
							/* check if inv exists */
							$data=get_inv_info_active($inv_id);
							if(!empty($data)){
								$inv_arr[]=$inv_id;
								$temp[]=$addon_products[$key2];
								$total_inv_price+=($addon->inv_price);
							}else{
								$change_in_state++;
								$change_in_state_fin++;
								unset($addon_products[$key2]);
							}
							/* check if inv exists */
						

					}
				}
				
				
			
				if(empty($temp)){
					$addon_products_fin='';
					$addon_products_status='0';
					$addon_inventorie='';
					$addon_total_price=$total_inv_price;
					$addon_total_shipping_price='0';
				}else{
					$addon_products_fin=json_encode($temp);
					$addon_products_status='1';
					$addon_total_price=$total_inv_price;
					$addon_inventories=implode(',',$inv_arr);
					$addon_total_shipping_price='0';
				}
			}

			if($change_in_state>0){
				/* update carttable */
					$resObj["addon_products"]=$addon_products_fin;
					$resObj["addon_products_status"]='1';
					$resObj["addon_total_price"]=$total_inv_price;
					$resObj["addon_inventories"]=implode(',',$inv_arr);
					$resObj["addon_total_shipping_price"]='0';

					//$addon_products=json_encode($addon_products_fin);

					$res1=$this->Customer_account->update_addon_changes_in_cart($cart_id,$addon_products_fin,$addon_products_status,$addon_inventories,$addon_total_price,$addon_total_shipping_price);

				/* update carttable */
			}

			/* check addons are active  */
		}
		
		if($change_in_state_fin>0){
			//$res=$this->Customer_account->get_cartinfo_in_json_format();
		}
		echo json_encode($res);
	}
	

	public function get_specific_available_delivery_mode_new($logistics_id,$logistics_territory_id,$vendor_id){
		$all_available_delivery_mode_obj=$this->Customer_account->get_specific_available_delivery_mode_new($logistics_id,$logistics_territory_id,$vendor_id);
		return $all_available_delivery_mode_obj;
	}
	
	public function get_cartinfo_in_json_format_checkout(){

		$res=$this->Customer_account->get_cartinfo_in_json_format_checkout();

		$get_shipping_address_pincode_arr=$this->Customer_account->get_shipping_address_pincode();
	
		$pincode=$get_shipping_address_pincode_arr["pincode"];
		
		$arr=array();
		$check_availability_pincode_temp_arr=array();
		$check_availability_pincode_arr=array();
		foreach($res as $resObj){
			$cart_id=$resObj["cart_id"];
			$inventory_id=$resObj["inventory_id"];
			$vendor_id=$resObj["vendor_id"];
			$result=$this->Model_search->get_default_logistics_id_for_all_vendors($vendor_id);
			$logistics_id_arr=array();
			foreach($result as $res){
				$logistics_id_arr[]=$res->logistics_id;
			}
			$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr=$this->Model_search->get_logistics_id_territory_name_id_by_pincode_vendor_id($logistics_id_arr,$pincode,$vendor_id);
			
			$logistics_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["logistics_id"];
			$territory_name_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["territory_name_id"];
			$flag=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["check_logistics_yes"];
			$check_availability_pincode_temp_arr[]=$flag;
			
		}
		$check_availability_at_lease_one="no";
		foreach($check_availability_pincode_temp_arr as $val){
			if($val==true){
				$check_availability_at_lease_one="yes";
				break;
			}
		}
		if($check_availability_at_lease_one=="no"){
			echo json_encode(array("pincode"=>$pincode,"check_availability_at_lease_one"=>$check_availability_at_lease_one));
			exit;
		}
		$res=$this->Customer_account->get_cartinfo_in_json_format_checkout();
		foreach($res as $resObj){
			$darr=array();
			$parr=array();
			$cart_id=$resObj["cart_id"];
			$inventory_id=$resObj["inventory_id"];
			$inventory_sku=$resObj["inventory_sku"];
                        /* check coupon availability */
                        $cp_data=$this->Customer_account->check_coupon_available($inventory_id,$inventory_sku);
                        /* check coupon availability */
                        if(!empty($cp_data)){
                            $resObj["cp_available_status"]=1;
                        }else{
                            $resObj["cp_available_status"]=0;
                        }
			$vendor_id=$resObj["vendor_id"];
			$result=$this->Model_search->get_default_logistics_id_for_all_vendors($vendor_id);
			$logistics_id_arr=array();
			foreach($result as $res){
				$logistics_id_arr[]=$res->logistics_id;
			}
			
			$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr=$this->Model_search->get_logistics_id_territory_name_id_by_pincode_vendor_id($logistics_id_arr,$pincode,$vendor_id);
			
			$logistics_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["logistics_id"];
			$territory_name_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["territory_name_id"];
			$flag=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["check_logistics_yes"];
			$check_availability_pincode_arr[]=$flag;
			$shipping_detail=$this->get_territory_of_pincode($pincode,$vendor_id,$logistics_id,$territory_name_id);
			$logistics_territory_id=$shipping_detail['logistics_territory_id'];
			$logistics_id=$shipping_detail['logistics'];
			$resObj["check_availability"]=$flag;
			if($flag==true){
				$this->Customer_account->update_logistics_territory_id($inventory_id,$vendor_id,$logistics_territory_id,$logistics_id,$cart_id);

				$delivery_modes=$this->get_specific_available_delivery_mode_new($logistics_id,$logistics_territory_id,$resObj["vendor_id"]);
				$resObj["delivery_modes"]=$delivery_modes;	
				
				foreach($delivery_modes as $dvarr){
					$darr[]=$dvarr["logistics_delivery_mode_id"];
				}
				$resObj["delivery_mode_ids"]=implode(",",$darr);
			
				$parcel_categories=$this->Customer_account->get_all_parcel_categories_by_logistics_id($resObj["parcel_category_ids"],$logistics_id);
				$resObj["parcel_categories"]=$parcel_categories;
				
				foreach($parcel_categories as $varr){
					$parr[]=$varr["logistics_parcel_category_id"];
				}
				$resObj["parcel_category_ids"]=implode(",",$parr);
				
				$get_delivery_mode_and_parcel_category_combinations_arr=$this->Customer_account->get_delivery_mode_and_parcel_category_combinations($darr,$parr,$logistics_id,$territory_name_id,$cart_id);
				$resObj["get_delivery_mode_and_parcel_category_combinations_arr"]=$get_delivery_mode_and_parcel_category_combinations_arr;
			}


			/* check addons are active  */

			$addon_products='';
			$addon_products_status=0;
			$addon_inventories='';
			$addon_total_price=0;
			$addon_total_shipping_price=0;
			$addon_products_fin='';
			
			$change_in_state=0;$temp=[];$inv_arr=[];

			if($resObj["addon_products_status"]=='1'){
				
				$addon_products=json_decode($resObj["addon_products"]);

				//print_r($addon_products);
				$total_inv_price=0;
				if(!empty($addon_products)){
				
					foreach($addon_products as $key2=> $addon){
						//print_r($addon);
							$inv_id=$addon->inv_id;
							/* check if inv exists */
							$data=get_inv_info_active($inv_id);
							if(!empty($data)){
								$inv_arr[]=$inv_id;
								$temp[]=$addon_products[$key2];
								$total_inv_price+=($addon->inv_price);
							}else{
								$change_in_state++;
								unset($addon_products[$key2]);
							}
							/* check if inv exists */
						

					}
				}
				
				
			
				if(empty($temp)){
					$addon_products_fin='';
					$addon_products_status='0';
					$addon_inventorie='';
					$addon_total_price=$total_inv_price;
					$addon_total_shipping_price='0';
				}else{
					$addon_products_fin=json_encode($temp);
					$addon_products_status='1';
					$addon_total_price=$total_inv_price;
					$addon_inventories=implode(',',$inv_arr);
					$addon_total_shipping_price='0';
				}
			}

			if($change_in_state>0){
				/* update carttable */
					$resObj["addon_products"]=$addon_products_fin;
					$resObj["addon_products_status"]='1';
					$resObj["addon_total_price"]=$total_inv_price;
					$resObj["addon_inventories"]=implode(',',$inv_arr);
					$resObj["addon_total_shipping_price"]='0';

					//$addon_products=json_encode($addon_products_fin);

					$res=$this->Customer_account->update_addon_changes_in_cart($cart_id,$addon_products_fin,$addon_products_status,$addon_inventories,$addon_total_price,$addon_total_shipping_price);

				/* update carttable */
			}

			/* check addons are active  */


				$arr[]=$resObj;
			
			}
			if(in_array(false,$check_availability_pincode_arr)){
				$check_availability="no";
			}
			else{
				$check_availability="yes";
			}
			$check_availability="yes";

			echo json_encode(array("get_cartinfo_in_json_format_checkout"=>$arr,"pincode"=>$pincode,"check_availability"=>$check_availability,"check_availability_at_lease_one"=>$check_availability_at_lease_one));
		
	}
	
	public function update_and_get_cartable_info_in_json_format(){
		
		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));

		if($update_cart_info_post_data->logistics_id!=-1){
			
			$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($update_cart_info_post_data->inventory_id);
				if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
					$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
					$logistics_territory_id=$update_cart_info_post_data->logistics_territory_id;
					// the below two conditions only for hurbz launch only local and karnataka territories enabled
					/*if($logistics_territory_id==1 || $logistics_territory_id==6 || $logistics_territory_id==14){
						$logistics_price=60;
					}
					if($logistics_territory_id==2 || $logistics_territory_id==7 || $logistics_territory_id==15){
						$logistics_price=150;
					}*/
				}
				else{
		$logistics_price=$this->Customer_account->get_logistics_price($update_cart_info_post_data);
				}
		$logistics_territory_id=$update_cart_info_post_data->logistics_territory_id;
		$default_delivery_mode_id=$update_cart_info_post_data->default_delivery_mode_id;
		
		$shipping_charge=$logistics_price;
		$res=$this->Customer_account->update_cart_info($update_cart_info_post_data,$logistics_price,$shipping_charge);
		}else{
			$logistics_price=-1;
			$shipping_charge=-1;
			$res=$this->Customer_account->update_cart_info($update_cart_info_post_data,$logistics_price,$shipping_charge);
		}		

		if($res==true){
			echo $this->get_cartinfo_in_json_format();
		}
		
		
		//echo $res;
	}
	public function update_and_get_cartable_info_in_json_format_checkout(){
		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));

		if($update_cart_info_post_data->logistics_id!=-1){
			
			$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($update_cart_info_post_data->inventory_id);
				if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
					$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
					$logistics_territory_id=$update_cart_info_post_data->logistics_territory_id;
					// the below two conditions only for hurbz launch only local and karnataka territories enabled
					/*if($logistics_territory_id==1 || $logistics_territory_id==6 || $logistics_territory_id==14){
						$logistics_price=60;
					}
					if($logistics_territory_id==2 || $logistics_territory_id==7 || $logistics_territory_id==15){
						$logistics_price=150;
					}*/
					
					
				}
				else{
					$logistics_price=$this->Customer_account->get_logistics_price($update_cart_info_post_data);
				}
		$logistics_territory_id=$update_cart_info_post_data->logistics_territory_id;
		$default_delivery_mode_id=$update_cart_info_post_data->default_delivery_mode_id;
	
		
		$shipping_charge=$logistics_price;
		$res=$this->Customer_account->update_cart_info($update_cart_info_post_data,$logistics_price,$shipping_charge);
		}else{
			$logistics_price=-1;
			$shipping_charge=-1;
			$res=$this->Customer_account->update_cart_info($update_cart_info_post_data,$logistics_price,$shipping_charge);
		}		

		if($res==true){
			echo $this->get_cartinfo_in_json_format_checkout();
		}
		
		
		//echo $res;
	}
	public function update_and_get_cartable_info_in_json_format_multiple_checkout(){
		$valid_cart_items_count=0;
		$temp=0;
		$default_shipping_detail=array();

		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));
		foreach($update_cart_info_post_data as $data_value){
			if($data_value->logistics_id!=-1){
				$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($data_value->inventory_id);
				if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
					$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
					$logistics_territory_id=$data_value->logistics_territory_id;
					// the below two conditions only for hurbz launch only local and karnataka territories enabled
					/*if($logistics_territory_id==1 || $logistics_territory_id==6 || $logistics_territory_id==14){
						$logistics_price=60;
					}
					if($logistics_territory_id==2 || $logistics_territory_id==7 || $logistics_territory_id==15){
						$logistics_price=150;
					}*/
					
				}
				else{

					/* check free territories are defined or not  */
					$default_shipping_detail=$this->get_default_shipping_charge($data_value->inventory_id,$data_value->logistics_territory_id,$data_value->logistics_id,$data_value->default_delivery_mode_id,$data_value->logistics_parcel_category_id,$data_value->logistics_price);

						$free_shipping_arr=$this->get_free_shipping_territories_by_inventory_id($data_value->inventory_id);
							
						$logistics_territory_name=$this->get_territory_name_by_territory_id($data_value->logistics_territory_id);
						
						if($free_shipping_arr!=false){	

							if(in_array($logistics_territory_name,$free_shipping_arr)) {
								$default_shipping_detail['shipping_charge']=0;
							}
						}
						
					$shipping_charge=$default_shipping_detail['shipping_charge'];
					$shipping_charge=round($shipping_charge);
					
					if($shipping_charge==0){
						$logistics_price=$shipping_charge;
					}else{
						$logistics_price=$this->Customer_account->get_logistics_price($data_value);
					}
					/* check free territories are defined or not  */
				}
				
			$logistics_territory_id=$data_value->logistics_territory_id;
			$default_delivery_mode_id=$data_value->default_delivery_mode_id;
			///$shipping_charge=$logistics_price;				
			$shipping_charge=round($shipping_charge);
			// the below if condition only for hurbz launch
			$valid_cart_items_count++;
			/*$shipping_charge_applied_index_arr=[];
			for($shipping_charge_applied_index=1;$shipping_charge_applied_index<=100;$shipping_charge_applied_index=$shipping_charge_applied_index+4){
				$shipping_charge_applied_index_arr[]=$shipping_charge_applied_index;
			}
			if(!in_array($valid_cart_items_count,$shipping_charge_applied_index_arr)){
				$logistics_price=0;
				$shipping_charge=0;
			}*/
			$res=$this->Customer_account->update_cart_info($data_value,$logistics_price,$shipping_charge);
			}else{
				$logistics_price=-1;
				$shipping_charge=-1;
				$res=$this->Customer_account->update_cart_info($data_value,$logistics_price,$shipping_charge);
			}		
			//echo $res;
			if($res!=true){
				$temp++;
			}
		}
		
		if($temp==0){
			echo $this->get_cartinfo_in_json_format_checkout();
		}
	}
	public function get_default_shipping_charge($inventory_id,$logistics_territory_id,$logistics_id,$default_delivery_mode_id,$logistics_parcel_category_id,$logistics_price){
		
		//echo $logistics_price;
		
		$logistics_territory_name=$this->get_territory_name_by_territory_id($logistics_territory_id);
		$free_shipping_arr=$this->get_free_shipping_territories_by_inventory_id($inventory_id);
		$territory_charge=$logistics_price;
		if($free_shipping_arr!=false){	

			if(in_array($logistics_territory_name,$free_shipping_arr)) {			
			$shipping_charge=0;	
			}
			else{
				//$shipping_charge=0;
				$shipping_charge=$territory_charge;
				//$shipping_charge=$logistics_price;
			}
		}else{
			
			 $shipping_charge=$territory_charge;
			 //$shipping_charge=$logistics_price;
			
		}	
		$shipping_charge=round($shipping_charge);
		
		$default_shipping_detail=array('shipping_charge'=>$shipping_charge);
		return $default_shipping_detail;
	
	}
	public function get_territory_name_by_territory_id($logistics_territory_id){
		$territory_name=$this->Model_search->get_territory_name_by_territory_id($logistics_territory_id);
		return $territory_name;
	}
	public function get_free_shipping_territories_by_inventory_id($inventory_id){
		$free_shipping_territories_arr=$this->Model_search->get_free_shipping_territories_by_inventory_id($inventory_id);
		
		$arr=array();
		if($free_shipping_territories_arr!=false){
			
			foreach($free_shipping_territories_arr as $free_shipping_territories_arr_value){
				$arr[]=$free_shipping_territories_arr_value["territory_name"];
			}
		return $arr;
		}else{
			return false;
		}
	}
	public function update_and_get_cartable_info_in_json_format_multiple(){
		$temp=0;
		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));
		$valid_cart_items_count=0;
		foreach($update_cart_info_post_data as $data_value){
			if($data_value->logistics_id!=-1){
				
				$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($data_value->inventory_id);
				if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
					$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
					$logistics_territory_id=$data_value->logistics_territory_id;
					// the below two conditions only for hurbz launch only local and karnataka territories enabled
					/*if($logistics_territory_id==1 || $logistics_territory_id==6 || $logistics_territory_id==14){
						$logistics_price=60;
					}
					if($logistics_territory_id==2 || $logistics_territory_id==7 || $logistics_territory_id==15){
						$logistics_price=150;
					}*/
					
				}
				else{
					$logistics_price=$this->Customer_account->get_logistics_price($data_value);
				}
			$logistics_territory_id=$data_value->logistics_territory_id;
			$default_delivery_mode_id=$data_value->default_delivery_mode_id;
			$shipping_charge=$data_value->shipping_charge;
			$shipping_charge=round($shipping_charge);
			// the below if condition only for hurbz launch
			$valid_cart_items_count++;
			/*$shipping_charge_applied_index_arr=[];
			for($shipping_charge_applied_index=1;$shipping_charge_applied_index<=100;$shipping_charge_applied_index=$shipping_charge_applied_index+4){
				$shipping_charge_applied_index_arr[]=$shipping_charge_applied_index;
			}
			if(!in_array($valid_cart_items_count,$shipping_charge_applied_index_arr)){
				$logistics_price=0;
				$shipping_charge=0;
			}*/
			$res=$this->Customer_account->update_cart_info($data_value,$logistics_price,$shipping_charge);
			}else{
				$logistics_price=-1;
				$shipping_charge=-1;
				$res=$this->Customer_account->update_cart_info($data_value,$logistics_price,$shipping_charge);
			}		
			//echo $res;
			if($res!=true){
				$temp++;
			}


		}
		
		if($temp==0){
			echo $this->get_cartinfo_in_json_format();
		}
	}
	
	public function update_delivery_speed_and_get_cartable_info_in_json_format_checkout(){
		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));
		
		$cart_id=$update_cart_info_post_data->cart_id;
		
		$temp=0;
		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));
		$update_cart_info_post_data->cart_data->default_delivery_mode_id=$update_cart_info_post_data->logistics_delivery_mode_id;
		
		$data_value=$update_cart_info_post_data->cart_data;
		
			if($data_value->logistics_id!=-1){
				
				
				$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($data_value->inventory_id);
				if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
					$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
				}
				else{
			$logistics_price=$this->Customer_account->get_logistics_price($data_value);
				}
			
			$logistics_territory_id=$data_value->logistics_territory_id;
			$logistics_delivery_mode_id=$update_cart_info_post_data->logistics_delivery_mode_id;
			$get_logistics_delivery_mode_data_by_logistics_delivery_mode_id_arr=$this->Customer_account->get_logistics_delivery_mode_data_by_logistics_delivery_mode_id($logistics_delivery_mode_id);
			$duration=$get_logistics_delivery_mode_data_by_logistics_delivery_mode_id_arr["speed_duration"];
			
					
			$shipping_charge_post=$data_value->shipping_charge;
			if($shipping_charge_post==0){
				$shipping_charge=0;
			}else{
				$shipping_charge=$logistics_price;	
			}
			$shipping_charge=round($shipping_charge);
			
			$res=$this->Customer_account->update_cart_info_with_delivery_mode($data_value,$shipping_charge,$logistics_delivery_mode_id,$duration);
			}else{
				$logistics_price=-1;
				$shipping_charge=-1;
				$res=$this->Customer_account->update_cart_info($data_value,$logistics_price,$shipping_charge);
			}		

		if($res==true){
			echo $this->get_cartinfo_in_json_format_checkout();
		}
				
	}
	public function update_parcel_category_and_get_cartable_info_in_json_format_checkout(){
		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));
		
		$cart_id=$update_cart_info_post_data->cart_id;
		
		$temp=0;
		$update_cart_info_post_data->cart_data->logistics_parcel_category_id=$update_cart_info_post_data->logistics_parcel_category_id;
		
		$data_value=$update_cart_info_post_data->cart_data;
			if($data_value->logistics_id!=-1){
				
				$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($data_value->inventory_id);
				if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
					$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
					$logistics_territory_id=$data_value->logistics_territory_id;
					
				}
				else{
					$logistics_price=$this->Customer_account->get_logistics_price($data_value);
				}
			$logistics_territory_id=$data_value->logistics_territory_id;
			$logistics_delivery_mode_id=$data_value->default_delivery_mode_id;
			$logistics_parcel_category_id=$update_cart_info_post_data->logistics_parcel_category_id;
			
			
			$shipping_charge=$logistics_price;
			$res=$this->Customer_account->update_cart_info_with_parcel_category_id($data_value,$shipping_charge,$logistics_parcel_category_id);
			}else{
				$logistics_price=-1;
				$shipping_charge=-1;
				$res=$this->Customer_account->update_cart_info($data_value,$logistics_price,$shipping_charge);
			}		

		if($res==true){
			echo $this->get_cartinfo_in_json_format_checkout();
		}
	}
	
	public function get_shipping_charge_without_session()
	{
		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));
		if($update_cart_info_post_data->logistics_id!=-1){
			
			$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($update_cart_info_post_data->inventory_id);
				if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
					$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
					$logistics_territory_id=$update_cart_info_post_data->logistics_territory_id;
				}
				else{
					
		$logistics_price=$this->Customer_account->get_logistics_price($update_cart_info_post_data);
				}
		$logistics_territory_id=$update_cart_info_post_data->logistics_territory_id;
		$default_delivery_mode_id=$update_cart_info_post_data->default_delivery_mode_id;
		$shipping_charge=$logistics_price;
		}else{
			$logistics_price=-1;
			$shipping_charge=-1;
			
		}	
		$shipping_charge=round($shipping_charge);
		$arr=array('logistics_price'=>$logistics_price,'shipping_charge'=>$shipping_charge);
		echo json_encode($arr);
		
	}
	
	
	public function update_cart_info(){
		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));
		$res=$this->Customer_account->update_cart_info($update_cart_info_post_data);
		echo $res;
	}
	public function remove_cart_info_for_session_customer(){
		$remove_cart_info_post_data = json_decode(file_get_contents("php://input"));
		$res=$this->Customer_account->remove_cart_info_for_session_customer($remove_cart_info_post_data);
		echo $res;
	}
	public function remove_cart_info_for_session_customer_addon(){
		$remove_cart_info_post_data = json_decode(file_get_contents("php://input"));
		$res=$this->Customer_account->remove_cart_info_for_session_customer_addon($remove_cart_info_post_data);
		echo $res;
	}
	public function get_all_products_from_wishlist()
	{
		$all_products_from_wishlist_arr=$this->Customer_account->get_all_products_from_wishlist();

		return $all_products_from_wishlist_arr;
	}
	public function remove_wishlist(){
		$wishlist_id=$this->input->post("wishlist_id");
		$flag=$this->Customer_account->remove_wishlist($wishlist_id);
		echo $flag;
	}
	public function add_new_delivery_address(){
		$new_delivery_address_post_data = json_decode(file_get_contents("php://input"));
		$customer_id=$this->session->userdata("customer_id");
		$name=$new_delivery_address_post_data->name;
		$door_address=$new_delivery_address_post_data->door_address;
		$mobile=$new_delivery_address_post_data->mobile;
		
		$street_address=$new_delivery_address_post_data->street_address;
		$city=$new_delivery_address_post_data->city;
		$pin=$new_delivery_address_post_data->pin;
		$state=$new_delivery_address_post_data->state;
		$country=$new_delivery_address_post_data->country;
		$make_default=$new_delivery_address_post_data->make_default;
		$flag=$this->Customer_account->add_new_address($customer_id,$name,$mobile,$door_address,$street_address,$city,$state,$pin,$country,$make_default);
		echo $flag;
	}
	public function get_cartinfo_for_customer(){
		$result=$this->Customer_account->get_cartinfo_for_customer();
		return $result;
	}
	public function call_back_check_paytm(){

			if(!empty($_POST)){
			$checksum = (!empty($_POST['CHECKSUMHASH'])) ? $_POST['CHECKSUMHASH'] : '';
			unset($_POST['CHECKSUMHASH']);
			$verifySignature = PaytmChecksum::verifySignature($_POST, PAYTM_MERCHANT_KEY, $checksum);

			if($verifySignature){

				'<h4 class="text-success text-left">Checksum is verified. Transaction details are below:</h4>	';
				'<table class="table table-bordered">';
				foreach($_POST as $key => $value){
					'<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
			 }
				'</table>';
			} else {
			'<h3 class="text-danger">Checksum is not verified.</h3>';
			 }
		 } else {
			'<h3 class="text-danger">Empty POST Response</h3>';
		 }
    }
	public function confirm_order_process_temp(){
		$email_stuff_arr=email_stuff();
		$payment_post_data = json_decode(file_get_contents("php://input"));
		
		$payment=$payment_post_data->total_amount_to_pay;
                $order_id=$payment_post_data->order_id;
		$result=$this->get_cartinfo_for_customer();
		$payment_post_data_arr=array();
		foreach($payment_post_data as $k => $v){
			$payment_post_data_arr[$k]=$v;
		}
		$payment_post_data_json=json_encode($payment_post_data_arr);
		$flag=$this->Customer_account->insert_temp_payment_post_data($payment_post_data_json,$order_id);
		
		//$flag=$this->Customer_account->confirm_order_process_temp($result,$order_id,$payment_post_data);
	}
	public function failed_order_process($order_id,$razorpay_payment_id,$razorpay_order_id){

		$cur_checkout=$this->session->userdata("cur_checkout");
		if($cur_checkout=='checkout_combo'){
			$this->failed_order_process_combo($order_id);
			exit;
		}

		$result=$this->get_cartinfo_for_customer();
		//////////////////////////
		$get_temp_payment_post_data_arr=$this->Customer_account->get_temp_payment_post_data($order_id);
		$payment_post_data_json=$get_temp_payment_post_data_arr["payment_post_data_json"];
		$payment_post_data=json_decode($payment_post_data_json,false);
		$this->Customer_account->delete_temp_payment_post_data($order_id);

        //$razorpayPaymentId=$this->session->flashdata('razorpay_payment_id');
        //$razorpayOrderId=$this->session->flashdata('merchant_order_id');
        //$razorpayOrderId=$this->session->flashdata('razorpay_order_id');
		
		$razorpayPaymentId=$razorpay_payment_id;
		$razorpayOrderId=$razorpay_order_id;
				

        //////////////////////////
		$flag=$this->Customer_account->failed_order_process($result,$order_id,$payment_post_data,$razorpayPaymentId,$razorpayOrderId);
		header('Location:'.base_url()."failed/".$order_id);
	}
	public function failed_order_process_combo($order_id){
	
		//////////////////////////
		$get_temp_payment_post_data_arr=$this->Customer_account->get_temp_payment_post_data($order_id);
		$payment_post_data_json=$get_temp_payment_post_data_arr["payment_post_data_json"];
		$payment_post_data=json_decode($payment_post_data_json,false);
		$this->Customer_account->delete_temp_payment_post_data($order_id);
		
		$flag=$this->Customer_account->failed_order_process_combo($order_id);
		header('Location:'.base_url()."failed_combo/".$order_id);
	}

	public function confirm_order_process_combo($order_id){
		//echo $order_id.'<br>';
		//echo 'Combo Pack order';
		$flag=$this->Customer_account->confirm_order_process_combo($order_id);
	
		/* mail function  */
		$email_stuff_arr=email_stuff();
		$customer_id=$this->session->userdata("customer_id");
		$email=$this->Customer_account->get_email_db($customer_id);
		$mobile=$this->Customer_account->get_mobile_db($customer_id);
		$customer_name=$this->get_customer_name($customer_id);
		$mobile_number_of_admin=$this->Customer_account->get_mobile_number_of_admin();

		$payment_obj=paid_amount($order_id);$payment=0;
		if(!empty($payment_obj)){
			$payment=$payment_obj->TXNAMOUNT;
		}

		if($flag==true){
		
		if($mobile != '' ){
			$mobile_stuff_arr=mobile_stuff();
			$authKey = $mobile_stuff_arr["authKey_smstemplate"];
			$mobileNumber = "+91".$mobile;
			$senderId = $mobile_stuff_arr["senderId_smstemplate"];

			$message = urlencode("Thank You! We have received your order ID $order_id for ".curr_sym."$payment and is in process. We will keep you informed.");
			$route = "4";
			$postData = array(
				'authkey' => $authKey,
				'mobiles' => $mobileNumber,
				'message' => $message,
				'sender' => $senderId,
				'route' => $route
			);

			$url="https://control.msg91.com/api/sendhttp.php";
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $postData
			));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			$output = curl_exec($ch);

			if(curl_errno($ch))
			{
				echo 'error:' . curl_error($ch);
			}
			curl_close($ch);
			}//sms
			
			
			
			$mobile_stuff_arr=mobile_stuff();
			$authKey = $mobile_stuff_arr["authKey_smstemplate"];
			$Mobile_Number_Admin = "+91".$mobile_number_of_admin;
			$senderId = $mobile_stuff_arr["senderId_smstemplate"];
			
			

			$secondmessage = urlencode("There is an order with order ID $order_id for ".curr_sym."$payment. Please login to Admin Panel and review the order.");
			$route = "4";
			$postData = array(
				'authkey' => $authKey,
				'mobiles' => $Mobile_Number_Admin,
				'message' => $secondmessage,
				'sender' => $senderId,
				'route' => $route
			);

			$url="https://control.msg91.com/api/sendhttp.php";
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $postData
			));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			$output = curl_exec($ch);

			if(curl_errno($ch))
			{
				echo 'error:' . curl_error($ch);
			}
			curl_close($ch);
		if($email!=''){
			$shipping_address_id=$this->Customer_account->get_shipping_address_id($order_id);
			$shipping_address_table_data_arr=$this->Customer_account->get_shipping_address($shipping_address_id);
		
			foreach($shipping_address_table_data_arr as $shipping_address_table_data){
				$shipping_address='<table style="color:#565656;line-height:15px;font-size:15px;background-color:#fff;"><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;padding-bottom:15px;"><b>Items will be shipped to:</b></td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;"><b>'.$shipping_address_table_data['customer_name'].'</b></td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;"> <span>Mob : </span>' .$shipping_address_table_data['mobile'].'</td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;">'.$shipping_address_table_data['address1'].',</td></tr><tr><td colspan="2" style="color: #565656;line-height: 22px;font-size: 15px;">'.$shipping_address_table_data['address2'].',</td></tr><tr><td colspan="2" style="color: #565656;line-height:15px;font-size: 15px;">'.$shipping_address_table_data['city'].', '.$shipping_address_table_data['pincode'].'</td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;">'.$shipping_address_table_data['state'].', '.$shipping_address_table_data['country'].'</td></tr></table><p style="padding:0;margin:0;color:#565656;font-size: 15px;"><br/>Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br>';
			}

			$order_summary_mail_data_arr=$this->Customer_account->get_order_summary_mail_data($order_id);
			$order_details='';
			$order_details.='<table>';
			$order_details.='<tr>';
			$order_details.='<th width="20%">Image</th>';
			$order_details.='<th width="40%">Product</th>';
			$order_details.='<th width="10%">Item Price</th>';
			$order_details.='<th width="10%">Qty</th>';
			$order_details.='<th width="20%">Subtotal</th>';
			$order_details.='</tr>';
			foreach($order_summary_mail_data_arr as $order_summary_mail_data){
				
				/* addon section */
			//if($order_summary_mail_data['ord_addon_products_status']=='1'){
			if(0){
				$order_details.='<tr>';
				$order_details.='<td>';
					$arr=json_decode($order_summary_mail_data['ord_addon_products']);
					$count=count($arr);
					if($order_summary_mail_data['ord_addon_inventories']!=''){
						//$order_details.='<tr><th align="center" colspan="5" style="text-align: center;text-decoration: underline;">
						 //Combo Products - Included</th></tr>';
						$i=1;
						foreach($arr as $val){
							
							$name=$val->inv_name;
							$image=$val->inv_image;
							$sku_id=$val->inv_sku_id;
							$price=$val->inv_price;
							$order_details.='<tr>
							<td align="center"><img src="'.$image.'" style="width:85px;"></td>
							<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.$name.'<br>SKU id : '.$sku_id.'</p></td>
							<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.curr_sym.$price.'</td>
							<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">1</td>
							<td  width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.curr_sym.$price.'
							</td></tr>';
						}
						
				}
				$order_details.='</td>';
				$order_details.='</tr>';
		
			}
			/* addon section */
			

				/*$order_details.='<tr>';
				$order_details.='<td align="center">';
				$order_details.="<img src='".base_url().$order_summary_mail_data['thumbnail']."'>";
				$order_details.='</td>';
				$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
				$order_details.=$order_summary_mail_data['product_name']."<br>".$order_summary_mail_data['product_description']."<br>";

				
				if(!empty($order_summary_mail_data['attribute_1'])){
					$order_details.=$order_summary_mail_data['attribute_1']." : ".$order_summary_mail_data['attribute_1_value'].'<br>';
				}
				if(!empty($order_summary_mail_data['attribute_2'])){
					$order_details.=$order_summary_mail_data['attribute_2']." : ".$order_summary_mail_data['attribute_2_value'].'<br>';
				}
				if(!empty($order_summary_mail_data['attribute_3'])){
					$order_details.=$order_summary_mail_data['attribute_3']." : ".$order_summary_mail_data['attribute_3_value'].'<br>';
				}
				if(!empty($order_summary_mail_data['attribute_4'])){
					$order_details.=$order_summary_mail_data['attribute_4']." : ".$order_summary_mail_data['attribute_4_value'];
				}
				if(!empty($order_summary_mail_data['sku_id'])){
					$order_details.="SKU : ".$order_summary_mail_data['sku_id'];
				}
				
				$order_details.='</p>';
				$order_details.='</td>';
				$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
				$order_details.=curr_sym.''.round($order_summary_mail_data['product_price']);
				$order_details.='</td>';
				$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
				$order_details.=$order_summary_mail_data['quantity'];
				$order_details.='</p></td>';
				$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
				//$order_details.=curr_sym.''.$order_summary_mail_data['subtotal'];
				// $order_details.=curr_sym.''.round($order_summary_mail_data['quantity']*$order_summary_mail_data['ord_max_selling_price']);
				$order_details.=curr_sym.''.round($order_summary_mail_data['subtotal']);
				$order_details.='</td>';
				$order_details.='</tr>';


				*/


				$shipping_charge_modified=$order_summary_mail_data['shipping_charge'];
				$subtotal_modified=$order_summary_mail_data['grandtotal'];		

				$order_details.='<tr>';
				$order_details.='<td colspan=5 align="center">';
				$order_details.='<p style="margin:10px 0 0 0;padding:5px;background-color:#f9f9f9;border:1px solid #eee;color:#666;font-size:13px;text-align:center">(+) Delivery: '.curr_sym.' '.($shipping_charge_modified);
				$order_details.='</p></td>';
				$order_details.='</tr>';
				
				
				$order_details.='<tr>';
				$order_details.='<td colspan=5 align="center" style="padding:0 0 17px 0">';
				$order_details.='<p style="padding:4px 5px;background-color:#fffed5;border:1px solid #f9e2b2;color:#565656;margin:10px 0 0 0;text-align:center;font-size:12px">Delivery scheduled by '.date("D j M, Y",strtotime($order_summary_mail_data['expected_delivery_date']));
				$order_details.='</p></td>';
				$order_details.='</tr>';
				$order_details.='<tr>';
				$order_details.='<td colspan=5 style="border-top: 2px solid #565656;border-bottom: 1px solid #e6e6e6; padding:10px 20px 10px 20px;;margin: 0;background-color: #f9f9f9;">';
				$order_details.='<p style="padding:0;margin:0;text-align:right;color:#004b9b;line-height:22px;white-space:nowrap;font-size:18px">Sub Total <span style="font-size:18;color:#004b9b;">'.curr_sym.' '.$subtotal_modified;
				$order_details.='</span></p></td>';
				$order_details.='</tr>';
			}
                        
			$order_details.='</table>';
                        
			$order_placed_date=$this->get_order_placed_date($order_id);
			$order_placed_date_format=date("jS F, Y \a\\t H:i",strtotime($order_placed_date))." Hrs";
			$invoice_offers=$this->Customer_account->get_all_order_invoice_offers($order_id);
			$inc_txt='';
                       
			
		   $invoice_offer_achieved_str="";
		   $outstanding_str='';
			
			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="left" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top"  align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi '.$customer_name.', </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Thank you for your order.</p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">We will send you another email once your order is processed. Meanwhile, you can check the status of your order on '.$email_stuff_arr["regardsteam_emailtemplate"].'</p><br><p style="text-align:center;padding:0;margin:0" align="center"> <a style="width:200px;margin:0px auto;background-color:#ff8c00;text-align:center;border:#ff8c00 solid 1px;padding:8px 0;text-decoration:none;border-radius:2px;display:block;color:#fff;font-size:13px" align="center" href="'.base_url().'Account/order_details_for_mail/'.$order_id.'" target="_blank"> <span style="color:#ffffff;font-size:13px;background-color:#ff8c00">TRACK ORDER</span> </a> </p></td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">	<table border="0" width="100%"><tbody><tr><td colspan="2" style="padding:10px;background-color:#004b9b;"><span style="color:#fff;"><b> Below is the summary of your order ID '.$order_id.' dated '.$order_placed_date_format.'</b></span></td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">'.$order_details.'</td></tr></tbody></table></td></tr><tr><td style="background-color:#fff;"><table width="100%" style="border-bottom:#e6e6e6 solid 1px;padding:20px 43px 0 0"><tbody>'.$outstanding_str.' '.$invoice_offer_achieved_str.'</tbody></table></td></tr><tr><td valign="top" align="left" style="margin: 0 auto;clear:both;padding: 20px 20px 0 20px;background-color:#fff;">'.$shipping_address.'</td></tr><tr><td><table width="100%" cellspacing="0" cellpadding="0" style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6"> <tbody><tr> <td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br> <table width="100%" cellspacing="0" cellpadding="0"> <tbody><tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0;border-right:dashed 1px #b3b3b3"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">What Next?</p> <p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">You will receive an email with your courier Tracking ID &amp; a link to track your order.</p></td></tr></tbody></table></td><td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br> <table width="100%" cellspacing="0" cellpadding="0"> <tbody><tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Get in touch with our customer care team at +91 80505 04950 </p> </td> </tr> </tbody></table> </td> </tr> </tbody></table></td></tr>';
			
			$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
		   
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($email);
			$mail->addReplyTo($email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = 'Your order is received - Order ID : '.$order_id;
			$mail->Body    = $usermessage;
			$mail->smtpConnect([
					'ssl' => [
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
					]
				]);		
			$mail->send();
			////////////////////// mail to admin starts /////
			
			
			$queryRecordsResult=$this->Customer_account->get_order_details_just_now_received($order_id);
			$dialy_orders_str="<span style='color:#ff3300'>Hello Voometstudio Admin, the below order is received just now in <a href='".SITE_HREF."'>".SITE_URL."</a></span><br><br>";
			if(!empty($queryRecordsResult)){
				$dialy_orders_str.="<hr>";
				foreach($queryRecordsResult as $queryRecordsObj){
					$dialy_orders_str.="<br>";
					$dialy_orders_str.="Timestamp : ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."<br><br>";
					
					$dialy_orders_str.="<b>".$queryRecordsObj->customer_name. "</b> | ".$queryRecordsObj->address1.",".$queryRecordsObj->address2.",".$queryRecordsObj->city.",".$queryRecordsObj->state.",".$queryRecordsObj->country." - ".$queryRecordsObj->pincode."<br>";
					$dialy_orders_str.="<b>Phone : ".$queryRecordsObj->mobile."</b><br><br>";
					
					$get_inventory_attr_details_str=$this->Customer_account->get_inventory_attr_details($queryRecordsObj->inventory_id);
					if($queryRecordsObj->ord_sku_name!=''){	
						$dialy_orders_str.=$queryRecordsObj->ord_sku_name;	
					}else{	
						$dialy_orders_str.=$queryRecordsObj->product_name;	
					}	
					$dialy_orders_str.=" | SKU : ".$queryRecordsObj->sku_id."<br>";
					$dialy_orders_str.=$get_inventory_attr_details_str;
					$dialy_orders_str.="<br>";
					
					$dialy_orders_str.="Qty: ".$queryRecordsObj->quantity."<br>";
					$dialy_orders_str.="Order Date: ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."<br>";
					$dialy_orders_str.="Order ID: ".$queryRecordsObj->order_id."<br><br>";
					
					$dialy_orders_str.="Total Item Price: Rs. ".$queryRecordsObj->subtotal."<br>";
					
					if($queryRecordsObj->promotion_invoice_free_shipping>0){
						$dialy_orders_str.="Shipping charge (waived off): Rs. ".$queryRecordsObj->shipping_charge."<br>";
						$dialy_orders_str.="Total: Rs. ".$queryRecordsObj->subtotal."<br>";
					}else{
						$dialy_orders_str.="Shipping charge: Rs. ".$queryRecordsObj->shipping_charge."<br>";
						$dialy_orders_str.="Total: Rs. ".$queryRecordsObj->grandtotal."<br>";
					}
					$dialy_orders_str.="<br>";
					$dialy_orders_str.="<hr>";
					
				}
			}
			
			$dialy_orders_str.="<br><span style='color:#ff3300'>This is autogenerated report from Flamingo Ecommerce</span>";
				
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress("rajitkumar1974@gmail.com");
			
			
			$mail->AddBCC("rajit@axonlabs.in");
			$mail->AddBCC("sucheta@axonlabs.in");
			$mail->AddBCC("karan@axonlabs.in");
			//$mail->addReplyTo($email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = 'Order Received - '.SITE_NAME;
			$mail->Body    = $dialy_orders_str;
			$mail->smtpConnect([
				'ssl' => [
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				]
			]);		
			$current_base_url=base_url();
			if((preg_match("/voometstudio.in/",$current_base_url))){
				$mail->send();
			}
			////////////////////// mail to admin ends /////
			
			$mail->smtpClose();
		}
			
		}
		/* mail function  */
		if($flag){
			header('Location:'.base_url()."thankyou/".$order_id);
			exit;
		}
		else{
			echo $flag;
		}
		
	}

	public function confirm_order_process($order_id,$razorpay_payment_id,$razorpay_order_id){
		include("assets/s3_fileupload/app/start.php");
		
				$email_stuff_arr=email_stuff();
				
				$customer_id=$this->session->userdata("customer_id");
				$cur_checkout=$this->session->userdata("cur_checkout");
				if($cur_checkout=='checkout_combo'){
					$get_temp_payment_post_data_arr=$this->Customer_account->get_temp_payment_post_data($order_id);
					$payment_post_data_json=$get_temp_payment_post_data_arr["payment_post_data_json"];
					$payment_post_data=json_decode($payment_post_data_json,false);
					// echo "<pre>";
					// print_r($payment_post_data);
					// echo "</pre>";
					// //exit;
			
					//$this->Customer_account->delete_temp_payment_post_data($order_id); //important uncomment
					//////////////////////////
					//$payment_post_data = json_decode(file_get_contents("php://input"));
					$payment=$payment_post_data->total_amount_to_pay;
					$order_id=$payment_post_data->order_id;
					echo '<pre>';
					$payment_method=$payment_post_data->payment_method;

					$result=$this->Customer_account->confirm_order_process_combo_normal_order($order_id,$payment_method);

					//print_r($result);
					//exit;

				$combo_details=get_combo_carttable($customer_id);
		
				if(!empty($combo_details)){
					$combo_products=json_decode($combo_details->combo_products);
					$combo_number_of_products=$combo_details->combo_number_of_products;
					$combo_total_price=$combo_details->combo_total_price;
					$combo_shipping_price=$combo_details->combo_shipping_price;
					$combo_grand_total=$combo_details->combo_grand_total;
					$combo_discount=$combo_details->combo_discount;
					$combo_total_amount_saved=$combo_details->combo_total_amount_saved;
				}
				$payment_post_data_object = new stdClass();
				$payment_post_data_object->promotion_invoice_cash_back=0;
				$payment_post_data_object->promotion_invoice_free_shipping=0;
				$payment_post_data_object->promotion_invoice_discount=0;
				$payment_post_data_object->total_amount_cash_back=0;

				$payment_post_data_object->total_amount_saved=0;
				$payment_post_data_object->total_amount_to_pay=$combo_grand_total;
				$payment_post_data_object->payment_method=$payment_method;

				$payment_post_data_object->invoice_offer_achieved=0;
				$payment_post_data_object->wallet_amount=0;
				$payment_post_data_object->promotion_invoice_cashback_quote=0;

				$payment_post_data_object->total_price_without_shipping_price=$combo_total_price;
				$payment_post_data_object->total_shipping_charge=$combo_shipping_price;

				$payment_post_data_object->order_id=$order_id;
				$payment_post_data_object->total_coupon_used_amount=0;
				$payment_post_data_object->products_coupon=new stdClass();
				$payment_post_data_object->invoice_coupon=new stdClass();
				$payment_post_data_object->inv_addon_products_amount=0;

				$payment_post_data=$payment_post_data_object;
				$payment=$payment_post_data->total_amount_to_pay;
				$surprise_gift_arr=array();
				
				// echo "<pre>";
				// print_r($payment_post_data_object);
				// echo "</pre>";
				//exit;

				}else{
				//////////////////////////
				$get_temp_payment_post_data_arr=$this->Customer_account->get_temp_payment_post_data($order_id);
				$payment_post_data_json=$get_temp_payment_post_data_arr["payment_post_data_json"];
				$payment_post_data=json_decode($payment_post_data_json,false);
				//echo "<pre>";
				//print_r($payment_post_data);
				//echo "</pre>";
			   // exit;
		
				$this->Customer_account->delete_temp_payment_post_data($order_id); //important uncomment
				//////////////////////////
				//$payment_post_data = json_decode(file_get_contents("php://input"));
				$payment=$payment_post_data->total_amount_to_pay;
				$order_id=$payment_post_data->order_id;
		
				/*$pincode_arr=$this->Customer_account->get_shipping_address_pincode();
				$pincode=$pincode_arr['pincode'];
				$flag=$this->Customer_account->get_availability_of_shipping($pincode);
				if(!$flag){
					echo "no_availability";
					exit;
				}
				*/
				$surprise_gift_arr=array();
				if(isset($payment_post_data->eligible_surprise_gift_on_invoice_uid)){
		
					$eligible_surprise_gift_on_invoice_uid=$payment_post_data->eligible_surprise_gift_on_invoice_uid;
		
					//echo $eligible_surprise_gift_on_invoice_uid;
					if($eligible_surprise_gift_on_invoice_uid!='0'){
						
						//echo $eligible_surprise_gift_on_invoice_uid;
						
						$surprise_gift_arr=$this->Customer_account->get_invoice_surprise_gift_details($eligible_surprise_gift_on_invoice_uid);
					}
				}
				$result=$this->get_cartinfo_for_customer();	
				} // normal cart
				
				
		
				$stock_is_reduced=$this->Customer_account->reduce_stocks_of_inventory($result);
		
				//$razorpayPaymentId=$this->session->flashdata('razorpay_payment_id');
				//$razorpayOrderId=$this->session->flashdata('merchant_order_id');
				//$razorpayOrderId=$this->session->flashdata('razorpay_order_id');
				
				//$razorpayPaymentId=$this->session->userdata('razorpay_payment_id');
				//$razorpayOrderId=$this->session->userdata('razorpay_order_id');
				$razorpayPaymentId=$razorpay_payment_id;
				$razorpayOrderId=$razorpay_order_id;
		
		
				echo "razorpayPaymentId=".$razorpayPaymentId;
				echo "razorpayPaymentId=".$razorpayOrderId;
				//exit;

				$flag=$this->Customer_account->confirm_order_process($result,$order_id,$payment_post_data,$surprise_gift_arr,$razorpayPaymentId,$razorpayOrderId,$cur_checkout);
				
		
				$email=$this->Customer_account->get_email_db($customer_id);
				$mobile=$this->Customer_account->get_mobile_db($customer_id);
				$customer_name=$this->get_customer_name($customer_id);
				$mobile_number_of_admin=$this->Customer_account->get_mobile_number_of_admin();
		
	//exit;//for mail

				if($flag==true){
				
				if($mobile != '' ){
					$mobile_stuff_arr=mobile_stuff();
					$authKey = $mobile_stuff_arr["authKey_smstemplate"];
					$mobileNumber = "+91".$mobile;
					$senderId = $mobile_stuff_arr["senderId_smstemplate"];
		
					$message = urlencode("Thank You! We have received your order ID $order_id for ".curr_sym."$payment and is in process. We will keep you informed.");
					$route = "4";
					$postData = array(
						'authkey' => $authKey,
						'mobiles' => $mobileNumber,
						'message' => $message,
						'sender' => $senderId,
						'route' => $route
					);
		
					$url="https://control.msg91.com/api/sendhttp.php";
					$ch = curl_init();
					curl_setopt_array($ch, array(
						CURLOPT_URL => $url,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_POST => true,
						CURLOPT_POSTFIELDS => $postData
					));
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
					$output = curl_exec($ch);
		
					if(curl_errno($ch))
					{
						echo 'error:' . curl_error($ch);
					}
					curl_close($ch);
					}//sms
					
					
					
					$mobile_stuff_arr=mobile_stuff();
					$authKey = $mobile_stuff_arr["authKey_smstemplate"];
					$Mobile_Number_Admin = "+91".$mobile_number_of_admin;
					$senderId = $mobile_stuff_arr["senderId_smstemplate"];
					
					
		
					$secondmessage = urlencode("There is an order with order ID $order_id for ".curr_sym."$payment. Please login to Admin Panel and review the order.");
					$route = "4";
					$postData = array(
						'authkey' => $authKey,
						'mobiles' => $Mobile_Number_Admin,
						'message' => $secondmessage,
						'sender' => $senderId,
						'route' => $route
					);
		
					$url="https://control.msg91.com/api/sendhttp.php";
					$ch = curl_init();
					curl_setopt_array($ch, array(
						CURLOPT_URL => $url,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_POST => true,
						CURLOPT_POSTFIELDS => $postData
					));
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
					$output = curl_exec($ch);
		
					if(curl_errno($ch))
					{
						echo 'error:' . curl_error($ch);
					}
					curl_close($ch);
				if($email!=''){
					$shipping_address_id=$this->Customer_account->get_shipping_address_id($order_id);
					$shipping_address_table_data_arr=$this->Customer_account->get_shipping_address($shipping_address_id);
				
					foreach($shipping_address_table_data_arr as $shipping_address_table_data){
						$shipping_address='<table style="color:#565656;line-height:15px;font-size:15px;background-color:#fff;"><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;padding-bottom:15px;"><b>Items will be shipped to:</b></td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;"><b>'.$shipping_address_table_data['customer_name'].'</b></td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;"> <span>Mob : </span>' .$shipping_address_table_data['mobile'].'</td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;">'.$shipping_address_table_data['address1'].',</td></tr><tr><td colspan="2" style="color: #565656;line-height: 22px;font-size: 15px;">'.$shipping_address_table_data['address2'].',</td></tr><tr><td colspan="2" style="color: #565656;line-height:15px;font-size: 15px;">'.$shipping_address_table_data['city'].', '.$shipping_address_table_data['pincode'].'</td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;">'.$shipping_address_table_data['state'].', '.$shipping_address_table_data['country'].'</td></tr></table><p style="padding:0;margin:0;color:#565656;font-size: 15px;"><br/>Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br>';
					}
		
					$order_summary_mail_data_arr=$this->Customer_account->get_order_summary_mail_data($order_id);
					$order_details='';
					$order_details.='<table>';
					$order_details.='<tr>';
					$order_details.='<th width="20%">Image</th>';
					$order_details.='<th width="40%">Product</th>';
					$order_details.='<th width="10%">Item Price</th>';
					$order_details.='<th width="10%">Qty</th>';
					$order_details.='<th width="20%">Subtotal</th>';
					$order_details.='</tr>';
					foreach($order_summary_mail_data_arr as $order_summary_mail_data){
						
						$order_details.='<tr>';
						$order_details.='<td align="center">';
						$order_details.="<img src='".base_url().$order_summary_mail_data['thumbnail']."'>";
						$order_details.='</td>';
						$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';

						if($order_summary_mail_data['sku_name']!=''){
							$order_details.=$order_summary_mail_data['sku_name']."<br>";
						}else{
							$order_details.=$order_summary_mail_data['product_name']."<br>";
						}
						
						$order_details.=$order_summary_mail_data['product_description']."<br>";
		
						
						if(!empty($order_summary_mail_data['attribute_1'])){
							$order_details.=$order_summary_mail_data['attribute_1']." : ".$order_summary_mail_data['attribute_1_value'].'<br>';
						}
						if(!empty($order_summary_mail_data['attribute_2'])){
							$order_details.=$order_summary_mail_data['attribute_2']." : ".$order_summary_mail_data['attribute_2_value'].'<br>';
						}
						if(!empty($order_summary_mail_data['attribute_3'])){
							$order_details.=$order_summary_mail_data['attribute_3']." : ".$order_summary_mail_data['attribute_3_value'].'<br>';
						}
						if(!empty($order_summary_mail_data['attribute_4'])){
							$order_details.=$order_summary_mail_data['attribute_4']." : ".$order_summary_mail_data['attribute_4_value'];
						}
						if(!empty($order_summary_mail_data['sku_id'])){
							$order_details.="SKU : ".$order_summary_mail_data['sku_id'];
						}
						
						$order_details.='</p>';
						$order_details.='</td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
						$order_details.=curr_sym.''.round($order_summary_mail_data['product_price']);
						$order_details.='</td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
						$order_details.=$order_summary_mail_data['quantity'];
						$order_details.='</p></td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
						//$order_details.=curr_sym.''.$order_summary_mail_data['subtotal'];
						// $order_details.=curr_sym.''.round($order_summary_mail_data['quantity']*$order_summary_mail_data['ord_max_selling_price']);
						$order_details.=curr_sym.''.round($order_summary_mail_data['subtotal']);
						$order_details.='</td>';
						$order_details.='</tr>';
						
						if($order_summary_mail_data['ord_addon_single_or_multiple_tagged_inventories_in_frontend']=="single" && $order_summary_mail_data['ord_addon_products_status']=="1"){
							// [{"inv_id":"2","inv_price":2000,"inv_mrp_price":3500,"inv_sku_id":"WPNC3","inv_image":"assets/pictures/images/products/WPNC3/86424.jpg","inv_name":"PNC5","inv_ship_price":0}]
							$ord_addon_products_json=$order_summary_mail_data["ord_addon_products"];
							if($ord_addon_products_json!=""){
								$ord_addon_products_json_arr=json_decode($ord_addon_products_json,true);
								
								$addon_inventory_id=$ord_addon_products_json_arr[0]["inv_id"];
								$addon_inventory_price=$ord_addon_products_json_arr[0]["inv_price"];
								$addon_inventory_mrp_price=$ord_addon_products_json_arr[0]["inv_mrp_price"];
								$addon_sku_id=$ord_addon_products_json_arr[0]["inv_sku_id"];
								$addon_inv_image=$ord_addon_products_json_arr[0]["inv_image"];
								$addon_inv_name=$ord_addon_products_json_arr[0]["inv_name"];
								$tagged_type=$ord_addon_products_json_arr[0]["tagged_type"];
								$addon_ship_price=$ord_addon_products_json_arr[0]["inv_ship_price"];
								$addon_inventory_info_obj=$this->Customer_account->get_inventory_info_by_inventory_id($addon_inventory_id);
								
								
								$order_details.='<tr style="background-color:#FFFFE0">';
								$order_details.='<td align="center">';
								$order_details.="<img src='".base_url().$addon_inv_image."'  style='width:85px;'>";
								$order_details.='</td>';
								$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
								$order_details.="<b>Addon For ".$order_summary_mail_data['product_name']."</b><br>".$addon_inv_name."<br>";
								if($tagged_type=="internal"){
									if(!empty($addon_inventory_info_obj->attribute_1)){
										$order_details.=$addon_inventory_info_obj->attribute_1." : ".$addon_inventory_info_obj->attribute_1_value.'<br>';
									}
									if(!empty($addon_inventory_info_obj->attribute_2)){
										$order_details.=$addon_inventory_info_obj->attribute_2." : ".$addon_inventory_info_obj->attribute_2_value.'<br>';
									}
									if(!empty($addon_inventory_info_obj->attribute_3)){
										$order_details.=$addon_inventory_info_obj->attribute_3." : ".$addon_inventory_info_obj->attribute_3_value.'<br>';
									}
									if(!empty($addon_inventory_info_obj->attribute_4)){
										$order_details.=$addon_inventory_info_obj->attribute_4." : ".$addon_inventory_info_obj->attribute_4_value.'<br>';
									}
								}
								if(!empty($addon_sku_id)){
									$order_details.="SKU : ".$addon_sku_id."<br>";
								}
								
								if(round($addon_inventory_mrp_price)!=round($addon_inventory_price)){
									$order_details.='<del>'.curr_sym.''.round($addon_inventory_mrp_price).'</del> ';
									$order_details.=curr_sym.''.round($addon_inventory_price);
								}
								else{
									$order_details.=curr_sym.''.round($addon_inventory_price);
								}
								
								
								$order_details.='</p>';
								$order_details.='</td>';
								$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
								
								$order_details.='</td>';
								$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
								$order_details.=1;
								$order_details.='</p></td>';
								$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
								//$order_details.=curr_sym.''.$order_summary_mail_data['subtotal'];
								// $order_details.=curr_sym.''.round($order_summary_mail_data['quantity']*$order_summary_mail_data['ord_max_selling_price']);
								$order_details.='';
								$order_details.='</td>';
								$order_details.='</tr>';
								
								
							}
						}
		
		
		
							//// stright n% discount and another promo radio both is there then starts 
						if($order_summary_mail_data['promotion_default_discount_promo']!="" && $order_summary_mail_data['promotion_quote']!="" && $order_summary_mail_data['default_discount']>0 && $order_summary_mail_data['promotion_quote']!=$order_summary_mail_data['promotion_default_discount_promo']){
							$order_details.='<tr>';
							$order_details.='<td align="center"></td>';
							$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							
							$order_details.="Base Discount : <br>".trim($order_summary_mail_data['promotion_default_discount_promo']);
							
							$order_details.='</td>';
							$order_details.='<td valign="top" style="padding:12px 10px 0 10px;margin:0;text-align:center;"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$product_price_after_default_discount=round($order_summary_mail_data['product_price']-(($order_summary_mail_data['product_price']*$order_summary_mail_data['default_discount'])/100));
							
							$order_details.=curr_sym." ".$product_price_after_default_discount." <del>".curr_sym." ".$order_summary_mail_data['product_price']."</del>";
							$order_details.='</td>';
							$order_details.='<td colspan="2"></td>';
							$order_details.='</tr>';
						}
						//// stright n% discount and another promo radio both is there then starts 
						
						
						
						// Straight 5% discount on SKU’s starts 
						if(intval($order_summary_mail_data['quantity_with_promotion'])>0){
							if(intval($order_summary_mail_data['promotion_discount'])>0 && (intval($order_summary_mail_data['promotion_discount'])==intval($order_summary_mail_data['default_discount']))){
								
								$promotion_straight_discount_str='Discount @ ('.$order_summary_mail_data['promotion_discount'].'%)';
								$eligible_straight_quantity_str='Eligible Quantity : '.$order_summary_mail_data['quantity_with_promotion'];
								$applied_straight_offer_str='Applied Offer : '.$order_summary_mail_data['promotion_quote'];
								//$promo_default_straight_discount_price=floatval($order_summary_mail_data['ord_max_selling_price']-$order_summary_mail_data['individual_price_of_product_with_promotion']);
								$promo_default_straight_discount_price=floatval($order_summary_mail_data['ord_max_selling_price']-$order_summary_mail_data['product_price']);
								$promo_straight_discount_offer_calculation_display=$order_summary_mail_data['quantity_with_promotion'].' *  '.curr_sym." ".$promo_default_straight_discount_price;
								$promo_straight_discount_offer_calculation=$order_summary_mail_data['quantity_with_promotion']*$promo_default_straight_discount_price;
								$order_details.='<tr>';
								$order_details.='<td>';
								$order_details.='</td>';
								$order_details.='<td colspan="2" style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.=$promotion_straight_discount_str;
									$order_details.=" ".$eligible_straight_quantity_str."<br>";
									$order_details.=$applied_straight_offer_str;
								$order_details.='</td>';
								$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.=$promo_straight_discount_offer_calculation_display;
								$order_details.='</td>';
								$order_details.='<td  valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.='<span style="color:red">-</span> '.curr_sym.' '.$promo_straight_discount_offer_calculation;
								$order_details.='</td>';
								$order_details.='</tr>';
							
							}
						}else{
											//if($order_summary_mail_data['ord_selling_discount']>0  ){
											if(0){	
								$promotion_discount_str='Discount @ ('.round($order_summary_mail_data['ord_selling_discount']).'%)';
								$eligible_quantity_str='Eligible Quantity : '.$order_summary_mail_data['quantity'];
								$applied_offer_str='Applied Offer : '.$order_summary_mail_data['ord_selling_discount'];
								
								$promo_default_discount_price=floatval((int)$order_summary_mail_data['ord_max_selling_price']-(int)$order_summary_mail_data['product_price']);
								$promo_discount_offer_calculation_display=$order_summary_mail_data['quantity'].' *  '.curr_sym." ".$promo_default_discount_price;
								$promo_discount_offer_calculation=(int)$order_summary_mail_data['quantity']*(int)$promo_default_discount_price;
								$order_details.='<tr>';
								$order_details.='<td>';
								$order_details.='</td>';
								$order_details.='<td colspan="2" style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.=$promotion_discount_str;
									$order_details.=" ".$eligible_quantity_str."<br>";
									//$order_details.=$applied_offer_str;
								$order_details.='</td>';
								$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.=$promo_discount_offer_calculation_display;
								$order_details.='</td>';
								$order_details.='<td  valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
														$order_details.='<span style="color:red">-</span> '.curr_sym.' '.$promo_discount_offer_calculation;
								$order_details.='</td>';
								$order_details.='</tr>';
											}
										}
						// Straight 5% discount on SKU’s ends
						/// buy 5 sku and 6% discount table starts 
						if(intval($order_summary_mail_data['quantity_with_promotion'])>0){
							if(intval($order_summary_mail_data['promotion_discount'])>0 && (intval($order_summary_mail_data['promotion_discount'])!=intval($order_summary_mail_data['default_discount']))){
								$promotion_discount_str='Promotion Discount @ ('.$order_summary_mail_data['promotion_discount'].'%)';
								$eligible_quantity_str='Eligible Quantity : '.$order_summary_mail_data['quantity_with_promotion'];
								$applied_offer_str='Applied Offer : '.$order_summary_mail_data['promotion_quote'];
								$promo_discount_price=floatval($order_summary_mail_data['ord_max_selling_price']-$order_summary_mail_data['individual_price_of_product_with_promotion']);
								$promo_discount_offer_calculation_display=$order_summary_mail_data['quantity_with_promotion'].' * '.curr_sym." ".$promo_discount_price;
								$promo_discount_offer_calculation=$order_summary_mail_data['quantity_with_promotion']*$promo_discount_price;
								$order_details.='<tr>';
								$order_details.='<td>';
								$order_details.='</td>';
								$order_details.='<td colspan="2" style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.=$promotion_discount_str;
									$order_details.=" ".$eligible_quantity_str."<br>";
									$order_details.=$applied_offer_str;
								$order_details.='</td>';
								$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.=$promo_discount_offer_calculation_display;
								$order_details.='</td>';
								$order_details.='<td  valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:normal;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.=' '.curr_sym.' '.$promo_discount_offer_calculation;
								$order_details.='</td>';
								$order_details.='</tr>';
							}
						}
						/// buy 5 sku and 6% discount table ends
						/// Free item starts 
						if($order_summary_mail_data['promotion_item']!=""){
							$free_items_arr=$this->get_free_skus_from_inventory_with_details($order_summary_mail_data['promotion_item'],$order_summary_mail_data['promotion_item_num']);
							foreach($free_items_arr as $free_itemsObj){
								$order_details.='<tr>';
								
								$order_details.='<td align="center">';
								$order_details.="<img src='".base_url().$free_itemsObj->image."'  style='width:85px;'>";
								$order_details.='</td>';
								
								$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
								
								if($free_itemsObj->sku_name!=''){
									$order_details.=$free_itemsObj->sku_name."<br>";
								}else{
									$order_details.=$free_itemsObj->product_name."<br>";
								}
								if(!empty($free_itemsObj->attribute_1)){
									$order_details.=$free_itemsObj->attribute_1." : ".$free_itemsObj->attribute_1_value.'<br>';
								}
								if(!empty($free_itemsObj->attribute_2)){
									$order_details.=$free_itemsObj->attribute_2." : ".$free_itemsObj->attribute_2_value.'<br>';
								}
								if(!empty($free_itemsObj->attribute_3)){
									$order_details.=$free_itemsObj->attribute_3." : ".$free_itemsObj->attribute_3_value.'<br>';
								}
								if(!empty($free_itemsObj->attribute_4)){
									$order_details.=$free_itemsObj->attribute_4." : ".$free_itemsObj->attribute_4_value.'<br>';
								}
								if(!empty($free_itemsObj->sku_id)){
									$order_details.="SKU : ".$free_itemsObj->sku_id;
								}
								$order_details.='</p>';
								$order_details.='</td>';
								$order_details.='<td colspan="3" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.='Free - You saved <del>'.curr_sym." ".$order_summary_mail_data['promotion_item_num']*$order_summary_mail_data['promotion_item_multiplier']*$free_itemsObj->selling_price.'</del> ('.$order_summary_mail_data['promotion_item_num']*$order_summary_mail_data['promotion_item_multiplier'].' * '.curr_sym.' '.$free_itemsObj->selling_price.')';
									$order_details.='<br>';
									$order_details.='(Offer Applied : '.$order_summary_mail_data['promotion_quote'].')';
								$order_details.='</td>';
								$order_details.='</tr>';
							}
						}
						/// Free item ends 
										
										/*sku_coupon*/
										if($order_summary_mail_data['ord_coupon_status']=='1'){
											$coupon_str='Coupon applied on SKU : <b>(';
											$coupon_str.=($order_summary_mail_data['ord_coupon_type']=='2') ? curr_sym : ''.' ';
											$coupon_str.=round($order_summary_mail_data['ord_coupon_value']);
											$coupon_str.=($order_summary_mail_data['ord_coupon_type']=='1') ? '%' : 'Flat' ;
											$coupon_str.=')';
											
										$order_details.='<tr>';
								$order_details.='<td>';
								$order_details.='</td>';
								$order_details.='<td colspan="3" style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
									$order_details.=$coupon_str;
									
								$order_details.='</td>';
							
								$order_details.='<td  valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
														$order_details.='<span style="color:red">-</span> '.curr_sym.' '.round($order_summary_mail_data['ord_coupon_reduce_price']);
								$order_details.='</td>';
								$order_details.='</tr>';
										}
										
										/*sku_coupon*/
										
						$invoice_offers=$this->Customer_account->get_all_order_invoice_offers($order_id);
						$shipping_charge_waived="no";
						if(!empty($invoice_offers)){
							foreach($invoice_offers as $data){
								$promotion_invoice_free_shipping=$data["promotion_invoice_free_shipping"];
								$invoice_offer_achieved=$data["invoice_offer_achieved"];
								if($promotion_invoice_free_shipping>0 && $invoice_offer_achieved==1){
									$shipping_charge_waived="yes";
								}else{
									$shipping_charge_waived="no";
								}
							}
						}
						if($shipping_charge_waived=="yes"){
							$shipping_charge_modified=0;
							$subtotal_modified=$order_summary_mail_data['subtotal'];
						}
						else{
							$shipping_charge_modified=$order_summary_mail_data['shipping_charge'];
							$subtotal_modified=$order_summary_mail_data['grandtotal'];
						}
		
						/* addon section */
					//if($order_summary_mail_data['ord_addon_products_status']=='1'){
					if(0){
						$order_details.='<tr>';
						$order_details.='<td>';
							$arr=json_decode($order_summary_mail_data['ord_addon_products']);
							$count=count($arr);
							if($order_summary_mail_data['ord_addon_inventories']!=''){
								$order_details.='<tr><th align="center" colspan="5" style="text-align: center;text-decoration: underline;">
								 + Additional Products - Included</th></tr>';
								$i=1;
								foreach($arr as $val){
									
									$name=$val->inv_name;
									$image=$val->inv_image;
									$sku_id=$val->inv_sku_id;
									$price=$val->inv_price;
									$order_details.='<tr>
									<td align="center"><img src="'.base_url().$image.'" style="width:85px;"></td>
									<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.$name.'<br>SKU id : '.$sku_id.'</p></td>
									<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.curr_sym.$price.'</td>
									<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">1</td>
									<td  width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.curr_sym.$price.'
									</td></tr>';
								}
								
						}
						$order_details.='</td>';
						$order_details.='</tr>';
				
					}
					/* addon section */
					
						$order_details.='<tr>';
						$order_details.='<td colspan=5 align="center">';
						$order_details.='<p style="margin:10px 0 0 0;padding:5px;background-color:#f9f9f9;border:1px solid #eee;color:#666;font-size:13px;text-align:center">(+) Delivery: '.curr_sym.' '.($shipping_charge_modified);
						$order_details.='</p></td>';
						$order_details.='</tr>';
						
						
						$order_details.='<tr>';
						$order_details.='<td colspan=5 align="center" style="padding:0 0 17px 0">';
						$order_details.='<p style="padding:4px 5px;background-color:#fffed5;border:1px solid #f9e2b2;color:#565656;margin:10px 0 0 0;text-align:center;font-size:12px">Delivery scheduled by '.date("D j M, Y",strtotime($order_summary_mail_data['expected_delivery_date']));
						$order_details.='</p></td>';
						$order_details.='</tr>';
						$order_details.='<tr>';
						$order_details.='<td colspan=5 style="border-top: 2px solid #565656;border-bottom: 1px solid #e6e6e6; padding:10px 20px 10px 20px;;margin: 0;background-color: #f9f9f9;">';
						$order_details.='<p style="padding:0;margin:0;text-align:right;color:#004b9b;line-height:22px;white-space:nowrap;font-size:18px">Sub Total <span style="font-size:18;color:#004b9b;">'.curr_sym.' '.$subtotal_modified;
						$order_details.='</span></p></td>';
						$order_details.='</tr>';
					}
								
								$order_details.='</table>';
						
					
								
					$order_placed_date=$this->get_order_placed_date($order_id);
					$order_placed_date_format=date("jS F, Y \a\\t H:i",strtotime($order_placed_date))." Hrs";
					$invoice_offers=$this->Customer_account->get_all_order_invoice_offers($order_id);
					$inc_txt='';
								if(!empty($invoice_offers)){
						foreach($invoice_offers as $data){
							$promotion_invoice_cash_back=$data["promotion_invoice_cash_back"];
							$promotion_invoice_free_shipping=$data["promotion_invoice_free_shipping"];
							$promotion_invoice_discount=$data["promotion_invoice_discount"];
							$total_coupon_used_amount=$data["total_coupon_used_amount"];
							$total_amount_cash_back=$data["total_amount_cash_back"];
							$total_amount_saved=$data["total_amount_saved"];
							$total_amount_to_pay=$data["total_amount_to_pay"];
							$payment_method=$data["payment_method"];
							$invoice_offer_achieved=$data["invoice_offer_achieved"];
							$invoice_coupon_status=$data["invoice_coupon_status"];
							$invoice_coupon_code=$data["invoice_coupon_code"];
							$invoice_coupon_name=$data["invoice_coupon_name"];
							$invoice_coupon_value=$data["invoice_coupon_value"];
							$invoice_coupon_type=$data["invoice_coupon_type"];
							$invoice_coupon_reduce_price=$data["invoice_coupon_reduce_price"];
					
							if($promotion_invoice_free_shipping>0 && $invoice_offer_achieved==1){
								$shipping_charge_waived="yes";
							}else{
								$shipping_charge_waived="no";
							}
												
		
					   }
					}
				   
					
					
					
				   $invoice_offer_achieved_str="";
				   if(!empty($invoice_offers)){
					   if($invoice_offer_achieved==1){
						   if($total_amount_cash_back>0){
								$invoice_offer_achieved_str.='<tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:3px 20px 0 20px" bgcolor="#ffffff"><p style="line-height:15px;margin:0;color:#565656;font-size:1.3em;text-align:right;"> Total Invoice cash back:  '.curr_sym.' '.$promotion_invoice_cash_back.'  </p></td></tr>';
						   }
						   if($promotion_invoice_free_shipping>0){
								$invoice_offer_achieved_str.='<tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:3px 20px 0 20px" bgcolor="#ffffff"><p style="line-height:15px;margin:0;color:#565656;font-size:1.3em;text-align:right;"> Shipping charges waived on invoice:  '.curr_sym.' '.$promotion_invoice_free_shipping.'  </p></td></tr>';
						   }
						   
						   if($promotion_invoice_discount>0){
								$invoice_offer_achieved_str.='<tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:3px 20px 0 20px" bgcolor="#ffffff"><p style="line-height:15px;margin:0;color:#565656;font-size:1.3em;text-align:right;"> Total Invoice Discount:  '.curr_sym.' '.$promotion_invoice_discount.'  </p></td></tr>';
						   }
										   
											/* invoice coupon */
											if($invoice_coupon_status=='1'){
											
													$inc_txt.='<b>(';
													$inc_txt.=($invoice_coupon_type=='2') ? curr_sym : ''.' ';
													$inc_txt.=round($invoice_coupon_value);
													$inc_txt.=($invoice_coupon_type=='1') ? '%' : 'Flat' ;
													$inc_txt.=')</b>';
													$invoice_offer_achieved_str.='<tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:3px 20px 0 20px" bgcolor="#ffffff"><p style="line-height:15px;margin:0;color:#565656;font-size:1.3em;text-align:right;"> Invoice Coupon Discount  '.$inc_txt.': '.curr_sym.round($invoice_coupon_reduce_price).'</p></td></tr>';
						   }
										   /* invoice coupon */
										   
						   if($total_coupon_used_amount>0){
								$invoice_offer_achieved_str.='<tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:3px 20px 0 20px" bgcolor="#ffffff"><p style="line-height:15px;margin:0;color:#565656;font-size:1.3em;text-align:right;"> Total Coupon Discount:  '.curr_sym.' '.round($total_coupon_used_amount).'  </p></td></tr>';
						   }
						   
						   if($total_amount_saved>0){
								$invoice_offer_achieved_str.='<tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:3px 20px 0 20px" bgcolor="#ffffff"><p style="line-height:15px;margin:0;color:#565656;font-size:1.3em;text-align:right;"> Total amount you saved on invoice:  '.curr_sym.' '.$total_amount_saved.'  </p></td></tr>';
						   }
						   
						   if($total_amount_to_pay>0){
								$invoice_offer_achieved_str.='<tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:3px 20px 0 20px" bgcolor="#ffffff"><p style="line-height:15px;margin:0;color:#565656;font-size:1.3em;text-align:right;"> Total amount paid on invoice:  '.curr_sym.' '.$total_amount_to_pay.'  </p></td></tr>';
						   }
					   }
				   }
		
				   $outstanding_str='';
		
				   if($payment_method=='COD') {
					   $outstanding_str = '<tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:20px 20px 0 20px" bgcolor="#ffffff"><p style="line-height:22px;padding:0 0 0 0;margin:0;color:#565656;font-size:1.2em;text-align:right;"> Outstanding Amount Payable on Delivery: <strong style="color:#004b9b;"> ' . curr_sym . ' ' . $payment . '  </strong></p></td></tr>';
				   }
					
					$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="left" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top"  align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi '.$customer_name.', </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Thank you for your order.</p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">We will send you another email once your order is processed. Meanwhile, you can check the status of your order on '.$email_stuff_arr["regardsteam_emailtemplate"].'</p><br><p style="text-align:center;padding:0;margin:0" align="center"> <a style="width:200px;margin:0px auto;background-color:#ff8c00;text-align:center;border:#ff8c00 solid 1px;padding:8px 0;text-decoration:none;border-radius:2px;display:block;color:#fff;font-size:13px" align="center" href="'.base_url().'Account/order_details_for_mail/'.$order_id.'" target="_blank"> <span style="color:#ffffff;font-size:13px;background-color:#ff8c00">TRACK ORDER</span> </a> </p></td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">	<table border="0" width="100%"><tbody><tr><td colspan="2" style="padding:10px;background-color:#004b9b;"><span style="color:#fff;"><b> Below is the summary of your order ID '.$order_id.' dated '.$order_placed_date_format.'</b></span></td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">'.$order_details.'</td></tr></tbody></table></td></tr>
					
					
					
					
					 
								
							
								
					
					
					
					
					
					
					
					<tr><td style="background-color:#fff;"><table width="100%" style="border-bottom:#e6e6e6 solid 1px;padding:20px 43px 0 0"><tbody>'.$outstanding_str.' '.$invoice_offer_achieved_str.'</tbody></table></td></tr><tr><td valign="top" align="left" style="margin: 0 auto;clear:both;padding: 20px 20px 0 20px;background-color:#fff;">'.$shipping_address.'</td></tr><tr><td><table width="100%" cellspacing="0" cellpadding="0" style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6"> <tbody><tr> <td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br> <table width="100%" cellspacing="0" cellpadding="0"> <tbody><tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0;border-right:dashed 1px #b3b3b3"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">What Next?</p> <p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">You will receive an email with your courier Tracking ID &amp; a link to track your order.</p></td></tr></tbody></table></td><td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br> <table width="100%" cellspacing="0" cellpadding="0"> <tbody><tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Get in touch with our customer care team at +91 80505 04950 </p> </td> </tr> </tbody></table> </td> </tr> </tbody></table></td></tr>';
					
					$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';
		
					$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';
		
					$usermessage.='</tbody> </table></tr></table></center> </body></html>';
					
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					//$email="shareeshk@gmail.com";
					$mail->addAddress($email);
					$mail->addReplyTo($email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = 'Your order is received - Order ID : '.$order_id;
					$mail->Body    = $usermessage;
					$mail->smtpConnect([
			'ssl' => [
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			]
		]);		
					$mail->send();
					////////////////////// mail to admin starts /////
					
					
					$queryRecordsResult=$this->Customer_account->get_order_details_just_now_received($order_id);
					$dialy_orders_str="<span style='color:#ff3300'>Hello Admin, the below order is received just now in <a href='".SITE_HREF."'>".SITE_URL."</a></span><br><br>";
					if(!empty($queryRecordsResult)){
						$dialy_orders_str.="<hr>";
						foreach($queryRecordsResult as $queryRecordsObj){
							$dialy_orders_str.="<br>";
							$dialy_orders_str.="Timestamp : ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."<br><br>";
							
							$dialy_orders_str.="<b>".$queryRecordsObj->customer_name. "</b> | ".$queryRecordsObj->address1.",".$queryRecordsObj->address2.",".$queryRecordsObj->city.",".$queryRecordsObj->state.",".$queryRecordsObj->country." - ".$queryRecordsObj->pincode."<br>";
							$dialy_orders_str.="<b>Phone : ".$queryRecordsObj->mobile."</b><br><br>";
							
							$get_inventory_attr_details_str=$this->Customer_account->get_inventory_attr_details($queryRecordsObj->inventory_id);
							if($queryRecordsObj->ord_sku_name!=''){	
								$dialy_orders_str.=$queryRecordsObj->ord_sku_name;	
							}else{	
								$dialy_orders_str.=$queryRecordsObj->product_name;	
							}	
							$dialy_orders_str.=" | SKU : ".$queryRecordsObj->sku_id."<br>";
							$dialy_orders_str.=$get_inventory_attr_details_str;
							$dialy_orders_str.="<br>";
							
							$dialy_orders_str.="Qty: ".$queryRecordsObj->quantity."<br>";
							$dialy_orders_str.="Order Date: ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."<br>";
							$dialy_orders_str.="Order ID: ".$queryRecordsObj->order_id."<br><br>";
							
							$dialy_orders_str.="Total Item Price: Rs. ".$queryRecordsObj->subtotal."<br>";
							
							if($queryRecordsObj->promotion_invoice_free_shipping>0){
								$dialy_orders_str.="Shipping charge (waived off): Rs. ".$queryRecordsObj->shipping_charge."<br>";
								$dialy_orders_str.="Total: Rs. ".$queryRecordsObj->subtotal."<br>";
							}else{
								$dialy_orders_str.="Shipping charge: Rs. ".$queryRecordsObj->shipping_charge."<br>";
								$dialy_orders_str.="Total: Rs. ".$queryRecordsObj->grandtotal."<br>";
							}
							$dialy_orders_str.="<br>";
							$dialy_orders_str.="<hr>";
							
						}
					}
					
					$dialy_orders_str.="<br><span style='color:#ff3300'>This is autogenerated report from Flamingo Ecommerce</span>";
					
					
					
					
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress("rajitkumar1974@gmail.com");
					
					
					$mail->AddBCC("rajit@axonlabs.in");
					$mail->AddBCC("sucheta@axonlabs.in");
					$mail->AddBCC("karan@axonlabs.in");
					//$mail->addReplyTo($email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = 'Order Received - '.SITE_NAME;
					$mail->Body    = $dialy_orders_str;
					$mail->smtpConnect([
			'ssl' => [
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			]
		]);		
					$current_base_url=base_url();
										
					$mail->send();

					////////////////////// mail to admin ends /////
					
					$mail->smtpClose();
				}
					
				}
				if($flag){
					if($cur_checkout=='checkout_combo'){
					$this->Customer_account->carttable_dump_combo_search_delete();
					}
					header('Location:'.base_url()."thankyou/".$order_id);
					exit;
				}
				else{
					echo $flag;
				}
			}
			
	public function check_availability_pincode(){
		$vendor_id=$this->input->post("vendor_id");
		$available_pincode=$this->input->post("available_pincode");
		
		$result=$this->Customer_account->get_logistics_id_by_vendor_id($vendor_id);
		$logistics_id_arr=array();
		foreach($result as $res){
			$logistics_id_arr[]=$res->logistics_id;
		}
		$default_logistics_id=$this->Customer_account->get_default_logistics($vendor_id);
		$flag=$this->Customer_account->check_availability_pincode($logistics_id_arr,$available_pincode,$default_logistics_id);
		
		if($flag==1){
			$cookie = array(
			'pincode'   => $available_pincode,
			'availability' => $flag
			);
			setcookie('searched_pincode',$available_pincode, time() + (86400 * 30), "/");
			//setcookie('pincode',$available_pincode, time() + (86400 * 30), "/");
			//setcookie('available_pincode',$available_pincode);
		}else{
			
			setcookie('searched_pincode',$available_pincode, time() + (86400 * 30), "/");
			/*unset($_COOKIE['pincode']);
			if(isset($_COOKIE['pincode'])){
				unset($_COOKIE['pincode']);
			}*/
		}
		//$this->input->cookie('pincode');
		echo $flag;
	}
	
	public function get_all_available_delivery_mode($logistics_id,$vendor_id)
	{
		$all_available_delivery_mode_obj=$this->Customer_account->get_all_available_delivery_mode($logistics_id,$vendor_id);
		return $all_available_delivery_mode_obj;
		//echo json_encode($all_available_delivery_mode_arr);
	}
	
	public function get_customer_address(){
		$customer_address_post_data = json_decode(file_get_contents("php://input"));
		$res=$this->Customer_account->get_customer_address($customer_address_post_data->customer_address_id);
		echo json_encode($res);
	}
	
	public function edit_delivery_address($customer_address_id){
		$new_delivery_address_post_data = json_decode(file_get_contents("php://input"));
		$customer_id=$this->session->userdata("customer_id");
		$name=$new_delivery_address_post_data->name;
		$door_address=$new_delivery_address_post_data->door_address;
		$mobile=$new_delivery_address_post_data->mobile;
		
		$street_address=$new_delivery_address_post_data->street_address;
		$city=$new_delivery_address_post_data->city;
		$pin=$new_delivery_address_post_data->pin;
		$state=$new_delivery_address_post_data->state;
		$country=$new_delivery_address_post_data->country;
		$make_default=$new_delivery_address_post_data->make_default;
		$flag=$this->Customer_account->edit_delivery_address($customer_address_id,$customer_id,$name,$mobile,$door_address,$street_address,$city,$state,$pin,$country,$make_default);
		echo $flag;
	}
	
	public function get_customer_all_address_in_json_format()
	{
		$customer_id=$this->session->userdata("customer_id");
		$this->load->model("Customer_account");
		$all_addresses=$this->Customer_account->get_customer_all_address_in_json_format($customer_id);
		echo json_encode($all_addresses);
		
	}
	
	public function get_count_customer_all_address_in_json_format()
	{
		$customer_id=$this->session->userdata("customer_id");
		$this->load->model("Customer_account");
		$count_all_customer_addresses=$this->Customer_account->get_count_customer_all_address_in_json_format($customer_id);
		return $count_all_customer_addresses;
		
	}
	public function get_cancellation_details_from_admin($order_item_id){
		$cancellation_details_from_admin=$this->Customer_account->get_cancellation_details_from_admin($order_item_id);
			return $cancellation_details_from_admin;
	}
	public function get_cancellation_reason_details_from_admin($reason_cancel_id){
		$cancellation_reason_details_from_admin=$this->Customer_account->get_cancellation_reason_details_from_admin($reason_cancel_id);
			return $cancellation_reason_details_from_admin;
	}
	
	public function get_order_cancelled_by_admin_or_not($order_item_id){
		$data["controller"]=$this;
		$flag=$this->Customer_account->get_order_cancelled_by_admin_or_not($order_item_id);
		return $flag;
	}
	public function get_cancelled_order_customer_payment_type($order_item_id){
		$data["controller"]=$this;
		$payment_type=$this->Customer_account->get_cancelled_order_customer_payment_type($order_item_id);
		return $payment_type;
	}
	public function get_wallet_exists_for_customer($customer_id){
		$data["controller"]=$this;
		$flag=$this->Customer_account->get_wallet_exists_for_customer($customer_id);
		return $flag;
	}
	public function refund_to_customer(){
		$data["controller"]=$this;
		$customer_id=$this->input->post("customer_id_for_refund");
		$order_item_id=$this->input->post("order_item_id_for_refund");
		$product_id=$this->input->post("product_id_for_refund");
		$sku_id=$this->input->post("sku_id_for_refund");
		$product_price=$this->input->post("product_price_for_refund");
		$grandtotal=$this->input->post("grandtotal_for_refund");
		$quantity=$this->input->post("quantity_for_refund");
		
		$refund_method=$this->input->post("refund_method_cancel_".$order_item_id);
		$bank_name=$this->input->post("bank_name");
		$bank_branch_name=$this->input->post("bank_branch_name");
		$bank_city=$this->input->post("bank_city");
		$bank_ifsc_code=$this->input->post("bank_ifsc_code");
		$bank_account_number=$this->input->post("bank_account_number");
		$bank_account_confirm_number=$this->input->post("bank_account_confirm_number");
		$account_holder_name=$this->input->post("account_holder_name");
		$bank_return_refund_phone_number=$this->input->post("bank_return_refund_phone_number");

			///////////////////////
			$cancel_id=$this->Customer_account->get_cancel_id_of_this_transaction($order_item_id);/*get cancel_id from cancels table*/
			
			////////////////////////////////////////
			if($refund_method=="Wallet"){
				/// Wallet
				

					//////////////////////////////////
					$refund_bank_id="";
				
		$flag=$this->Customer_account->update_cancel_table_refund_type($refund_method,$refund_bank_id,$order_item_id);
			}
			if($refund_method=="Bank Transfer"){
				
				if(isset($bank_account_number)){
					$get_bank_account_id=$this->Customer_account->get_bank_account_details($bank_account_number,$customer_id);
					if(!$get_bank_account_id){
						$add_bank_details=$this->Customer_account->add_bank_details($bank_name,$bank_branch_name,$bank_city,$bank_ifsc_code,$bank_account_number,$bank_account_confirm_number,$account_holder_name,$bank_return_refund_phone_number,$customer_id);
						if($add_bank_details){
							$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number,$customer_id);
						}
					}else{
									$refund_bank_account_details_arr=$this->Customer_account->get_refund_bank_account_details($get_bank_account_id);
									
									$add_bank_details=$this->Customer_account->add_bank_details_delivered($refund_bank_account_details_arr["bank_name"],$refund_bank_account_details_arr["branch_name"],$refund_bank_account_details_arr["city"],$refund_bank_account_details_arr["ifsc_code"],$refund_bank_account_details_arr["account_number"],$refund_bank_account_details_arr["confirm_account_number"],$refund_bank_account_details_arr["account_holder_name"],$refund_bank_account_details_arr["mobile_number"],$refund_bank_account_details_arr["customer_id"]);
									if($add_bank_details){
										$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number,$customer_id);
									}
								}
				}
								
						$refund_bank_id=$get_bank_account_id;
						$flag=$this->Customer_account->update_cancel_table_refund_type($refund_method,$refund_bank_id,$order_item_id);
			}
			
			echo $flag;
		}
		
		public function get_availability_of_shipping(){
			$address_arr=$this->Customer_account->get_shipping_address_pincode();
			
			
			$pincode=$address_arr["pincode"];
			$city=$address_arr["city"];
			
			$vendor_id=$this->Customer_account->get_vendor_id();
			$result=$this->Customer_account->get_default_logistics_id_for_all_vendors($vendor_id);
			$logistics_id_arr=array();
			foreach($result as $res){
				$logistics_id_arr[]=$res->logistics_id;
			}
			$flag=$this->Customer_account->get_availability_of_shipping($pincode);
			if($flag){
				//////////////// modified starts updating territory id /////////////////
					
				$cart_result_obj_arr=$this->Customer_account->get_specific_cartinfo_for_customer();
			
				foreach($cart_result_obj_arr as $cartObj){
					$inventory_id=$cartObj->inventory_id;
					$vendor_id=$cartObj->vendor_id;
					$shipping_detail=$this->get_territory_of_pincode($pincode,$vendor_id);
					$logistics_territory_id=$shipping_detail['logistics_territory_id'];
					$this->Customer_account->update_logistics_territory_id($inventory_id,$vendor_id,$logistics_territory_id);
				}
				
				//////////////// modified ends updating territory id /////////////////
				// calculation of shipping_charge
				$cart_result_obj_arr=$this->get_cartinfo_for_customer();
			//	$this->Customer_account->insert_shipping_charge_to_carttable($cart_result_obj_arr,$pincode);
			}
			
			$address_arr['flag']=$flag;
			
			echo json_encode($address_arr);
			
		}
		
		public function get_default_address_pincode_and_city(){
			$address_arr=$this->Customer_account->get_shipping_address_pincode();
			$pincode=$address_arr["pincode"];
			$city=$address_arr["city"];
			echo json_encode($address_arr);
		}
		public function get_returns_conservation_chain_in_customerpanel($return_order_id){
			if($this->session->userdata("logged_in")){
				$result=$this->Customer_account->get_returns_conservation_chain_in_customerpanel($return_order_id);
				return $result;
			}
			else{
				redirect(base_url());
			}	
		}
		
		public function get_return_order_id_by_order_item_id($order_item_id){
			if($this->session->userdata("logged_in")){
				$return_order_id=$this->Customer_account->get_return_order_id_by_order_item_id($order_item_id);
				return $return_order_id;
			}
			else{
				redirect(base_url());
			}	
		}
		
		public function contact_admin(){
			if($this->session->userdata("logged_in")){
				$return_order_id=$this->input->post("return_order_id");
				$user_type=$this->input->post("user_type");
				$status=$this->input->post("status");
				$description=$this->db->escape_str($this->input->post("description"));
				$customer_id=$this->input->post("customer_id");
				$admin_action=$this->input->post("admin_action");
				$customer_action=$this->input->post("customer_action");
				
				$image_path="";
				if($_FILES["image_attachment"]["name"]!=""){
					$ext = pathinfo($_FILES["image_attachment"]["name"], PATHINFO_EXTENSION);
					/////
					$rand=mt_rand(1, 1000000);
					$milliseconds = round(microtime(true) * 1000);
					$str=$milliseconds;
					//////
					$fname=$str.".".$ext;
					move_uploaded_file($_FILES["image_attachment"]["tmp_name"],"assets/returns/".$fname);
					$image_path="assets/returns/".$fname;
				}
				$query=$this->Customer_account->contact_admin($return_order_id,$user_type,$status,$description,$image_path,$customer_id,$admin_action,$customer_action);
				echo $query;
			}
			else{
				redirect(base_url());
			}	
		}
		
		public function get_customer_name($customer_id){
			if($this->session->userdata("logged_in")){
				$customer_name=$this->Customer_account->get_customer_name($customer_id);
				return $customer_name;
			}
			else{
				redirect(base_url());
			}
		}
		
		public function returns_file_download($id){
		if($this->session->userdata("logged_in")){
			$this->load->helper("download");
				   
				$data=$this->Customer_account->getspecified_returns_file($id);
				$image=$data[0]->image;
				   
				$data = file_get_contents($image);
				   
				force_download(basename($image),$data);
		}
		else{
				redirect(base_url());
			}
	}
		
		public function get_unread_return_msg($return_order_id){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$unread_msg=$this->Customer_account->get_unread_return_msg($return_order_id);
			return $unread_msg;
		}
		else{
				redirect(base_url());
			}
	}
	
	public function update_msg_status(){
		if($this->session->userdata("logged_in")){
			$return_order_id=$this->input->post("return_order_id");
			$flag=$this->Customer_account->update_msg_status($return_order_id);
			echo $flag;
		}
		else{
			redirect(base_url());
		}
	}
	
	public function get_order_details_admin_acceptance_refund_data($id){
		if($this->session->userdata("logged_in")){
			$order_item_id=$id;
			$refund_data=$this->Customer_account->get_order_details_admin_acceptance_refund_data($order_item_id);
			return $refund_data;
		}
		else{
			redirect(base_url());
		}
	}
	public function get_order_details_admin_acceptance_refund_data_for_mail($id){
			$order_item_id=$id;
			$refund_data=$this->Customer_account->get_order_details_admin_acceptance_refund_data($order_item_id);
			return $refund_data;
	}
	
	public function accept_reject_refund_status(){
		if($this->session->userdata("logged_in")){
			$order_item_id=$this->input->post("order_item_id");
			$st=$this->input->post("st");
			$flag=$this->Customer_account->accept_reject_refund_status($order_item_id,$st);
			echo $flag;
		}
		else{
			redirect(base_url());
		}
	}
	
	public function get_orders_status_data($order_item_id){
		$orders_status_data=$this->Customer_account->get_orders_status_data($order_item_id);
		return $orders_status_data;
	}
	
	public function get_orders_status_data_prev($order_item_id){
		$orders_status_data=$this->Customer_account->get_orders_status_data_prev($order_item_id);
		return $orders_status_data;
	}
	
	public function return_cancel(){
		$order_item_id=$this->input->post("order_item_id");
		$cancel_returns_comments=$this->input->post("cancel_returns_comments");
		
		$flag=$this->Customer_account->return_cancel($order_item_id,$cancel_returns_comments);
		echo $flag;
	}
	
	public function get_data_cancels_table($order_item_id){
		$cancels_table_data_arr=$this->Customer_account->get_data_cancels_table($order_item_id);
		return $cancels_table_data_arr;
		
	}
	public function get_data_cancelled_orders($order_item_id){
		$cancels_orders_data_arr=$this->Customer_account->get_data_cancelled_orders($order_item_id);
		return $cancels_orders_data_arr;
		
	}
	/*public function get_data_refund_table($cancel_id){
		$refund_table_data_arr=$this->Customer_account->get_data_refund_table($cancel_id);
		return $refund_table_data_arr;
		
	}*/
	
	public function get_data_cancel_refund_table($order_item_id){
		$refund_table_data_arr=$this->Customer_account->get_data_cancel_refund_table($order_item_id);
		return $refund_table_data_arr;
		
	}
	public function get_customer_skills_info($skills_in_arr){
		$skills_arr=explode(",",$skills_in_arr);
		$result=$this->Customer_account->get_customer_skills_info($skills_arr);
		$total_skill_arr=array();
		foreach($result as $resarr){
			$total_skill_arr[]=$resarr["profile_name"];
		}
		return implode(", ",$total_skill_arr);
	}
	
	public function customer_prefer_material_info($materials_in_arr){
		$materials_arr=explode(",",$materials_in_arr);
		$result=$this->Customer_account->customer_prefer_material_info($materials_arr);
		$total_materials_arr=array();
		foreach($result as $resarr){
			$total_materials_arr[]=$resarr["profile_value"];
		}
		return implode(", ",$total_materials_arr);
	}
	
	public function get_in_returned_table_or_not($order_item_id){
		$result=$this->Customer_account->get_in_returned_table_or_not($order_item_id);
		return $result;
	}
	
	public function get_in_completed_table_or_not($order_item_id){
		$result=$this->Customer_account->get_in_completed_table_or_not($order_item_id);
		return $result;
	}
	public function get_return_refund_table_by_order_item_id($order_item_id){
		$return_refund_table=$this->Customer_account->get_return_refund_table_by_order_item_id($order_item_id);
		return $return_refund_table;
	}
	public function sub_request_refund(){
		
		$order_item_id=$this->input->post("order_item_id");
		$return_order_id=$this->input->post("return_order_id");
		$refund_method=$this->input->post("refund_method");
		$refund_bank_id=$this->input->post("refund_bank_id");
		
		
		$refund_remaining_deductions=$this->input->post("refund_remaining_deductions");
		$refund_remaining_quantities=$this->input->post("refund_remaining_quantities");
		$refund_all=$this->input->post("refund_all");
		
		////promo_data/////////////
		
		$promotion_available=$this->input->post("promotion_available");
		
		if(isset($promotion_available) && (isset($refund_remaining_quantities) || isset($refund_all))){

			$promotion_minimum_quantity=$this->input->post("promotion_minimum_quantity");
			$promotion_quote=$this->input->post("promotion_quote");
			$promotion_default_discount_promo=$this->input->post("promotion_default_discount_promo");
			$promotion_item=$this->input->post("promotion_item");
			$promotion_item_num=$this->input->post("promotion_item_num");
			
			$promotion_surprise_gift=$this->input->post("promotion_surprise_gift");
			$promotion_surprise_gift_type=$this->input->post("promotion_surprise_gift_type");
			$promotion_surprise_gift_skus=$this->input->post("promotion_surprise_gift_skus");
			$promotion_surprise_gift_skus_nums=$this->input->post("promotion_surprise_gift_skus_nums");
			
			$promotion_cashback=$this->input->post("promotion_cashback");
			$promotion_item_multiplier=$this->input->post("promotion_item_multiplier");
			$promotion_discount=$this->input->post("promotion_discount");
			$default_discount=$this->input->post("default_discount");
			
			$quantity_without_promotion=$this->input->post("req_quantity_without_promotion");
			$quantity_with_promotion=$this->input->post("req_quantity_with_promotion");
			$cash_back_value=$this->input->post("req_cash_back_value");
			//the price when purchased... should remain....it will not change....
			$individual_price_of_product_with_promotion=$this->input->post("req_individual_price_of_product_with_promotion");
			$individual_price_of_product_without_promotion=$this->input->post("req_individual_price_of_product_without_promotion"); 
			$total_price_of_product_with_promotion=$this->input->post("req_total_price_of_product_with_promotion");
			$total_price_of_product_without_promotion=$this->input->post("req_total_price_of_product_without_promotion");
			
		}
		
		if($refund_remaining_deductions && $refund_remaining_quantities){
			$refund_remaining_quantities_input=$this->input->post("refund_remaining_quantities_input");
			
			if($promotion_available==1){
				
				$flag=$this->Customer_account->update_sub_refund_with_promo_details($order_item_id,$return_order_id,$refund_method,$refund_bank_id,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_item,$promotion_item_num,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion,$refund_remaining_quantities_input,"refund_remaining_deductions","refund_remaining_quantities","");

			}else{
				$flag=$this->Customer_account->update_sub_refund($order_item_id,$return_order_id,$refund_method,$refund_bank_id,$refund_remaining_quantities_input,"refund_remaining_deductions","refund_remaining_quantities","");
			}
			
			
		}
		else if($refund_all){
			$refund_remaining_quantities_input=$this->input->post("remaining_quantity");
			
			if($promotion_available==1){
			
				$flag=$this->Customer_account->update_sub_refund_with_promo_details($order_item_id,$return_order_id,$refund_method,$refund_bank_id,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_item,$promotion_item_num,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion,$refund_remaining_quantities_input,"","","refund_all");
			}else{
				$flag=$this->Customer_account->update_sub_refund($order_item_id,$return_order_id,$refund_method,$refund_bank_id,$refund_remaining_quantities_input,"","","refund_all");	
			}
			
		}else if($refund_remaining_deductions && !$refund_remaining_quantities){

			$flag=$this->Customer_account->update_sub_refund($order_item_id,$return_order_id,$refund_method,$refund_bank_id,'0',"refund_remaining_deductions","","");
		}else if(!$refund_remaining_deductions && $refund_remaining_quantities){
			$refund_remaining_quantities_input=$this->input->post("refund_remaining_quantities_input");
			
			if($promotion_available==1){
				$flag=$this->Customer_account->update_sub_refund_with_promo_details($order_item_id,$return_order_id,$refund_method,$refund_bank_id,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_item,$promotion_item_num,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion,$refund_remaining_quantities_input,"","refund_remaining_quantities","");
				
			}else{
				
				$flag=$this->Customer_account->update_sub_refund($order_item_id,$return_order_id,$refund_method,$refund_bank_id,$refund_remaining_quantities_input,"","refund_remaining_quantities","");
			}
			
		}
		echo $flag;
	}
	
	public function get_grandtotal_of_return_refund_success($order_item_id){
		$grandtotal_of_return_refund_success=$this->Customer_account->get_grandtotal_of_return_refund_success($order_item_id);
		return $grandtotal_of_return_refund_success;
	}
	public function get_grandtotal_of_purchased_quantity($order_item_id){
		$grandtotal_of_purchased_quantity=$this->Customer_account->get_grandtotal_of_purchased_quantity($order_item_id);
		return $grandtotal_of_purchased_quantity;
	}
	public function return_refund_request_again_not_refunded($order_item_id){
		$return_refund_request_again_not_refunded_arr=$this->Customer_account->return_refund_request_again_not_refunded($order_item_id);
		return $return_refund_request_again_not_refunded_arr;
	}
	public function return_refund_request_again_refunded($order_item_id){
		$return_refund_request_again_refunded_arr=$this->Customer_account->return_refund_request_again_refunded($order_item_id);
		return $return_refund_request_again_refunded_arr;
	}
	public function return_refund_request_again($order_item_id){
		$return_refund_request_again_arr=$this->Customer_account->return_refund_request_again($order_item_id);
		return $return_refund_request_again_arr;
	}
	public function get_sub_return_refund_table_by_order_item_id($order_item_id){
		$sub_return_refund_table=$this->Customer_account->get_sub_return_refund_table_by_order_item_id($order_item_id);
		return $sub_return_refund_table;
	}
	/////////////////////////// replacements starts ////////////////////////////////////
	public function get_unread_return_msg_replacement($return_order_id){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$unread_msg=$this->Customer_account->get_unread_return_msg_replacement($return_order_id);
			return $unread_msg;
		}
		else{
				redirect(base_url());
			}
	}
	
	public function get_return_order_id_by_order_item_id_for_replacement($order_item_id){
			if($this->session->userdata("logged_in")){
				$return_order_id=$this->Customer_account->get_return_order_id_by_order_item_id_for_replacement($order_item_id);
				return $return_order_id;
			}
			else{
				redirect(base_url());
			}	
		}
		
		public function get_returns_conservation_chain_in_customerpanel_for_replacement($return_order_id){
			if($this->session->userdata("logged_in")){
				$result=$this->Customer_account->get_returns_conservation_chain_in_customerpanel_for_replacement($return_order_id);
				return $result;
			}
			else{
				redirect(base_url());
			}	
		}
		
		public function get_order_details_admin_acceptance_refund_data_for_replacement($id){
		if($this->session->userdata("logged_in")){
			$order_item_id=$id;
			$refund_data=$this->Customer_account->get_order_details_admin_acceptance_refund_data_for_replacement($order_item_id);
			return $refund_data;
		}
		else{
			redirect(base_url());
		}
	}
	public function get_order_details_admin_acceptance_refund_data_for_replacement_for_mail($id){
			$order_item_id=$id;
			$refund_data=$this->Customer_account->get_order_details_admin_acceptance_refund_data_for_replacement($order_item_id);
			return $refund_data;
	}
	
	public function contact_admin_replacement(){
			if($this->session->userdata("logged_in")){
				$return_order_id=$this->input->post("return_order_id");
				$user_type=$this->input->post("user_type");
				$status=$this->input->post("status");
				$description=$this->db->escape_str($this->input->post("description"));
				$customer_id=$this->input->post("customer_id");
				$admin_action=$this->input->post("admin_action");
				$customer_action=$this->input->post("customer_action");
				
				$image_path="";
				if($_FILES["image_attachment"]["name"]!=""){
					$ext = pathinfo($_FILES["image_attachment"]["name"], PATHINFO_EXTENSION);
					/////
					$rand=mt_rand(1, 1000000);
					$milliseconds = round(microtime(true) * 1000);
					$str=$milliseconds;
					//////
					$fname=$str.".".$ext;
					move_uploaded_file($_FILES["image_attachment"]["tmp_name"],"assets/returns/".$fname);
					$image_path="assets/returns/".$fname;
				}
				$query=$this->Customer_account->contact_admin_replacement($return_order_id,$user_type,$status,$description,$image_path,$customer_id,$admin_action,$customer_action);
				echo $query;
			}
			else{
				redirect(base_url());
			}	
		}
		
		public function update_msg_status_replacement(){
			if($this->session->userdata("logged_in")){
				$return_order_id=$this->input->post("return_order_id");
				$flag=$this->Customer_account->update_msg_status_replacement($return_order_id);
				echo $flag;
			}
			else{
				redirect(base_url());
			}
		}
		
		public function returns_file_download_replacement($id){
			if($this->session->userdata("logged_in")){
				$this->load->helper("download");
					   
					$data=$this->Customer_account->getspecified_returns_file_replacement($id);
					$image=$data[0]->image;
					   
					$data = file_get_contents($image);
					   
					force_download(basename($image),$data);
			}
			else{
					redirect(base_url());
				}
		}
		
		public function get_availability_of_order_item_id_in_refunds_replacements($order_item_id){
			$availability_of_order_item_id_in_refunds_replacements=$this->Customer_account->get_availability_of_order_item_id_in_refunds_replacements($order_item_id);
			if($availability_of_order_item_id_in_refunds_replacements>0){
				$flag="yes";
			}
			else{
				$flag="no";
			}
			return $flag;
		}
		public function get_inventory_info_by_inventory_id($inventory_id){
				$data["controller"]=$this;
				$inventory_obj=$this->Customer_account->get_inventory_info_by_inventory_id($inventory_id);
				return $inventory_obj;
		}
		public function get_primary_replacement_reason_admin($reason_return_id){
			return $this->Customer_account->get_primary_replacement_reason_admin($reason_return_id);
		}
		
		public function accept_reject_replacement_status(){
			if($this->session->userdata("logged_in")){
				$order_item_id=$this->input->post("order_item_id");
				$st=$this->input->post("st");
				$flag=$this->Customer_account->accept_reject_replacement_status($order_item_id,$st);
				echo $flag;
			}
			else{
				redirect(base_url());
			}
		}
		
		public function get_replaced_orders_by_prev_order_item_id($prev_order_item_id){
			$replaced_orders_by_prev_order_item_id_arr=$this->Customer_account->get_replaced_orders_by_prev_order_item_id($prev_order_item_id);
			return $replaced_orders_by_prev_order_item_id_arr;
		}
		
		public function get_customer_wallet_amount($customer_id){
			return $this->Customer_account->get_customer_wallet_amount($customer_id);
		}
		public function payment_by_customer(){
			$amount_paid=$this->input->post("amount_paid");
			$return_order_id=$this->input->post("return_order_id");
			$order_item_id=$this->input->post("order_item_id");
			$customer_payment_ways=$this->input->post("customer_payment_ways");
			if($customer_payment_ways=="wallet"){
			
			$customer_id=$this->session->userdata("customer_id");
				/// Wallet
				
				/*get customer wallet id*/
				$wallet_summary=$this->Customer_account->get_customer_wallet_summary($customer_id);
				foreach($wallet_summary as $summary){
					$wallet_id=$summary['wallet_id'];
					$amount=$summary['wallet_amount'];
				}
				
				$transaction_details="Customer paid for return order id :".$return_order_id;
				$debit=$amount_paid;
				$credit="";
				$amount=$amount-$amount_paid;
				$wallet_type="Customer paid for return order";
				$this->Customer_account->update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$wallet_type);
					//////////////////////////////////
				echo $this->Customer_account->update_payment_status($order_item_id,$customer_payment_ways);
			}
			
			else{
				echo $this->Customer_account->update_payment_status($order_item_id,$customer_payment_ways);
			}
			
		}
		
		public function proceed_for_replacement_from_stock(){
			if($this->session->userdata("logged_in")){
				$order_item_id=$this->input->post("order_item_id");
				$return_stock_notification_arr=$this->Customer_account->get_return_stock_notification_data_by_order_item_id($order_item_id);
				
				$selected_inventory_id_for_rep=$return_stock_notification_arr["replacement_with_inventory_id"];
				$order_item_id=$return_stock_notification_arr["order_item_id"];
				$return_order_id=$return_stock_notification_arr["return_order_id"];
				$cancel_reason=$return_stock_notification_arr["reason_return_id"];
				$sub_cancel_reason=$return_stock_notification_arr["sub_issue_id"];
				$cancel_reason_comment=$return_stock_notification_arr["return_reason_comments"];
				$replacement_value_each=$return_stock_notification_arr["replacement_value_each_inventory_id"];
				$return_value_each=$return_stock_notification_arr["return_value_each_inventory_id"];
				$desired_quantity_replacement=$return_stock_notification_arr["quantity_replacement"];
				$refund_method=$return_stock_notification_arr["refund_method"];
				$refund_bank_id=$return_stock_notification_arr["refund_bank_id"];
				$customer_decision=$return_stock_notification_arr["customer_decision"];
				$customer_comments=$return_stock_notification_arr["customer_comments"];
				$edit_timelimit_comments=$return_stock_notification_arr["edit_timelimit_comments"];
				
				$shipping_charge_purchased=$return_stock_notification_arr["shipping_charge_purchased"];
				$shipping_charge_requested=$return_stock_notification_arr["shipping_charge_requested"];
				$shipping_charge_percent_paid_by_customer=$return_stock_notification_arr["shipping_charge_percent_paid_by_customer"];
				$original_inventory_id=$return_stock_notification_arr["original_inventory_id"];
				$order_item_invoice_discount_value_each=$return_stock_notification_arr["order_item_invoice_discount_value_each"];
				
					$paid_by="";
					
					if($return_value_each>$replacement_value_each){
						$paid_by="admin";
					}
					if($return_value_each<$replacement_value_each){
						$paid_by="customer";
						
					}
					$balance_amount_paid_by="";
					if($paid_by=="customer"){
						//$balance_amount_paid_by="wallet";
						$balance_amount_paid_by="";
						//$refund_method="";
					}
					$amount_paid=$desired_quantity_replacement*abs($return_value_each-$replacement_value_each);
					$payment_status="pending";
					$amount_paid_item_price="";
					//////////////////
					//$shipping_charge_requested=$this->get_corresponding_shipping_charge_for_replacement_request($order_item_id,$desired_quantity_replacement,$selected_inventory_id_for_rep);
					
					$replacement_with_limited_stock_available=$this->Customer_account->insert_into_returns_replacement_desired_table_from_stock($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method,$refund_bank_id,$paid_by,$amount_paid,$amount_paid_item_price,$payment_status,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$shipping_charge_percent_paid_by_customer,$original_inventory_id,$order_item_invoice_discount_value_each);

					$customer_id=$this->session->userdata("customer_id");
					$quantity=$desired_quantity_replacement;
					$balance_amount=$amount_paid;
					$history_orders_data_obj=$this->Customer_account->get_history_orders_data_by_order_item_id($order_item_id);
					$history_inventory_id=$history_orders_data_obj->inventory_id;
					$inventory_id=$history_inventory_id;
					$replacement_with_inventory_id=$selected_inventory_id_for_rep;
					$product_price="";
					$shipping_charge="";
					$sub_reason_return_id="";
					$status="accept";
					$comments="";
					$disable_comm_flag=0;
					$allow_cust_to_make_decision_flag=0;
					$customer_decision="accept";
					$vendor_id="";
					$customer_status="pending";
					$customer_status_comments="";
					$return_shipping_concession_chk=$return_stock_notification_arr["return_shipping_concession_chk"];
					$return_shipping_concession=0;

					//$order_replacement_decision_status=$this->Customer_account->insert_into_order_replacement_decision_table_from_stock($return_order_id,$customer_id,$quantity,$balance_amount,$inventory_id,$replacement_with_inventory_id,$product_price,$shipping_charge,$order_item_id,$sub_reason_return_id,$status,$comments,$disable_comm_flag,$allow_cust_to_make_decision_flag,$customer_decision,$vendor_id,$customer_status,$customer_status_comments,$return_shipping_concession_chk,$return_shipping_concession);
					
					$this->Customer_account->delete_from_return_stock_notification_by_order_item_id($order_item_id);
					echo $replacement_with_limited_stock_available;
			}
			else{
				redirect(base_url());
			}
		}
		
		public function proceed_for_cancellation_from_stock(){
			if($this->session->userdata("logged_in")){
				$order_item_id=$this->input->post("r_order_item_id");
				$quantity_replacement=$this->input->post("quantity_replacement");
				$reserved_stock=$this->input->post("reserved_stock");
				$replacement_with_inventory_id=$this->input->post("replacement_with_inventory_id");
				
				if($reserved_stock!=null && $reserved_stock!='' && $reserved_stock>0){
					$update_stock=$this->Customer_account->add_inventory_stock_when_customer_cancels($replacement_with_inventory_id,$quantity_replacement);
				}
					
				$flag=$this->Customer_account->proceed_for_cancellation_from_stock($order_item_id);
				echo $flag;
			}else{
				redirect(base_url());
			}
		}
		
		public function customer_decision_for_move_to_refund_from_stock(){
			if($this->session->userdata("logged_in")){
				$order_item_id=$this->input->post("order_item_id");
				$customer_decision=$this->input->post("customer_decision");
				$status_of_refund_for_cashback_on_sku=$this->input->post("status_of_refund_for_cashback_on_sku");
				$flag=$this->Customer_account->customer_decision_for_move_to_refund_from_stock($order_item_id,$customer_decision);
				if($status_of_refund_for_cashback_on_sku=="pending"){
					$flag2=$this->Customer_account->update_reject_order_item_customer_response($order_item_id);
				}
				
				echo $flag;
			}
			else{
				redirect(base_url());
			}
		}
		public function get_active_orders_data($prev_order_item_id){
			$active_orders_data=$this->Customer_account->get_active_orders_data($prev_order_item_id);
			return $active_orders_data;
		}
		
		public function replacement_cancel(){
			
			$order_item_id=$this->input->post("order_item_id");
			
			$reason_for_undelivered=$this->Customer_account->get_undelivered_replacement_desired($order_item_id);
			
			$cancel_replacement_comments=$this->input->post("cancel_replacement_comments");
			$type_of_replacement_cancel=$this->input->post("type_of_replacement_cancel");
			$paid_by=$this->input->post("paid_by");
			
			$replacement_with_inventory_id=$this->input->post("replacement_with_inventory_id");
			$quantity_replacement=$this->input->post("quantity_replacement");
			
			if($reason_for_undelivered==""){

				$flag=$this->Customer_account->replacement_cancel($order_item_id,$cancel_replacement_comments,$type_of_replacement_cancel);	
			}
			
			$stock_updated=$this->Customer_account->add_inventory_stock_when_customer_cancels($replacement_with_inventory_id,$quantity_replacement);
			
			///////////////////////////////////////
			
		/** updating the refund method starts  **/
		if($type_of_replacement_cancel=="refund_back" && ($paid_by=="customer" || $paid_by=="")){
			
			$customer_id=$this->session->userdata("customer_id");
			$order_item_id=$this->input->post("order_item_id");
			$refund_method=$this->input->post("refund_method_".$order_item_id);
			$bank_name=$this->input->post("bank_name_rep");
			$bank_branch_name=$this->input->post("bank_branch_name_rep");
			$bank_city=$this->input->post("bank_city_rep");
			$bank_ifsc_code=$this->input->post("bank_ifsc_code_rep");
			$bank_account_number=$this->input->post("bank_account_number_rep");
			$bank_account_confirm_number=$this->input->post("bank_account_confirm_number_rep");
			$account_holder_name=$this->input->post("account_holder_name_rep");
			$bank_return_refund_phone_number=$this->input->post("bank_return_refund_phone_number_rep");
			
				if($this->input->post("refund_method_".$order_item_id)){
				if($refund_method=="Wallet"){
					$refund_bank_id="";
				}
				if($refund_method=="Bank Transfer"){
					
					if(isset($bank_account_number)){
							$get_bank_account_id=$this->Customer_account->get_bank_account_details($bank_account_number,$customer_id);
							if(!$get_bank_account_id){
								$add_bank_details=$this->Customer_account->add_bank_details($bank_name,$bank_branch_name,$bank_city,$bank_ifsc_code,$bank_account_number,$bank_account_confirm_number,$account_holder_name,$bank_return_refund_phone_number,$customer_id);
								if($add_bank_details){
									$get_bank_account_id=$this->Customer_account->get_bank_account_details($bank_account_number,$customer_id);
								}
							}else{
								$refund_bank_account_details_arr=$this->Customer_account->get_refund_bank_account_details($get_bank_account_id);
								
								$add_bank_details=$this->Customer_account->add_bank_details_delivered($refund_bank_account_details_arr["bank_name"],$refund_bank_account_details_arr["branch_name"],$refund_bank_account_details_arr["city"],$refund_bank_account_details_arr["ifsc_code"],$refund_bank_account_details_arr["account_number"],$refund_bank_account_details_arr["confirm_account_number"],$refund_bank_account_details_arr["account_holder_name"],$refund_bank_account_details_arr["mobile_number"],$refund_bank_account_details_arr["customer_id"]);
								if($add_bank_details){
									$get_bank_account_id=$this->Customer_account->get_bank_account_details_delivered($bank_account_number,$customer_id);
								}
							}
						}
							
					$refund_bank_id=$get_bank_account_id;
							
				}
				$flag=$this->Customer_account->refund_method_to_customer_for_cancels_insert($order_item_id,$refund_method,$refund_bank_id);
				}
		}
			/** updating the refund method ends  **/
			//////////////////////////////////////
	
		if($reason_for_undelivered==""){
				
			$active_orders_data=$this->get_active_orders_data($order_item_id);
			if(count($active_orders_data)!=0){
				$order_item_id=$active_orders_data["order_item_id"];
				
				$refund_method="Wallet";
				$cancel_reason="";
				$cancel_reason_comment=$cancel_replacement_comments;
				/*first add order item to cancels*/
				$user_type=$this->session->userdata("user_type");

						$order_active_data=$this->Customer_account->get_all_order_data_from_order_item($order_item_id);/*fetch data from active order table based on order_item_id*/
							
						foreach($order_active_data as $data){
							$purchased_price=$data['purchased_price'];
							$prev_order_item_id=$data['prev_order_item_id'];
							$order_item_id=$data['order_item_id'];
							$customer_id=$data['customer_id'];
							$order_id=$data['order_id'];
							$shipping_address_id=$data['shipping_address_id'];
							$inventory_id=$data['inventory_id'];
							$product_id=$data['product_id'];
							$sku_id=$data['sku_id'];
							$tax_percent=$data['tax_percent'];
							$product_price=$data['product_price'];
							$shipping_charge=$data['shipping_charge'];
							$subtotal=$data['subtotal'];
							$wallet_status=$data['wallet_status'];
							$wallet_amount=$data['wallet_amount'];
							$grandtotal=$data['grandtotal'];
							$quantity=$data['quantity'];
							$timestamp=$data['timestamp'];
							$image=$data['image'];
							$random_number=$data['random_number'];
							$invoice_email=$data['invoice_email'];
							$invoice_number=$data['invoice_number'];
							$logistics_name=$data['logistics_name'];
							$logistics_weblink=$data['logistics_weblink'];
							$tracking_number=$data['tracking_number'];
							$expected_delivery_date=$data['expected_delivery_date'];
							$vendor_id=$data['vendor_id'];
							$payment_type=$data['payment_type'];
							$payment_status=$data['payment_status'];
							$return_period=$data['return_period'];
							$delivery_mode=$data['delivery_mode'];
							
							$logistics_id=$data['logistics_id'];
							$logistics_delivery_mode_id=$data['logistics_delivery_mode_id'];
							$logistics_territory_id=$data['logistics_territory_id'];
							$logistics_parcel_category_id=$data['logistics_parcel_category_id'];

							$promotion_available=$data['promotion_available'];
							$promotion_residual=$data['promotion_residual'];
							$promotion_discount=$data['promotion_discount'];
							$promotion_cashback=$data['promotion_cashback'];
							$promotion_surprise_gift=$data['promotion_surprise_gift'];
							$promotion_surprise_gift_type=$data['promotion_surprise_gift_type'];
							$promotion_surprise_gift_skus=$data['promotion_surprise_gift_skus'];
							$promotion_surprise_gift_skus_nums=$data['promotion_surprise_gift_skus_nums'];
							$default_discount=$data['default_discount'];
							$promotion_item_multiplier=$data['promotion_item_multiplier'];
							$promotion_item=$data['promotion_item'];
							$promotion_item_num=$data['promotion_item_num'];
							$promotion_clubed_with_default_discount=$data['promotion_clubed_with_default_discount'];
							$promotion_id_selected=$data['promotion_id_selected'];
							$promotion_type_selected=$data['promotion_type_selected'];
							$promotion_minimum_quantity=$data['promotion_minimum_quantity'];
							$promotion_quote=$data['promotion_quote'];
							$promotion_default_discount_promo=$data['promotion_default_discount_promo'];
							$individual_price_of_product_with_promotion=$data['individual_price_of_product_with_promotion'];
							$individual_price_of_product_without_promotion=$data['individual_price_of_product_without_promotion'];
							$total_price_of_product_without_promotion=$data['total_price_of_product_without_promotion'];
							$total_price_of_product_with_promotion=$data['total_price_of_product_with_promotion'];
							$quantity_without_promotion=$data['quantity_without_promotion'];
							$quantity_with_promotion=$data['quantity_with_promotion'];
							$cash_back_value=$data['cash_back_value'];
                            $razorpayOrderId=$data['razorpayOrderId'];
                            $razorpayPaymentId=$data['razorpayPaymentId'];
							$total_price_without_shipping_price=$data['total_price_without_shipping_price'];
							$promotion_invoice_discount=$data['promotion_invoice_discount'];
							
							
							$ord_max_selling_price=$data['ord_max_selling_price'];
							$ord_selling_discount=$data['ord_selling_discount'];
							$ord_tax_percent_price=$data['ord_tax_percent_price'];
							$ord_taxable_price=$data['ord_taxable_price'];
							$ord_sku_name=$data['ord_sku_name'];
							
							/* coupon */

							$ord_coupon_code=$data['ord_coupon_code'];
							$ord_coupon_name=$data['ord_coupon_name'];
							$ord_coupon_type=$data['ord_coupon_type'];
							$ord_coupon_value=$data['ord_coupon_value'];
							$ord_coupon_reduce_price=$data['ord_coupon_reduce_price'];
							$ord_coupon_status=$data['ord_coupon_status'];

							/* coupon */
							/* addon products details */
							$ord_addon_products_status=$data['ord_addon_products_status'];
							$ord_addon_products=$data['ord_addon_products'];
							$ord_addon_inventories=$data['ord_addon_inventories'];
							$ord_addon_total_price=$data['ord_addon_total_price'];
							
							/* addon products details */
							//,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price
								
							
						}
						
						//$stock_updated=$this->Customer_account->add_inventory_stock_when_customer_cancels($replacement_with_inventory_id,$quantity_replacement);
						
						/**/
						if($promotion_invoice_discount>0){
							
							$invoice_discount=($subtotal/$total_price_without_shipping_price)*100;					
							$each_invoice_amount=$promotion_invoice_discount*$invoice_discount/100;
							//$invoice_amount=round($promotion_invoice_discount-$each_invoice_amount,2);
							$order_item_invoice_discount_value=round($each_invoice_amount,2);
							$order_item_invoice_discount_value_each=round($order_item_invoice_discount_value/$quantity,2);
						}else{
							$order_item_invoice_discount_value=0;
							$order_item_invoice_discount_value_each=0;
						}
						
						/**/
						$notify_cancel=0;
						/* move above data to cancelled_orders table*/
						$add_to_cancel_orders=$this->Customer_account->active_order_add_to_cancel_orders($purchased_price,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$vendor_id,$return_period,$delivery_mode,$payment_type,$payment_status,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$order_item_invoice_discount_value,$prev_order_item_id,$notify_cancel,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price);
						
						if($add_to_cancel_orders){
						 
							$update_order_item_id_to_cancel=$this->Customer_account->update_order_item_id_to_cancel($order_item_id);/*changing order_item to cancelled*/
					
							if($update_order_item_id_to_cancel){
								$this->Customer_account->remove_data_from_active_order_table($order_item_id);
							}
						}
				
					//////////////////////////////
				}
		}
			echo $flag;
		}
		
		public function get_order_shipped_of_item_by_return_order_id($return_order_id){
			return $this->Customer_account->get_order_shipped_of_item_by_return_order_id($return_order_id);
		}

		public function get_replaced_quantity_in_replacements($order_item_id){
			$order_replacement_decision_arr=$this->Customer_account->get_replaced_quantity_in_replacements($order_item_id);
			
			$get_returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj=$this->Customer_account->returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin($order_item_id);
			$quantity_replacement_no_balance=0;
			if(count($get_returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj)!=0){
				$quantity_replacement_no_balance=$get_returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj->quantity;		
			}
			if(count($order_replacement_decision_arr)>0){
				if($order_replacement_decision_arr["status"]=="refunded"){
					return $order_replacement_decision_arr["quantity"];
				}
				else if($quantity_replacement_no_balance!=0){
					return $quantity_replacement_no_balance;
				}
				else{
					return 0;
				}
			}
			else{
				return 0;
			}
		}
		
		public function cancel_refund_request(){
			$return_stock_notification_id=$this->input->post("return_stock_notification_id");
			$flag==$this->Customer_account->cancel_refund_request($return_stock_notification_id);
			echo $flag;
		}
		
		public function get_minimum_and_maximum_order_quantity($inventory_id){
			$obj=$this->Customer_account->get_minimum_and_maximum_order_quantity($inventory_id);
			return $obj;
		}
		public function get_corresponding_shipping_charge(){
			//this function is used in return_order page (customer view file)
			$order_item_id=$this->input->post("order_item_id");
			$desired_quantity_refund=$this->input->post("desired_quantity_refund");
			$selected_inventory_id=$this->input->post("selected_inventory_id");
			$logistics_details_arr=$this->Customer_account->get_logistics_details($order_item_id);
			
			if(!empty($logistics_details_arr)){		
				
				$logistics_id=$logistics_details_arr["logistics_id"];
				$logistics_delivery_mode_id=$logistics_details_arr["logistics_delivery_mode_id"];
				$logistics_territory_id=$logistics_details_arr["logistics_territory_id"];
				$logistics_parcel_category_id=$logistics_details_arr["logistics_parcel_category_id"];
				$inventory_id=$logistics_details_arr["inventory_id"];
				
				$customer_has_to_pay_arr=$this->Customer_account->get_customer_pays_shipping_charge_percentage($selected_inventory_id);
				
				if(!empty($customer_has_to_pay_arr)){
					$customer_has_to_pay=0;
					foreach($customer_has_to_pay_arr as $customer_has_to_pay_arr_val){
						$customer_has_to_pay=$customer_has_to_pay_arr_val['percent_paid_by_customer'];
					}
				}else{
					$customer_has_to_pay=0;
				}
				$logistics_inventory_arr=$this->Customer_account->get_logistics_inventory($inventory_id);
				
				if(!empty($logistics_inventory_arr)){
					$inventory_weight_in_kg=$logistics_inventory_arr["inventory_weight_in_kg"];
					$inventory_moq=$logistics_inventory_arr["inventory_moq"];
					$inventory_volume_in_kg=$logistics_inventory_arr["inventory_volume_in_kg"]*$desired_quantity_refund;
					$weight_per_unit=$inventory_weight_in_kg/$inventory_moq;	
					$calculated_weight_in_kg=$weight_per_unit*$desired_quantity_refund;
					
						$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($inventory_id);
						if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
							$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
						}
						else{
					
							$logistics_price=$this->Customer_account->get_logistics_price_for_desired_quantity_refund($desired_quantity_refund,$order_item_id,$calculated_weight_in_kg,$inventory_weight_in_kg,$inventory_volume_in_kg,$inventory_id,$logistics_territory_id,$logistics_delivery_mode_id,$logistics_parcel_category_id);
						}
					
					echo json_encode(array("shipping_charge"=>$logistics_price,"customer_has_to_pay"=>$customer_has_to_pay));
				}else{
					echo 'll';
				}				
			}else{
				echo 'opp';
			}
		}
		public function get_corresponding_shipping_charge_customer_has_to_pay($order_item_id,$desired_quantity_refund,$selected_inventory_id){
			//this function is used in return stock notification when requesting
			
			$logistics_details_arr=$this->Customer_account->get_logistics_details($order_item_id);
			
			if(!empty($logistics_details_arr)){		
				
				$logistics_id=$logistics_details_arr["logistics_id"];
				$logistics_delivery_mode_id=$logistics_details_arr["logistics_delivery_mode_id"];
				$logistics_territory_id=$logistics_details_arr["logistics_territory_id"];
				$logistics_parcel_category_id=$logistics_details_arr["logistics_parcel_category_id"];
				$inventory_id=$logistics_details_arr["inventory_id"];
				
				$customer_has_to_pay_arr=$this->Customer_account->get_customer_pays_shipping_charge_percentage($selected_inventory_id);
				
				if(!empty($customer_has_to_pay_arr)){
					$customer_has_to_pay=0;
					foreach($customer_has_to_pay_arr as $customer_has_to_pay_arr_val){
						$customer_has_to_pay=$customer_has_to_pay_arr_val['percent_paid_by_customer'];
					}
				}else{
					$customer_has_to_pay=0;
				}
				$logistics_inventory_arr=$this->Customer_account->get_logistics_inventory($inventory_id);
				
				if(!empty($logistics_inventory_arr)){
					$inventory_weight_in_kg=$logistics_inventory_arr["inventory_weight_in_kg"];
					$inventory_moq=$logistics_inventory_arr["inventory_moq"];
					$inventory_volume_in_kg=$logistics_inventory_arr["inventory_volume_in_kg"]*$desired_quantity_refund;
					$weight_per_unit=$inventory_weight_in_kg/$inventory_moq;	
				
					$calculated_weight_in_kg=$weight_per_unit*$desired_quantity_refund;
					
					$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($inventory_id);
						if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
							$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
						}
						else{
							
					$logistics_price=$this->Customer_account->get_logistics_price_for_desired_quantity_refund($desired_quantity_refund,$order_item_id,$calculated_weight_in_kg,$inventory_weight_in_kg,$inventory_volume_in_kg,$inventory_id,$logistics_territory_id,$logistics_delivery_mode_id,$logistics_parcel_category_id);
					
						}
					
					
					return array("shipping_charge"=>$logistics_price,"customer_has_to_pay"=>$customer_has_to_pay);
					
				}else{
					echo '';
				}				
			}else{
				echo '';
			}
		}
		
		public function get_corresponding_shipping_charge_for_replacement_request($order_item_id,$desired_quantity_refund,$selected_inventory_id_for_rep){
			
			$logistics_details_arr=$this->Customer_account->get_logistics_details($order_item_id);
			if(!empty($logistics_details_arr)){		
				$logistics_id=$logistics_details_arr["logistics_id"];
				$logistics_delivery_mode_id=$logistics_details_arr["logistics_delivery_mode_id"];
				$logistics_territory_id=$logistics_details_arr["logistics_territory_id"];
				$logistics_parcel_category_id=$logistics_details_arr["logistics_parcel_category_id"];
				$inventory_id=$logistics_details_arr["inventory_id"];
				
				$customer_has_to_pay_arr=$this->Customer_account->get_customer_pays_shipping_charge_percentage($selected_inventory_id_for_rep);
				
				if(!empty($customer_has_to_pay_arr)){
					$customer_has_to_pay=0;
					foreach($customer_has_to_pay_arr as $customer_has_to_pay_arr_val){
						$customer_has_to_pay=$customer_has_to_pay_arr_val['percent_paid_by_customer'];
					}
				}else{
					$customer_has_to_pay=0;
				}
				$logistics_inventory_arr=$this->Customer_account->get_logistics_inventory($inventory_id);
				
				if(!empty($logistics_inventory_arr)){
					$inventory_weight_in_kg=$logistics_inventory_arr["inventory_weight_in_kg"];
					$inventory_moq=$logistics_inventory_arr["inventory_moq"];
					$inventory_volume_in_kg=$logistics_inventory_arr["inventory_volume_in_kg"];
					$weight_per_unit=round($inventory_weight_in_kg/$inventory_moq);	
				
					$calculated_weight_in_kg=$weight_per_unit*$desired_quantity_refund;
					
					$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($inventory_id);
						if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
							$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
						}
						else{
					
								$logistics_price=$this->Customer_account->get_logistics_price_for_desired_quantity_refund($desired_quantity_refund,$order_item_id,$calculated_weight_in_kg,$inventory_weight_in_kg,$inventory_volume_in_kg,$inventory_id,$logistics_territory_id,$logistics_delivery_mode_id,$logistics_parcel_category_id);
						}
					$shipping_charge=$logistics_price;
					
					if($customer_has_to_pay>0){
						$shipping_charge=round(($customer_has_to_pay/100)*$shipping_charge);
					}else{
						$shipping_charge=0;
					}
					echo $shipping_charge;
				}else{
					echo 'll';
				}				
			}else{
				echo 'opp';
			}
		}

		public function update_bank_details_for_wallet(){
			$customer_id=$this->input->post("customer_id");
			$wallet_id=$this->input->post("wallet_id");
			$bank_name=$this->input->post("bank_name");
			$branch_name=$this->input->post("bank_branch_name");
			$city=$this->input->post("bank_city");
			$ifsc_code=$this->input->post("bank_ifsc_code");
			$bank_account_number=$this->input->post("bank_account_number");		
		   	$confirm_account_number=$this->input->post("bank_account_confirm_number");
		   	$account_holder_name=$this->input->post("account_holder_name");
		   	$mobile_number=$this->input->post("bank_return_refund_phone_number");
		   	$transaction_amount=$this->input->post("transaction_amount");
		   	$wallet_trans_id_in=$this->input->post("wallet_trans_id_in");

		   	$date = new DateTime();
			$mt=microtime();
			$salt="wallet_request_id";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$wallet_request_id=md5($str);
			
			
			$wallet_transfer_bank=$this->Customer_account->add_wallet_transfer_bank($customer_id,$wallet_id,$bank_name,$branch_name,$city,$ifsc_code,$bank_account_number,$confirm_account_number,$account_holder_name,$mobile_number,$transaction_amount,$wallet_request_id,$wallet_trans_id_in);

			if($wallet_transfer_bank==true){
				echo "success";
			}else{
				echo "error";
			}
		}

		public function wallet_transfer_bank_processing(){
		
		$customer_id=$this->input->post("customer_id");
		 $wallet_id=$this->input->post("wallet_id");
		 $bank_name=$this->input->post("bank_name");
		 $branch_name=$this->input->post("bank_branch_name");
		 $city=$this->input->post("bank_city");
		 $ifsc_code=$this->input->post("bank_ifsc_code");
		 $bank_account_number=$this->input->post("bank_account_number");		
		$confirm_account_number=$this->input->post("bank_account_confirm_number");
		$account_holder_name=$this->input->post("account_holder_name");
		$mobile_number=$this->input->post("bank_return_refund_phone_number");
		$transaction_amount=$this->input->post("transaction_amount");
		$wallet_trans_id_in=$this->input->post("wallet_trans_id_in");
		
		
		/* this functionality moved when admin makes successful transaction */
		$wallet_summary=$this->Customer_account->get_customer_wallet_summary_for_wallet_bank($customer_id);
		foreach($wallet_summary as $summary){
			$wallet_id=$summary['wallet_id'];
			$amount=$summary['wallet_amount'];
		}
		
		$transaction_details="Transferring Wallet Money to Bank";
		$debit=$transaction_amount;
		$credit=0;
		$amount=$amount-$debit;
		$wallet_type="Bank Transfer Request";
		$this->Customer_account->update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$wallet_type);
		/* this functionality moved when admin makes successful transaction */	
				
		//generate a return_order_id
			$date = new DateTime();
			$mt=microtime();
			$salt="wallet_request_id";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$wallet_request_id=md5($str);
			
			
		$wallet_transfer_bank=$this->Customer_account->add_wallet_transfer_bank($customer_id,$wallet_id,$bank_name,$branch_name,$city,$ifsc_code,$bank_account_number,$confirm_account_number,$account_holder_name,$mobile_number,$transaction_amount,$wallet_request_id,$wallet_trans_id_in);

		if($wallet_transfer_bank==true){
			echo "success";
		}else{
			echo "error";
		}
		
		}
	
		
public function update_bank_details_wallet_bank($wallet_transaction_bank_id){
	if($this->session->userdata("logged_in")){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="update_bank_details";
		
		$bank_account_number=$this->Customer_account->get_bank_account_number_bank_id_wallet_transaction_bank_id($wallet_transaction_bank_id);
		$bank_detail=array();
		if(!empty($bank_account_number)){
			$bank_detail=$this->get_bank_detail_wallet_transaction_bank_id($bank_account_number);
		}
		$data['bank_detail']=$bank_detail;
		$data['order_item_id']="";
		$data['wallet_bank_transfer']="yes";
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('customer/update_bank_details');
		$this->load->view('footer');
	}
	else{
		redirect(base_url());
	}	
}


public function get_bank_detail_wallet_transaction_bank_id($account_number){
	$customer_id=$this->session->userdata("customer_id");
	return $this->Customer_account->get_bank_detail_wallet_transaction_bank_id($account_number,$customer_id);
}

public function get_order_cancel_reason_by_reason_cancel_id($reason_cancel_id){
	$reason_cancel_arr=$this->Customer_account->get_order_cancel_reason_by_reason_cancel_id($reason_cancel_id);
	return $reason_cancel_arr;
}
public function get_order_cancel_admin_reason_by_reason_cancel_id($reason_cancel_id){
	$reason_cancel_arr=$this->Customer_account->get_order_cancel_admin_reason_by_reason_cancel_id($reason_cancel_id);
	return $reason_cancel_arr;
}

public function get_replaced_quantity_in_replacements_by_order_item_id($order_item_id){
	$order_replacement_decision_arr=$this->Customer_account->get_replaced_quantity_in_replacements($order_item_id);
	return $order_replacement_decision_arr;
}


public function get_replacement_desired_data_by_order_item_id($order_item_id){
	$replacement_desired_arr=$this->Customer_account->get_replacement_desired_data_by_order_item_id($order_item_id);
	return $replacement_desired_arr;
}
public function get_sub_refunded_details_from_returned_orders_table($order_item_id){
	$sub_refunded_details_from_returned_orders_table_arr=$this->Customer_account->get_sub_refunded_details_from_returned_orders_table($order_item_id);
	return $sub_refunded_details_from_returned_orders_table_arr;
}


/*policy*/
public function getPolicyForInventory($inventory_id){
    $this->load->model('Front_promotions');
    $this->load->model('front_policy');
     $product_id=$this->Front_promotions->get_product_id($inventory_id);
     $policy_id_list=$this->front_policy->getAllActivePolicyList();
        $policy_array=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                    $inventory_id;
                    $product_id;
                    $brand_id;
                    $subcat_id;
                    $cat_id;
                    $pcat_id;
$payment_policy=array();
                foreach($policy_id_list as $policy){
                    
      
                       /*check wheather payment policy exists or not*/
                       if($this->front_policy->check_policy_exist($policy['id'],$policy['name'])){
                               /*Payment*/
                           $id=$policy['id'];
                           $name=$policy['name'];
                            $storeLevelPaymentPolicy=$this->front_policy->getStoreLevelPaymentPolicy($id);
                            if(!empty($storeLevelPaymentPolicy)){
                                
                                foreach($storeLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                    $policy_data=$this->front_policy->store_level_policy($id,$name,$policy_uid);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $parentCategoryPaymentLevelPolicy=$this->front_policy->getParentCategoryLevelPaymentPolicy($id,$pcat_id);
                            if(!empty($parentCategoryPaymentLevelPolicy)){
     
                                foreach($parentCategoryPaymentLevelPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                    $policy_data=$this->front_policy->parent_category_level_policy($id,$name,$policy_uid,$pcat_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $CategoryLevelPaymentPolicy=$this->front_policy->getCategoryLevelPaymentPolicy($id,$cat_id);
                            if(!empty($CategoryLevelPaymentPolicy)){
 
                                foreach($CategoryLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                   $policy_data=$this->front_policy->category_level_policy($id,$name,$policy_uid,$cat_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $subCategoryLevelPaymentPolicy=$this->front_policy->getSubCategoryLevelPaymentPolicy($id,$subcat_id);
                            if(!empty($subCategoryLevelPaymentPolicy)){

                                foreach($subCategoryLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                   $policy_data=$this->front_policy->sub_category_level_policy($id,$name,$policy_uid,$subcat_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $brandLevelPaymentPolicy=$this->front_policy->getBrandLevelPaymentPolicy($id,$brand_id);
                            if(!empty($brandLevelPaymentPolicy)){

                                foreach($brandLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                   $policy_data=$this->front_policy->brand_level_policy($id,$name,$policy_uid,$brand_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $productLevelPaymentPolicy=$this->front_policy->getProductLevelPaymentPolicy($id,$product_id);
                            if(!empty($productLevelPaymentPolicy)){

                                foreach($productLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                   $policy_data=$this->front_policy->product_level_policy($id,$name,$policy_uid,$product_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $invLevelPaymentPolicy=$this->front_policy->getInvLevelPaymentPolicy($id,$inventory_id);
                            if(!empty($invLevelPaymentPolicy)){

                                foreach($invLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                    $policy_data=$this->front_policy->inventory_level_policy($id,$name,$policy_uid,$inventory_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                       }
                       
                   
                 
                }
                
                return $payment_policy;
                /*Replacement*/
            }
        }
}
	
	public function get_all_order_invoice_offers($order_id){
		return $this->Customer_account->get_all_order_invoice_offers_obj($order_id);
	}
	public function get_all_order_invoice_offers_arr($order_id){
		
		$data=$this->Customer_account->get_all_order_invoice_offers($order_id);
		return $data;
	}
	public function get_order_items_in_order_id($order_id,$order_item_id){
		
		$data=$this->Customer_account->get_order_items_in_order_id($order_id,$order_item_id);
		return $data;
	}
	
	public function get_free_skus_from_inventory($promo_item,$promotion_item_num){
		
		$promo_item=rtrim($promo_item,',');
		$promotion_item_num=rtrim($promotion_item_num,',');
		
		$promo_item_arr = explode(',', $promo_item);
		$promo_item_num_arr = explode(',', $promotion_item_num);
		
		$two=array_combine($promo_item_arr,$promo_item_num_arr);
		
		$prmo_item_res=$this->Customer_account->get_free_skus_from_inventory($promo_item);
		$str_entire=array();
		
		foreach($prmo_item_res as $prmo_item){
			$str=array();
			if($prmo_item->sku_name!=''){	
				$pro_name=$this->db->escape_str($prmo_item->sku_name);	
			}else{	
				$pro_name=$this->db->escape_str($prmo_item->product_name);	
			}	
			$str['str']='<small><span><b>'.$prmo_item->sku_id.'</b> - '.$pro_name.'</span></small>';
			$str['count']=$two[$prmo_item->id];
			$str['image']=$prmo_item->image;
			$str_entire[]=$str;
		}
		return $str_entire;
		
	}
	
	public function get_free_skus_from_inventory_with_details($promo_item,$promotion_item_num){
		
		$promo_item=rtrim($promo_item,',');
		$promotion_item_num=rtrim($promotion_item_num,',');
		
		$promo_item_arr = explode(',', $promo_item);
		$promo_item_num_arr = explode(',', $promotion_item_num);
		
		$two=array_combine($promo_item_arr,$promo_item_num_arr);
		
		$prmo_item_res=$this->Customer_account->get_free_skus_from_inventory($promo_item);
		$str_entire=array();

		return $prmo_item_res;
		
	}
	public function check_all_order_items_delivered_report($order_id){
		
		$all_order_items_delivered=$this->Customer_account->check_all_order_items_delivered_report($order_id);
		return $all_order_items_delivered;
	}
	public function check_all_order_items_canceled($order_id){
		$all_order_items_cancelled=$this->Customer_account->check_all_order_items_canceled($order_id);
		return $all_order_items_cancelled;
	}
	public function update_customer_status_surprise_gifts_on_invoice(){
		$order_id=$this->input->post("order_id");
		$status=$this->input->post("status");
		$customer_id=$this->session->userdata("customer_id");
		if($status=="accept"){
			//reduce the stock
			//if money not credited, credit the amount
			//$status_flag=$this->Customer_account->update_invoice_surprise_gifts_on_accept($order_id,$customer_id);
		}
		if($status=="reject"){
			//add the stock 
			//if money credited, debit the amount
			//$status_flag=$this->Customer_account->update_invoice_surprise_gifts_on_reject($order_id,$customer_id);
		}
		
		$flag=$this->Customer_account->update_customer_status_surprise_gifts_on_invoice($order_id,$status,$customer_id);
		
		if($status=="accept"){
			$obj=$this->Customer_account->get_all_order_invoice_offers_obj($order_id);
			if(!empty($obj)){
				$invoice_surprise_gift_skus=$obj->invoice_surprise_gift_skus;
				$invoice_surprise_gift_skus_nums=$obj->invoice_surprise_gift_skus_nums;
				if(!empty($invoice_surprise_gift_skus)){
					$flag2=$this->Customer_account->update_stock_invoice_surprise_gifts_on_accept($invoice_surprise_gift_skus,$invoice_surprise_gift_skus_nums);
				}
				
			}
		}
		echo $flag;
	}
	public function update_customer_response_for_invoice_cashback(){
		$order_id=$this->input->post("order_id");
		$status=$this->input->post("status");
		$total_amount_cash_back=$this->input->post("total_amount_cash_back");
		$customer_id=$this->session->userdata("customer_id");
		$flag=$this->Customer_account->update_customer_response_for_invoice_cashback($order_id,$status,$customer_id,$total_amount_cash_back);
			
		echo $flag;
	}
	public function get_status_of_free_items($invoices_offers_id){
		
		$result=$this->Customer_account->get_status_of_free_items($invoices_offers_id);
		return $result;
	}
	public function get_customer_pays_shipping_charge_percentage($inventory_id){
		$customer_has_to_pay_arr=$this->Customer_account->get_customer_pays_shipping_charge_percentage($inventory_id);
		if(!empty($customer_has_to_pay_arr)){
			$customer_has_to_pay=0;
			foreach($customer_has_to_pay_arr as $customer_has_to_pay_arr_val){
				$customer_has_to_pay=$customer_has_to_pay_arr_val['percent_paid_by_customer'];
			}
		}else{
			$customer_has_to_pay=0;
		}
		return $customer_has_to_pay;
	}
	public function get_details_of_order_item($order_item_id){
		
		$data=$this->Customer_account->get_details_of_order_item($order_item_id);
		return $data;
	}
	/* notification */
	///////// Notification starts ///////////////////////////
	
	public function clear_notifications(){
		$customer_id=$this->session->userdata("customer_id");	
		$flag=$this->Customer_account->clear_notifications($customer_id);
		return $flag;
	}
	public function notification(){
		if($this->session->userdata("logged_in")){
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_page"]="notification";
			$customer_id=$this->session->userdata("customer_id");
			
			//$notification_details=$this->Customer_account->get_all_notificatins($customer_id);
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/notification_view');
			$this->load->view('footer');
		}else{
			redirect(base_url());
		}	
	}
	public function get_inventory_details($inventory_id){
		return $this->Customer_account->get_inventory_details($inventory_id);
	}
	public function get_shipping_address_of_order($order_id){
		return $this->Customer_account->get_shipping_address_of_order($order_id);
	}
	public function ccMasking($number, $maskingCharacter = 'X') {
		//$masked = preg_replace("/(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})/", "XXXXXXXX", $str);
		return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
	}
		/*reviews*/
	public function reviews(){
		if($this->session->userdata("logged_in")){
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_page"]="reviews";
			$customer_id=$this->session->userdata("customer_id");
			$customer_name=$this->session->userdata("customer_name");
			$data["customer_name"]=$customer_name;
			$data["reviews"]=$this->Customer_account->get_reviews_of_customer($customer_id);
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/reviews');
			$this->load->view('footer');
		}else{
			redirect(base_url());
		}
	}
	public function get_details_of_products($inventory_id){
		return $this->Customer_account->get_details_of_products($inventory_id);
	}
	public function get_replaced_orders_by_prev_order_item_id_from_history_orders($prev_order_item_id){
		return $this->Customer_account->get_replaced_orders_by_prev_order_item_id_from_history_orders($prev_order_item_id);
	}
	public function get_sub_reasons(){
		$reason_id=$this->input->post('reason_id');
		$res=$this->Customer_account->get_sub_reasons($reason_id);
		
		//print_r($res);
		$str="";
		foreach($res as $data){
			$str.='<option value="'.$data->sub_issue_id.'">'.$data->sub_issue_name.'</option>';
		}
		echo $str;
	}
	public function offline_contact_admin(){
		
		if($this->session->userdata("logged_in")){
			//print_r($this->input->post());
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$return_order_id=$this->input->post('return_order_id');
			$customer_action=$this->input->post('customer_action');
			$admin_action=$this->input->post('admin_action');
			$user_type=$this->input->post('user_type');
			$customer_id=$this->input->post('customer_id');
			$status=$this->input->post('status');
			$order_item_id=$this->input->post('order_item_id');
			$order_id=$this->input->post('order_id');
			
			$data['return_order_id']=$return_order_id;
			$data['customer_action']=$customer_action;
			$data['admin_action']=$admin_action;
			$data['user_type']=$user_type;
			$data['customer_id']=$customer_id;
			$data['status']=$status;
			$data['order_item_id']=$order_item_id;
			$data['order_id']=$order_id;
			
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_page"]="offline_contact_admin";				
					
			$get_refund_data=$this->get_order_details_admin_acceptance_refund_data($order_item_id);
			$data["get_refund_data"]=$get_refund_data;
			$returns_conservation_obj_arr=$this->get_returns_conservation_chain_in_customerpanel($return_order_id);
			$data["returns_conservation_obj_arr"]=$returns_conservation_obj_arr;
			
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/offline_contact_admin');
			$this->load->view('footer');	

		}else{
			redirect(base_url());
		}
	}
	public function offline_contact_admin_replacement(){
		
		if($this->session->userdata("logged_in")){
			//print_r($this->input->post());
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$return_order_id=$this->input->post('return_order_id');
			$customer_action=$this->input->post('customer_action');
			$admin_action=$this->input->post('admin_action');
			$user_type=$this->input->post('user_type');
			$customer_id=$this->input->post('customer_id');
			$status=$this->input->post('status');
			$order_item_id=$this->input->post('order_item_id');
			$order_id=$this->input->post('order_id');
			
			$data['return_order_id']=$return_order_id;
			$data['customer_action']=$customer_action;
			$data['admin_action']=$admin_action;
			$data['user_type']=$user_type;
			$data['customer_id']=$customer_id;
			$data['status']=$status;
			$data['order_item_id']=$order_item_id;
			$data['order_id']=$order_id;
			
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_page"]="offline_contact_admin_replacement";				
					
			$res=$this->Customer_account->get_returns_conservation_chain_in_customerpanel_for_replacement($return_order_id);
			$data["replacements_conservation_obj_arr"]=$res;
			$get_replacement_data=$this->get_order_details_admin_acceptance_refund_data_for_replacement($order_item_id);
			$data["get_replacement_data"]=$get_replacement_data;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/offline_contact_admin_replacement');
			$this->load->view('footer');	

		}else{
			redirect(base_url());
		}
	}
	
	public function offline_contact_admin_return_stock(){
		
		if($this->session->userdata("logged_in")){
			//print_r($this->input->post());
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$return_order_id=$this->input->post('return_order_id');
			$customer_action=$this->input->post('customer_action');
			$admin_action=$this->input->post('admin_action');
			$user_type=$this->input->post('user_type');
			$customer_id=$this->input->post('customer_id');
			$status=$this->input->post('status');
			$order_item_id=$this->input->post('order_item_id');
			$order_id=$this->input->post('order_id');
			
			$data['return_order_id']=$return_order_id;
			$data['customer_action']=$customer_action;
			$data['admin_action']=$admin_action;
			$data['user_type']=$user_type;
			$data['customer_id']=$customer_id;
			$data['status']=$status;
			$data['order_item_id']=$order_item_id;
			$data['order_id']=$order_id;
			
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_page"]="offline_contact_admin_return_stock";				
					
			$replacements_conservation_obj_arr=$this->get_returns_conservation_chain_in_customerpanel_for_replacement($return_order_id);
			$data["replacements_conservation_obj_arr"]=$replacements_conservation_obj_arr;
			
			$get_replacement_data=$this->get_order_details_admin_acceptance_refund_data_for_replacement($order_item_id);
			$data["get_replacement_data"]=$get_replacement_data;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/offline_contact_admin_return_stock');
			$this->load->view('footer');	

		}else{
			redirect(base_url());
		}
	}
	public function send_mail_to_customer_email(){
		
		if($this->session->userdata("logged_in")){
			$rand=rand(100000,999999);
			$email=$this->input->post('email');
			$email_stuff_arr=email_stuff();
						//through Email
				if($email!=''){
					
					$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
					
					<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Your verification code  <b>'.$rand.'</b></p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br></td></tr>';
					
					$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

					$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

					$usermessage.='</tbody> </table></tr></table></center> </body></html>';
				
					
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($email);
					//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
					//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = 'Email Verification';
					$mail->Body    = $usermessage;
					if($email != ''){
							$mail->send();
							$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
							echo $flag;
					}
				}
		
		}else{
			redirect(base_url());
		}
	}
	public function verify_email_customer(){
		if($this->session->userdata("logged_in")){
			$cust_id=$this->session->userdata("customer_id");
			$email=$this->input->post('email');
			$rn=$this->input->post('verify_email_code');			
			$flag=$this->Customer_account->get_verif_code_db($cust_id,$rn,$email);
			echo $flag;

		}else{
			redirect(base_url());
		}
	}
	public function send_msg_to_customer_mobile(){
		
		if($this->session->userdata("logged_in")){
			$rand=rand(100000,999999);
			$mobile=$this->input->post('mobile');
			
						//through Email
				if($mobile != '' ){
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
			

						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$flag=$this->Customer_account->sended_code_to_mobile($mobile,$rand);
							echo $flag;
						}

						curl_close($ch);
					}//sms
					
		
		}else{
			redirect(base_url());
		}
	}
	public function verify_mobile_customer(){
		if($this->session->userdata("logged_in")){
			$cust_id=$this->session->userdata("customer_id");
			$mobile=$this->input->post('mobile');
			$rn=$this->input->post('otp_mobile');			
			$flag=$this->Customer_account->get_verif_code_db_mobile($cust_id,$rn,$mobile);
			echo $flag;

		}else{
			redirect(base_url());
		}
	}
	/*Blogs starts*/
       
        public function getAllBlogs(){
            $params = $columns = $totalRecords = $data = array();
            
            $params = $this->input->post();
            
            $totalRecords=$this->Customer_blogs->getAllBlogs_processing($params,"get_total_num_recs");
            
            $queryRecordsResult=$this->Customer_blogs->getAllBlogs_processing($params,"get_recs");
            
            $i=1;
            foreach($queryRecordsResult as $queryRecordsObj){
                $status="";
                $writingstatus="";
                if($queryRecordsObj->blogger_request_publish==0 && $queryRecordsObj->admin_publish==0){
                    $writingstatus='<small style="color:#0000ff">Draft</small>';
                }
                if($queryRecordsObj->blogger_request_publish==1 && $queryRecordsObj->admin_publish==0){
                    $writingstatus='<small style="color:#0000ff">Sent to Admin for Publishing</small>';
                }
                $publishstatus="";
                if($queryRecordsObj->blogger_request_publish==1 && $queryRecordsObj->admin_publish==1){
                    $publishstatus='<small style="color:#0000ff">Published by Admin</small>';
                    $writingstatus="";
                }
                $unpublishstatus="";
                if($queryRecordsObj->blogger_request_unpublish==1 && $queryRecordsObj->admin_publish==1){
                    $unpublishstatus='<small style="color:#0000ff">Unpublish request sent to Admin</small>';
                    $publishstatus="";
                    $writingstatus="";
                }
                

                
                if($writingstatus!=""){
                    $status=$writingstatus;
                }
                if($publishstatus!=""){
                    $status=$publishstatus;
                }
                if($unpublishstatus!=""){
                    $status=$unpublishstatus;
                }
                

                
                $customer_id=$this->session->userdata("customer_id");
                $row=array();
                $row[]=$i;
                $row[]='<a target="_blank" href="'.base_url().'Account/update_blog/'.$queryRecordsObj->blog_id.'">'.$queryRecordsObj->blog_title.'</a>';
                $row[]=$queryRecordsObj->topic_sel;
                $row[]=$queryRecordsObj->timestamp;
                $row[]=$status;
                $row[]='<a target="_blank" href="'.base_url().'get_blog_post_preview/'.$queryRecordsObj->slug.'/'.$customer_id.'"><i class="fa fa-eye"></i></a>';
                $data[]=$row;
                $row=array();
                $i++;
            }
            $json_data = array(
                    "draw"            => intval( $params['draw'] ),   
                    "recordsTotal"    => intval( $totalRecords ),  
                    "recordsFiltered" => intval($totalRecords),
                    "data"            => $data   // total data array
                    );
            echo json_encode($json_data);  // send data as json format
        }
        public function all_blogs(){
            if($this->session->userdata("logged_in")){
				$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
                $data['p_category']=$this->provide_first_level_menu();
                $data["controller"]=$this;
                $data["current_controller"]="account";
                $data["cur_page"]="all_blogs";
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
                $this->load->view('header',$data);
                $this->load->view('customer/all_blogs');
                $this->load->view('footer');
            }
            else{
                redirect(base_url());
            }
            
        }
        public function request_unpublish_cancel($blog_id){
            if($this->session->userdata("logged_in")){
                $customer_id=$this->session->userdata("customer_id");
                $blogger_request_unpublish=0;
               
                $updated=$this->Customer_blogs->blogger_request_unpublish_admin($blogger_request_unpublish,$blog_id,$customer_id);
                
                if($updated){
                    if($updated==1){
                        $this->session->set_flashdata('notification','Updated Request');
                        redirect(base_url()."Account/all_blogs/");
                    }
                    if($updated==2){
                        $this->session->set_flashdata('notification','No Change');
                        redirect(base_url()."Account/all_blogs/");
                    }
                    
                }
                else{
                    $this->session->set_flashdata('error','Some Error while adding, try again');
                    redirect(base_url()."Account/update_blog/".$blog_id);
                }
            }
            else{
                redirect(base_url());
            }
        }
        
        public function request_unpublish($blog_id){
            if($this->session->userdata("logged_in")){
                $customer_id=$this->session->userdata("customer_id");
                $blogger_request_unpublish=1;

                 $updated=$this->Customer_blogs->blogger_request_unpublish_admin($blogger_request_unpublish,$blog_id,$customer_id);

                if($updated){
                    if($updated==1){
                        $this->session->set_flashdata('notification','Updated Request');
                        redirect(base_url()."Account/all_blogs/");
                    }
                    if($updated==2){
                        $this->session->set_flashdata('notification','No Change');
                        redirect(base_url()."Account/all_blogs/");
                    }
                    
                }
                else{
                    $this->session->set_flashdata('error','Some Error while adding, try again');
                    redirect(base_url()."Account/update_blog/".$blog_id);
                }
            }
            else{
                redirect(base_url());
            }
        }
        
        public function request_publish_cancel($blog_id){
            if($this->session->userdata("logged_in")){
                 $customer_id=$this->session->userdata("customer_id");
                $updated=$this->Customer_blogs->blogger_request_publish_admin($blogger_request_publish,$blog_id,$customer_id);
                if($updated){
                    if($updated==1){
                        $this->session->set_flashdata('notification','Updated Request');
                        redirect(base_url()."Account/all_blogs/");
                    }                    
                }
                else{
                    $this->session->set_flashdata('error','Some Error while adding, try again');
                    redirect(base_url()."Account/update_blog/".$blog_id);
                }
            }
            else{
                redirect(base_url());
            }
        }
        public function request_publish($blog_id){
            if($this->session->userdata("logged_in")){
                $blogger_request_publish=1;
                 $customer_id=$this->session->userdata("customer_id");
                
                 $updated=$this->Customer_blogs->blogger_request_publish_admin($blogger_request_publish,$blog_id,$customer_id);
                
                if($updated){
                    if($updated==1){
                        $this->session->set_flashdata('notification','Updated Request');
                        redirect(base_url()."Account/all_blogs/");
                    }                    
                }
                else{
                    $this->session->set_flashdata('error','Some Error while adding, try again');
                    redirect(base_url()."Account/update_blog/".$blog_id);
                }
            }
            else{
                redirect(base_url());
            }
        }
        
        /*public function blogger_request_admin(){
            if($this->session->userdata("logged_in")){
                //print_r($this->input->post());exit;
                extract($this->input->post());
                 $customer_id=$this->session->userdata("customer_id");
                if($blogger_request_publish!=""){
                    $updated=$this->Customer_blogs->blogger_request_publish_admin($blogger_request_publish,$blog_id,$customer_id);
                }
                if($blogger_request_unpublish!=""){
                    $updated=$this->Customer_blogs->blogger_request_unpublish_admin($blogger_request_unpublish,$blog_id,$customer_id);
                }
               
                
                if($updated){
                    if($updated==1){
                        $this->session->set_flashdata('notification','Updated Request');
                        redirect(base_url()."Account/all_blogs/");
                    }
                    if($updated==2){
                        $this->session->set_flashdata('notification','No Change');
                        redirect(base_url()."Account/all_blogs/");
                    }
                    
                }
                else{
                    $this->session->set_flashdata('error','Some Error while adding, try again');
                    redirect(base_url()."Account/update_blog/".$blog_id);
                }
            }
            else{
                redirect(base_url());
            }
        }*/

        public function update_blog_content(){
            if($this->session->userdata("logged_in")){
                //print_r($this->input->post());exit;
                extract($this->input->post());
                $customer_id=$this->session->userdata("customer_id");
				
                $updated=$this->Customer_blogs->update_blog_content($blog_content,$blog_id,$customer_id);
                if($updated){
                    if($updated==1){
                        http_response_code(200);
                        echo 'Updated Blog';
                    }
                    if($updated==2){
                        http_response_code(200);
                        echo 'You have made no changes';
                    }
                    
                }else{
                    http_response_code(400);
                    echo "Oops! Some error, try after some time.";
                }
            }
            else{
                redirect(base_url());
            }
        }
       
        
        public function add_blog(){
            if($this->session->userdata("logged_in")){
				
                $data['p_category']=$this->provide_first_level_menu();
                $data["controller"]=$this;
                $data["current_controller"]="account";
                //print_r($this->input->post());exit;
                extract($this->input->post());
                $date = new DateTime();
                $mt=microtime();
                $salt="blog_id";
                $rand=mt_rand(1, 1000000);
                $ts=$date->getTimestamp();
                $str=$salt.$rand.$ts.$mt;
                $blog_id=md5($str);
                $customer_id=$this->session->userdata("customer_id");
                $tag_sel=json_encode($tag_sel);
                $added=$this->Customer_blogs->add_blog($topic_sel,$tag_sel,$blog_title,$blog_id,$customer_id);
                if($added){
                    $this->session->set_flashdata('notification','Initials Of Blogs Created,');
                    redirect(base_url()."Account/update_blog/".$blog_id);
                }
                else{
                    $this->session->set_flashdata('error','Some Error while adding, try again');
                    redirect(base_url()."Account/write_blog");
                }
            }
            else{
                redirect(base_url());
            }
        }
        
        /*Blogs ends*/
	public function get_territory_of_pincode($pincode,$vendor_id,$logistics_id,$territory_name_id){
		$default_logistics_arr=$this->Model_search->get_default_logistics($vendor_id,$logistics_id);
		$default_logistics_id=$default_logistics_arr["logistics_id"];
		$logistics_name=$default_logistics_arr["logistics_name"];
		$default_delivery_mode_arr=$this->Model_search->default_delivery_mode($logistics_id);	

		$default_delivery_mode_id=$default_delivery_mode_arr['logistics_delivery_mode_id'];
		$default_delivery_mode=$default_delivery_mode_arr['delivery_mode'];
		$speed_duration_json=$default_delivery_mode_arr['speed_duration'];
		
		$shipping_detail=array('logistics_territory_id'=>$territory_name_id,'logistics'=>$default_logistics_id,'default_delivery_mode_id'=>$default_delivery_mode_id,'default_delivery_mode'=>$default_delivery_mode,'logistics_name'=>$logistics_name,"speed_duration"=>$speed_duration_json);
		return $shipping_detail;
	}
	
	
	public function rateorderbymail($order_item_id,$val){
		
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="account";
				$data["cur_page"]="my_order";
				$customer_id=$this->session->userdata("customer_id");
				$order_item_id_arr=array();
				$all_orders_gp=array();
				$all_orders_collection=array();
				$all_orders_collection=$this->Customer_account->get_all_orders($customer_id);
				if(!empty($all_orders_collection)){
					foreach($all_orders_collection as $order_statusarr){
						$order_item_id_arr[]=$order_statusarr["order_item_id"];
					}
				}
				
				$all_distinct_orders=$this->Customer_account->get_all_distinct_orders_id($customer_id);
				$all_distinct_orders=array_unique($all_distinct_orders);
				
				foreach($all_distinct_orders as $distinct_ordersObj){
					$all_orders_gp[$distinct_ordersObj][]=$this->Customer_account->all_orders($distinct_ordersObj);
				}		
				
				$data["all_orders"]=$all_orders_gp;
				$ft_img=$this->Customer_frontend->foot_image_index();
				$data['ft_img']='';
				if(!empty($ft_img)){
					if(isset($ft_img->background_image)){
						$data['ft_img']=$ft_img->background_image;
					}
				}
				$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
				$data["given_rating"]=$val;
				//////////////////////
				$order_summary_mail_data_arr=$this->Customer_account->get_order_summary_mail_data_rating($order_item_id);
				$data["order_summary_mail_data_arr"]=$order_summary_mail_data_arr;
				$data["get_feedback_questions_arr"]=$this->Customer_account->get_feedback_questions($val);
				$data["get_feedback_questions_heading"]=$this->Customer_account->get_feedback_questions_headings($val);
				$flag_arr=$this->Customer_account->check_rateorder_for_feedback_status($order_summary_mail_data_arr,$val);
				if($flag_arr["status"]=="old"){
					$this->load->view('header',$data);
					$this->load->view('rateorderbymail_old',$data);
					$this->load->view('footer');
				}
				if($flag_arr["status"]=="new"){
					$data["rateorder_id"]=$flag_arr["rateorder_id"];
					$this->load->view('header',$data);
					$this->load->view('rateorderbymail',$data);
					$this->load->view('footer');
				}
				//////////
				
				
			
		
	}
	
	public function rateorderbymail_action(){
		/// Array ( [order_item_id] => 1 [rateorder_id] => 5 [question] => Array ( [question_4] => 1 [question_5] => 1 [question_6] => 2 [question_7] => 2 [question_8] => 3 [question_9] => 3 [question_10] => 2 ) [comments] => teset )
		$order_item_id=$this->input->post("order_item_id");
		$rateorder_id=$this->input->post("rateorder_id");
		$question_arr=$this->input->post("question");
		if($this->input->post("comments")){
			$comments=addslashes($this->input->post("comments"));
		}
		else{
			$comments="";
		}
		$flag=$this->Customer_account->rateorderbymail_action($order_item_id,$rateorder_id,$comments,$question_arr);
		///
		
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="account";
		$data["cur_page"]="my_order";
		$customer_id=$this->session->userdata("customer_id");
		$order_item_id_arr=array();
		$all_orders_gp=array();
		$all_orders_collection=array();
		$all_orders_collection=$this->Customer_account->get_all_orders($customer_id);
		if(!empty($all_orders_collection)){
			foreach($all_orders_collection as $order_statusarr){
				$order_item_id_arr[]=$order_statusarr["order_item_id"];
			}
		}
		
		$all_distinct_orders=$this->Customer_account->get_all_distinct_orders_id($customer_id);
		$all_distinct_orders=array_unique($all_distinct_orders);
		
		foreach($all_distinct_orders as $distinct_ordersObj){
			$all_orders_gp[$distinct_ordersObj][]=$this->Customer_account->all_orders($distinct_ordersObj);
		}		
		
		$data["all_orders"]=$all_orders_gp;
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		
		//////////////////////
		$order_summary_mail_data_arr=$this->Customer_account->get_order_summary_mail_data_rating($order_item_id);
		$data["order_summary_mail_data_arr"]=$order_summary_mail_data_arr;
		////
		$this->load->view('header',$data);
		$this->load->view('rateorderbymail_success',$data);
		$this->load->view('footer');
		
	}
	public function get_shipping_module_info(){
			$get_shipping_module_info_arr=$this->Customer_account->get_shipping_module_info();
			return $get_shipping_module_info_arr;
		}
	public function test_sample(){
		//echo "This is the test page";
		echo $this->get_baseurl();
	}
	
	public function get_baseurl(){
		$url_current_page=sprintf(
		"%s://%s/",
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME'],
			$_SERVER['REQUEST_URI']
		  );
		if(preg_match("/localhost/",$url_current_page)){
			return "http://localhost/voometstudio/";
		}
		else{
			return $url_current_page;
		}
	}
	public function get_city_state_details_by_pincode(){
		$get_city_state_details_by_pincode_data = json_decode(file_get_contents("php://input"));
		$pin='';
		if(!empty($get_city_state_details_by_pincode_data)){
		    $pin=$get_city_state_details_by_pincode_data->pin;
        }
		$res=$this->Customer_account->get_city_state_details_by_pincode($pin);
		echo json_encode($res);
	}
	
	
	public function get_check_availability_of_free_shipping_promotion(){
		$check_availability_of_free_shipping_promotion=$this->Front_promotions->get_check_availability_of_free_shipping_promotion();
		return $check_availability_of_free_shipping_promotion;
	}
	public function get_flat_shipping_available_or_not($inventory_id){
		$shipping_charge_not_applicable="";
		$flat_shipping_charge="";
		//$check_availability_of_free_shipping_promotion_arr=$this->get_check_availability_of_free_shipping_promotion();
		//if($check_availability_of_free_shipping_promotion_arr["status"]=="yes"){
			$get_logistics_inventory_data_obj=$this->Model_search->get_logistics_inventory_data($inventory_id);
			if($get_logistics_inventory_data_obj->shipping_charge_not_applicable=="yes"){
				$flat_shipping_charge=$get_logistics_inventory_data_obj->flat_shipping_charge;
				return array("shipping_charge_not_applicable"=>"yes","flat_shipping_charge"=>$flat_shipping_charge);
			}
		//}
		return array("shipping_charge_not_applicable"=>"no","flat_shipping_charge"=>"");
	}
	
	
	public function carttable_dump_with_withoutsession(){
	   $inventory_id=$_REQUEST["inventory_id"];
	   $inventory_sku=$_REQUEST["inventory_sku"];
	   $inventory_moq=$_REQUEST["inventory_moq"];
	   $addon_inventories=$_REQUEST["addon_inventories"];
	 
	   if($this->session->userdata("customer_id")){
			$customer_id=$this->session->userdata("customer_id");
	   }
	   else{
		   $customer_id="NA";
	   }
	   $this->Customer_account->carttable_dump_with_withoutsession($inventory_id,$inventory_sku,$inventory_moq,$customer_id,$addon_inventories);
   }
    public function apply_coupon_code_sku(){
       $cp_data = json_decode(file_get_contents("php://input"));
       $coupon_arr=$this->Customer_account->apply_coupon_code_sku($cp_data);
       if(!empty($coupon_arr)){
            echo json_encode(array("status"=>1,"coupon_arr"=>$coupon_arr,"msg"=>"Coupon applied successfully"));
            exit;
       }else{
            echo json_encode(array("status"=>2,"coupon_arr"=>$coupon_arr,"msg"=>"Invalid coupon"));
            exit;
       }

   }
   public function apply_coupon_code_invoice(){
       $cp_data = json_decode(file_get_contents("php://input"));
       $coupon_arr=$this->Customer_account->apply_coupon_code_invoice($cp_data);
       if(!empty($coupon_arr)){
            echo json_encode(array("status"=>1,"coupon_arr"=>$coupon_arr,"msg"=>"Coupon applied successfully"));
            exit;
       }else{
            echo json_encode(array("status"=>2,"coupon_arr"=>$coupon_arr,"msg"=>"Invalid coupon"));
            exit;
       }

   }
   
   public function testt(){
	   echo "11111";
   }
   public function update_paytm_data(){
	print_r($this->input->post());
	$data_arr=$this->input->post();
	$data_str=json_encode($this->input->post());
	$order_id=$data_arr['ORDERID'];
	$status=$data_arr['STATUS'];
	$amount=$data_arr['TXNAMOUNT'];

	$res=update_paytm_data($data_str,$order_id,$status,$amount);

   }
   public function get_cartinfo_in_json_format_checkout_combo(){

	//$res=$this->Customer_account->get_cartinfo_in_json_format_checkout();
	
	$get_shipping_address_pincode_arr=$this->Customer_account->get_shipping_address_pincode();

	$pincode=$get_shipping_address_pincode_arr["pincode"];
	
	$arr=array();
	$check_availability_pincode_temp_arr=array();
	$check_availability_pincode_arr=array();
	$customer_id=$this->session->userdata("customer_id");
	$res_combo=get_combo_carttable($customer_id);

	if(!empty($res_combo)){
		$combo_products=json_decode($res_combo->combo_products);
		if(!empty($combo_products)){
			$ids = array_column($combo_products, 'inv_id');

			for($i=0;$i<count($ids);$i++){
				$inventory_id=$ids[$i];
				$inv_data=get_all_inventory_data($inventory_id);
				if(!empty($inv_data)){
					$vendor_id=$inv_data->vendor_id;
					$result=$this->Model_search->get_default_logistics_id_for_all_vendors($vendor_id);
					$logistics_id_arr=array();
					foreach($result as $res){
						$logistics_id_arr[]=$res->logistics_id;
					}
					$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr=$this->Model_search->get_logistics_id_territory_name_id_by_pincode_vendor_id($logistics_id_arr,$pincode,$vendor_id);
					
					$logistics_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["logistics_id"];
					$territory_name_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["territory_name_id"];
					$flag=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["check_logistics_yes"];
					$check_availability_pincode_temp_arr[]=$flag;

				}
			}

		}
		$arr[]=$res_combo;
	}

	$check_availability_at_lease_one="no";
	foreach($check_availability_pincode_temp_arr as $val){
		if($val==true){
			$check_availability_at_lease_one="yes";
			break;
		}
	}
	if($check_availability_at_lease_one=="no"){
		echo json_encode(array("pincode"=>$pincode,"check_availability_at_lease_one"=>$check_availability_at_lease_one));
		exit;
	}

	$check_availability="yes";
	echo json_encode(array("get_cartinfo_in_json_format_checkout"=>$arr,"pincode"=>$pincode,"check_availability"=>$check_availability,"check_availability_at_lease_one"=>$check_availability_at_lease_one));
	
}





  
  
  public function get_inventory_id_with_promotion_uid(){
		$temp=0;
		$customer_id=$this->session->userdata("customer_id");
		$update_cart_info_post_data = json_decode(file_get_contents("php://input"));
		$inventory_id_with_promotion_uid_arr=array();
		//print_r($update_cart_info_post_data);
		foreach($update_cart_info_post_data as $data_value){
			if($data_value->customer_id==$customer_id){
				$sku_id=$this->Customer_account->get_sku_id_by_inventory_id($data_value->inventory_id);
				$promotion_id_selected=$this->Customer_account->get_promotion_id_selected_by_inventory_id($data_value->inventory_id,$data_value->cart_id,$customer_id);
				$inventory_id_with_promotion_uid_arr[$data_value->inventory_id]=array("promotion_id_selected"=>$promotion_id_selected,"sku_id"=>$sku_id,"cart_id"=>$data_value->cart_id);
			}
		}
		echo json_encode($inventory_id_with_promotion_uid_arr);
	}
	public function delete_promotion_differed_inventory_ids(){
		$customer_id=$this->session->userdata("customer_id");
		$promotion_differed_inventory_cart_ids_data = json_decode(file_get_contents("php://input"));
		//print_r($promotion_differed_inventory_cart_ids_data);
		$flag=$this->Customer_account->delete_carttable_ids($promotion_differed_inventory_cart_ids_data->promotion_differed_inventory_cart_ids,$customer_id);
		//$flag=true;
		echo json_encode(array("status"=>$flag));
	}
	
	
	
  
  public function testmail(){
	        
			
			
	        $email_stuff_arr=email_stuff();
			//$email_stuff_arr["fromemail_emailtemplate"]="voometstudio.in@gmail.com";
			//$email_stuff_arr["fromemail_Host"]="smtp.gmail.com";
			//$email_stuff_arr["fromemail_Username"]="voometstudio.in@gmail.com";
			//$email_stuff_arr["fromemail_Password"]="susose2020";
			//$email_stuff_arr["name_emailtemplate"]="Voometstudio";
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->SMTPDebug = 1;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			//$email="rajit@axonlabs.in";
			$mail->addAddress($email);
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = "Test";
			$mail->Body    = "Test";
			if(!$mail->send()){
				echo $mail->ErrorInfo;
			}
			else{
				echo "sent";
			}
  }
  
  public function request_a_quote_common_action_mail_status($rq_unique_id){
			$vendor_email_list_sent=$this->Customer_account->request_a_quote_common_action_mail_status($rq_unique_id);
			echo json_encode(array("vendor_email_list_sent"=>$vendor_email_list_sent));
		}
  
  public function request_a_quote_common_action(){
			$post_arr=$this->input->post();
			//print_r($post_arr);
			$rq_unique_id=$this->input->post("rq_unique_id");
			$rq_customer_id=$this->input->post("rq_customer_id");
			$rq_sku_ids=$this->input->post("rq_sku_ids");
			$rq_inv_ids=$this->input->post("rq_inv_ids");
			$rq_inv_qtys=$this->input->post("rq_inv_qtys");
			$rq_pincode=$this->input->post("rq_pincode");
			$rq_name=$this->input->post("rq_name");
			$rq_email=$this->input->post("rq_email");
			$rq_mobile=$this->input->post("rq_mobile");
			$rq_location=$this->input->post("rq_location");
			$vendor_id_list=$this->input->post("vendor_id_list");
			
			$flag=$this->Customer_account->request_a_quote_common_action(
				$rq_unique_id,
				$rq_customer_id,
				$rq_sku_ids,
				$rq_inv_ids,
				$rq_inv_qtys,
				$rq_pincode,
				$rq_name,
				$rq_email,
				$rq_mobile,
				$rq_location
			);
			
			if($flag==true){
					
					
				$str='<table style="border-collapse:collapse;">';
						
						
						$str.='<tr align="left">';
							$str.='<th>';
								$str.='Postal Code ';
							$str.='</th>';
							$str.='<td>';
								$str.=$rq_pincode;
							$str.='</td>';
						$str.='</tr>';
						
						$str.='<tr align="left">';
							$str.='<th valign="top">';
								$str.='List of SKU IDs';
							$str.='</th>';
							$str.='<td>';
			
								$rq_sku_ids_arr=explode(',',$rq_sku_ids);
								$rq_inv_qtys=explode(',',$rq_inv_qtys);

								
								for($i=0;$i<count($rq_sku_ids_arr);$i++){
									$str.='<span>'.$rq_sku_ids_arr[$i].' (qty:'.$rq_inv_qtys[$i].')</span><br>';	
								}
								

							$str.='</td>';
						$str.='</tr>';
						
						
						
						$str.='<tr align="left">';
							$str.='<th valign="top" colspan="2">';
								$str.='Customer Info<br>';
							$str.='</th>';
						$str.='</tr>';
						$str.='<tr align="left">';
							$str.='<td colspan="2">';
								$str.='<b>Name : </b>'.$rq_name."<br>";
								$str.='<b>Email : </b>'.$rq_email."<br>";
								$str.='<b>Mobile : </b>'.$rq_mobile."<br>";
								$str.='<b>City : </b>'.$rq_location."<br>";
							$str.='</td>';
						$str.='</tr>';
						
						
						
					$str.='</table>';
						   $email_stuff_arr=email_stuff();
					////// Admin and Vendors mail starts ///
					
                                            
                                            
						$arr=array();
                        $arr['rfq_details']=$str;
						$arr['customer_name']=$rq_name;
                                               
						$usermessage_vendor=$this->load->view('template/notification_vendor_RFQ',$arr,true);

						$email_of_admin="nanthinivinothkumar0227@gmail.com";
						$mail = new PHPMailer;
						/*$mail->isSMTP();
						$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
						$mail->Port = '587';//  live - comment it 
						$mail->Auth = true;//  live - comment it 
						$mail->SMTPAuth = true;
						$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
						$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
						$mail->SMTPSecure = 'tls';*/
						$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
						$mail->FromName = $email_stuff_arr["name_emailtemplate"];
						$mail->addAddress($email_of_admin);
						$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
						$mail->WordWrap = 50;
						$mail->isHTML(true);
						$mail->Subject = "New 'Request For Quotation' Received";
						$mail->Body    = $usermessage_vendor;
						if($email_of_admin != ''){
							 /*if($mail->send()){
								 //$flag=$this->Customer_account->update_vendor_email_list_log_sent($rq_unique_id);
								 //echo "mail_sent";
							 }
							 else{
								// echo "mail_not_sent";
							 }*/
						}
					
					////// Admin and Vendors mail ends ///
					
					
					////// Customer Mail starts ///
					//$rq_email="shareeshk@gmail.com";
					//$rq_email="nanthinivinothkumar0227@gmail.com";
					if($rq_email!=''){
                                            
                                            
						$arr=array();
						$arr['rfq_details']=$str;
						 $arr['customer_name']=$rq_name;
						$usermessage_customer=$this->load->view('template/notification_customer_RFQ',$arr,true);

						
						$mail = new PHPMailer;
						/*$mail->isSMTP();
						$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
						$mail->Port = '587';//  live - comment it 
						$mail->Auth = true;//  live - comment it 
						$mail->SMTPAuth = true;
						$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
						$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
						$mail->SMTPSecure = 'tls';*/
						$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
						$mail->FromName = $email_stuff_arr["name_emailtemplate"];
                                                //$mail->to=$rq_email; 
						$mail->addAddress($rq_email);
						$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
						$mail->WordWrap = 50;
						$mail->isHTML(true);
						$mail->Subject = 'Request for Quotation Details';
						$mail->Body    = $usermessage_customer;						
						 if($mail->send()){
							 echo "mail_sent";
						 }
						 else{
							 echo "mail_not_sent";
						 }
					}
					////// Customer Mail ends ///
					
			}
	}

	/* Franchise module starts */

	public function franchise_account(){
		if($this->session->userdata("logged_in")){
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_Spage"]="cur_Spage";
			$data["cur_page"]="p_info";
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["user_type"]="franchise_customer";
			$cust_info=$this->Customer_frontend->get_customer_info();
			$data["cust_info"]=$cust_info;
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/home');
			$this->load->view('footer');
		}
		else{
			redirect(base_url());
		}
		
	}

	public function franchise_bank_details(){
		if($this->session->userdata("logged_in")){
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_Spage"]="cur_Spage";
			$data["cur_page"]="bank";
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["user_type"]="franchise_customer";
			$cust_info=$this->Customer_frontend->get_customer_info();
			$data["cust_info"]=$cust_info;
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/franchise_bank_details');
			$this->load->view('footer');
		}
		else{
			redirect(base_url());
		}
		
	}
	public function update_bank_details_franchise(){
		if($this->session->userdata("logged_in")){
			
			$bank_name=$this->input->post("bank_name");
			$bank_branch_name=$this->input->post("bank_branch_name");
			$bank_city=$this->input->post("bank_branch_city");
			$bank_ifsc_code=$this->input->post("bank_ifsc_code");
			$bank_account_number=$this->input->post("bank_account_number");
			$account_holder_name=$this->input->post("account_holder_name");
			
			$customer_id=$this->session->userdata("customer_id");
			
			$flag=$this->Customer_account->update_bank_details_franchise($customer_id,$bank_name,$bank_branch_name,$bank_city,$bank_ifsc_code,$bank_account_number,$account_holder_name);
			echo $flag;
			
		}else{
			redirect(base_url());
		}	
	}
	public function franchise_projects(){
		if($this->session->userdata("logged_in")){
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_page"]="projects";
			$customer_id=$this->session->userdata("customer_id");
			$project_detail=$this->Franchise_frontend->get_franchise_all_projects($customer_id);
			
			$data['project_detail']=$project_detail;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/franchise_projects');
			$this->load->view('footer');
		}
		else{
			redirect(base_url());
		}	
	}
	public function create_project(){
		if($this->session->userdata("logged_in")){
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_page"]="projects";
			$customer_id=$this->session->userdata("customer_id");
			$project_detail=$this->Franchise_frontend->get_franchise_all_projects($customer_id);
			
			$data['project_detail']=$project_detail;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/franchise_create_project');
			$this->load->view('footer');
		}
		else{
			redirect(base_url());
		}	
	}
	public function create_project_details_action(){
		if($this->session->userdata("logged_in")){
			
			$project_name=$this->input->post("project_name");
			$client_name=$this->input->post("client_name");
			$client_email=$this->input->post("client_email");
			$client_mobile=$this->input->post("client_mobile");
			$project_location=$this->input->post("project_location");
			$project_type=$this->input->post("project_type");
			$project_requirements=$this->input->post("project_requirements");
			$project_deadline=$this->input->post("project_deadline");
			$project_tentative_budget=$this->input->post("project_tentative_budget");
			$project_status=$this->input->post("project_status");
			
			$customer_id=$this->session->userdata("customer_id");
			

			$date = new DateTime();
			$mt=microtime();
			$salt="franchise";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$key=md5($str);
			$unique_id=$key;

			$project_file='';

			// echo '<pre>';
			// print_r($_FILES);
			// echo '</pre>';

			$arr=[];

			if(!empty($_FILES)){
				if (!file_exists($folder="franchise_docs")) {
					$mask=umask(0);
					mkdir($folder,0777);
					umask($mask);
				}

				//find extention
				$filename=$_FILES["project_file"]["name"];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				//echo $ext;

				if(move_uploaded_file($_FILES["project_file"]["tmp_name"],$folder."/".$unique_id.'.'.$ext)){
					$project_file=$folder."/".$unique_id.'.'.$ext;
					//fileupload_s3($ff);	
				}else{
					$arr["status"]=false;
					$arr["unique_id"]=$unique_id;
					$arr["file_uploaded"]="no";
					
					
				}
			}

			if(empty($arr)){

			$flag=$this->Franchise_frontend->create_project_details_action($customer_id,$project_name,$client_name,$client_email,$client_mobile,$project_location,$project_type,$project_deadline,$project_requirements,$project_tentative_budget,$project_status,$project_file);

			$arr["status"]=$flag;
			$arr["unique_id"]=$unique_id;
			$arr["file_uploaded"]="yes";
			echo json_encode($arr);

			}else{
				echo json_encode($arr);
			}
				
		}
		else{
			redirect(base_url());
		}	
	}
	public function update_project_details($project_id){
		if($this->session->userdata("logged_in")){
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_page"]="update_project_details";
			$customer_id=$this->session->userdata("customer_id");
			$project_detail=$this->Franchise_frontend->get_franchise_projects($customer_id,$project_id);
			
			$data['project_id']=$project_id;
			$data['project_detail']=$project_detail;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/franchise_update_project_details');
			$this->load->view('footer');
		}
		else{
			redirect(base_url());
		}	
	}

	public function update_project_details_action(){
		if($this->session->userdata("logged_in")){
			
			$project_id=$this->input->post("project_id");
			$project_name=$this->input->post("project_name");
			$client_name=$this->input->post("client_name");
			$client_email=$this->input->post("client_email");
			$client_mobile=$this->input->post("client_mobile");
			$project_location=$this->input->post("project_location");
			$project_type=$this->input->post("project_type");
			$project_requirements=$this->input->post("project_requirements");
			$project_deadline=$this->input->post("project_deadline");
			$project_tentative_budget=$this->input->post("project_tentative_budget");
			$project_status=$this->input->post("project_status");
			$old_project_file=$this->input->post("old_project_file");

			$customer_id=$this->session->userdata("customer_id");
			
			$date = new DateTime();
			$mt=microtime();
			$salt="franchise";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$key=md5($str);
			$unique_id=$key;

			$project_file='';

			// echo '<pre>';
			// print_r($_FILES);
			// echo '</pre>';

			$arr=[];

			if(!empty($_FILES)){
				if (!file_exists($folder="franchise_docs")) {
					$mask=umask(0);
					mkdir($folder,0777);
					umask($mask);
				}

				//find extention
				$filename=$_FILES["project_file"]["name"];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				//echo $ext;

				if(move_uploaded_file($_FILES["project_file"]["tmp_name"],$folder."/".$unique_id.'.'.$ext)){
					
					if(file_exists($old_project_file)){
						unlink($old_project_file);
					}
					$project_file=$folder."/".$unique_id.'.'.$ext;
					//fileupload_s3($ff);	
				}else{

					//$arr["status"]=false;
					$arr["unique_id"]=$unique_id;
					$arr["file_uploaded"]="no";
					$project_file=$this->input->post("old_project_file");
				}
			}else{
				$project_file=$this->input->post("old_project_file");
			}

			$flag=$this->Franchise_frontend->update_project_details_action($customer_id,$project_id,$project_name,$client_name,$client_email,$client_mobile,$project_location,$project_type,$project_deadline,$project_requirements,$project_tentative_budget,$project_status,$project_file);
			
			$arr["status"]=$flag;

			if(empty($arr)){

			
			
			$arr["unique_id"]=$unique_id;
			
			$arr["file_uploaded"]="yes";
			echo json_encode($arr);

			}else{
				echo json_encode($arr);
			}
				
		}
		else{
			redirect(base_url());
		}	
	}
	/* Franchise module ends */
		
	
	}//end of class
?>