<?php
	class Loginsession extends CI_controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Customer_frontend');			
			$this->load->library('My_PHPMailer');	
			$this->load->helper('cookie');		
			$this->load->helper('emailsmstemplate');
			$this->load->helper('general');			
			$this->load->helper('user_analytics');			
		}
	
		public function index(){
			redirect("Loginsession");
			
		}
		public function email_exist($input){
				$this->load->helper("email");
				$type = "";
				if(valid_email($input)):
					$type = "email";
					endif;
					switch($type){
						case "email":
							$sql_customer ="SELECT * FROM `customer` WHERE `email` = '$input'";
							break;
					}

				$exec_customer = $this->db->query($sql_customer);
				if(($exec_customer->num_rows()==0)){
					$this->form_validation->set_message("email_exist" , "The $type you entered does not exist");	
					return FALSE;
				}
		}
		
		function checkemailverification(){
			$email=$this->input->post('email');
			$data=$this->Customer_frontend->checkemailverification($email);
			echo $data[0]->active;
			}
		
		function check_email()
		{	
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->email))
				{
					$email=$data->email;
					$data=$this->Customer_frontend->verify_email($email);
				}
				else{
					return null;
				}
			}
		}
		
		function check_mobile()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->mobile))
				{
					$mobile=$data->mobile;
					$data=$this->Customer_frontend->verify_mobile($mobile);
				}
				else{
					return null;
				}
			}
		}
		function check_otpcode()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->c_id))
				{
					$c_id=$data->c_id;
					$rn=$data->otp_code;
					$data=$this->Customer_frontend->get_verif_code_db($c_id,$rn);
				}
				else{
					//echo 'session not started';
					return null;
				}
			}
		}
		
		function add_customer(){
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					//[password_c] => 1
					$name=$data->name;
					$email=$data->email;
					$temppassword=$data->temppassword;
					
					$pwtemp="";
					for($i=0;$i<strlen($temppassword);$i++){
						$pwtemp.=chr(ord($temppassword[$i])+6);
					}				
					$mobile=$data->mobile;
					$rand=mt_rand(1000,10000);
					
					if($mobile != '' ){
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						

						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$data=$this->Customer_frontend->add_customer($name,$email,$mobile,$pwtemp,$rand);	
						}

						curl_close($ch);
					}//sms
					
					
				}
			}
		}
		function update_customer()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					//[password_c] => 1
					$c_id=$data->c_id;
					$name=$data->name;
					$email=$data->email;
					$password=$data->password;
					$pwtemp="";
					for($i=0;$i<strlen($password);$i++){
						$pwtemp.=chr(ord($password[$i])+6);
					}
					$mobile=$data->mobile;
					$rand=mt_rand(1000,10000);
					
					if($mobile != '' ){
						
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						

						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$data=$this->Customer_frontend->update_new_customer($c_id,$name,$email,$mobile,$pwtemp,$rand);	
						}

						curl_close($ch);
					}//sms
					
				}
			}
		}
		function resend_code_to_newuser()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$c_id=(isset($data->c_id)) ? $data->c_id : '';
					$mobile=(isset($data->val)) ? $data->val : '';
					$rand=rand(1000,10000);

					if($c_id!='' && $mobile==''){
						//get mob number from db
						$mobile=mob_customer($c_id);
					}

					if($mobile != ''){
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						

						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$data=$this->Customer_frontend->resend_code_to_newuser($c_id,$mobile,$rand);
						}

						curl_close($ch);
					}//sms
					
				}
			}
		}
		function activate_new_user()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$c_id=$data->c_id;
					
					$wallet_created=$this->Customer_frontend->create_wallet_account($c_id);
					if($wallet_created){
						
						$result=$this->Customer_frontend->activate_new_user($c_id);
						
						if($result){	
						
							$customer_info_arr=$this->Customer_frontend->get_customer_info_by_cid($c_id);
							$this->session->set_userdata("customer_id",$c_id);
							$this->session->set_userdata("logged_in",true);
							$this->session->set_userdata("customer_name",$customer_info_arr->name);
							$this->session->set_userdata("customer_email",$customer_info_arr->email);
							$this->session->set_userdata("customer_mobile",$customer_info_arr->mobile);
							$this->session->set_userdata("user_type","customer");
							
							$valid=true;
							echo json_encode(array(
							'data' => $valid,
							'mobileno_in_session'=>$customer_info_arr->mobile
							));
						}else{
							$valid=false;
							echo json_encode(array(
							'data' => $valid,
							));
						}
					
					}else{
						$valid=false;
						echo json_encode(array(
						'data' => $valid,
						));
					}
				
				}
			}
		}
		function customer_login()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->password))
				{
					$un=$data->username;
					$pw=$data->password;
					$type=$data->type;
					$pwtemp="";
					for($i=0;$i<strlen($pw);$i++){
						$pwtemp.=chr(ord($pw[$i])+7);
					}
					//echo $pwtemp;exit;
					$data=$this->Customer_frontend->customer_login($un,$pwtemp,$type);

					
				}
				else{
					return null;
				}
			}
		}
		function customer_check_user()
		{	
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->val))
				{
					$val=$data->val;
					$data=$this->Customer_frontend->customer_verify_user($val);
				}
				else{
					return null;
				}
			}
		}
		function resend_code_to_setpassword()
		{
			$email_stuff_arr=email_stuff();
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$val=$data->val;
					$rand=rand(100000,999999);
					
					if(is_numeric($val))
					{
						$mobile=$val;
						if($mobile != '' ){
						
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						
						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$data=$this->Customer_frontend->resend_code_to_setpassword($val,$rand);
						}

						curl_close($ch);
						}//sms
					
						
					}else{
							
						//through Email
						if($val!=''){
							$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
							
							<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Your verification code to reset your password <b>'.$rand.'</b></p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>Team Axonlabs</b></p><br></td></tr>';
							
							$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

							$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

							$usermessage.='</tbody> </table></tr></table></center> </body></html>';
						
							
							$mail = new PHPMailer;
							/*$mail->isSMTP();
							$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
							$mail->Port = '587';//  live - comment it 
							$mail->Auth = true;//  live - comment it 
							$mail->SMTPAuth = true;
							$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
							$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
							$mail->SMTPSecure = 'tls';*/
							$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
							$mail->FromName = $email_stuff_arr["name_emailtemplate"];
							$mail->addAddress($val);
							//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
							//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
							$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
							$mail->WordWrap = 50;
							$mail->isHTML(true);
							$mail->Subject = 'Reset Password';
							$mail->Body    = $usermessage;
							if($val != ''){		
									$mail->send();
									$data=$this->Customer_frontend->resend_code_to_setpassword($val,$rand);
							}
						}
						
					}//mail

				}
			}
		}
		function resend_code_to_newpassword()
		{
			$email_stuff_arr=email_stuff();
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$c_id=$data->c_id;
					$val=$data->val;
					$rand=rand(100000,999999);
					if(is_numeric($val))
					{
						$mobile=$val;
						if($mobile != '' ){
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						

						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$data=$this->Customer_frontend->resend_code_to_newpassword($c_id,$val,$rand);
						}

						curl_close($ch);
						}//sms
					
						
					}else{
						
						//through Email
						if($val!=''){
							$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
							
							<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Your verification code to reset your password <b>'.$rand.'</b></p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>Team Axonlabs</b></p><br></td></tr>';
							
							$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';
							
							$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

							$usermessage.='</tbody> </table></tr></table></center> </body></html>';
						
							
							$mail = new PHPMailer;
							/*$mail->isSMTP();
							$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
							$mail->Port = '587';//  live - comment it 
							$mail->Auth = true;//  live - comment it 
							$mail->SMTPAuth = true;
							$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
							$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
							$mail->SMTPSecure = 'tls';*/
							$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
							$mail->FromName = $email_stuff_arr["name_emailtemplate"];
							$mail->addAddress($val);
							//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
							//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
							$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
							$mail->WordWrap = 50;
							$mail->isHTML(true);
							$mail->Subject = 'Reset Password';
							$mail->Body    = $usermessage;
							if($val != ''){		
									$mail->send();
									$data=$this->Customer_frontend->resend_code_to_newpassword($c_id,$val,$rand);
							}
						}
						
					}//mail
					
					
				}
			}
		}
		function reset_password()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$c_id=$data->c_id;
					$pw=$data->password;
					$pwtemp="";
					for($i=0;$i<strlen($pw);$i++){
						$pwtemp.=chr(ord($pw[$i])+7);
					}
					
					$data=$this->Customer_frontend->reset_password($c_id,$pwtemp);
				}
			}
		}
		
		public function logout(){
		if($this->session->userdata("logged_in")){
			
			$update_logout=$this->Customer_frontend->update_visitor_logout();
			
			$this->session->sess_destroy();
			//redirect($_SERVER["HTTP_REFERER"]);		
			redirect(base_url());
		}
		else{
			redirect(base_url());
		}
	}
        public function add_vendor(){
                if(isset($_REQUEST))
                {
                        $data = json_decode(file_get_contents("php://input"));
                        $comp_data=$data;
                        if(isset($data))
                        {
                                //[password_c] => 1
                                $name=$data->name;
                                $email=$data->email;
                                $temppassword=$data->temppassword;
                                
                                $address1=(isset($data->address1)) ? $data->address1 : '';
                                $address2=(isset($data->address2)) ? $data->address2 : '';
                                $pincode=(isset($data->pincode)) ? $data->pincode : '';

                                $pwtemp="";
                                for($i=0;$i<strlen($temppassword);$i++){
                                    $pwtemp.=chr(ord($temppassword[$i])+6);
                                }				

                                $mobile=$data->mobile;
                                //$rand=mt_rand(1000,10000);


//print_r($data);exit;

                                $flag=$this->Customer_frontend->add_vendor($name,$email,$mobile,$pwtemp,$data);
                                //echo $flag;
                                
                                if($flag!=''){
                                $email_stuff_arr=email_stuff();
                                $str='Thanks for registering as a seller. We have received your submission. Our team will review and get back to you soonest.';

                            if($email!=''){

								$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
								
								<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">'.$str.'</p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br></td></tr>';
									
									$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

									$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

									$usermessage.='</tbody> </table></tr></table></center> </body></html>';


                                    $mail = new PHPMailer;
                                    /*$mail->isSMTP();
                                    $mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
                                    $mail->Port = '587';//  live - comment it 
                                    $mail->Auth = true;//  live - comment it 
                                    $mail->SMTPAuth = true;
                                    $mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
                                    $mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
                                    $mail->SMTPSecure = 'tls';*/
                                    $mail->From = $email_stuff_arr["fromemail_emailtemplate"];
                                    $mail->FromName = $email_stuff_arr["name_emailtemplate"];
                                    $mail->addAddress($email);
                                    $mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
                                    $mail->WordWrap = 50;
                                    $mail->isHTML(true);
                                    $mail->Subject = 'Seller Registration At Voomet Studio';
                                    $mail->Body    = $usermessage;
                                    if($email != ''){
                                        @$mail->send();
                                        //$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
                                        //echo $flag;
                                    }
								}
									/* mail to admin */
									$admin_email=SITE_ADMIN_EMAIL;
									$admin_email_1=SITE_ADMIN_EMAIL_1;
									$admin_email_2=SITE_ADMIN_EMAIL_2;
									$admin_email_3=SITE_ADMIN_EMAIL_3;
									
									$admim_name="Admin";

								if($admin_email!=''){

									$str='There is a new seller registration. the details are below,';

									$str.='Name :'.$name;
									$str.='<br>Email :'.$email;
									$str.='<br>Mobile :'.$mobile;
									$str.='<br>pincode :'.$pincode;


									$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
								
									<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi Admin, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">'.$str.'</p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br></td></tr>';
										
										$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';
	
										$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';
	
										$usermessage.='</tbody> </table></tr></table></center> </body></html>';
	
	
										$mail = new PHPMailer;
										/*$mail->isSMTP();
										$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
										$mail->Port = '587';//  live - comment it 
										$mail->Auth = true;//  live - comment it 
										$mail->SMTPAuth = true;
										$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
										$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
										$mail->SMTPSecure = 'tls';*/
										$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
										$mail->FromName = $email_stuff_arr["name_emailtemplate"];
										$mail->addAddress($admin_email_1);
										$mail->AddCC($admin_email_2);
										$mail->AddCC($admin_email_3);
										$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										$mail->Subject = 'Seller Registration At Voomet Studio';
										$mail->Body    = $usermessage;
										if($email != ''){
											@$mail->send();
											//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
											//echo $flag;
											
										}

									}
									/* mail to admin */

                        
                                }
								
                                echo $flag;
                                
                                /*sending mail to vendor */

                        }
                }
        }
        
	
	
		
		 function generateRandomString($length = 6) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}
		
		
		
		
	

		function check_email_vendor()
		{	
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->email))
				{
					$email=$data->email;
					$data=$this->Customer_frontend->verify_email_vendor($email);
				}
				else{
					return null;
				}
			}
		}
		
		function check_mobile_vendor()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->mobile))
				{
					$mobile=$data->mobile;
					$data=$this->Customer_frontend->verify_mobile_vendor($mobile);
				}
				else{
					return null;
				}
			}
		}
		

	}
?>