<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom404 extends CI_Controller {

  public function __construct() {

    parent::__construct();

    // load base_url
    $this->load->helper('url');
	$this->load->helper('emailsmstemplate');
	$this->load->helper('general');	
	$this->load->helper('notifications');	
	$this->load->model('Customer_frontend');
  }

  public function index(){
 
    $this->output->set_status_header('404'); 
    //$this->load->view('error404_old');
	
	
	
	
	
			
		//visitor_activity();
		//$method=current_url();
		//$this->session->set_userdata("cur_page",$method);
		//$data['current_page_url']=$method;
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		
		
			$data['p_category']=$this->provide_first_level_menu();
			
			$data["controller"]=$this;
			$data["current_controller"]="search";
			
			
		
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			
			$data["Custom404"]="yes";
			
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			
			$this->load->view('error404');
			$this->load->view('footer');
		
	
	
 
  }
  public function sample_code()
	{
		$seed = str_split('abcdefghijklmnopqrstuvwxyz'
                     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                     .'0123456789'); // and any other characters
				shuffle($seed); // probably optional since array_is randomized; this may be redundant
				$rand = '';
				foreach (array_rand($seed, 9) as $k) 
					$rand .= $seed[$k];	
				return $rand;
	}
  public function sortBy($field,&$array,$direction='asc')
	{
		/*usort($array, create_function('$a, $b', '
			$a = $a["' . $field . '"];
			$b = $b["' . $field . '"];

			if ($a == $b)
			{
				return 0;
			}

			return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
		'));
*/
		return true;
	}
	public function provide_category($pcat_id)
	{
		return $this->Customer_frontend->provide_category($pcat_id);
		
	}
  
  public function provide_first_level_menu()
	{
		$p_menu=$this->Customer_frontend->provide_parent_category_level();
		$menu1=array();
		
		foreach($p_menu as $val)
		{
			if($val->menu_level==1)
			{
				$menu1[]=array('parent_menu'=>$val->pcat_name,'type'=>'parent_cat','id'=>$val->pcat_id,'pcat_id'=>$val->pcat_id,'order'=>$val->menu_sort_order,'show_sub_menu'=>$val->show_sub_menu);
			}
		}
		
		$s_menu=$this->Customer_frontend->provide_category_level();
		
		foreach($s_menu as $value)
		{
			if($value->menu_level==1)
			{
				$menu1[]=array('parent_menu'=>$value->cat_name,'type'=>'cat','id'=>$value->cat_id,'order'=>$value->menu_sort_order,'pcat_id'=>$value->pcat_id);
			}
		}
		@$this->sortBy('order',$menu1);
		return $menu1;
	}
	
	public function provide_sub_category($cat_id)
	{
		$sub_cat=$this->Customer_frontend->provide_sub_category($cat_id);
		//$sub_cat=$this->Customer_frontend->provide_sub_category($cat_value->cat_id);
				
		$menu3=array();
		foreach ($sub_cat as $subcat_value)
		{
			$menu3[]=array('subcat_id'=>$subcat_value->subcat_id,'subcat_name'=>$subcat_value->subcat_name,'order'=>$subcat_value->menu_sort_order);			
		}
			@$this->sortBy('order',$menu3);
		return $menu3;

	}
	public function provide_brands($subcat_id)
	{
		return $this->Customer_frontend->provide_brands($subcat_id);
		
	}

}