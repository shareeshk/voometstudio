<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	//$ci->//instead //$this 


	function getUserIP()
		{
			//print_r($_SERVER);

			switch(true){
			case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
			case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
			case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
			default : return $_SERVER['REMOTE_ADDR'];
			}
		}
		function detect_device(){
			$useragent=$_SERVER['HTTP_USER_AGENT'];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
	return "mobile";
}else{
	return "desktop";
}
//header('Location: http://detectmobilebrowser.com/');


		}

		
		function get_geo_data_of_ip($ip){

			return '';
			
			/* https://apilayer.com/marketplace/ip_to_location-api#documentation-tab */
			/* subscribe for free - get apiKey - 10,000 per day */
			
			$apiKey='YPEgwWhWflkipM5MYBAKQuZI4l0F1xGc';
			$ch = curl_init();
		
			//$ip = getUserIP(); // for localhost not valid ip
			$service_url="https://api.apilayer.com/ip_to_location/182.48.79.85";

			curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $service_url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);  
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 3);     
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'apikey: ' . $apiKey
					));
					
			$data = curl_exec($ch);
			curl_close($ch);
		
			$city='';
			$geo_data = json_decode($data, TRUE);
			if(!empty($geo_data) && is_array($geo_data)){
				if(isset($geo_data["city"])){
					$city=$geo_data["city"];
				}
			}
			return $city;
		}


		function read_and_update_user_data($json_data_vistitor,$userID,$do_status,$timeSpent='',$request_uri='',$previous_uri=''){


			$ci =& get_instance();
			$c_id = $ci->session->userdata("customer_id");
			if($c_id != '' && $c_id != null){
				$user_type=$c_id;
			}else{
				$user_type='guest';
			}
			$current_datetime_folder=Date("Y-m-d");
			$folder_name=Date("Y-m");
			$file_name="analytics_user/".$folder_name."/".$current_datetime_folder.".txt";
			
			
			if(!is_dir("analytics_user")){
				$mask=umask(0);
				mkdir("analytics_user",0777);
				umask($mask);
			}
		   
			if(!is_dir("analytics_user/".$folder_name)){
				$mask=umask(0);
				mkdir("analytics_user/".$folder_name,0777);
				umask($mask);
				$file = fopen("analytics_user/".$folder_name."/".$current_datetime_folder.".txt","w");
			}else{
				$file = fopen("analytics_user/".$folder_name."/".$current_datetime_folder.".txt","a");
			}
		
			$file = fopen($file_name,"r");
		
			$page_url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			
			$current_datetime=Date("Y-m-d H:i:s");
			
			$json_data_arr = json_decode(file_get_contents($file_name), true);
			
			$searched_text=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

			$index_key=$searched_text.'_'.$user_type;
			
			$index_key=$userID;

			if($request_uri==''){
				$request_uri=$ci->db->escape_str($_SERVER["REQUEST_URI"]);
			}
			if($previous_uri==''){
				$previous_uri=isset($_SERVER["HTTP_REFERER"]) ? $ci->db->escape_str($_SERVER["HTTP_REFERER"]) : '';
			}

		
			//echo date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);//human readable
				
			$request_time=$ci->db->escape_str($_SERVER["REQUEST_TIME"]);
			$current_datetime=Date("Y-m-d H:i:s");        
			$device=detect_device();

			$request_page=array("request_uri"=>$request_uri,"previous_uri"=>$previous_uri,"request_time"=>[$request_time.'_'.$user_type],"timeSpent"=>[$timeSpent]);
			$rq=(array($request_page)); //while first insert

			$ip = getUserIP(); 

			/* checking */
			//  $gd=get_geo_data_of_ip($ip);
			//  echo $gd;
			//  $geo_data = json_encode($gd, TRUE);

			//  echo $geo_data;
			//  exit;
			/* checking */

			$data = array(
					'visitor_id' => $userID,
					'user_type' => $user_type,
					'c_id'=>array($user_type),
					'ip' => $ip,
					'device'=>$device,
					'request_page'=>$rq,
					'user_behaviour'=>'',
					'log_activity'=>array(),
					'user_details'=>array()
					
			);

			if(empty($json_data_arr) || $json_data_arr==null){
				
				if($do_status=='create'){
					$gd=get_geo_data_of_ip($ip);
					//$geo_data = json_encode($gd, TRUE);
					$data['geo_data']=$gd;
				}else{
					if(isset($data['geo_data'])){
						if($data['geo_data']==''){
							$gd=get_geo_data_of_ip($ip);
							//$geo_data = json_encode($gd, TRUE);
							$data['geo_data']=$gd;
						}
					}else{
						$gd=get_geo_data_of_ip($ip);
						//$geo_data = json_encode($gd, TRUE);
						$data['geo_data']=$gd;
					}
				}

				$json_data_arr[]=$data;

			}else{

				$key = array_search($index_key, array_column($json_data_arr, 'visitor_id'));
				
					if($key !== false){

					$request_page_new=array();

					if(isset($json_data_arr[$key]['request_page'])){

						if(is_array($json_data_arr[$key]['request_page'])){
							$request_page_new=(array) ($json_data_arr[$key]['request_page']);
						}else{
							$request_page_new=(array) ($json_data_arr[$key]['request_page']);
						}
						
						$key_time = array_search($request_uri, array_column($request_page_new, 'request_uri'));

						if($key_time !== false){
							$request_page_new[$key_time]['request_time'][]=$request_time.'_'.$user_type;	
							$request_page_new[$key_time]['timeSpent'][]=$timeSpent;	
						}else{
							$request_page_new[]=$json_data_vistitor['request_page'];
						}

					}else{

						$request_page_new[]=$json_data_vistitor['request_page'];

					}

					$json_data_arr[$key]['request_page']=($request_page_new);

					if(isset($json_data_arr[$key]['geo_data'])){
						if($json_data_arr[$key]['geo_data']==''){
							
							$gd=get_geo_data_of_ip($ip);
							//$geo_data = json_encode($gd, TRUE);
							$json_data_arr[$key]['geo_data']=$gd;
							$json_data_arr[$key]['ip']=$ip;
						}
					}else{
						
						$gd=get_geo_data_of_ip($ip);
						//$geo_data = json_encode($gd, TRUE);
						$json_data_arr[$key]['geo_data']=$gd;
						$json_data_arr[$key]['ip']=$ip;
					}

				}else{

					if(isset($json_data_vistitor['geo_data'])){
						if($json_data_arr[$key]['geo_data']==''){
							
							$gd=get_geo_data_of_ip($ip);
							//$geo_data = json_encode($gd, TRUE);
							$json_data_vistitor['geo_data']=$gd;
							$json_data_vistitor['ip']=$ip;
						}
					}else{
						
						$gd=get_geo_data_of_ip($ip);
						//$geo_data = json_encode($gd, TRUE);
						$json_data_vistitor['geo_data']=$gd;
						$json_data_vistitor['ip']=$ip;
					}

					$json_data_arr[]=$json_data_vistitor;

				}
		 
			}

			//print_r($json_data_arr);
			//exit;
			$file = fopen($file_name,"w");
			fputs($file, json_encode($json_data_arr));
			fclose($file);
		}

	function visitor_activity_analytics($timeSpent='',$request_uri='',$previous_uri=''){		
		$ci =& get_instance();
		$customer_id=$ci->session->userdata("customer_id");
		$ci->load->database();	

		
		if(isset($customer_id) && $customer_id!=''){
			$user_type=$customer_id;
		}else{
			$user_type="guest";
		}

		if($request_uri==''){
			$request_uri=$ci->db->escape_str($_SERVER["REQUEST_URI"]);
		}
		if($previous_uri==''){
			$previous_uri=isset($_SERVER["HTTP_REFERER"]) ? $ci->db->escape_str($_SERVER["HTTP_REFERER"]) : '';
		}


		//echo date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);//human readable
		
		$request_time=$ci->db->escape_str($_SERVER["REQUEST_TIME"]);
		$request_page=array("request_uri"=>$request_uri,"previous_uri"=>$previous_uri,"request_time"=>array($request_time.'_'.$user_type),"timeSpent"=>array($timeSpent));
		//$request_page=json_encode($request_page);
		
		//$request_page='"'.time().'"'.":".$request_page;
		/*echo '<pre>';
		print_r($request_page);
		echo '</pre>';*/
		$rq=(array($request_page)); //while first insert	
		$userID=get_cookie('userID');
		//echo "user ID || ".$userID;
	
		if($userID===false || $userID===null){
			
			//echo 'inside';exit;
			
			//$time=time()+86400;
			$time=86400;//24 hours
			$ip = getUserIP();
			$int = (preg_replace('/[^0-9]+/', '', $ip));

			$rand=rand(100,1000);
			$userID=$rand.$int;
			
			$cookie = array(
                        'name'   => 'userID',
                        'value'  => $userID ,                          
                        'expire' => $time
                        );
			set_cookie($cookie);

			$gd=get_geo_data_of_ip($ip);
			
			
			$geo_data = json_decode($gd, TRUE);

			$device=detect_device();

			$data = array(
					'visitor_id' => $userID,
					'user_type' => $user_type,
					'c_id'=>array($user_type),
					'ip' => $ip,
					'geo_data'=>$gd,
					'device'=>$device,
					'request_page'=>array($rq),
					'user_behaviour'=>'',
					'log_activity'=>array(),
					'user_details'=>array()
			);
			
			//$res=$ci->db->insert('visitor_data', $data);
		
			$do_status="create";

		}else{
			$do_status="update";
			//$ip = getUserIP(); 
			//$gd=get_geo_data_of_ip();
			//$geo_data = json_decode($gd, TRUE);
			$device=detect_device();
			$data = array(
					'visitor_id' => $userID,
					'user_type' => $user_type,
					'c_id'=>array($user_type),
					'device'=>$device,
					'request_page'=>$request_page,
					'user_behaviour'=>'',
					'log_activity'=>array(),
					'user_details'=>array()
			);
	
		}
		//print_r($_COOKIE); 
		//echo $do_status;
		//echo 'userID'.$userID ;

		$status = read_and_update_user_data($data,$userID,$do_status,$timeSpent,$request_uri,$previous_uri);

		return $status;
		
	}

	function update_user_login($user_type,$log_activity){
		
		$current_datetime_folder=Date("Y-m-d");
		$folder_name=Date("Y-m");
		$file_name="analytics_user/".$folder_name."/".$current_datetime_folder.".txt";
		
		if(!is_dir("analytics_user")){
			$mask=umask(0);
			mkdir("analytics_user",0777);
			umask($mask);
		}
	   
		if(!is_dir("analytics_user/".$folder_name)){
			$mask=umask(0);
			mkdir("analytics_user/".$folder_name,0777);
			umask($mask);
			$file = fopen("analytics_user/".$folder_name."/".$current_datetime_folder.".txt","w");
		}else{
			$file = fopen("analytics_user/".$folder_name."/".$current_datetime_folder.".txt","a");
		}
	
		$file = fopen($file_name,"r");
		$json_data_arr = json_decode(file_get_contents($file_name), true);

		if(empty($json_data_arr) || $json_data_arr==null){

		}else{
			$userID=get_cookie('userID');
			$index_key=$userID;
			$key = array_search($index_key, array_column($json_data_arr, 'visitor_id'));
				
			if($key !== false){
				if(isset($json_data_arr[$key]['log_activity'])){

					$json_data_arr[$key]['c_id'][]=$user_type;

					$json_data_arr[$key]['log_activity'][]=$log_activity;
					$file = fopen($file_name,"w");
					fputs($file, json_encode($json_data_arr));
					fclose($file);
				}
			}
			
		}
		//fclose($file);

	}

	function update_login_activity($customer_id){
		$user_type=$customer_id;
		$log_activity=array("login"=>time());
		$user_activity=array("user_type"=>$user_type,"time"=>time());
		//$log_activity=json_encode($log_activity);
		$status = update_user_login($user_type,$log_activity);
	}
	function update_logout_activity($customer_id){
		$user_type=$customer_id;
		$log_activity=array("logout"=>time());
		$user_activity=array("user_type"=>$user_type,"time"=>time());
		//$log_activity=json_encode($log_activity);
		$status = update_user_login($user_type,$log_activity);

	}

	