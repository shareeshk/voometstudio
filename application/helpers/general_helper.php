<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('get_pricetobeshown'))
{
    function get_pricetobeshown(){
       //get main CodeIgniter object
       $ci =& get_instance();
      
       //load databse library
       $ci->load->database();
     
	   //get data from database
	   
	    $sql="select pricetobeshown from pricetobeshown";
		
	   // "BETWEEN NOW() - INTERVAL 30 DAY AND NOW()";
	   $query=$ci->db->query($sql);
	   return $query->row_array()["pricetobeshown"];
	}
}
function isMobileDevice() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo
|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i"
        , $_SERVER["HTTP_USER_AGENT"]);
}
function razorpay_ids($order_id){
    //get main CodeIgniter object
    //$order_id='241021PKB00006';
    $arr=array();
    $ci =& get_instance();
    $ci->load->database();
    $sql="select razorpayPaymentId,razorpayOrderId from active_orders where order_id='$order_id'";
    $query=$ci->db->query($sql);
    if($query->num_rows()>0){
        $arr=$query->row_array();
    }else{
        $sql="select razorpayPaymentId,razorpayOrderId from history_orders where order_id='$order_id'";
        $query=$ci->db->query($sql);
        $arr=$query->row_array();
    }
    //print_r($arr);
    return $arr;
}
function get_promotions_cart(){
    $ci =& get_instance();
    $ci->load->database();

    /* from front promotion */
    $sql="select * from promotions where `promotions`.`promo_active`=1 and promotions.applied_at_invoice=1 and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
    $result=$ci->db->query($sql);
    if($result->num_rows()>0){
        $promotions_at_cart=$result->result_array();

    /* from front promotion */

    //$promotions_at_cart=$this->Front_promotions->get_promotions_at_cart();

        $updated_promotion_cart=array();

        if($promotions_at_cart!=false){

            foreach($promotions_at_cart as $key=>$data_val){
                $arrs=[];

                $arrs['promo_uid']=$data_val['promo_uid'];
                $arrs['promo_type']=$data_val['promo_type'];
                $arrs['quote_selector']=$data_val['quote_selector'];
                $arrs['promo_name']=$data_val['promo_name'];
                $arrs['to_buy']=$data_val['to_buy'];
                $arrs['to_get']=$data_val['to_get'];
                $arrs['buy_type']=$data_val['buy_type'];
                $arrs['get_type']=$data_val['get_type'];
                $arrs['applied_at_invoice']=$data_val['applied_at_invoice'];

                $offer = $data_val['promo_quote'];
                if(preg_replace("/\([^)]+\)/","",$offer)){
                    $offer = preg_replace("/\([^)]+\)/","",$offer);
                    $offer = preg_replace('/\s\s\s+/', ' ', $offer);
                    $arrs['promo_quote']=$offer;
                }else{
                    $arrs['promo_quote']=$offer;
                }
                array_push($updated_promotion_cart,$arrs);
            }
        }
        return $updated_promotion_cart;
    }else{
        return false;
    }
}
function get_promotion_active_status($id){

    $ci =& get_instance();
    $ci->load->database();

    /* from front promotion */
    $sql="select * from promotions where `promotions`.`promo_active`=1 and now() > promotions.promo_start_date  and not promotions.promo_end_date < now() and promo_uid='$id'";
    $result=$ci->db->query($sql);
    $promotions_at_cart=array();
    if($result->num_rows()>0){
        $promotions_at_cart=$result->row_array();
    }
    return $promotions_at_cart;
}
function sample_code()
{
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
             .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
             .'0123456789'); // and any other characters
                        shuffle($seed); // probably optional since array_is randomized; this may be redundant
                        $rand = '';
                        foreach (array_rand($seed, 9) as $k) 
                                $rand .= $seed[$k];	
                        return $rand;
}
	



/*** oct 9th 2022 newly added starts ****/
function add_visitor_count_traffic($page_index){
    if(!is_dir("analytics")){
		$mask=umask(0);
		mkdir("analytics",0777);
		umask($mask);
	}
	$current_datetime_folder=Date("Y-m-d");
	$folder_name=Date("Y-m");
	if(!is_dir("analytics/".$folder_name)){
		$mask=umask(0);
		mkdir("analytics/".$folder_name,0777);
		umask($mask);
		$file = fopen("analytics/".$folder_name."/".$current_datetime_folder.".txt","w");
	}else{
		$file = fopen("analytics/".$folder_name."/".$current_datetime_folder.".txt","a");
	}
	
	$page_url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;
	$view_count=4;
	$current_datetime=Date("Y-m-d H:i:s");
	
	$page_info_arr=array("page_index"=>$page_index,"page_url"=>$page_url,"view_count"=>$view_count,"datetime"=>$current_datetime);
	$page_info_str=json_encode($page_info_arr);

	fwrite($file,$page_info_str."\n");
	fclose($file);
}

function read_and_update_traffic($page_url=''){
    $ci =& get_instance();
    $c_id = $ci->session->userdata("customer_id");
    if($c_id != '' && $c_id != null){
        $user_type=$c_id;
    }else{
        $user_type='guest';
    }
	$current_datetime_folder=Date("Y-m-d");
	$folder_name=Date("Y-m");
    $file_name="analytics/".$folder_name."/".$current_datetime_folder.".txt";
	
	
    if(!is_dir("analytics")){
		$mask=umask(0);
		mkdir("analytics",0777);
		umask($mask);
	}
   
	if(!is_dir("analytics/".$folder_name)){
		$mask=umask(0);
		mkdir("analytics/".$folder_name,0777);
		umask($mask);
		$file = fopen("analytics/".$folder_name."/".$current_datetime_folder.".txt","w");
	}else{
		$file = fopen("analytics/".$folder_name."/".$current_datetime_folder.".txt","a");
	}

    $file = fopen($file_name,"r");

    if($page_url==''){
        $page_url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
	
	$current_datetime=Date("Y-m-d H:i:s");
    
    $json_data_arr = json_decode(file_get_contents($file_name), true);
    
    //$searched_text=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $searched_text=$page_url;
    $index_key=$searched_text.'_'.$user_type;
        
    if(empty($json_data_arr) || $json_data_arr==null){
        
        //$page_url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;

        $current_datetime=Date("Y-m-d H:i:s");        
        $json_data_arr=array(array("index_key"=>$index_key,"page_url"=>$page_url,"view_count"=>1,"user_type"=>$user_type,"datetime"=>$current_datetime));
        $file = fopen($file_name,"w");
        
    }else{
        
        $key = array_search($index_key, array_column($json_data_arr, 'page_url'));
        $view_count_new=1;
        if(intval($key)>=0 && $key!=''){

            $user_type=$json_data_arr[$key]['user_type'];
            
            $view_count_new=isset($json_data_arr[$key]['view_count']) ? ($json_data_arr[$key]['view_count']+1): 1;
            $json_data_arr[$key]['view_count']=$view_count_new;
        }else{
            //$page_url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;
            $view_count=1;
            $current_datetime=Date("Y-m-d H:i:s");
            $json_data_arr[]=array("index_key"=>$index_key,"page_url"=>$page_url,"view_count"=>$view_count,"user_type"=>$user_type,"datetime"=>$current_datetime,);
        }
 
    }
    $file = fopen($file_name,"w");
    fputs($file, json_encode($json_data_arr));
    fclose($file);
}
function displayDates($date1, $date2, $format = 'Y-m-d' ) {
    $dates = array();
    $current = strtotime($date1);
    $date2 = strtotime($date2);
    $stepVal = '+1 day';
    while( $current <= $date2 ) {   
        
       $mY=date('Y-m',$current);
       $dates[$mY][]=date($format, $current);
       $current = strtotime($stepVal, $current);
    }
    return $dates;
 }
/*** oct 9th 2022 newly added ends  *****/

function get_product_details($product_id){
    $ci =& get_instance();
    $ci->load->database();

    $sql="select * from products where product_id='$product_id'";
    $result=$ci->db->query($sql);
    return $result->row();
    
}
function get_all_inventory_data($inv_id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select * from inventory ";
    $sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';
                $sql.=" where id = '$inv_id'";
    $result=$ci->db->query($sql);
    
    return $result->row();
}
function get_all_external_inventory_data($inv_id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select * from external_inventory where external_inventory_id = '$inv_id' ";
    $result=$ci->db->query($sql);
    
    return $result->row();
}
function get_inventory_tagged_products($inv_id,$tagged_type=""){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select * from tagged_inventory ";
    $sql.=' LEFT JOIN `inventory` ON `inventory`.`id` = `tagged_inventory`.`tagged_main_inventory_id`';
    $sql.=' LEFT JOIN `products` ON `products`.`product_id` = `tagged_inventory`.`tagged_product_id`';
    $sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `tagged_inventory`.`tagged_pcat_id`';
    $sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `tagged_inventory`.`tagged_cat_id`';
    $sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `tagged_inventory`.`tagged_subcat_id`';
    $sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `tagged_inventory`.`tagged_brand_id`';
    $sql.=" where id = '$inv_id'";
    if($tagged_type=="internal"){
        $sql.=" and tagged_type='$tagged_type'";
    }
    //$sql.=" and inventory.active='1'";
   
      
    //echo $sql;

    $result=$ci->db->query($sql);
    
    return $result->result();

}
function update_paytm_data($data,$order_id,$status,$amount){
    $ci =& get_instance();
    $ci->load->database();

    $arr=array('paytm_post_data'=>$data,'paytm_post_data_timestamp'=>date('Y-m-d H:i:s'),'paytm_post_data_order_id'=>$order_id,'paytm_post_data_status'=>$status,'TXNAMOUNT'=>$amount);

    $result=$ci->db->insert("paytm_post_data",$arr);
}
function get_admin_combo_settings(){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select * from admin_settings ";
    $result=$ci->db->query($sql);
    return $result->row();
}
function get_email_of_admin(){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select email from admin_country where user_type='Master Country'";
    $result=$ci->db->query($sql);
    return $result->row()->email;
}
function get_combo_carttable($customer_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select * from carttable_combo where combo_customer_id='$customer_id'";
    $result=$ci->db->query($sql);
    return $result->row();
}
function paid_amount($order_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select * from paytm_post_data where paytm_post_data_order_id='$order_id'";
    $result=$ci->db->query($sql);
    return $result->row();

}
function get_orderitem_details($order_item_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select * from active_orders where order_item_id='$order_item_id'";
    $result=$ci->db->query($sql);
    return $result->row();

}
function get_orderitem_details_completed($order_item_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select * from completed_orders where order_item_id='$order_item_id'";
    $result=$ci->db->query($sql);
    return $result->row();

}

function get_orderitem_details_history($order_item_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select * from history_orders where order_item_id='$order_item_id'";
    $result=$ci->db->query($sql);
    return $result->row();

}
function date_status($startdate,$enddate){

    $startdate=date("Y-m-d H:i:s", strtotime($startdate));
    $enddate=date("Y-m-d H:i:s", strtotime($enddate));

    $today = date("Y-m-d H:i:s");    
    $today_date = new DateTime($today);
    $expiry_date = new DateTime($enddate);

    $status='';

    if($startdate < $today_date && $expiry_date > $today_date){
        $status='Active';
    }else if ($expiry_date < $today_date) { 
        $status='Expired';
    }
    return $status;
}
function get_inventory_image_path($inv_id,$path){
    $ci =& get_instance();
    $ci->load->database();
    $path_full=base_url().$path;
    if (file_exists($path_full)) {   
        return $path;
    }else{
        $sql="select * from inventory where id='$inv_id'";
        $result=$ci->db->query($sql);
        $obj=$result->row();
        if($result->num_rows()>0){
            $path=$obj->thumbnail;
            $path_full=base_url().$obj->thumbnail;
            if (file_exists($path_full)) {   
                return $path;
            }

        }
    }

    return $path;

}
function get_inventory_image_path_combo($inv_id,$path_full){
    $ci =& get_instance();
    $ci->load->database();
    if (file_exists($path_full)) {   
        return $path_full;
    }else{
        $sql="select * from inventory where id='$inv_id'";
        $result=$ci->db->query($sql);
        $obj=$result->row();
        if($result->num_rows()>0){
            $path=$obj->thumbnail;
            $path_full=base_url().$obj->thumbnail;
            if (file_exists($path_full)) {   
                return $path_full;
            }

        }
    }

    return $path_full;

}




function get_cat_list_for_pincode_index($pincode){
    
    $ci =& get_instance();
    $ci->load->database();
    
    //1.with pincode get vendor ids - matching with cities of operation
    //2.with vendor ids - get inventory ids 
    //3.with inventory lists , get category list 
 
    
        $city_list_arr=array();
        $pin=substr($pincode, 0, 3);
        $state_id='';
        $city_list_arr=array();
        $vendor_ids_list_arr=array();
        $cat_ids_list_arr=array();
        $res=array();
        $vendor_ids='';
        $city_arr=array();
        
        $pin=substr($pincode,0,3);
    
        $sql="select * from  logistics_all_pincodes where pin='$pin' and pincodes like '%$pincode%'";
        $result=$ci->db->query($sql);
        $pincode_valid=($result->num_rows() > 0) ? "valid" : 'not_valid' ;
        if($pincode_valid=='valid'){
            
            $sql_city="select state_name,city from logistics_all_pincodes "
                    . " left join logistics_all_states on logistics_all_states.state_id = logistics_all_pincodes.state_id "
                    ." where pin='$pin' and pincodes like '%$pincode%'";
            $q_city=$ci->db->query($sql_city);
            $city_arr=$q_city->row_array();
            
            $sql="select GROUP_CONCAT(pincode_id) as pincode_ids from logistics_all_pincodes where pin='$pin' and pincodes like '%$pincode%'";
            $query1=$ci->db->query($sql); // 797,1481,2129,2402
       
            if($query1->num_rows()>0){
                $city_list=$query1->row()->pincode_ids;

                if($city_list!=''){

                        $city_list_arr=explode(',',$city_list);
                        $city_list_arr=array_unique($city_list_arr);
                        array_walk($city_list_arr, function (&$value, $key) {
                            if($value!=''){
                                $value=" find_in_set($value,cities_of_operation)>0 ";
                            }
                         });
                        $where_q_string= implode(' OR ',$city_list_arr);
						// find_in_set(797,cities_of_operation)>0 OR find_in_set(1481,cities_of_operation)>0 OR find_in_set(2129,cities_of_operation)>0 OR find_in_set(2402,cities_of_operation)>0

                       $sql_vendor="select GROUP_CONCAT(vendor_id) as vendor_ids from vendors where ($where_q_string) and approval_status='1'";
                        $query_v=$ci->db->query($sql_vendor);

                        if($query_v->num_rows()>0){
                            $vendor_ids_list=$query_v->row()->vendor_ids;
                            if($vendor_ids_list!=''){
                                $vendor_ids_list_arr=explode(',',$vendor_ids_list);
                                $vendor_ids_list_arr=array_unique($vendor_ids_list_arr);
                                $vendor_ids=$vendor_ids_list;
                                array_walk($vendor_ids_list_arr, function (&$value, $key) {
                                    if($value!=''){
                                        $value=" find_in_set($value,vendor_id)>0 ";
                                    }
                                 });

                                $where_q_string_vendor= implode(' OR ',$vendor_ids_list_arr); 
								// find_in_set(4,vendor_id)>0 

                                $sql_cat="select GROUP_CONCAT(products.cat_id) as cat_ids from inventory "
                                        . "LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id` "
                                        ." where inventory.active=1 ";
                                        //. " where $where_q_string_vendor";

                                $query_cat=$ci->db->query($sql_cat);
                                if($query_cat->num_rows()>0){
                                    $cat_ids_list=$query_cat->row()->cat_ids;
                                    if($cat_ids_list!=''){
                                        $cat_ids_list_arr=explode(',',$query_cat->row()->cat_ids);
                                        $cat_ids_list_arr=array_unique($cat_ids_list_arr);
                                        if(!empty($cat_ids_list_arr)){
                                          //$sql_vendor="select * from category where in $where_q_string";
                                            $query_v=$ci->db->select("*")->from("category")->where_in("cat_id",$cat_ids_list_arr)->get();
                                            $res=$query_v->result();   
                                        }

                                    }//cat_list
                                }
                            }//vendor_list
                        }


                }//city list
            }
        }//valid pincode
    $arr=array('pincode'=>$pincode,
        'pincode_valid'=>$pincode_valid,
        'city_list_arr'=>$city_list_arr,
        'vendor_id_arr'=>$vendor_ids_list_arr,
        'cat_ids_list_arr'=>$cat_ids_list_arr,
        'cat_list_result'=>$res,
        'vendor_ids'=>$vendor_ids,
        'city_arr'=>$city_arr
        );
    return $arr;
}


function request_reference_id($rq_customer_name,$rq_gsm){
    $rq_customer_name=str_replace(' ', '', strtolower($rq_customer_name));
    $cus_name=substr($rq_customer_name, 0, 4);
    $gsm=str_replace(' ', '', strtolower($rq_gsm));
    $rand=mt_rand(1000, 9999);
    $date=date('d');
    $month=date('m');
    
    return $str=$cus_name.$date.$month.$gsm.$rand;
    
}

function get_vendor_details($id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select *,(select count(*) from inventory where (find_in_set(vendors.vendor_id,vendor_id) <> 0) ) as invs_count from vendors where vendor_id = '$id'";
    $result=$ci->db->query($sql);
    
    return $result->row();
}

function get_franchise_details($id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select * from franchise_customer where id = '$id'";
    $result=$ci->db->query($sql);
    
    return $result->row();
}
function get_franchise_projects_count(){
    $ci =& get_instance();
    $ci->load->database();
    
    $c_id = $ci->session->userdata("customer_id");
    
    $sql="select count(*) as projects_count from franchise_projects where franchise_customer_id='$c_id'";
    $result=$ci->db->query($sql);
    
    return $result->row()->projects_count;
}
function mob_customer($c_id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select mobile from customer where id='$c_id'";
    $result=$ci->db->query($sql);
    
    return $result->row()->mobile;
}
function mob_franchise_customer($c_id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select mobile from franchise_customer where id='$c_id'";
    $result=$ci->db->query($sql);
    
    return $result->row()->mobile;
}
function front_product_display($pcat_id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql = "select * from inventory ";

    $sql.= ' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`'
            .' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`'
            .' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`'
            .' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`'
            .' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id` '
            ." where  inventory.active='1' and brands.approval_status='1' and find_in_set('$pcat_id',inventory.under_pcat_frontend)<>0";
    

    //echo $sql;
    $result=$ci->db->query($sql);
    return $result->result();
    
 }
function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
function get_marquee(){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select * from marquee where status='1'";
    $query=$ci->db->query($sql);
    return $query->result();
}
function get_tagged_info($vendor_catalog_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select * from inventory where inv_vendor_catalog_id='$vendor_catalog_id'";
    $query=$ci->db->query($sql);
    return $query->row();
}

function get_inv_info($arr){
    $ci =& get_instance();
    $ci->load->database();
    $query_v=$ci->db->select("*")->from("inventory")->where_in("id",$arr)->get();
    $res=$query_v->result();   

    return $res;
}
function get_inv_info_active($id,$main_inv_id=''){
    $ci =& get_instance();
    $ci->load->database();

    $sql="select *,IFNULL((inventory.stock-inventory.cutoff_stock),0) as stock_available from inventory where active='1' and product_status='In Stock'  and (inventory.stock-inventory.cutoff_stock)>0 and id='$id'";

    $query_v=$ci->db->query($sql);
    $res=$query_v->row();   

    if($main_inv_id!=''){
        
        /* check it is tagged to same inv */

        //echo $sql="SELECT * FROM tagged_inventory WHERE '$id' IN (tagged_inventory_ids) and tagged_main_inventory_id='$main_inv_id' ";
        
        $sql="select * from tagged_inventory where tagged_main_inventory_id='$main_inv_id' and find_in_set('$id',tagged_inventory_ids) <> 0";

        $result=$ci->db->query($sql);
        $res2=$result->row();

        if($query_v->num_rows()>0 && $result->num_rows()>0){
            return $res;
        }else{
            return '';
        }
        /* check it is tagged to same inv */

   
    }else{
        if($query_v->num_rows()>0){
            return $res;
        }else{
            return '';
        }
    }
   
    
}
function get_all_view_parent_categories(){
	$ci =& get_instance();
    $ci->load->database();
    
    $sql="select pcat_id,pcat_name from parent_category where view=1 order by pcat_id asc";
    $result=$ci->db->query($sql);
    return $result->result_array();
}
 function provide_parent_category() {
    $ci =& get_instance();
    $ci->load->database();
    $qry_bp = 'SELECT pcat_id,pcat_name from `parent_category` where `view` = "1" ';
    $result = $ci->db->query($qry_bp);
    return $result->result();
}
function get_all_inventory_data_by_pcat($pcat_id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select * from inventory ";
    $sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';
                $sql.=" where products.pcat_id = '$pcat_id' order by inventory.timestamp desc limit 0,4";
    $result=$ci->db->query($sql);
    return $result->result();
}
function get_purchased_count($promo_uid){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select * from history_orders where find_in_set('$promo_uid',promotion_id_selected)>0 ";
    $result=$ci->db->query($sql);
    return $result->num_rows();

}
function filter_array($arr,$key1){
    $filtered = array();
    
    foreach($arr as $index => $columns) {
        foreach($columns as $key => $value) {
            if ($key == $key1 && $value == true) {
                $filtered[] = $columns->value;
            }
        }
    }
    return $filtered;
}
function get_inventory_tagged_products_combo($inv_id,$tagged_type=""){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select * from tagged_inventory_combo ";
    $sql.=' LEFT JOIN `inventory` ON `inventory`.`id` = `tagged_inventory_combo`.`tagged_main_inventory_id`';
    $sql.=' LEFT JOIN `products` ON `products`.`product_id` = `tagged_inventory_combo`.`tagged_product_id`';
		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `tagged_inventory_combo`.`tagged_pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `tagged_inventory_combo`.`tagged_cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `tagged_inventory_combo`.`tagged_subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `tagged_inventory_combo`.`tagged_brand_id`';
                $sql.=" where id = '$inv_id'";
    $result=$ci->db->query($sql);
    
    return $result->result();

}

function milliseconds_to_minutes($milliseconds){
    $minutes = floor ( $milliseconds / (60 * 1000));
    return $minutes;
}
function timestamp_diff($a,$b){

    
    $datetime1 = new DateTime();
    $datetime1->setTimestamp($a);
    $datetime2 = new DateTime();
    $datetime2->setTimestamp($b);

    $seconds = $a / 1000;
    $seconds2 = $b / 1000;
    
    /*echo date("d-m-Y", $seconds);
    echo (date('Y-m-d H:s' , $a));
    echo '||||';
    echo (date('Y-m-d H:s' , $b));
    */

    $interval = $datetime1->diff($datetime2);

    $d=$interval->format('%a');
    $h=$interval->format('%h');
    $m=$interval->format('%i');

    $hours = intdiv($m, 60).':'. ($m % 60);

    return $m;

    //return 'Expire in :'.$d.'day and '.$h.'hr'.$m.'min';
}  
function local_origin_setup($pincode){
    
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select imp_info_json from logistics_all_pincodes where pincodes like '%$pincode%'";
    $query=$ci->db->query($sql);

    $actual_data=array();

    if($query->num_rows()>0){
        $imp_info_json=$query->row()->imp_info_json;
        $imp_info_arr=json_decode($imp_info_json);
        if(!empty($imp_info_arr)){
            $key = array_search($pincode, array_column($imp_info_arr, 'Pincode'));

            $actual_data = (array) $imp_info_arr[ $key];

            // echo '<pre>';
            // print_r($actual_data);
            // echo '</pre>';

            if(!empty($actual_data)){
                $state_id=$actual_data["state_id"];
                $city=$actual_data["city"];
                $Division_ID=$actual_data["Division_ID"];
                $Circle_ID=$actual_data["Circle_ID"];
                $Office_Id=$actual_data["Office_Id"];// Office_Id is unique
            }

        }
    }
    return $actual_data;

}
function get_all_inventory_data_by_sku_id($sku_id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select * from inventory ";
    $sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';
                $sql.=" where sku_id = '$sku_id'";
    $result=$ci->db->query($sql);
    
    return $result->row_array();
}

 function provide_parent_category_first_2() {
    $ci =& get_instance();
    $ci->load->database();
    $qry_bp = 'SELECT pcat_id,pcat_name from `parent_category` where `view` = "1" limit 0,2';
    $result = $ci->db->query($qry_bp);
    return $result->result();
}

function sample_code_random_numerics($len){
	$seed = str_split('0123456789'); // and any other characters
			shuffle($seed); // probably optional since array_is randomized; this may be redundant
			$rand = '';
			foreach (array_rand($seed, $len) as $k) 
				$rand .= $seed[$k];	
			return $rand;
}
function sample_code_random_alphabets($len){
	$seed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
			shuffle($seed); // probably optional since array_is randomized; this may be redundant
			$rand = '';
			foreach (array_rand($seed, $len) as $k) 
				$rand .= $seed[$k];	
			return $rand;
}
function getNumberInWords(float $number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $str = implode('', array_reverse($str));
   
	$inwords = preg_replace('/\s+/', ' ', $str);
	return ucfirst($inwords)."";
}
function get_addon_selection_limit($id){
    $ci =& get_instance();
    $ci->load->database();
    
    $sql="select addon_single_or_multiple_tagged_inventories_in_frontend from tagged_inventory where tagged_main_inventory_id='$id' ";
    $result=$ci->db->query($sql);
    if($result->num_rows()>0){
      $n=  isset($result->row()->addon_single_or_multiple_tagged_inventories_in_frontend) ? $result->row()->addon_single_or_multiple_tagged_inventories_in_frontend: 1;
    }else{
        $n=1;
    }
    return  $n;
}
function get_main_sku($id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select sku_id from history_orders where order_item_id='$id' ";
    $result=$ci->db->query($sql);
    if($result->num_rows()>0){
      $n=isset($result->row()->sku_id) ? $result->row()->sku_id: '';
    }else{
        $n='';
    }
    return  $n;
}
function get_product_vendor_id($inventory_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select vendor_catalog_inventory.vendor_id as product_vendor_id from inventory ";
    $sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id` where `inventory`.inv_vendor_catalog_id IS NOT NULL ';

    $sql.=" and id='$inventory_id' ";

    $result=$ci->db->query($sql);
    if($result->num_rows()>0){
        $n=isset($result->row()->product_vendor_id) ? $result->row()->product_vendor_id: '';
      }else{
          $n='';
      }
      return  $n;
}
function get_customer_email($customer_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select email from customer";

    $sql.=" where id='$customer_id' ";

    $result=$ci->db->query($sql);
    if($result->num_rows()>0){
        $email=isset($result->row()->email) ? $result->row()->email: '';
    }else{
        $email='';
    }
      return  $email;
}
function get_product_vendor_details($vendor_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select product_vendors.* from product_vendors where vendor_id='$vendor_id' ";
    $result=$ci->db->query($sql);
    return $result->row();
}

function get_sku_ids_of_product_vendor($vendor_id){
    $ci =& get_instance();
    $ci->load->database();
    $sql="select inventory.sku_id from inventory ";
    $sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id` where `inventory`.inv_vendor_catalog_id IS NOT NULL ';
    $sql.=" and vendor_catalog_inventory.vendor_id='$vendor_id' ";

    $result=$ci->db->query($sql);
    if($result->num_rows()>0){
        
        return  $result->row_array();
    }else{
        return array();
    }
    
}
function filterArray($value,$num){
    return ($value.number > $num);
}
function custom_echo($x, $length)
{
  if(strlen($x)<=$length){
    return $x;
  }else{
    $y=substr($x,0,$length) . '...';
    return $y;
  }
}
