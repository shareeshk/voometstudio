<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	//$ci->//instead //$this 
	function visitor_activity(){
		
		$ci =& get_instance();
		$customer_id=$ci->session->userdata("customer_id");
		$ci->load->database();		
		$userID=get_cookie('userID');
		
		function getUserIP()
{
     switch(true){
      case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
      case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
      case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
      default : return $_SERVER['REMOTE_ADDR'];
    }
}


$ip = getUserIP();
		if(isset($customer_id) && $customer_id!=''){
			$user_type=$customer_id;
		}else{
			$user_type="guest";
		}

		$request_uri=$ci->db->escape_str($_SERVER["REQUEST_URI"]);
		
		//echo date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);//human readable
		
		$request_time=$ci->db->escape_str($_SERVER["REQUEST_TIME"]);
		$request_page=array("request_uri"=>$request_uri,"request_time"=>$request_time);
		$request_page=json_encode($request_page);
		//$request_page='"'.time().'"'.":".$request_page;
		/*echo '<pre>';
		print_r($request_page);
		echo '</pre>';*/
		
		if($userID==false){
			
			//$time=time()+86400;
			$time=86400;
			$int = (preg_replace('/[^0-9]+/', '', $ip));

			$rand=rand(100,1000);
			$userID=$rand.$int;
			
			$cookie = array(
                        'name'   => 'userID',
                        'value'  => $userID,                            
                        'expire' => $time
                        );
			set_cookie($cookie);
			/* commented because of some issue on 14th JUne 2023 starts */
			//$url = "http://api.ipstack.com/".$ip."?access_key=7e3b6ee74b2d016a999c63f74096d634&output=json&legacy=1";
			//$geo_data = file_get_contents($url);
			/* commented because of some issue on 14th JUne 2023 ends */
			$geo_data="";
			
			
			//$geo_data = json_decode($json, TRUE);
		
			$data = array(
					'visitor_id' => $userID,
					'user_type' => $user_type,
					'IP' => $ip,
					'geo_data'=>$geo_data,
					'request_page'=>$request_page,
					'user_behaviour'=>'',
					'user_btn_activity'=>'',
					'reviews_read'=>'',
					'filter_selection'=>'',
					'log_actitivity'=>''
			);
			
			//$res=$ci->db->insert('visitor_data', $data);
		
		}else{

			$today=date('Y-m-d');
			
			$sql_check="select date(timestamp) from visitor_data where date(timestamp)='$today' and user_type='$user_type' limit 1";
			
			$check_res=$ci->db->query($sql_check);
			$num_rows=$check_res->num_rows();
			if($num_rows==0){
				/* commented because of some issue on 14th JUne 2023 starts */
				//$url = "http://api.ipstack.com/".$ip."?access_key=7e3b6ee74b2d016a999c63f74096d634&output=json&legacy=1";
				 
				//$geo_data = file_get_contents($url);
				/* commented because of some issue on 14th JUne 2023 ends */
				$geo_data="";
				
				//$geo_data = json_decode($json, TRUE);
				$data = array(
						'visitor_id' => $userID,
						'user_type' => $user_type,
						'IP' => $ip,
						'geo_data'=>$geo_data,
						'request_page'=>$request_page,
						'user_behaviour'=>'',
						'user_btn_activity'=>'',
						'reviews_read'=>'',
						'filter_selection'=>'',
						'log_actitivity'=>''
				);
				//$res=$ci->db->insert('visitor_data', $data);
			}else{
				$sql="update visitor_data set request_page=concat(request_page,',','$request_page') where visitor_id='$userID' and user_type='$user_type'";
				//$res=$ci->db->query($sql);
			}
	
		}
		
	}
	
	
	
	
