<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('call_notification_function')){
	function call_notification_function(){
       //get main CodeIgniter object
       $ci =& get_instance();
       $customer_id=$ci->session->userdata("customer_id");
	   
       //load databse library
       $ci->load->database();
       
	   //(order_item_notification.approved_status_view='1' or order_item_notification.confirmed_status_view='1' or order_item_notification.packed_status_view='1' or order_item_notification.shipped_status_view='1' or order_item_notification.delivered_status_view='1' or order_item_notification.cancelled_status_view='1') and
       
	   //get data from database
	   
	    $sql="select order_item_notification.*,orders_status.*,history_orders.product_id,history_orders.inventory_id,history_orders.sku_id,history_orders.ord_sku_name,products.product_name,inventory.thumbnail from order_item_notification,orders_status,history_orders,products,inventory where  order_item_notification.customer_id='$customer_id' and DATE(order_item_notification.timestamp) BETWEEN DATE(NOW()) - INTERVAL 7 DAY AND NOW() and order_item_notification.order_item_id=orders_status.order_item_id and ((history_orders.order_item_id=order_item_notification.order_item_id and history_orders.product_id=products.product_id and inventory.id=history_orders.inventory_id))";
		
	   // "BETWEEN NOW() - INTERVAL 30 DAY AND NOW()";
	   $query=$ci->db->query($sql);
	   return $query->result();
	}
    function unviewed_notification(){
		$total_count=0;
		$notify=array();
        $ci =& get_instance();
        $customer_id=$ci->session->userdata("customer_id");
        $ci->load->database();
	   
		$sql="select * from order_item_notification where (approved_status_view='1' or confirmed_status_view='1' or packed_status_view='1' or shipped_status_view='1' or delivered_status_view='1' or cancelled_status_view='1') and customer_id='$customer_id' and DATE(timestamp) BETWEEN DATE(NOW()) - INTERVAL 7 DAY AND NOW() ORDER BY id";
		
		$query=$ci->db->query($sql);
		$obj=$query->result();
		
		$notify["orders_notification_count"]=count($obj);
		
		$total_count+=count($obj);
		/*//////////////////*/
		$sql_unread_noti="select * from refund_notification where (admin_accept_status_view=1 or admin_reject_status_view=1 or admin_msg_view=1 or refund_label_status_view=1 or refund_success_view=1) and DATE(timestamp) BETWEEN DATE(NOW()) - INTERVAL 7 DAY AND NOW() and customer_id='$customer_id' ORDER BY id DESC";;
		
		$query_return_unread=$ci->db->query($sql_unread_noti);
		$obj_return_unread=$query_return_unread->result();
		$total_count+=count($obj_return_unread);
		/*//////////////////*/
		
		
		/*//////////////////*/
  
		$sql_unread_noti_repl="select * from replacement_notification t1 where (t1.admin_accept_status_view=1 or t1.admin_reject_status_view=1 or t1.admin_msg_view=1 or t1.repl_label_status_view=1) and DATE(t1.timestamp) BETWEEN DATE(NOW()) - INTERVAL 7 DAY AND NOW() and t1.customer_id='$customer_id' and NOT EXISTS (select prev_order_item_id from order_item_notification t2 where t2.prev_order_item_id=t1.order_item_id) ORDER BY t1.id DESC ";
		
		$query_repl_unread=$ci->db->query($sql_unread_noti_repl);
		$obj_repl_unread=$query_repl_unread->result();
		$total_count+=count($obj_repl_unread);
		/*//////////////////*/

		$sql_msg="select return_replies.*,history_orders.order_item_id,history_orders.order_id,history_orders.sku_id,products.product_name,inventory.thumbnail from  return_replies,history_orders,returns_desired,inventory,products where return_replies.user_type='admin' and return_replies.is_read='0' and history_orders.order_item_id=returns_desired.order_item_id and return_replies.return_order_id=returns_desired.return_order_id and history_orders.product_id=products.product_id and inventory.id=history_orders.inventory_id and return_replies.customer_id='$customer_id' ORDER BY return_replies.id DESC LIMIT 1";
		
		$query_msg=$ci->db->query($sql_msg);
		$obj_msg=$query_msg->row();
		$notify["returns_msg_notification"]=$obj_msg;
		
		
		$sql_msg_repl="select replacement_replies.*,history_orders.order_item_id,history_orders.order_id,history_orders.sku_id,products.product_name,inventory.thumbnail from  replacement_replies,history_orders,replacement_desired,inventory,products where replacement_replies.user_type='admin' and replacement_replies.is_read='0' and history_orders.order_item_id=replacement_desired.order_item_id and replacement_replies.return_order_id=replacement_desired.return_order_id and history_orders.product_id=products.product_id and inventory.id=history_orders.inventory_id and replacement_replies.customer_id='$customer_id' ORDER BY replacement_replies.id DESC LIMIT 1";
		
		$query_msg_repl=$ci->db->query($sql_msg_repl);
		$obj_msg_repl=$query_msg_repl->row();
		$notify["repl_msg_notification"]=$obj_msg_repl;
		
		$sql_msg_repl_stock="select replacement_replies.*,history_orders.order_item_id,history_orders.order_id,history_orders.sku_id,products.product_name,inventory.thumbnail from  replacement_replies,history_orders,return_stock_notification,inventory,products where replacement_replies.user_type='admin' and replacement_replies.is_read='0' and history_orders.order_item_id=return_stock_notification.order_item_id and replacement_replies.return_order_id=return_stock_notification.return_order_id and history_orders.product_id=products.product_id and inventory.id=history_orders.inventory_id and replacement_replies.customer_id='$customer_id' ORDER BY replacement_replies.id DESC LIMIT 1";
		
		$query_msg_repl_stock=$ci->db->query($sql_msg_repl_stock);
		$obj_msg_repl_stock=$query_msg_repl_stock->row();
		$notify["repl_msg_notification_stock"]=$obj_msg_repl_stock;
		
		
		$sql_return="select refund_notification.*,history_orders.order_item_id,history_orders.order_id,history_orders.sku_id,products.product_name,inventory.thumbnail from  refund_notification,history_orders,returns_desired,inventory,products where history_orders.order_item_id=returns_desired.order_item_id and refund_notification.return_order_id=returns_desired.return_order_id and history_orders.product_id=products.product_id and inventory.id=history_orders.inventory_id and DATE(refund_notification.timestamp) BETWEEN DATE(NOW()) - INTERVAL 7 DAY AND NOW() and refund_notification.customer_id='$customer_id' ORDER BY refund_notification.id DESC";
		
		$query_return=$ci->db->query($sql_return);
		$obj_return=$query_return->result();
		
		$notify["returns_notification"]=$obj_return;
		
		///////////////////////////////////////
		
		$sql_repl="select replacement_notification.*,history_orders.order_item_id,history_orders.order_id,history_orders.sku_id,products.product_name,inventory.thumbnail from replacement_notification,history_orders,replacement_desired,inventory,products where history_orders.order_item_id=replacement_desired.order_item_id and replacement_notification.return_order_id=replacement_desired.return_order_id and history_orders.product_id=products.product_id and inventory.id=history_orders.inventory_id and DATE(replacement_notification.timestamp) BETWEEN DATE(NOW()) - INTERVAL 7 DAY AND NOW() and replacement_notification.customer_id='$customer_id' and NOT EXISTS (select prev_order_item_id from order_item_notification t2 where t2.prev_order_item_id=replacement_notification.order_item_id) ORDER BY replacement_notification.id DESC";
		
		$query_repl=$ci->db->query($sql_repl);
		$obj_return=$query_repl->result();

		$notify["replacement_notification"]=$obj_return;
		
		$sql_repl_stock="select replacement_notification.*,history_orders.order_item_id,history_orders.order_id,history_orders.sku_id,products.product_name,inventory.thumbnail from replacement_notification,history_orders,return_stock_notification,inventory,products where history_orders.order_item_id=return_stock_notification.order_item_id and replacement_notification.return_order_id=return_stock_notification.return_order_id and history_orders.product_id=products.product_id and inventory.id=history_orders.inventory_id and DATE(replacement_notification.timestamp) BETWEEN DATE(NOW()) - INTERVAL 7 DAY AND NOW() and replacement_notification.customer_id='$customer_id' and NOT EXISTS (select prev_order_item_id from order_item_notification t2 where t2.prev_order_item_id=replacement_notification.order_item_id) ORDER BY replacement_notification.id DESC";
		
		$query_repl_stock=$ci->db->query($sql_repl_stock);
		$obj_stock=$query_repl_stock->result();

		$notify["repl_stock_notification"]=$obj_stock;
		
		///////////////////////////////////////

		$notify["total_count"]=$total_count;
		
		return $notify;
	}
}