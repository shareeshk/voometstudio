/*! =========================================================
 *
 * Material Bootstrap Wizard - V1.0.1
 *
 * =========================================================
 *
 * Copyright 2016 Creative Tim (http://www.creative-tim.com/product/material-bootstrap-wizard)
 *
 *
 *                       _oo0oo_
 *                      o8888888o
 *                      88" . "88
 *                      (| -_- |)
 *                      0\  =  /0
 *                    ___/`---'\___
 *                  .' \|     |// '.
 *                 / \|||  :  |||// \
 *                / _||||| -:- |||||- \
 *               |   | \\  -  /// |   |
 *               | \_|  ''\---/''  |_/ |
 *               \  .-\__  '-'  ___/-. /
 *             ___'. .'  /--.--\  `. .'___
 *          ."" '<  `.___\_<|>_/___.' >' "".
 *         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *         \  \ `_.   \_ __\ /__ _/   .-` /  /
 *     =====`-.____`.___ \_____/___.-`___.-'=====
 *                       `=---='
 *
 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *               Buddha Bless:  "No Bugs"
 *
 * ========================================================= */

// Material Bootstrap Wizard Functions

searchVisible = 0;
transparent = true;

$(document).ready(function(){

    $.material.init();

    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();

    // Code for the Validator
    var $validator = $('.wizard-card form').validate({
		
		  rules: {
			
			
        },

        errorPlacement: function(error, element) {
            $(element).parent('div').addClass('has-error');
         }
	});
	
    // Wizard Initialization
  	$('.wizard-card').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',
		//'previouspageSelector': '.btn-previouspage',
		

        onNext: function(tab, navigation, index) {
        	var $valid = $('.wizard-card form').valid();
        	if(!$valid) {
        		$validator.focusInvalid();
        		return false;
        	}
        },
		

        onInit : function(tab, navigation, index){

          //check number of tabs and fill the entire row
          var $total = navigation.find('li').length;
          $width = 100/$total;
          var $wizard = navigation.closest('.wizard-card');

          $display_width = $(document).width();

          if($display_width < 600 && $total > 3){
              $width = 50;
          }

           navigation.find('li').css('width',$width + '%');
           $first_li = navigation.find('li:first-child a').html();
           $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
           $('.wizard-card .wizard-navigation').append($moving_div);
           refreshAnimation($wizard, index);
           $('.moving-tab').css('transition','transform 0s');
       },

        onTabClick : function(tab, navigation, index){
            var $valid = $('.wizard-card form').valid();

            if(!$valid){
                return false;
            } else{
                return false;
            }
        },
//btn-previouspage
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $wizard = navigation.closest('.wizard-card');
if($current==1){
	$($wizard).find('.btn-previouspage').show();
}
else{
	$($wizard).find('.btn-previouspage').hide();
}
            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $($wizard).find('.btn-next').hide();
                $($wizard).find('.btn-finish').show();
				
            } else {
                $($wizard).find('.btn-next').show();
                $($wizard).find('.btn-finish').hide();
				
            }
			

            button_text = navigation.find('li:nth-child(' + $current + ') a').html();

            setTimeout(function(){
                $('.moving-tab').text(button_text);
            }, 150);

            var checkbox = $('.footer-checkbox');

            if( !index == 0 ){
                $(checkbox).css({
                    'opacity':'0',
                    'visibility':'hidden',
                    'position':'absolute'
                });
            } else {
                $(checkbox).css({
                    'opacity':'1',
                    'visibility':'visible'
                });
            }

            refreshAnimation($wizard, index);
        }
  	});

	
    // Prepare the preview for profile picture
	$("#wizard-picture-Common").change(function(){
        readURL50(this);
    });
	
	$("#wizard-picture-Common-parent").change(function(){
        readURL50parent(this);
    });
	$("#wizard-picture-Common-brand").change(function(){
        readURL50Brand(this);
    });
	
	$("#wizard-picture-Common200").change(function(){
        readURL200(this);
    });
	
	$("#wizard-picture-large1").change(function(){
        readURL20(this);
    });
    $("#wizard-picture-big1").change(function(){
        readURL(this);
    });
	$("#wizard-picture-small1").change(function(){
        readURL1(this);
    });
	$("#wizard-picture-large2").change(function(){
        readURL21(this);
    });
	$("#wizard-picture-big2").change(function(){
        readURL4(this);
    });
	$("#wizard-picture-small2").change(function(){
        readURL5(this);
    });
	$("#wizard-picture-large3").change(function(){
        readURL22(this);
    });
	$("#wizard-picture-big3").change(function(){
        readURL6(this);
    });
	$("#wizard-picture-small3").change(function(){
        readURL7(this);
    });
	$("#wizard-picture-large4").change(function(){
        readURL23(this);
    });
	$("#wizard-picture-big4").change(function(){
        readURL8(this);
    });
	$("#wizard-picture-small4").change(function(){
        readURL9(this);
    });
	$("#wizard-picture-large5").change(function(){
        readURL24(this);
    });
	$("#wizard-picture-big5").change(function(){
        readURL10(this);
    });
	$("#wizard-picture-small5").change(function(){
        readURL11(this);
    });
	
	$("#wizard-picture-large6").change(function(){
        readURL241(this);
    });
	$("#wizard-picture-big6").change(function(){
        readURL101(this);
    });
	$("#wizard-picture-small6").change(function(){
        readURL111(this);
    });
	
	///////////edit/////////////
	$("#edit_wizard-picture-Common").change(function(){
        readURL51(this);
    });
	
	$("#edit_wizard-picture-large1").change(function(){
        readURL25(this);
    });
	$("#edit_wizard-picture-big1").change(function(){
        readURL2(this);
    });
	$("#edit_wizard-picture-small1").change(function(){
        readURL3(this);
    });
	$("#edit_wizard-picture-large2").change(function(){
        readURL26(this);
    });
	$("#edit_wizard-picture-big2").change(function(){
        readURL12(this);
    });
	$("#edit_wizard-picture-small2").change(function(){
        readURL13(this);
    });
	$("#edit_wizard-picture-large3").change(function(){
        readURL27(this);
    });
	$("#edit_wizard-picture-big3").change(function(){
        readURL14(this);
    });
	$("#edit_wizard-picture-small3").change(function(){
        readURL15(this);
    });
	$("#edit_wizard-picture-large4").change(function(){
        readURL28(this);
    });
	$("#edit_wizard-picture-big4").change(function(){
        readURL16(this);
    });
	$("#edit_wizard-picture-small4").change(function(){
        readURL17(this);
    });
	$("#edit_wizard-picture-large5").change(function(){
        readURL29(this);
    });
	$("#edit_wizard-picture-big5").change(function(){
        readURL18(this);
    });
	$("#edit_wizard-picture-small5").change(function(){
        readURL19(this);
    });
	$("#edit_wizard-picture-large6").change(function(){
        readURL291(this);
    });
	$("#edit_wizard-picture-big6").change(function(){
        readURL181(this);
    });
	$("#edit_wizard-picture-small6").change(function(){
        readURL191(this);
    });
	
    $('[data-toggle="wizard-radio"]').click(function(){
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked','true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function(){
        if( $(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).find('[type="checkbox"]').removeAttr('checked');
        } else {
            $(this).addClass('active');
            $(this).find('[type="checkbox"]').attr('checked','true');
        }
    });

    $('.set-full-height').css('height', 'auto');

});



 //Function to show image before upload
 function readURL200(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==200 && height==200 && size<1024){
					$('#wizardPictureCommon200').attr('src', e.target.result).fadeIn('slow');
					
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureCommon200_picname").html(image_name);
					$('div#common200Pic').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (300x366) which is less than 1MB in size. '
					});
					
					$("#wizard-picture-Common200").replaceWith($("#wizard-picture-Common200").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
	}
}
function readURL50(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==300 && height==366 && size<1024){
					$('#wizardPictureCommon').attr('src', e.target.result).fadeIn('slow');
					
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureCommon_picname").html(image_name);
					$('div#commonPic').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (300x366) which is less than 1MB in size. '
					});
					
					$("#wizard-picture-Common").replaceWith($("#wizard-picture-Common").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
	}
}

function readURL50parent(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==300 && height==366 && size<1024){
					$('#wizardPictureCommonParent').attr('src', e.target.result).fadeIn('slow');
					
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureCommonParent_picname").html(image_name);
					$('div#commonPic').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (300x366) which is less than 1MB in size. '
					});
					
					$("#wizard-picture-Common-parent").replaceWith($("#wizard-picture-Common-parent").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
	}
}


function readURL50Brand(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==300 && height==366 && size<1024){
					$('#wizardPictureCommonBrand').attr('src', e.target.result).fadeIn('slow');
					
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureCommonBrand_picname").html(image_name);
					$('div#commonPic').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (300x366) which is less than 1MB in size. '
					});
					
					$("#wizard-picture-Common-brand").replaceWith($("#wizard-picture-Common-brand").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
	}
}



function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#wizardPictureSmall1').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureSmall_picname1").html(image_name);
					$('div#smallpic1').css("border", "2px solid #4caf50");
				
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#wizard-picture-small1").replaceWith($("#wizard-picture-small1").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL5(input) {
	
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#wizardPictureSmall2').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureSmall_picname2").html(image_name);
					$('div#smallpic2').css("border", "2px solid #4caf50");
				
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#wizard-picture-small2").replaceWith($("#wizard-picture-small2").val('').clone(true));
					return false;
				}
			};
            
			
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL7(input) {
	
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#wizardPictureSmall3').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureSmall_picname3").html(image_name);
					$('div#smallpic3').css("border", "2px solid #4caf50");
				
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#wizard-picture-small3").replaceWith($("#wizard-picture-small3").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL9(input) {
	
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#wizardPictureSmall4').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureSmall_picname4").html(image_name);
					$('div#smallpic4').css("border", "2px solid #4caf50");

				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#wizard-picture-small4").replaceWith($("#wizard-picture-small4").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL11(input) {
	
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#wizardPictureSmall5').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureSmall_picname5").html(image_name);
					$('div#smallpic5').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#wizard-picture-small5").replaceWith($("#wizard-picture-small5").val('').clone(true));
					return false;
				}
			};
			
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL111(input) {
	
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#wizardPictureSmall6').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureSmall_picname6").html(image_name);
					$('div#smallpic6').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#wizard-picture-small6").replaceWith($("#wizard-picture-small6").val('').clone(true));
					return false;
				}
			};
			
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					 $('#wizardPictureBig1').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureBig_picname1").html(image_name);
					$('div#bigpic1').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#wizard-picture-big1").replaceWith($("#wizard-picture-big1").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL4(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					 $('#wizardPictureBig2').attr('src', e.target.result).fadeIn('slow');	
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureBig_picname2").html(image_name);
					$('div#bigpic2').css("border", "2px solid #4caf50");

				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#wizard-picture-big2").replaceWith($("#wizard-picture-big2").val('').clone(true));
					return false;
				}
			};
			
            
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL6(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					$('#wizardPictureBig3').attr('src', e.target.result).fadeIn('slow');	
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureBig_picname3").html(image_name);
					$('div#bigpic3').css("border", "2px solid #4caf50");

				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#wizard-picture-big3").replaceWith($("#wizard-picture-big3").val('').clone(true));
					return false;
				}
			};
			
           
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL8(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					$('#wizardPictureBig4').attr('src', e.target.result).fadeIn('slow');	
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureBig_picname4").html(image_name);
					$('div#bigpic4').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#wizard-picture-big4").replaceWith($("#wizard-picture-big4").val('').clone(true));
					return false;
				}
			};
			
           
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL10(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					 $('#wizardPictureBig5').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureBig_picname5").html(image_name);
					$('div#bigpic5').css("border", "2px solid #4caf50");

				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#wizard-picture-big5").replaceWith($("#wizard-picture-big5").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
    }
}


function readURL101(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					 $('#wizardPictureBig6').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureBig_picname6").html(image_name);
					$('div#bigpic6').css("border", "2px solid #4caf50");

				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#wizard-picture-big6").replaceWith($("#wizard-picture-big6").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL20(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#wizardPictureLarge1').attr('src', e.target.result).fadeIn('slow');		
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureLarge_picname1").html(image_name);
					$('div#largepic1').css("border", "2px solid #4caf50");	
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					
					$("#wizard-picture-large1").replaceWith($("#wizard-picture-large1").val('').clone(true));
					return false;
				}
			};
			
            
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL21(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#wizardPictureLarge2').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureLarge_picname2").html(image_name);
					$('div#largepic2').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#wizard-picture-large2").replaceWith($("#wizard-picture-large2").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL22(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#wizardPictureLarge3').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureLarge_picname3").html(image_name);
					$('div#largepic3').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#wizard-picture-large3").replaceWith($("#wizard-picture-large3").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL23(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#wizardPictureLarge4').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureLarge_picname4").html(image_name);
					$('div#largepic4').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#wizard-picture-large4").replaceWith($("#wizard-picture-large4").val('').clone(true));
					return false;
				}
			};
			
            
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL24(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#wizardPictureLarge5').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureLarge_picname5").html(image_name);
					$('div#largepic5').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#wizard-picture-large5").replaceWith($("#wizard-picture-large5").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}






function readURL241(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#wizardPictureLarge6').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#wizardPictureLarge_picname6").html(image_name);
					$('div#largepic6').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#wizard-picture-large6").replaceWith($("#wizard-picture-large6").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}



////////////edit//////////////

function readURL51(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==300 && height==366 && size<1024){
					$('#edit_wizardPictureCommon').attr('src', e.target.result).fadeIn('slow');
					
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureCommon_picname").html(image_name);
					$('div#edit_commonPic').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (300x366) which is less than 1MB in size. '
					});
					
					$("#edit_wizard-picture-Common").replaceWith($("#edit_wizard-picture-Common").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
    }
}


function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#edit_wizardPictureSmall1').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureSmall_picname1").html(image_name);
					$('div#edit_smallpic1').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					
					$("#edit_wizard-picture-small1").replaceWith($("#edit_wizard-picture-small1").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL13(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#edit_wizardPictureSmall2').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureSmall_picname2").html(image_name);
					$('div#edit_smallpic2').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-small2").replaceWith($("#edit_wizard-picture-small2").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL15(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#edit_wizardPictureSmall3').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureSmall_picname3").html(image_name);
					$('div#edit_smallpic3').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-small3").replaceWith($("#edit_wizard-picture-small3").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL17(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#edit_wizardPictureSmall4').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureSmall_picname4").html(image_name);
					$('div#edit_smallpic4').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-small4").replaceWith($("#edit_wizard-picture-small4").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL19(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#edit_wizardPictureSmall5').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureSmall_picname5").html(image_name);
					$('div#edit_smallpic5').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-small5").replaceWith($("#edit_wizard-picture-small5").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL191(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==100 && height==122 && size<1024){
					$('#edit_wizardPictureSmall6').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureSmall_picname6").html(image_name);
					$('div#edit_smallpic6').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (100x122) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-small6").replaceWith($("#edit_wizard-picture-small6").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					$('#edit_wizardPictureBig1').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureBig_picname1").html(image_name);
					$('div#edit_bigpic1').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					
					$("#edit_wizard-picture-big1").replaceWith($("#edit_wizard-picture-big1").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL12(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					$('#edit_wizardPictureBig2').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureBig_picname2").html(image_name);
					$('div#edit_bigpic2').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-big2").replaceWith($("#edit_wizard-picture-big2").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL14(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					$('#edit_wizardPictureBig3').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureBig_picname3").html(image_name);
					$('div#edit_bigpic3').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-big3").replaceWith($("#edit_wizard-picture-big3").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL16(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					$('#edit_wizardPictureBig4').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureBig_picname4").html(image_name);
					$('div#edit_bigpic4').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-big4").replaceWith($("#edit_wizard-picture-big4").val('').clone(true));
					return false;
				}
			};
			
            
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL18(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					$('#edit_wizardPictureBig5').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureBig_picname5").html(image_name);
					$('div#edit_bigpic5').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-big5").replaceWith($("#edit_wizard-picture-big5").val('').clone(true));
					return false;
				}
			};
			
        }
        reader.readAsDataURL(input.files[0]);
    }
}


function readURL181(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==420 && height==512 && size<1024){
					$('#edit_wizardPictureBig6').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureBig_picname6").html(image_name);
					$('div#edit_bigpic6').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (420x512) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-big6").replaceWith($("#edit_wizard-picture-big6").val('').clone(true));
					return false;
				}
			};
			
        }
        reader.readAsDataURL(input.files[0]);
    }
}


function readURL25(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#edit_wizardPictureLarge1').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureLarge_picname1").html(image_name);
					$('div#edit_largepic1').css("border", "2px solid #4caf50");

				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-large1").replaceWith($("#edit_wizard-picture-large1").val('').clone(true));
					return false;
				}
			};
			
            
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL26(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#edit_wizardPictureLarge2').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureLarge_picname2").html(image_name);
					$('div#edit_largepic2').css("border", "2px solid #4caf50");

				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-large2").replaceWith($("#edit_wizard-picture-large2").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL27(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#edit_wizardPictureLarge3').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureLarge_picname3").html(image_name);
					$('div#edit_largepic3').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-large3").replaceWith($("#edit_wizard-picture-large3").val('').clone(true));
					return false;
				}
			};
    
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL28(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#edit_wizardPictureLarge4').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureLarge_picname4").html(image_name);
					$('div#edit_largepic4').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-large4").replaceWith($("#edit_wizard-picture-large4").val('').clone(true));
					return false;
				}
			};

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL29(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#edit_wizardPictureLarge5').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureLarge_picname5").html(image_name);
					$('div#edit_largepic5').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-large5").replaceWith($("#edit_wizard-picture-large5").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL291(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			
			var image = new Image();
			image.src = e.target.result;
			image.onload = function() {
				width=this.width;
				height=this.height;
				size=Math.round(input.files[0].size/1024);	
				
				if(width==850 && height==1036 && size<1024){
					$('#edit_wizardPictureLarge6').attr('src', e.target.result).fadeIn('slow');
					image_name=input.value;
					image_name=image_name.substr(image_name.lastIndexOf("\\")+1);
					$("#edit_wizardPictureLarge_picname6").html(image_name);
					$('div#edit_largepic6').css("border", "2px solid #4caf50");
				}else{
					swal({
					  type: 'info',
					  html:'Please upload image with correct dimension (850x1036) which is less than 1MB in size. '
					});
					$("#edit_wizard-picture-large6").replaceWith($("#edit_wizard-picture-large6").val('').clone(true));
					return false;
				}
			};
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(window).resize(function(){
    $('.wizard-card').each(function(){
        $wizard = $(this);
        index = $wizard.bootstrapWizard('currentIndex');
        refreshAnimation($wizard, index);

        $('.moving-tab').css({
            'transition': 'transform 0s'
        });
    });
});

function refreshAnimation($wizard, index){
    //total_steps = $wizard.find('li').length;
	total_steps=$(".wizard-navigation ul li").length;
    move_distance = $wizard.width() / total_steps;
    step_width = move_distance;
    move_distance *= index;

    $current = index + 1;

    if($current == 1){
        move_distance -= 8;
    } else if($current == total_steps){
        move_distance += 8;
    }

    $wizard.find('.moving-tab').css('width', step_width);
    $('.moving-tab').css({
		'width':'182.33px',
        'transform':'translate3d(' + move_distance + 'px, 0, 0)',
        'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

    });
}

materialDesign = {

    checkScrollForTransparentNavbar: debounce(function() {
                if($(document).scrollTop() > 260 ) {
                    if(transparent) {
                        transparent = false;
                        $('.navbar-color-on-scroll').removeClass('navbar-transparent');
                    }
                } else {
                    if( !transparent ) {
                        transparent = true;
                        $('.navbar-color-on-scroll').addClass('navbar-transparent');
                    }
                }
        }, 17)

}

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		clearTimeout(timeout);
		timeout = setTimeout(function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		}, wait);
		if (immediate && !timeout) func.apply(context, args);
	};
};
