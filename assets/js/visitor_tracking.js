function isScrolledIntoView(el) {
  var elemTop = el.getBoundingClientRect().top;
  var elemBottom = el.getBoundingClientRect().bottom;
  var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
  return isVisible;
}

var flag_body_image=0;
var testimonial=0;

var products_range=0;
var mostpurchased_range=0;
var shopbycategory_range=0;
var knockoutdeals_range=0;
var trendingproducts_range=0;
var dealoftheday_range=0;

var footer=0;

var category_banner=0;
var know_the_facts=0;
var category_range=0;
var category_testimonial=0;
var category_slider=0;
var category_promises=0;
var category_ingredients=0;
var category_steps=0;

var customer_reviews=0;
var recentlyViewed=0;
var similar_products=0;

$(window).on('scroll', function() {
	if(document.getElementById('testimonial')!=null){
		if (isScrolledIntoView(document.getElementById('bodyimage'))) {
			testimonial=0;
			products_range=0;
			mostpurchased_range=0;
shopbycategory_range=0;
knockoutdeals_range=0;
trendingproducts_range=0;
dealoftheday_range=0;
			why_content=0;
			footer=0;
		  
			if(flag_body_image==0){
			  flag_body_image=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"bodyimage","time":start};
				
				var test = localStorage.getItem("section_activity");

				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));
				
				//alert(JSON.stringify(result))
				return false;
			}
		}
	}
	
	if(document.getElementById('testimonial')!=null){
		if (isScrolledIntoView(document.getElementById('testimonial'))) {
			flag_body_image=0;
			products_range=0;
			mostpurchased_range=0;
shopbycategory_range=0;
knockoutdeals_range=0;
trendingproducts_range=0;
dealoftheday_range=0;
			why_content=0;
			footer=0;
			
			if(testimonial==0){
				testimonial=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"testimonial","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
			}
		}
	}

	if(document.getElementById('products_range')!=null){
		if (isScrolledIntoView(document.getElementById('products_range'))){
			flag_body_image=0;
			testimonial=0;
			why_content=0;
			footer=0;
			mostpurchased_range=0;
			shopbycategory_range=0;
			knockoutdeals_range=0;
			trendingproducts_range=0;
			dealoftheday_range=0;
			
			if(products_range==0){
				products_range=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"products_range","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				 return false;
			}
		}
	}
	
	if(document.getElementById('mostpurchased_range')!=null){
		if (isScrolledIntoView(document.getElementById('mostpurchased_range'))){
			flag_body_image=0;
			testimonial=0;
			why_content=0;
			footer=0;
			products_range=0;
			shopbycategory_range=0;
			knockoutdeals_range=0;
			trendingproducts_range=0;
			dealoftheday_range=0;
			
			if(mostpurchased_range==0){
				mostpurchased_range=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"mostpurchased_range","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				 return false;
			}
		}
	}
	
	if(document.getElementById('shopbycategory_range')!=null){
		if (isScrolledIntoView(document.getElementById('shopbycategory_range'))){
			flag_body_image=0;
			testimonial=0;
			why_content=0;
			footer=0;
			products_range=0;
			mostpurchased_range=0;
			knockoutdeals_range=0;
			trendingproducts_range=0;
			dealoftheday_range=0;
			
			if(shopbycategory_range==0){
				shopbycategory_range=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"shopbycategory_range","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				 return false;
			}
		}
	}
	
	if(document.getElementById('knockoutdeals_range')!=null){
		if (isScrolledIntoView(document.getElementById('knockoutdeals_range'))){
			flag_body_image=0;
			testimonial=0;
			why_content=0;
			footer=0;
			products_range=0;
			mostpurchased_range=0;
			shopbycategory_range=0;
			trendingproducts_range=0;
			dealoftheday_range=0;
			
			if(knockoutdeals_range==0){
				knockoutdeals_range=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"knockoutdeals_range","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				 return false;
			}
		}
	}
	
	if(document.getElementById('trendingproducts_range')!=null){
		if (isScrolledIntoView(document.getElementById('trendingproducts_range'))){
			flag_body_image=0;
			testimonial=0;
			why_content=0;
			footer=0;
			products_range=0;
			mostpurchased_range=0;
			shopbycategory_range=0;
			knockoutdeals_range=0;
			dealoftheday_range=0;
			
			if(trendingproducts_range==0){
				trendingproducts_range=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"trendingproducts_range","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				 return false;
			}
		}
	}
	
	if(document.getElementById('dealoftheday_range')!=null){
		if (isScrolledIntoView(document.getElementById('dealoftheday_range'))){
			flag_body_image=0;
			testimonial=0;
			why_content=0;
			footer=0;
			products_range=0;
			mostpurchased_range=0;
			shopbycategory_range=0;
			knockoutdeals_range=0;
			trendingproducts_range=0;
			
			if(dealoftheday_range==0){
				dealoftheday_range=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"dealoftheday_range","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				 return false;
			}
		}
	}
	
	if(document.getElementById('why_content')!=null){
		if(document.getElementById('why_content')!=null){
			if (isScrolledIntoView(document.getElementById('why_content'))) {
				flag_body_image=0;
				testimonial=0;
				products_range=0;
				mostpurchased_range=0;
shopbycategory_range=0;
knockoutdeals_range=0;
trendingproducts_range=0;
dealoftheday_range=0;
				footer=0;
				
			  if(why_content==0){
					why_content=1;
					var start = new Date();
					start=start.getTime();
					var sec_activity={"id":"why_content","time":start};			
					var test = localStorage.getItem("section_activity");
					if(test !=null){
						var stored = JSON.parse(localStorage.getItem("section_activity"));
						stored.push(sec_activity);
						localStorage.setItem("section_activity", JSON.stringify(stored));
					}else{
						var obj = [];
						obj.push(sec_activity);
						localStorage.setItem("section_activity", JSON.stringify(obj));
					}

					var result = JSON.parse(localStorage.getItem("section_activity"));

					//alert(JSON.stringify(result))
					return false;
			  }
			
			
			}
		}
	}
	if(document.getElementById('footer_span')!=null){
		if (isScrolledIntoView(document.getElementById('footer_span'))){

			flag_body_image=0;
			testimonial=0;
			products_range=0;
			mostpurchased_range=0;
shopbycategory_range=0;
knockoutdeals_range=0;
trendingproducts_range=0;
dealoftheday_range=0;
			why_content=0;
			
		  if(footer==0){
				footer=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"footer","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
		  }
		}
	}
	
		/*--------------category page----------*/
	/*
	category_banner
	know_the_facts
	category_range
	category_testimonial
	category_slider
	category_promoses
	category_ingredients
	category_steps*/
	
	if(document.getElementById('category_banner')!=null){
		if (isScrolledIntoView(document.getElementById('category_banner'))){

			know_the_facts=0;
			category_range=0;
			category_testimonial=0;
			category_slider=0;
			category_promises=0;
			category_ingredients=0;
			category_steps=0;
			
		  if(category_banner==0){
				category_banner=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"category_banner","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
		  }
		}
	}
	if(document.getElementById('know_the_facts')!=null){
		if (isScrolledIntoView(document.getElementById('know_the_facts'))){
			
			category_banner=0;
			category_range=0;
			category_testimonial=0;
			category_slider=0;
			category_promises=0;
			category_ingredients=0;
			category_steps=0;
			
		  if(know_the_facts==0){
				know_the_facts=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"know_the_facts","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
		  }
		}
	}
	if(document.getElementById('category_range')!=null){
		if (isScrolledIntoView(document.getElementById('category_range'))){
			
			category_banner=0;
			know_the_facts=0;
			category_testimonial=0;
			category_slider=0;
			category_promises=0;
			category_ingredients=0;
			category_steps=0;
			
		  if(category_range==0){
				category_range=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"category_range","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
		  }
		}
	}
	if(document.getElementById('category_testimonial')!=null){
		if (isScrolledIntoView(document.getElementById('category_testimonial'))){
			
			category_banner=0;
			know_the_facts=0;
			category_range=0;
			category_slider=0;
			category_promises=0;
			category_ingredients=0;
			category_steps=0;
			
		  if(category_testimonial==0){
				category_testimonial=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"category_testimonial","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
		  }
		}
	}
	if(document.getElementById('category_slider')!=null){
		if (isScrolledIntoView(document.getElementById('category_slider'))){
			
			category_banner=0;
			know_the_facts=0;
			category_range=0;
			category_testimonial=0;
			category_promises=0;
			category_ingredients=0;
			category_steps=0;
			
		  if(category_slider==0){
				category_slider=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"category_slider","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
		  }
		}
	}
	if(document.getElementById('category_promises')!=null){
		if (isScrolledIntoView(document.getElementById('category_promises'))){
			
			category_banner=0;
			know_the_facts=0;
			category_range=0;
			category_testimonial=0;
			category_slider=0;
			category_ingredients=0;
			category_steps=0;
			
		  if(category_promises==0){
				category_promises=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"category_promises","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
		  }
		}
	}
	if(document.getElementById('category_ingredients')!=null){
		if (isScrolledIntoView(document.getElementById('category_ingredients'))){
			
			category_banner=0;
			know_the_facts=0;
			category_range=0;
			category_testimonial=0;
			category_slider=0;
			category_promises=0;
			category_steps=0;
			
		  if(category_ingredients==0){
				category_ingredients=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"category_ingredients","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
		  }
		}
	}
	if(document.getElementById('category_steps')!=null){
		if (isScrolledIntoView(document.getElementById('category_steps'))){
			
			category_banner=0;
			know_the_facts=0;
			category_range=0;
			category_testimonial=0;
			category_slider=0;
			category_promises=0;
			category_ingredients=0;
			
		  if(category_steps==0){
				category_steps=1;
				var start = new Date();
				start=start.getTime();
				var sec_activity={"id":"category_steps","time":start};			
				var test = localStorage.getItem("section_activity");
				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(sec_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));

				//alert(JSON.stringify(result))
				return false;
		  }
		}
	}
	
	/* detail page */
	if(document.getElementById('reviews')!=null){
		if(isScrolledIntoView(document.getElementById('reviews'))) {
			recentlyViewed=0;
			similar_products=0;
			
			if(customer_reviews==0){
				customer_reviews=1;
				var start = new Date();
				start=start.getTime();
				var section_activity={"id":"customer_reviews","time":start};
				
				var test = localStorage.getItem("section_activity");

				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(section_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(section_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));
				
				//alert(JSON.stringify(result));
				
				return false;
			}
			
		}
	}
	
	console.log("similar_products "+document.getElementById('similar_products'));
	
	if(document.getElementById('similar_products')!=null){	

		console.log("isScrolledIntoView "+isScrolledIntoView(document.getElementById('similar_products')));


		if(isScrolledIntoView(document.getElementById('similar_products'))) {

			customer_reviews=0;
			recentlyViewed=0;
			if(similar_products==0){
				similar_products=1;
				var start = new Date();
				start=start.getTime();
				var section_activity={"id":"similar_products","time":start};
				
				var test = localStorage.getItem("section_activity");

				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(section_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(section_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));
				
				//alert(JSON.stringify(result)+"xxx")
				return false;
			}
			
		}
	}
	
	if(document.getElementById('recentlyViewed')!=null){
		if(isScrolledIntoView(document.getElementById('recentlyViewed'))) {
			
			if(recentlyViewed==0 && document.getElementById('recentlyViewed_div').style.display!="none"){
				customer_reviews=0;
				similar_products=0;
				recentlyViewed=1;
				var start = new Date();
				start=start.getTime();
				var section_activity={"id":"recentlyViewed","time":start};
				
				var test = localStorage.getItem("section_activity");

				if(test !=null){
					var stored = JSON.parse(localStorage.getItem("section_activity"));
					stored.push(section_activity);
					localStorage.setItem("section_activity", JSON.stringify(stored));
				}else{
					var obj = [];
					obj.push(section_activity);
					localStorage.setItem("section_activity", JSON.stringify(obj));
				}

				var result = JSON.parse(localStorage.getItem("section_activity"));
				
				//alert(JSON.stringify(result)+"yyyy")
				return false;
			}
		}
	}
	
	
});

function activity_on_products(id){

	var start = new Date();
	start=start.getTime();
	var section_activity={"id":id,"time":start};
	
	var test = localStorage.getItem("section_activity");

	if(test !=null){
		var stored = JSON.parse(localStorage.getItem("section_activity"));
		stored.push(section_activity);
		localStorage.setItem("section_activity", JSON.stringify(stored));
	}else{
		var obj = [];
		obj.push(section_activity);
		localStorage.setItem("section_activity", JSON.stringify(obj));
	}

	var result = JSON.parse(localStorage.getItem("section_activity"));
	
	//alert(JSON.stringify(result))
	return false;	
}