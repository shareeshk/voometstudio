<?php

use Aws\S3\Exceptio\S3Exception;

require 'app/start.php';

if(isset($_FILES['file'])){
    $file=$_FILES['file'];
    $name=$file['name'];
    $temp_name=$file['tmp_name'];
    
    $extension=explode('.',$name);
    $extension=strtolower(end($extension));
    
    $key=md5(uniqid());
    $tmp_file_name="{$key}.{$extension}";
    $tmp_file_path="files/{$tmp_file_name}";

    move_uploaded_file($temp_name, $tmp_file_path);
    
    try{
        $fh=fopen($tmp_file_path,'rb');
       $result = $s3->putObject([
            'Bucket'=>$config['s3']['bucket'],
            'Key'=>"uploads/{$name}",
            'Body'=>$fh,
            'ACL'=>'public-read'
        ]);
		fclose($fh);
        unlink($tmp_file_path);
		 
		//unlink("files/3c666299b659978c92095969f3727d93.jpg");
    }catch(S3Exception $e){
        die("error in uploading");
    }
     
}


?>
<body>
<form action="" method="post" enctype="multipart/form-data">
    <input type="file" name="file">
    <input type="submit" value="Upload">
</form>
</body>
