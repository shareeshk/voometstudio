<?php

use Aws\S3\S3Client;

require 'assets/s3_fileupload/vendor/autoload.php';

$config=require('assets/s3_fileupload/app/config.php');

$s3=S3Client::factory([
'credentials' => [
    'key'=>$config['s3']['key'],
    'secret'=>$config['s3']['secret'],
  ],
    
    'bucket'=>$config['s3']['bucket'],
    'region'=>$config['s3']['region'],
    'version'=>$config['s3']['version'],
]);
