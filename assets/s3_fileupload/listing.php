<?php 

require 'app/start.php';

$objects=$s3->getIterator('ListObjects',[
    'Bucket'=>$config['s3']['bucket']
]);

?>


<body>
	<table>
	    <thead>
	        <tr>
	            <th>File</th>
	            <th>Download</th>
	        </tr>
	    </thead>
	    <tbody>
	        <?php foreach($objects as $object):?> 
	        <tr>
                <td><?php echo $object['Key']; ?></td>
                <td> <a href="<?php echo $s3->getObjectUrl($config['s3']['bucket'],$object['Key']); ?>" download="<?php $object['Key'];?>">Download</a></td>
                <td> <button attrName="<?php echo $object['Key'];?>" onclick="DeleteObject(this)">Delete</button></td>
            </tr>
            <?php endforeach;?>
	    </tbody>
	</table>
<script>
    function DeleteObject(obj){
        var objName=obj.getAttribute('attrName');
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    //obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText==1){
                                location.reload()
                            }else{
                                alert('not deleted');
                            }                        
                        }
                    }
                    xhr.open('POST','delete.php', true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('obj='+objName);
                }
        
    }
</script>	
	
</body>	