
  function VendorCustomerGender(datae) {
    var datae=JSON.parse(datae)
    
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Gender Division'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: datae
                }]
            });
        }

        function VendorCustomerAge (datae) {
            var datae=JSON.parse(datae)
            data=[]
            for(var x=0;x<datae.length;x++){
                datas=[]
                datas.push(parseInt(datae[x].COUNT))
                data.push(datas)
            }
       Highcharts.chart('container', {
           chart: {
               type: 'column'
           },
           title: {
               text: 'Age Group Registered'
           },
           /*subtitle: {
               text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
           },*/
           xAxis: {
               categories: ['18-25', '26-35', '36-45', '46-55', 'Over 56'],
               title: {
                   text: null
               }
           },
           yAxis: {
               min: 0,
               title: {
                   text: 'Count',
                   align: 'high'
               },
               labels: {
                   overflow: 'justify'
               }
           },
           tooltip: {
               /*valueSuffix: ' millions'*/
           },
           plotOptions: {
               bar: {
                   dataLabels: {
                       enabled: true
                   }
               }
           },
           legend: {
               layout: 'vertical',
               align: 'right',
               verticalAlign: 'top',
               x: -40,
               y: 80,
               floating: true,
               borderWidth: 1,
               backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
               shadow: true
           },
           credits: {
               enabled: false
           },
           series: [{
               name: 'Count',
               data: data
           }]
       });
}
function VendorCustomerAge (datae) {
    var datae=JSON.parse(datae)
    data=[]
    for(var x=0;x<datae.length;x++){
        datas=[]
        datas.push(parseInt(datae[x].COUNT))
        data.push(datas)
    }
Highcharts.chart('container', {
   chart: {
       type: 'column'
   },
   title: {
       text: 'Age Group Registered'
   },
   /*subtitle: {
       text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
   },*/
   xAxis: {
       categories: ['18-25', '26-35', '36-45', '46-55', 'Over 56'],
       title: {
           text: null
       }
   },
   yAxis: {
       min: 0,
       title: {
           text: 'Count',
           align: 'high'
       },
       labels: {
           overflow: 'justify'
       }
   },
   tooltip: {
       /*valueSuffix: ' millions'*/
   },
   plotOptions: {
       bar: {
           dataLabels: {
               enabled: true
           }
       }
   },
   legend: {
       layout: 'vertical',
       align: 'right',
       verticalAlign: 'top',
       x: -40,
       y: 80,
       floating: true,
       borderWidth: 1,
       backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
       shadow: true
   },
   credits: {
       enabled: false
   },
   series: [{
       name: 'Count',
       data: data
   }]
});
}
       
function VendorCustomerRegistrations (datae) {
    var datae=JSON.parse(datae)
    data=[]
    for(var x=0;x<datae.length;x++){
        datas=[]
        if(datae[x].m!=""){
            datas.push(Date.UTC(datae[x].y,datae[x].m,datae[x].d),parseInt(datae[x].count))
        }
        data.push(datas)
    }

    
           Highcharts.chart('container', {
           
               chart: {
                   type: 'area',
                   spacingBottom: 30,
                   zoomType: 'x'
               },
               title: {
                   text: 'New Registration over the time'
               },
               subtitle: {
                   text: document.ontouchstart === undefined ?
                           'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in',
                   floating: true,
                   align: 'right',
                   verticalAlign: 'bottom',
                   y: 15
               },
               xAxis: {
                   type: 'datetime'
               },
               yAxis: {
                   title: {
                       text: 'Number Of Registration'
                   }
               },
               legend: {
                   enabled: false
               },
               plotOptions: {
                   area: {
                       fillColor: {
                           linearGradient: {
                               x1: 0,
                               y1: 0,
                               x2: 0,
                               y2: 1
                           },
                           stops: [
                               [0, Highcharts.getOptions().colors[0]],
                               [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                           ]
                       },
                       marker: {
                           radius: 5
                       },
                       lineWidth: 1,
                       states: {
                           hover: {
                               lineWidth: 1
                           }
                       },
                       threshold: null
                   }
               },
   
               series: [{
                   type: 'area',
                   name: 'Registration',
                   data: data
               }]
           });
   }
   function VendorCustomerRevenue(datae){
    var datae=JSON.parse(datae);
    console.log(datae);
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Revenue',
            align: 'left'
        },
        xAxis: {
            categories: ['Did Not buy','Revenue']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Revenue percentage'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray',
                    textOutline: 'none'
                }
            }
        },
        legend: {
            align: 'left',
            x: 70,
            verticalAlign: 'top',
            y: 70,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series:
        datae
        /*[ 
            
            {
            name: 'Did Not Buy',
            data: [0, 8]
        },
        {
            name: 'Revenue',
            data: [5,8]
        },
    ]*/

    });
   }
 

   function VendorCustomerRevenueGender(datae) {
    var datae=JSON.parse(datae)
    
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Revenue from Male and Female'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
                series: [{
                    name: 'Revenue',
                    colorByPoint: true,
                    data: datae
                }]
            });
        }


        function VendorCustomerSuscribed(datae) {
            
            var datae=JSON.parse(datae)
            
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Subscribers'
                },
                xAxis: {
                    categories: ['Total Subscribers', 'Total registered', 'Total Non Registered']
                },
                yAxis: {
                    title: {
                        useHTML: true,
                        text: 'Count of Subscibers'
                    }
                },
                tooltip: {
					headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
					pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
				},
                plotOptions: {
                    series: {
                        pointWidth: 100,
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            
                series: [{
                    name:"Subscriber",
                    /*data: [29.9, 71.5, 106.4, 129.2, 144.0]*/
                    data:datae
                }]
            });

        }
        function VendorCustomerTotalVisitorTraffic(datae) {
    
            var datae=JSON.parse(datae)
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Total Visitors'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}</b>'
                },
                xAxis: {
                    categories: datae.category
                },
                yAxis: {
                    title: {
                        useHTML: true,
                        text: 'View Count'
                    }
                },
                tooltip: {
					
					pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
				},
                plotOptions: {
                    series: {
                        pointWidth: 100,
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            
                series: [{
                    name:"Guest",
                    data: datae.guest
                },
                {
                    name:"Users",
                    data: datae.user
                }
            
            ]
            });

        }
        function VendorCustomerTotalVisitorTrafficPageWise(datae,page) {
    
            var datae=JSON.parse(datae)
            if(page==""){
                page="Home";
            }
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Total Visitors - '+page,
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}</b>'
                },
                xAxis: {
                    categories: datae.category
                },
                yAxis: {
                    title: {
                        useHTML: true,
                        text: 'View Count'
                    }
                },
                tooltip: {
					
					pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
				},
                plotOptions: {
                    series: {
                        pointWidth: 100,
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            
                series: [{
                    name:"Guest",
                    data: datae.guest
                },
                {
                    name:"Users",
                    data: datae.user
                }
            
            ]
            });

        }
        function VendorCustomerTotalVisitorTrafficPageWiseTop(datae,limit) {
    
            
            var datae=JSON.parse(datae)
            
           // Build the chart
           Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Top '+limit+' Visited pages'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
            series: [{
                name: 'User',
                colorByPoint: true,
                data: datae.user
            },
            {
                name: 'Guest',
                colorByPoint: true,
                data: datae.guest
            }
        ]
        });

        }
        function VendorCustomerTotalVisitorTrafficPageWiseBottom(datae,limit) {
    
            
            var datae=JSON.parse(datae)
            
           // Build the chart
           Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Bottom '+limit+' Visited pages'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
            series: [{
                name: 'User',
                colorByPoint: true,
                data: datae.user
            },
            {
                name: 'Guest',
                colorByPoint: true,
                data: datae.guest
            }
        ]
        });

        }
        function VendorCustomerSustainability(datae) {
    
            var datae=JSON.parse(datae)
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Sustainabilty Index'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                xAxis: {
                    categories: ['One time purchase','RP within 7 days', '30 days', '60 days', '90 days', 'more than 90 days']
                },
                yAxis: {
                    title: {
                        useHTML: true,
                        text: 'Purcase order Count'
                    }
                },
                tooltip: {
					
					pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
				},
                plotOptions: {
                    series: {
                        pointWidth: 100,
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            
                series: [{
                    name:"Repeat Purchase",
                    data: datae
                }]
            });

        }
        function VendorCustomerLifeCycle(datae,limit_result_val) {
    
            var datae=JSON.parse(datae)
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Customer Life Cycle'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}</b>'
                },
                xAxis: {
                    categories: ['Rs.0 - Rs.50000','Rs.50K-Rs.1L', 'Rs.1L-Rs.1.5L','More than 1.5L']
                },
                yAxis: {
                    title: {
                        useHTML: true,
                        text: 'Customer count'
                    }
                },
                tooltip: {
					
					pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
				},
                plotOptions: {
                    series: {
                        pointWidth: 100,
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            
                series: [{
                    name:"Customer Life Cycle for "+limit_result_val+"yrs",
                    data: datae
                }]
            });

        }

        function VendorCustomerAC_weekly(datae) {
    
            var datae=JSON.parse(datae)
            // Build the chart

            Highcharts.chart('container', {

                title: {
                    text: 'Abandoned Cart Value weekly'
                },
            
                yAxis: {
                    title: {
                        text: 'Abandoned Cart value in (Rs)'
                    }
                },
            
                xAxis: {
                    accessibility: {
                        rangeDescription: 'Abandoned Cart value in (Rs)'
                    },
                    categories: datae.x_axis_arr
                },
            
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
            
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 0
                    }
                },
            
                series: [
                    {
                    name: 'Cart value',
                    /*data: [43934, 48656, 65165, 81827, 112143, 142383,
                        171533, 165174, 155157, 161454, 154610]*/
                    data:datae.res
                }
            ],
            
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            
            });
        }
        function VendorCustomerCartClearance(datae) {
    
            var datae=JSON.parse(datae)
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Cart Clearance'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}</b>'
                },
                xAxis: {
                    categories: ['Abandoned Cart Value','Cart Incentivized', 'Cart Cleared']
                },
                yAxis: {
                    title: {
                        useHTML: true,
                        text: 'Cart Value'
                    }
                },
                tooltip: {
					
					pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
				},
                plotOptions: {
                    series: {
                        pointWidth: 100,
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            
                series: [{
                    name:"Cart",
                    data: datae
                }]
            });

        }
        function VendorCustomerVisitorTraffic(datae){
            var datae=JSON.parse(datae)
    
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Total Visitors'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
                series: [{
                    name: 'Visitors',
                    colorByPoint: true,
                    data: datae
                }]
            });
        }
        
        function VendorCustomerVisitorRequestPages(datae){
            var datae=JSON.parse(datae)
    
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Top 10 viewed pages'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
                series: [{
                    name: 'URL',
                    colorByPoint: true,
                    data: datae
                }]
            });
        }

        function VendorCustomerTopViewedSkus(datae){
            var datae=JSON.parse(datae)
            $('#table_format').html(datae.tabl);
            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Top viewed Skus'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
                series: [{
                    name: 'View Count',
                    colorByPoint: true,
                    data: datae.res
                }]
            });
        }
        function VendorCustomerBottomViewedSkus(datae){
            var datae=JSON.parse(datae)
    
            $('#table_format').html(datae.tabl);

            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Bottom viewed Skus'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
                series: [{
                    name: 'View Count',
                    colorByPoint: true,
                    data: datae.res
                }]
            });
        }
        
		
		
		
		
		
		
		
		
		
		
/* main function  */


function VendorGetCustomer(obj){


    var report_type=$(obj).attr('report');
    var from_date=document.getElementById('from_date').value
    var to_date=document.getElementById('to_date').value
    var limit_result_val=document.getElementById('limit_result_val').value

    var page=document.getElementById('page_val').value;
    //alert(page+"init");

    var particular_year='';

    //alert(report_type);
    
    var xhr = false;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    }else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var xhr = new XMLHttpRequest();
            if (xhr) {
                             
                //obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                xhr.onreadystatechange = function () {
                    
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        document.getElementById('dateRangePicker').style.display="";
                        document.getElementById('filter_container').style.display="block";
                        document.getElementById('filter_button').style.display="";
                        
                        /* newly added */
                        if(report_type=="vendor_customer_gender"){
                            VendorCustomerGender(xhr.responseText)
                            document.getElementById('filter_button').setAttribute('report','vendor_customer_gender');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                            
                        }
                        
                        if(report_type=="vendor_customer_age"){
                            VendorCustomerAge(xhr.responseText)
                            document.getElementById('filter_button').setAttribute('report','customer_age');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                            
                        }
                        if(report_type=="vendor_customer_reg"){
                            VendorCustomerRegistrations(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','vendor_customer_reg');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        if(report_type=="vendor_customer_revenue"){
                            VendorCustomerRevenue(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','vendor_customer_revenue');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        if(report_type=="vendor_customer_revenue_gender"){
                            VendorCustomerRevenueGender(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','vendor_customer_revenue_gender');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        
                        if(report_type=="vendor_customer_subscribed"){
                            VendorCustomerSuscribed(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','vendor_customer_subscribed');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        
                        if(report_type=="vendor_customer_sustainability"){
                            VendorCustomerSustainability(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','vendor_customer_sustainability');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        if(report_type=="vendor_customer_life_cycle"){
                            $('#limit_result_life_cycle').show();
                            limit_result_val=$('#life_cycle_years_val').val();
                            VendorCustomerLifeCycle(xhr.responseText,limit_result_val);
                            document.getElementById('filter_button').setAttribute('report','vendor_customer_life_cycle');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }else{
                            if($('#limit_result_life_cycle')){
                                $('#limit_result_life_cycle').hide();
                            }
                        }
                        if(report_type=="vendor_abandoned_cart_weekly"){
                            VendorCustomerAC_weekly(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','vendor_abandoned_cart_weekly');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        if(report_type=="vendor_abandoned_cart_clearance"){
                            VendorCustomerCartClearance(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','vendor_abandoned_cart_clearance');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        if(report_type=="visitor_traffic"){
                            VendorCustomerVisitorTraffic(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','visitor_traffic');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        if(report_type=="total_visitor"){
                            VendorCustomerTotalVisitorTraffic(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','total_visitor');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        if(report_type=="total_visitor_pagewise"){
                            $('#page_val_div').show();
                            page=$('#page_val').val();
                            //alert('inside'+page);
                            VendorCustomerTotalVisitorTrafficPageWise(xhr.responseText,page);
                            document.getElementById('filter_button').setAttribute('report','total_visitor_pagewise');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }else{
                            if($('#page_val_div')){
                                $('#page_val_div').hide();
                            }
                            page='';
                        }

                        if(report_type=="total_visitor_pagewise_top"){
                            
                            VendorCustomerTotalVisitorTrafficPageWiseTop(xhr.responseText,limit_result_val);
                            document.getElementById('filter_button').setAttribute('report','total_visitor_pagewise');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }else{
                            if($('#limit_result_life_cycle')){
                                $('#limit_result_life_cycle').hide();
                            }
                        }
                        
                        if(report_type=="total_visitor_pagewise_bottom"){
                            
                            VendorCustomerTotalVisitorTrafficPageWiseBottom(xhr.responseText,limit_result_val);
                            document.getElementById('filter_button').setAttribute('report','total_visitor_pagewise');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }else{
                            if($('#limit_result_life_cycle')){
                                $('#limit_result_life_cycle').hide();
                            }
                        }
                       
                        if(report_type=="vistors_request_pages"){
                            VendorCustomerVisitorRequestPages(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','vistors_request_pages');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        if(report_type=="visitor_top_viewed"){
                            VendorCustomerTopViewedSkus(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','visitor_top_viewed');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
                        if(report_type=="vistors_bottom_viewed"){
                            VendorCustomerBottomViewedSkus(xhr.responseText);
                            document.getElementById('filter_button').setAttribute('report','vistors_bottom_viewed');
                            document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                            //document.getElementById('from_date').value=""
                            //document.getElementById('to_date').value=""
                        }
						
						
                        /* newly added */
                        
                        if(xhr.responseText==1){

                        }else{
                           //obj.innerHTML='<i class="fa fa-trash-o" aria-hidden="true"></i>'
                        }
                    }
                }
                xhr.open('POST', base_url+"admin/Analytics/getCustomerReportVendor", true);
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.send('report_type='+report_type+'&from_date='+from_date+'&to_date='+to_date+'&limit_result_val='+limit_result_val+'&particular_year='+particular_year+'&page='+page);
            }
}

/* main function  */        









