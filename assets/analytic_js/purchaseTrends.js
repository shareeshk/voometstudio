     function PurchaseTrends (datae) {
         var datae=JSON.parse(datae)
         data=[]; hits=[];
        var dattime;
        var datadata;
         for(var x=0;x<datae.length;x++){
             dattime=datae[x].timeRange
             datadata=datae[x].ordersData
             for(var j=0;j<datadata.length;j++){
                 hoursOfDay=[]; number_of_hits=[];
                 hoursOfDay.push(j+" hour ")
                 number_of_hits.push(parseInt(datadata[j]))
                 data.push(hoursOfDay);
                 hits.push(number_of_hits);
             }
             
         }
       
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Carts Ordered '+dattime
        },
        xAxis: {
            categories:data,
            title: {
                text: "Carts Ordered"
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Carts Ordered ',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Number of carts Ordered',
            data: hits
        }]
    });



}