var chart
function RatingItem (datae) {

var datab=JSON.parse(datae)
var datae;
var drill;
var datas=[]
var datased=[]
var datasd=[]
var ShowData;
 var select_level;
   var dats=[];
   
for(var i=0;i<datab.length;i++){
   datae= datab[i].series
   drill= datab[i].drill
   select_level= datab[i].select_level
   name_chart= datab[i].name_chart
   
   if(name_chart=="Brands"){
       ShowData=false;
   }else{
       ShowData=true;
   }
   
   var totals=0;

    for(var j=0;j<datae.data.length;j++){
 
        
        dats.push({name:datae.data[j].name,y:parseInt(datae.data[j].y),drilldown:datae.data[j].drilldown})
        totals+=parseInt(datae.data[j].y);
    }
   
   datas.push({id:datae.id,name:datae.name,colorByPoint:datae.colorByPoint,data:dats})

   for(var j=0;j<drill.length;j++){
      
       var data_data=[];
       for(var k=0;k<drill[j].data.length;k++){
           data_data.push({id:drill[j].data[k].id,name:drill[j].data[k].name,y:drill[j].data[k].y,drilldown:drill[j].data[k].drilldown})
           
       }
       datasd.push({id:drill[j].id,name:drill[j].name,data:data_data,z:j})
   }
   
}
var extradata="";
  if(select_level!=""){
      extradata=" in "+select_level
  } 
chart = new Highcharts.Chart({

        credits: {
            enabled: false,
        },

        chart: {
            renderTo: 'container',
            type: 'bar',
            backgroundColor:'rgba(255, 255, 255, 0.1)',
            events: {
                    drillup: function (e) {
                           changeTitle(e.seriesOptions.id);
                    },
                    drilldown: function (e) {
                           changeTitle(e.seriesOptions.id);
                    },
                    load: function(event) {
                      var total = 0;
                        for(var i=0, len=this.series[0].yData.length; i<len; i++){
                            total += this.series[0].yData[i];
                        }
                          var text = this.renderer.text(
                            'Total: ' + total,
                            this.plotLeft,
                            this.plotTop - 20
                        ).attr({
                            zIndex: 5
                        }).add()
                    }
             },
             spacingBottom: 30,
             zoomType: 'x'
        },
        title: {
            text: name_chart
        },
        title: {
            text: name_chart + extradata
        },

        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            allowDecimals: false,
        },
        legend: {
            enabled: false,
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            y: 30,
            labelFormat: '{name} ({y})',
            navigation: {
                activeColor: '#3E576F',
                animation: true,
                arrowSize: 12,
                inactiveColor: '#CCC',
                style: {
                    fontWeight: 'bold',
                    color: '#333',
                    fontSize: '12px'    
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">Total Ratings: '+totals+' </span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} times(s) rated </b>  <br/>'
        },
        series: datas,
        drilldown: {
            series: datasd
            }

    });
    
}

function changeTitle(obj){
    //document.getElementsByClassName('highcharts-title')[0].getElementsByTagName('tspan')[0].html="";
    //alert(document.getElementsByTagName('tspan')[1].html)
    chart.setTitle({
            text: obj
        }, {
            text: ''
        });
        return false;
}





/*function add(a, b) {
    return parseInt(a) + parseInt(b);
}


function RatingItem (datae) {
    var sum;
         var datab=JSON.parse(datae)
         var datae;
         var select_level;
         
       for(var i=0;i<datab.length;i++){
           datae= datab[i].data
           select_level= datab[i].select_level
         
         data=[]; counts=[];
         for(var x=0;x<datae.length;x++){
             rating=[]; count=[];
             rating.push(datae[x].rating+" Star ")
             count.push(parseInt(datae[x].Count))
             data.push(rating);
             counts.push(count);
             
         }
         
         }
  var extradata="";
  if(select_level!=""){
      extradata=" in "+select_level
  }       
         
       sum=counts.reduce(add, 0);
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total Number Of Purchases '+sum + extradata
        },
        xAxis: {
            categories:data,
            title: {
                text: "Ratings"
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Ratings',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Number of Purchased',
            data: counts
        }]
    });



}*/

