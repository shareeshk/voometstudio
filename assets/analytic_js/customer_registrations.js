       
   function CustomerRegistrations (datae) {
         var datae=JSON.parse(datae)
         data=[]
         for(var x=0;x<datae.length;x++){
             datas=[]
             if(datae[x].m!=""){
                 datas.push(Date.UTC(datae[x].y,datae[x].m,datae[x].d),parseInt(datae[x].count))
             }
             data.push(datas)
         }

         
                Highcharts.chart('container', {
                
                    chart: {
                        type: 'area',
                        spacingBottom: 30,
                        zoomType: 'x'
                    },
                    title: {
                        text: 'New Registration over the time'
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in',
                        floating: true,
                        align: 'right',
                        verticalAlign: 'bottom',
                        y: 15
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Number Of Registration'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 5
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
        
                    series: [{
                        type: 'area',
                        name: 'Registration',
                        data: data
                    }]
                });
        }