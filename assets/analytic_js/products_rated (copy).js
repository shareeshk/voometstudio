function add(a, b) {
    return parseInt(a) + parseInt(b);
}


function RatingItem (datae) {
    var sum;
         var datab=JSON.parse(datae)
         var datae;
         var select_level;
         
       for(var i=0;i<datab.length;i++){
           datae= datab[i].data
           select_level= datab[i].select_level
         
         data=[]; counts=[];
         for(var x=0;x<datae.length;x++){
             rating=[]; count=[];
             rating.push(datae[x].rating+" Star ")
             count.push(parseInt(datae[x].Count))
             data.push(rating);
             counts.push(count);
             
         }
         
         }
  var extradata="";
  if(select_level!=""){
      extradata=" in "+select_level
  }       
         
       sum=counts.reduce(add, 0);
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total Number Of Purchases '+sum + extradata
        },
        xAxis: {
            categories:data,
            title: {
                text: "Ratings"
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Ratings',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Number of Purchased',
            data: counts
        }]
    });



}