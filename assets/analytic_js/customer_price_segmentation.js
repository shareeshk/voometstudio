function CustomerPriceSegmentation (datae) {
         var datae=JSON.parse(datae)
         data=[]
         for(var x=0;x<datae.length;x++){
             datas=[]
             datas.push(parseInt(datae[x].COUNT))
             data.push(datas)
         }
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Price Segmentation'
        },
        /*subtitle: {
            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
        },*/
        xAxis: {
            categories: ['less than 500', '5001 - 15000', '15001 - 30000', '30001 - 50000', 'Over 50000'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            /*valueSuffix: ' millions'*/
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Count',
            data: data
        }]
    });
}