function MostProductPurchased (datae) {
         var datae=JSON.parse(datae)
         data=[]; hits=[];
         for(var x=0;x<datae.length;x++){
             searched_keyword=[]; number_of_hits=[];
             searched_keyword.push(datae[x].product_name)
             number_of_hits.push(parseInt(datae[x].count))
             data.push(searched_keyword);
             hits.push(number_of_hits);
         }
       
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top purchased Products'
        },
        xAxis: {
            categories:data,
            title: {
                text: "Products"
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Purchased',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Number of Purchased',
            data: hits
        }]
    });



}