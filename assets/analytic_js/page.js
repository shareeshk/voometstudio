    function getPagesViewed(obj){
        if($(obj).attr('report')){
            var report_type=$(obj).attr('report');
        }
        else{
            var report_type=obj
        }
	
        
        var from_date=document.getElementById('from_date').value;
        var to_date=document.getElementById('to_date').value;
		
		
        if(document.getElementById('limit_result_val')){
        var limit_result_val=document.getElementById('limit_result_val').value
        }
        var particular_year=document.getElementById("particular_year").value
        var select_level=[];
        var select_levelTxt=[];
        $('#select_level :selected').each(function(i, sel){ 
            select_level.push( $(sel).val() ); 
        
        });
        $('#select_level :selected').each(function(i, sel){ 
            if($(sel).text()!=="Select Type"){
                select_levelTxt.push( $(sel).text() ); 
            }
        });
        if(document.getElementById('limit_result_order_val')){
            var limit_result_order_val=document.getElementById('limit_result_order_val').value
        }
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                          
                    //obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            document.getElementById('filter_container').style.display="";
                            document.getElementById('dateRangePicker').style.display="";
							 document.getElementById('filter_button').style.display="";
							 document.getElementById('limit_result_order_val').style.display="none";
                            if(report_type=="home" || report_type=="category" || report_type=="product" || report_type=="button" || report_type=="reviews"){
                             //alert(xhr.responseText);
								resText=JSON.parse(xhr.responseText);
                                form_table_structure(resText,report_type);
                                //document.getElementById('filter_button').setAttribute('report','home_page');
								document.getElementById('filter_button').setAttribute('report',report_type);
                                document.getElementById('filter_button').setAttribute('onclick','getPagesViewed(this)');
                               // document.getElementById('from_date').value=""
                               // document.getElementById('to_date').value=""
                                document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result').style.display=""
                              //  document.getElementById('limit_result_order_val').style.display="";
                                document.getElementById('limit_result_order').style.display="";
                                document.getElementById('select_level').style.display="none";
                            }
                            

                            if(xhr.responseText==1){

							}else{
                               //obj.innerHTML='<i class="fa fa-trash-o" aria-hidden="true"></i>'
							}
                        }
                    }
				
                    xhr.open('POST', base_url+"admin/Analytics/getPageViewedDetails", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('report_type='+report_type+'&from_date='+from_date+'&to_date='+to_date+"&particular_year="+particular_year+"&limit_result_order_val="+limit_result_order_val+"&limit_result_val="+limit_result_val);
                }
    }
    
    function form_table_structure(data_format,type){
		document.getElementById("type_of_menu").value=type;
		if(type=="product"){
			$("#graph_format_json").html(JSON.stringify(data_format.graph_format));
			sku_id_list_obj=data_format.sku_list.product.sku_id_list;
			sku_id_list_dropdown='<option value="">-SKU List-</option>';
			for(x in sku_id_list_obj){
				sku_id_list_dropdown+="<option value='"+sku_id_list_obj[x]+"'>"+sku_id_list_obj[x]+"</option>";
			}
			document.getElementById("sku_list").innerHTML=sku_id_list_dropdown;
			
			document.getElementById("container_graph_users_types").innerHTML="";
			document.getElementById("container_graph_total").innerHTML="";
			document.getElementById("container_graph_guest").innerHTML="";
			document.getElementById("container_graph_users").innerHTML="";
			document.getElementById("product_inventories_div").style.display="";
			document.getElementById("categories_div").style.display="none";
			document.getElementById("buttons_graph_div").style.display="none";
			document.getElementById("reviews_graph_div").style.display="none";
		}
		else if(type=="home"){
			//alert(JSON.stringify(data_format.graph_format));
			$("#graph_format_json").html(JSON.stringify(data_format.graph_format));
			
			document.getElementById("container_graph_users_types").innerHTML="";
			document.getElementById("container_graph_total").innerHTML="";
			document.getElementById("container_graph_guest").innerHTML="";
			document.getElementById("container_graph_users").innerHTML="";
			
			
			graph_home();
			document.getElementById("product_inventories_div").style.display="none";
			document.getElementById("categories_div").style.display="none";
			document.getElementById("buttons_graph_div").style.display="none";
			document.getElementById("reviews_graph_div").style.display="none";
		}
		else if(type=="category"){
			
			$("#graph_format_json").html(JSON.stringify(data_format.graph_format));
			sku_id_list_obj=data_format.sku_list.category.sku_id_list;
			sku_id_list_dropdown='<option value="">-Category List-</option>';
			for(x in sku_id_list_obj){
				
				sku_id_list_dropdown+="<option value='"+sku_id_list_obj[x]+"'>"+sku_id_list_obj[x]+"</option>";
			}
			document.getElementById("category_list").innerHTML=sku_id_list_dropdown;
			
			document.getElementById("container_graph_users_types").innerHTML="";
			document.getElementById("container_graph_total").innerHTML="";
			document.getElementById("container_graph_guest").innerHTML="";
			document.getElementById("container_graph_users").innerHTML="";
			
			document.getElementById("product_inventories_div").style.display="none";
			document.getElementById("categories_div").style.display="";
			document.getElementById("buttons_graph_div").style.display="none";
			document.getElementById("reviews_graph_div").style.display="none";
		}
		else if(type=="button"){
			$("#graph_format_json").html(JSON.stringify(data_format.graph_format));
			/////////////////
			sku_id_list_obj=data_format.sku_list.button.sku_id_list;
			sku_id_list_dropdown='<option value="">-SKU List-</option>';
			for(x in sku_id_list_obj){
				
				sku_id_list_dropdown+="<option value='"+sku_id_list_obj[x]+"'>"+sku_id_list_obj[x]+"</option>";
			}
			document.getElementById("sku_list_button").innerHTML=sku_id_list_dropdown;
			
			document.getElementById("container_graph_users_types").innerHTML="";
			document.getElementById("container_graph_total").innerHTML="";
			document.getElementById("container_graph_guest").innerHTML="";
			document.getElementById("container_graph_users").innerHTML="";
		
			
			document.getElementById("product_inventories_div").style.display="none";
			document.getElementById("categories_div").style.display="none";
			document.getElementById("buttons_graph_div").style.display="";
			document.getElementById("reviews_graph_div").style.display="none";
		}
		
		else if(type=="reviews"){
			
			$("#graph_format_json").html(JSON.stringify(data_format.graph_format));
			/////////////////
			sku_id_list_obj=data_format.sku_list.reviews.sku_id_list;
			sku_id_list_dropdown='<option value="">-SKU List-</option>';
			for(x in sku_id_list_obj){
				
				sku_id_list_dropdown+="<option value='"+sku_id_list_obj[x]+"'>"+sku_id_list_obj[x]+"</option>";
			}
			document.getElementById("sku_list_reviews").innerHTML=sku_id_list_dropdown;
			
			
			document.getElementById("container_graph_users_types").innerHTML="";
			document.getElementById("container_graph_total").innerHTML="";
			document.getElementById("container_graph_guest").innerHTML="";
			document.getElementById("container_graph_users").innerHTML="";
			
			
			document.getElementById("product_inventories_div").style.display="none";
			document.getElementById("categories_div").style.display="none";
			document.getElementById("buttons_graph_div").style.display="none";
			document.getElementById("reviews_graph_div").style.display="";
			
			
		}
		$("#from_date_for_graph").val(data_format.from_date);
		$("#to_date_for_graph").val(data_format.to_date);
		$('#container').html(data_format.table_format);
	} 
 
