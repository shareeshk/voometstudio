function cancelledOrdersNewFun (datae) {
         var datae=JSON.parse(datae)
         var timeRanges
         var datas
         data=[]; hits=[];
         for(var i=0;i<datae.length;i++){
             timeRanges=datae[i].timeRange
             datas=datae[i].data
         }
         var keys = [];var nums=[]
            for (var key in datas[0]) {
                keys.push(key);
                nums.push(parseInt(datas[0][key]));
            }
//nums=[41,41,0,41,0,41,0,41,0]
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Cancelled Orders '+timeRanges
        },
        xAxis: {
            categories:keys,
            title: {
                text: "Snapshort"
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Cancelled Count',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
              formatter: function() {
                    return '<b>'+ this.x +
                           '</b> : <b>'+ this.y +'</b>';
                  }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            showInLegend: false,
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            showInLegend: false,
            data: nums
        }]
    });



}