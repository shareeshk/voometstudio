    function getOrders(obj){
		
        if($(obj).attr('report')){
            var report_type=$(obj).attr('report');
        }
        else{
            var report_type=obj
        }
		//alert(report_type)
        //getAllSelectionOptions(report_type,obj);
		
        var from_date=document.getElementById('from_date').value
        var to_date=document.getElementById('to_date').value
        if(document.getElementById('limit_result_val')){
        var limit_result_val=document.getElementById('limit_result_val').value
        }
        var particular_year=document.getElementById("particular_year").value
        var select_level=[];
        var select_levelTxt=[];
        $('#select_level :selected').each(function(i, sel){ 
            select_level.push( $(sel).val() ); 
        
        });
        $('#select_level :selected').each(function(i, sel){ 
            select_levelTxt.push( $(sel).text() ); 
        
        });
        if(document.getElementById('limit_result_order_val')){
            var limit_result_order_val=document.getElementById('limit_result_order_val').value
        }
		
		limit_result_order_val="";
		particular_year="";
		select_levelTxt="";
	
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    //obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            document.getElementById('filter_container').style.display="";
                            document.getElementById('dateRangePicker').style.display="";
							 document.getElementById('filter_button').style.display="";
							 document.getElementById('limit_result_order_val').style.display="none";
                            if(report_type=="ordersStages"){
                                
                                orderStages(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','ordersStages');
                                document.getElementById('filter_button').setAttribute('onclick','getOrders(this)');
                                ////document.getElementById('from_date').value=""
                                ////document.getElementById('to_date').value=""
                                document.getElementById('limit_result').style.display="none"
                                document.getElementById('limit_result_order_val').style.display="none";
                                document.getElementById('limit_result_order').style.display="none";
                                document.getElementById('select_level').style.display="none";
                                document.getElementById('particular_year').value="";
                                document.getElementById('particular_year').style.display="none";
								document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result_val').style.display="none";
								document.getElementById('filter_container').style.display="";
                            }
                            
                            if(report_type=="dailyPurchases"){
                                dailyPurchases(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','dailyPurchases');
                                document.getElementById('filter_button').setAttribute('onclick','getOrders(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""
                                document.getElementById('limit_result_val').value=""
								document.getElementById('limit_result_val').style.display="none";
                                document.getElementById('limit_result').style.display="none"
                               // document.getElementById('limit_result_order_val').style.display="";
                                document.getElementById('limit_result_order').style.display="";
                                document.getElementById('select_level').style.display="none";
								document.getElementById('filter_container').style.display="";
								document.getElementById('particular_year').value="";
                                document.getElementById('particular_year').style.display="none";
                            }
                            /* cancelledOrders old coding commented 6/10/2021
                            if(report_type=="cancelledOrders"){
                                cancelledOrders(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','dailyPurchases');
                                document.getElementById('filter_button').setAttribute('onclick','getOrders(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""
                                document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result').style.display=""
                               // document.getElementById('limit_result_order_val').style.display="";
                                document.getElementById('limit_result_order').style.display="";
                                document.getElementById('select_level').style.display="none";
								document.getElementById('filter_container').style.display="none";
                            }*/
							
							if(report_type=="cancelledOrdersNew"){
                                
                                cancelledOrdersNewFun(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','cancelledOrdersNew');
                                document.getElementById('filter_button').setAttribute('onclick','getOrders(this)');
                                ////document.getElementById('from_date').value=""
                                ////document.getElementById('to_date').value=""
                                document.getElementById('limit_result').style.display="none"
                                document.getElementById('limit_result_order_val').style.display="none";
                                document.getElementById('limit_result_order').style.display="none";
                                document.getElementById('select_level').style.display="none";
								document.getElementById('particular_year').value="";
                                document.getElementById('particular_year').style.display="none";
								document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result_val').style.display="none";
								document.getElementById('filter_container').style.display="";
                            }
							
                            if(report_type=="dailyPurchasesComplete"){
                                
                                dailyPurchasesComplete(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','dailyPurchasesComplete');
                                document.getElementById('filter_button').setAttribute('onclick','getOrders(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""
								document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result_val').value="none"
                                document.getElementById('limit_result').style.display="none"
                              //  document.getElementById('limit_result_order_val').style.display="";
                                document.getElementById('limit_result_order').style.display="";
                                document.getElementById('select_level').style.display="none";
								document.getElementById('filter_container').style.display="";
								document.getElementById('particular_year').value="";
                                document.getElementById('particular_year').style.display="none";
                            }
                            if(report_type=="purchaseTrend"){
                                PurchaseTrends(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','purchaseTrend');
                                document.getElementById('filter_button').setAttribute('onclick','getOrders(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""
								document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result_val').value="none"
                                document.getElementById('limit_result').style.display="none"
                                document.getElementById('limit_result_order_val').style.display="none";
                                document.getElementById('limit_result_order').style.display="none";
                                document.getElementById('select_level').style.display="none";
                                document.getElementById('particular_year').value="";
                                document.getElementById('particular_year').style.display="none";
								document.getElementById('filter_container').style.display="";
                            }
                            
                            if(report_type=="paymentMethods"){
                                PaymentMethods(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','paymentMethods');
                                document.getElementById('filter_button').setAttribute('onclick','getOrders(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""
								document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result_val').value="none"
                                document.getElementById('limit_result').style.display="none"
                                document.getElementById('limit_result_order_val').style.display="none";
                                document.getElementById('limit_result_order').style.display="none";
                                document.getElementById('select_level').style.display="none";
                                document.getElementById('particular_year').value="";
                                document.getElementById('particular_year').style.display="none";
								document.getElementById('filter_container').style.display="";
                            }
                            
                            

                            if(xhr.responseText==1){

                           }
                           else{
                               //obj.innerHTML='<i class="fa fa-trash-o" aria-hidden="true"></i>'
                           }
                        }
                    }
                    xhr.open('POST', base_url+"admin/Analytics/getOrderReport", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('report_type='+report_type+'&from_date='+from_date+'&to_date='+to_date+'&limit_result_val='+limit_result_val+"&select_level="+select_level+"&limit_result_order_val="+limit_result_order_val+'&particular_year='+particular_year+'&select_levelTxt='+select_levelTxt);
                }
    }
    
    
 function getAllSelectionOptions(report_type,obj){
     var obj=report_type;
     var xhr = false;
                if (window.XMLHttpRequest) {
                    xhr = new XMLHttpRequest();
                }
                else {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
                 var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    //obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                           var ajaxRes=JSON.parse(xhr.responseText);
                            
                            var select=document.getElementById('select_level')
                            select.classList.add('form-control');
                            select.setAttribute("onchange",'getProducts("'+obj+'")');
                            select.innerHTML=""
                            var option = new Option ("Select Type","");
                            select.options[select.options.length] = option;
                            for (var i=0;i < ajaxRes.length;i++)
                            {
                                var option = new Option (ajaxRes[i].name,ajaxRes[i].id);
                                select.options[select.options.length] = option;
                            }
                           
                           
                        }
                    }
                    xhr.open('POST', base_url+"admin/Analytics/getRelatedSelectionOptions", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('report_type='+report_type);
                }
 }
      
 
