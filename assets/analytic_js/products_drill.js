
var chart
function ProductDrill (datae) {
var datab=JSON.parse(datae)
var datae;
var drill;
var datas=[]
var datased=[]
var datasd=[]
var ShowData;
if(document.getElementById("select_level").value.trim()==""){
	document.getElementById("container").innerHTML="";
	return false;
}
for(var i=0;i<datab.length;i++){
	 
   datae= datab[i].series
   drill= datab[i].drill
   name_chart= datab[i].name_chart
   
   if(name_chart=="Brands"){
       ShowData=false;
   }else{
       ShowData=true;
   }
   
 
   var dat=[]
   
    for(var j=0;j<datae.data.length;j++){
        dat.push({name:datae.data[j].name,y:parseInt(datae.data[j].y),drilldown:datae.data[j].drilldown})
    }
   datas.push({id:datae.id,name:datae.name,colorByPoint:datae.colorByPoint,data:dat})
 
   for(var j=0;j<drill.length;j++){
      
       var data_data=[];
       for(var k=0;k<drill[j].data.length;k++){
           data_data.push({id:drill[j].data[k].id,name:drill[j].data[k].name,y:drill[j].data[k].y,drilldown:drill[j].data[k].drilldown})
           
       }
       datasd.push({id:drill[j].id,name:drill[j].name,data:data_data,z:j})
   }
   
}

chart = new Highcharts.Chart({

        credits: {
            enabled: false,
        },

        chart: {
            renderTo: 'container',
            type: 'pie',
            backgroundColor:'rgba(255, 255, 255, 0.1)',
            events: {
                    drillup: function (e) {
                           changeTitle(e.seriesOptions.id);
                    },
                    drilldown: function (e) {
                           changeTitle(e.seriesOptions.id);
                    }
             }
        },
        title: {
            text: name_chart
        },

        subtitle: {
            text: ''
        },
        plotOptions: {
            series: {
                cursor: 'pointer',
                dataLabels: {
                    enabled: ShowData,
                    format: '{point.name}',
                    style: {
                        color: '#000',
                        fontWeight: 'bold',
                        fontSize: '12px',
                        textShadow: "0px #ffffff"

                     }
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
        },
        series: datas,
        drilldown: {
            series: datasd
            }

    });
    
}

function changeTitle(obj){
    //document.getElementsByClassName('highcharts-title')[0].getElementsByTagName('tspan')[0].html="";
    //alert(document.getElementsByTagName('tspan')[1].html)
    chart.setTitle({
            text: obj
        }, {
            text: ''
        });
        return false;
}
