function popularPromotions (datae) {
         var datae=JSON.parse(datae)
         data=[]; hits=[];
         for(var x=0;x<datae.length;x++){
             promotion_name=[]; number_of_promos=[];
             promotion_name.push(datae[x].promo_quote)
             number_of_promos.push(parseInt(datae[x].promo_num))
             data.push(promotion_name);
             hits.push(number_of_promos);
         }
       
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top Rated Promotions'
        },
        xAxis: {
            categories:data,
            title: {
                text: "promotion"
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Purchases',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Number of Purchased',
            data: hits
        }]
    });



}