    function getProducts(obj,brandlist=""){
        if($(obj).attr('report')){
            var report_type=$(obj).attr('report');
        }
        else{
            var report_type=obj
        }
		/* This block I modified for samrick 6/7/2021 starts */
        if(brandlist=="category_inventory" || brandlist=="subcategory_inventory" || brandlist=="brand_inventory" || brandlist=="products_inventory"){
			document.getElementById("select_level").value="";
		}
		/* This block I modified for samrick 6/7/2021 ends */
        getAllSelectionOptions_prod(report_type,obj,brandlist);
        var from_date=document.getElementById('from_date').value
        var to_date=document.getElementById('to_date').value
        if(document.getElementById('limit_result_val')){
        var limit_result_val=document.getElementById('limit_result_val').value
        }
        var particular_year=document.getElementById("particular_year").value
        var select_level=[];
        var select_levelTxt=[];
        $('#select_level :selected').each(function(i, sel){ 
            select_level.push( $(sel).val() ); 
        
        });
        $('#select_level :selected').each(function(i, sel){ 
            if($(sel).text()!=="Select Type"){
                select_levelTxt.push( $(sel).text() ); 
            }
        });
        if(document.getElementById('limit_result_order_val')){
            var limit_result_order_val=document.getElementById('limit_result_order_val').value
        }
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
		 /* This block I modified for samrick 6/7/2021 starts */
		 if(brandlist=="mostpurchased_inventory" || brandlist=="ratings_inventory"){
			select_levelTxt="";
			select_level="";
		}
		/* This block I modified for samrick 6/7/2021 ends */
                if (xhr) {
                                           
                    //obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
					/* This if conditions display none display "" I modified for samrick 6/7/2021 */
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            document.getElementById('filter_container').style.display="";
                            document.getElementById('dateRangePicker').style.display="";
                            document.getElementById('filter_button').style.display="";
							document.getElementById('limit_result_order_val').style.display="none";
                            if(report_type=="dailyPurchases"){
                                
                                dailyPurchasesData(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','searched_keyword');
                                document.getElementById('filter_button').setAttribute('onclick','getProducts(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""
                                document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result').style.display=""
                             //   document.getElementById('limit_result_order_val').style.display="";
                                document.getElementById('limit_result_order').style.display="";
                                document.getElementById('select_level').style.display="none";
                            }
							
							 if(report_type=="searched_keyword"){
								
                                //alert(xhr.responseText);
								
                                ProductSearchedKeyword(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report',report_type);
                                document.getElementById('filter_button').setAttribute('onclick','getProducts(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""

								document.getElementById('from_date').style.display="";
                                document.getElementById('to_date').style.display="";

                                document.getElementById('limit_result_val').value="none";
                                document.getElementById('limit_result').style.display="none";
                                document.getElementById('limit_result_order_val').style.display="none";
                                document.getElementById('limit_result_order').style.display="none";
                                //document.getElementById('select_level').setAttribute('multiple','multiple');
                                document.getElementById('select_level').style.display="none";
								document.getElementById('particular_year').value=""
								document.getElementById('particular_year').style.display="none";
								
                                document.getElementById('filter_button').style.display="";
                            }
                            if(report_type=="all_Inventory"||report_type=="parent_category"||report_type=="category"||report_type=="sub_category"||report_type=="brand"||report_type=="products"){
                                ProductDrill(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report',report_type);
                                document.getElementById('filter_button').setAttribute('onclick','getProducts(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""

                                document.getElementById('from_date').style.display="none";
                                document.getElementById('to_date').style.display="none";
                                
                                document.getElementById('limit_result_val').value="none";
                                document.getElementById('limit_result').style.display="none";
                                document.getElementById('limit_result_order_val').style.display="none";
                                document.getElementById('limit_result_order').style.display="none";
                               // document.getElementById('select_level').setAttribute('multiple','multiple');
                                document.getElementById('select_level').style.display="";

								document.getElementById('filter_button').style.display="none";
                            }
                            if(report_type=="most_purchased"){
                                
                                MostProductPurchased(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','most_purchased');
                                document.getElementById('filter_button').setAttribute('onclick','getProducts(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""
                                document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result').style.display="none"
                              //  document.getElementById('limit_result_order_val').style.display="";
                                document.getElementById('limit_result_order').style.display="";
                                document.getElementById('select_level').style.display="none";
								document.getElementById('filter_button').style.display="none";
                            }
                            
                            if(report_type=="ratings"){
                                
                                RatingItem(xhr.responseText);
                                document.getElementById('filter_button').setAttribute('report','ratings');
                                document.getElementById('filter_button').setAttribute('onclick','getProducts(this)');
                                //document.getElementById('from_date').value=""
                                //document.getElementById('to_date').value=""
                                document.getElementById('limit_result_val').value=""
                                document.getElementById('limit_result').style.display="none"
                             //   document.getElementById('limit_result_order_val').style.display="";
                                document.getElementById('limit_result_order').style.display="";
                                document.getElementById('select_level').style.display="none";
                                document.getElementById('select_level').removeAttribute('multiple');
								document.getElementById('filter_button').style.display="none";
                            }

                            if(xhr.responseText==1){

                           }
                           else{
                               //obj.innerHTML='<i class="fa fa-trash-o" aria-hidden="true"></i>'
                           }
                        }
                    }
                    xhr.open('POST', base_url+"admin/Analytics/getProductReport", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('report_type='+report_type+'&from_date='+from_date+'&to_date='+to_date+'&limit_result_val='+limit_result_val+"&select_level="+select_level+"&limit_result_order_val="+limit_result_order_val+'&particular_year='+particular_year+'&select_levelTxt='+select_levelTxt);
                }
    }
    
    
 function getAllSelectionOptions_prod(report_type,obj,brandlist){
     var obj=report_type;
     var xhr = false;
                if (window.XMLHttpRequest) {
                    xhr = new XMLHttpRequest();
                }
                else {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
                 var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    //obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                           var ajaxRes=JSON.parse(xhr.responseText);
						   
                            if(brandlist!="brandlist"){ /* This condition I modified for samrick 6/7/2021 */
								
                            var select=document.getElementById('select_level')
                            select.classList.add('form-control');
                            select.setAttribute("onchange",'getProducts("'+obj+'",\'brandlist\')');
                            select.innerHTML=""
                            var option = new Option ("Select Type"," ");
                            select.options[select.options.length] = option;
                            for (var i=0;i < ajaxRes.length;i++)
                            {
                                var option = new Option (ajaxRes[i].name,ajaxRes[i].id);
                                select.options[select.options.length] = option;
                            }
							}
                           
                        }
                    }
                    xhr.open('POST', base_url+"admin/Analytics/getRelatedSelectionOptions", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('report_type='+report_type);
                }
 }
      
 
