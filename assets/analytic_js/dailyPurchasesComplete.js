       
   function dailyPurchasesComplete (datab) {
         var datab=JSON.parse(datab)
         var timeRange
         var datae=[]
         for(var s=0;s<datab.length;s++){
             timeRange=datab[s].timeRange;
             datae.push(datab[s].datass);
         }
         itemQuantity=[]
         skuQuantity=[]
         cartQuantity=[]
         promoQuantity=[]
         for(var x=0;x<datae.length;x++){

            for(var j=0;j<datae[x].daily_quantity.length;j++){
                 datas=[]
                 if(datae[x].daily_quantity[j].m!=""){
                     datas.push(Date.UTC(datae[x].daily_quantity[j].y,datae[x].daily_quantity[j].m,datae[x].daily_quantity[j].d),parseInt(datae[x].daily_quantity[j].count))
                 }
                 itemQuantity.push(datas)
                
            }
            for(var j=0;j<datae[x].distinct_skus.length;j++){
                datas=[]
                 if(datae[x].distinct_skus[j].m!=""){
                     datas.push(Date.UTC(datae[x].distinct_skus[j].y,datae[x].distinct_skus[j].m,datae[x].distinct_skus[j].d),parseInt(datae[x].distinct_skus[j].count))
                 }
                 skuQuantity.push(datas)
            }
            for(var j=0;j<datae[x].carts.length;j++){
                datas=[]
                 if(datae[x].carts[j].m!=""){
                     datas.push(Date.UTC(datae[x].carts[j].y,datae[x].carts[j].m,datae[x].carts[j].d),parseInt(datae[x].carts[j].count))
                 }
                 cartQuantity.push(datas)
            }
            for(var j=0;j<datae[x].promotion_quantity.length;j++){
                datas=[]
                 if(datae[x].promotion_quantity[j].m!=""){
                     datas.push(Date.UTC(datae[x].promotion_quantity[j].y,datae[x].promotion_quantity[j].m,datae[x].promotion_quantity[j].d),parseInt(datae[x].promotion_quantity[j].count))
                 }
                 promoQuantity.push(datas)
            }
            
         }
         //+parseInt(datae[x].daily_quantity[j].count)
                Highcharts.chart('container', {
                
                    chart: {
                        type: 'line',
                        spacingBottom: 30,
                        zoomType: 'x'
                    },
                    title: {
                        text: 'Delivered Orders  '+timeRange
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in',
                        floating: true,
                        align: 'right',
                        verticalAlign: 'bottom',
                        y: 15
                    },
                    xAxis: {
                        type: 'datetime',
                        allowDecimals: false
                    },
                    yAxis: {
                        title: {
                            text: 'Number for each Category'
                        },

                    },
                    legend: {
                        enabled: true
                    },
                    tooltip: {
                        formatter: function() {
                            
                            var text = '';
                            if(this.series.name == 'Promo Items Quantity') {
                                
                                text = '<b>' + this.series.name +'</b><br/> On ' +
                                            Highcharts.dateFormat('%e  %b  %Y',new Date(this.x))
                                        + ' , '  +(this.y)+  ' number(s).';
                            } else {
                                text = '<b>' + this.series.name +'</b><br/> On ' +
                                            Highcharts.dateFormat('%e  %b  %Y',new Date(this.x))
                                        + ' , ' + this.y + ' number(s).';
                            }
                            return  text;
                        }
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                    enabled: false,
                                    symbol: 'circle',
                                    radius: 2,
                                    states: {
                                        hover: {
                                            enabled: true
                                        }
                                    }
                                },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    colors: [
                        '#003399',
                        '#ff0000',
                        '#009900',
                        '#cc9900'
                    ],        
                    series: [
                    {
                        name: 'Total Units Sold',
                        data: itemQuantity
                    },
                    {
                        name: 'Distinct SKU`s Sold',
                        data: skuQuantity
                    },
                    {
                        name: 'Distinct Cart Quantity',
                        data: cartQuantity
                    },
                    {
                        name: 'Total Promotion Items Sold',
                        data: promoQuantity
                    }
                    ]
                });
        }