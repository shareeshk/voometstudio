function replacementCases(datae) {
         var datae=JSON.parse(datae)
         replacementClosedByAdminOrCustomer=[];
         replacementClosedByCustomerInitial=[];
         replacementraised=[];
         RemainPendings=[];
         for(var x=0;x<datae.length;x++){
             
             for(var i=0;i<datae[x].replacementRaised.length;i++){
                 datas=[]
                 if(datae[x].replacementRaised[i].m!=""){
                     datas.push(Date.UTC(datae[x].replacementRaised[i].y,datae[x].replacementRaised[i].m,datae[x].replacementRaised[i].d),parseInt(datae[x].replacementRaised[i].counts))
                 }
                 replacementraised.push(datas)
             }
             for(var i=0;i<datae[x].replacementClosedByAdminOrCustomer.length;i++){
                 datas=[]
                 if(datae[x].replacementClosedByAdminOrCustomer[i].m!=""){
                     datas.push(Date.UTC(datae[x].replacementClosedByAdminOrCustomer[i].y,datae[x].replacementClosedByAdminOrCustomer[i].m,datae[x].replacementClosedByAdminOrCustomer[i].d),parseInt(datae[x].replacementClosedByAdminOrCustomer[i].counts))
                 }
                 replacementClosedByAdminOrCustomer.push(datas)
             }
             for(var i=0;i<datae[x].RemainPending.length;i++){
                 datas=[]
                 if(datae[x].RemainPending[i].m!=""){
                     datas.push(Date.UTC(datae[x].RemainPending[i].y,datae[x].RemainPending[i].m,datae[x].RemainPending[i].d),parseInt(datae[x].RemainPending[i].counts))
                 }
                 RemainPendings.push(datas)
             }
             
             for(var i=0;i<datae[x].replacementClosedByCustomerItself.length;i++){
                 datas=[]
                 if(datae[x].replacementClosedByCustomerItself[i].m!=""){
                     datas.push(Date.UTC(datae[x].replacementClosedByCustomerItself[i].y,datae[x].replacementClosedByCustomerItself[i].m,datae[x].replacementClosedByCustomerItself[i].d),parseInt(datae[x].replacementClosedByCustomerItself[i].counts))
                 }
                 replacementClosedByCustomerInitial.push(datas)
             }
             
             /*datas=[]
             if(datae[x].m!=""){
                 datas.push(Date.UTC(datae[x].y,datae[x].m,datae[x].d),parseInt(datae[x].count))
             }
             if(parseInt(datae[x].stopped)==1){
                 dataStopped.push(datas);
             }
             else if(parseInt(datae[x].active)==0){
                 dataExpired.push(datas);
             }
             else{
                 data.push(datas)
             }*/
             
         }

         
                Highcharts.chart('container', {
                
                    chart: {
                        type: 'column',
                        spacingBottom: 30,
                        zoomType: 'x'
                    },
                    title: {
                        text: 'Replacement Request Raised And Closed over the time'
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in',
                        floating: true,
                        align: 'right',
                        verticalAlign: 'bottom',
                        y: 15
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Number Of Requests'
                        }
                    },
                    legend: {
                        enabled: true
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 5
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    colors: [
                        '#003399',
                        '#ff0000',
                        '#009900',
                        '#cc9900'
                    ],
        
                    series: [
                    {
                        name: 'Request Raised',
                        data: replacementraised
                    },
                    {
                        name: 'Cancelled By Customer',
                        data: replacementClosedByCustomerInitial
                    },
                    {
                        name: 'Closed By Customer Or By Admin',
                        data: replacementClosedByAdminOrCustomer
                    },
                    {
                        name:'Remain Pending',
                        data:RemainPendings
                    }]
                });
        }