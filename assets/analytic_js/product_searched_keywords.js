function ProductSearchedKeyword (datae) {
         var datae=JSON.parse(datae)
         data=[]; hits=[];
         for(var x=0;x<datae.length;x++){
             searched_keyword=[]; number_of_hits=[];
             searched_keyword.push(datae[x].searched_keyword)
             number_of_hits.push(parseInt(datae[x].number_of_hits))
             data.push(searched_keyword);
             hits.push(number_of_hits);
         }
       
    Highcharts.chart('container', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Top searched keywords'
        },
        xAxis: {
            categories:data,
            title: {
                text: "Searched Keywords"
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Hits',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Number of hits',
            data: hits
        }]
    });



}