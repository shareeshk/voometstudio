         
   function cancelledOrders (datae) {
         var datae=JSON.parse(datae)
         dataActive=[]
         dataCancelledByCustomer=[]
         dataCancelledByAdmin=[]
         dataDelivered=[]
         for(var x=0;x<datae.length;x++){
             if(datae[x].cancelled_by=="customer"){
                 datas=[]
                 if(datae[x].m!=""){
                     datas.push(Date.UTC(datae[x].y,datae[x].m,datae[x].d,datae[x].h),parseInt(datae[x].cancelcount))
                 }
                 dataCancelledByCustomer.push(datas)
             }
             if(datae[x].cancelled_by=="admin"){
                 datas=[]
                 if(datae[x].m!=""){
                     datas.push(Date.UTC(datae[x].y,datae[x].m,datae[x].d,datae[x].h),parseInt(datae[x].cancelcount))
                 }
                 dataCancelledByAdmin.push(datas)
             }
                 datas=[]
                 if(datae[x].m!=""){
                     datas.push(Date.UTC(datae[x].y,datae[x].m,datae[x].d,datae[x].h),parseInt(datae[x].histCount))
                 }
                 dataDelivered.push(datas)
                 datasa=[]
                 if(datae[x].m!=""){
                     datasa.push(Date.UTC(datae[x].y,datae[x].m,datae[x].d,datae[x].h),parseInt(datae[x].actcount))
                 }
             dataActive.push(datasa)
             
         }
         
                Highcharts.chart('container', {
                
                    chart: {
                        type: 'column',
                        spacingBottom: 30,
                        zoomType: 'x'
                    },
                    title: {
                        text: 'Cancelled Data over the time'
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in',
                        floating: true,
                        align: 'right',
                        verticalAlign: 'bottom',
                        y: 15
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Number Of Cart Items'
                        },
                        allowDecimals: false,
                    },
                    legend: {
                        enabled: true
                    },
                    tooltip: {
                        formatter: function() {
                            
                            var text = '';
                            if(this.series.name == 'Cancelled By Customer') {
                                
                                text = '<b>' + this.series.name +'</b><br/>' +
                                            Highcharts.dateFormat('%H:%M %A, %b %e - %Y',
                                                                  new Date(this.x))
                                        + ' date, ' + this.y +' order cancelled by customer';
                            } 
                            if(this.series.name == 'Cancelled By Admin') {
                                
                                text = '<b>' + this.series.name +'</b><br/>' +
                                            Highcharts.dateFormat('%H:%M %A, %b %e - %Y',
                                                                  new Date(this.x))
                                        + ' date, ' + this.y +' order cancelled by admin';
                            }
                            if(this.series.name == 'Active Orders Count') {
                                
                                text = '<b>' + this.series.name +'</b><br/>' +
                                            Highcharts.dateFormat('%H:%M %A, %b %e - %Y',
                                                                  new Date(this.x))
                                        + ' date, ' + this.y +' order active';
                            }
                            if(this.series.name == 'Delivered Orders Count') {
                                
                                text = '<b>' + this.series.name +'</b><br/>' +
                                            Highcharts.dateFormat('%H:%M %A, %b %e - %Y',
                                                                  new Date(this.x))
                                        + ' date, ' + this.y +' order delivered';
                            }
                            return  text;
                        }

                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 5
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
        
                    series: [
                    {
                        name: 'Cancelled By Customer',
                        data: dataCancelledByCustomer
                    },
                    {
                        name: 'Cancelled By Admin',
                        data: dataCancelledByAdmin
                    }
                    /*{
                        name: 'Active Orders Count',
                        data: dataActive
                    },
                    {
                        name: 'Delivered Orders Count',
                        data: dataDelivered
                    },*/]
                });
        }     
   